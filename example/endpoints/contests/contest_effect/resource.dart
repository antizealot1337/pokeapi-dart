// SPDX-License-Identifier: MIT

// ignore_for_file: unused_local_variable

import 'package:pokeapi/pokeapi.dart';

void main() async {
  // Get the default client.
  final client = PokeAPIClient.defaultClient();

  // Get the contest types
  final contestEffects = await client.contestEffects();

  // Use results for stuff...
}
