// SPDX-License-Identifier: MIT

// ignore_for_file: unused_local_variable

import 'package:pokeapi/pokeapi.dart';

void main() async {
  const ID = 1;

  // Get the default client.
  final client = PokeAPIClient.defaultClient();

  // Get the move battle style by id
  final moveBattleStyle = await client.moveBattleStyle(ID);

  // Use results for stuff...
}
