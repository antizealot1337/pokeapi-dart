// SPDX-License-Identifier: MIT

// ignore_for_file: unused_local_variable

import 'package:pokeapi/pokeapi.dart';

void main() async {
  const ID = 1;

  // Get the default client.
  final client = PokeAPIClient.defaultClient();

  // Get the location area
  final location = await client.locationArea(ID);

  // Use results for stuff...
}
