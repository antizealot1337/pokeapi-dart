// SPDX-License-Identifier: MIT

// ignore_for_file: unused_local_variable

import 'package:pokeapi/pokeapi.dart';

void main() async {
  // Get the default client.
  final client = PokeAPIClient.defaultClient();

  // Get the regions
  final regions = await client.regions();

  // Use results for stuff...
}
