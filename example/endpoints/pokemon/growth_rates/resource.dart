// SPDX-License-Identifier: MIT

// ignore_for_file: unused_local_variable

import 'package:pokeapi/pokeapi.dart';

void main() async {
  // Get the default client.
  final client = PokeAPIClient.defaultClient();

  // Get the growth rates
  final growthRates = await client.growthRates();

  // Use results for stuff...
}
