// SPDX-License-Identifier: MIT

// ignore_for_file: unused_local_variable

import 'package:pokeapi/pokeapi.dart';

void main() async {
  const POKEMON_ID = 1;

  // Get the default client.
  final client = PokeAPIClient.defaultClient();

  // Get the pokemon encounter by (pokemon) id
  final pokemonLocationArea = await client.pokemonLocationAreas(POKEMON_ID);

  // Use results for stuff...
}
