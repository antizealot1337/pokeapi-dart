// SPDX-License-Identifier: MIT

// There is no endpoint to get a list of all pokemon location areas. The only
// way to get areas is via a (pokemon) id.
