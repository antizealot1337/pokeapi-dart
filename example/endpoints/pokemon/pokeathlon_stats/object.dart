// SPDX-License-Identifier: MIT

// ignore_for_file: unused_local_variable

import 'package:pokeapi/pokeapi.dart';

void main() async {
  const ID = 1;

  // Get the default client.
  final client = PokeAPIClient.defaultClient();

  // Get the Pokeathlon stat by id
  final pokeathlonStat = await client.pokeathlonStat(ID);

  // Use results for stuff...
}
