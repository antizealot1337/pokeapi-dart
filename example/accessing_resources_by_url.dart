import 'package:pokeapi/pokeapi.dart';
import 'package:pokeapi/src/models/models.dart';

void main() async {
  final client = PokeAPIClient.defaultClient();

  final results = await client.allPokemon(limit: 50);

  for (var resource in results.results) {
    final pokemon = await client.get<Pokemon>(Uri.parse(resource.url));
    print(pokemon.name);
  }
}
