# PokeAPI

A dart library for access the excellent API ([v2][pokeapi docs]) from [PokeAPI][pokeapi].

## Fair Use Policy

PokeAPI is free  and open to use to use and views itself as primarliy an
educational tool. They ask every developer to abide by their
[Fair Use Policy][fair use policy]. They do not tolerate abuse (DoS, etc). They
also ask developers:
* Locally cache resources whenever you request them.
* Be nice and friendly to your fellow PokéAPI developers.

## Caching

This library does not do caching natively. Use a compatible HttpClient that has
cachine or implement it manually.

## Endpoints

### [Berries][berries group]

<details>
<summary>Berries</summary>

* Get all berries (resource).
```dart
final berries = await client.berries();
```
* Get a berry (by id).
```dart
final berry = await client.berry(1);
```

</details>

<details>
<summary>Berry Firmnesses</summary>

* Get all berry firmnesses (resource).
```dart
final berryFirmnesses = await client.berryFirmnesses();
```
* Get a berry firmness (by id).
```dart
final berryFirmness = await client.berryFirmness(1);
```

</details>

<details>
<summary>Berry Flavors</summary>

* Get all berryFlavors (resource).
```dart
final berryFlavors = await client.berryFlavors();
```
* Get a berryFlavor (by id).
```dart
final berryFlavor = await client.berryFlavor(1);
```

</details>

### [Contests][contests group]

<details>
<summary>Contest Types</summary>

* Get all contest types (resource).
```dart
final contestTypes = await client.contestTypes();
```
* Get a contest type (by id).
```dart
final contestType = await client.contestType(1);
```

</details>

<details>
<summary>Contest Effects</summary>

* Get all contest effects (resource).
```dart
final contestEffects = await client.contestEffects();
```
* Get a contest effect (by id).
```dart
final contestEffect = await client.contestEffect(1);
```

</details>

<details>
<summary>Super Contest Effects</summary>

* Get all super contest effects (resource).
```dart
final superContestEffects = await client.superContestEffects();
```
* Get a super contest effect (by id).
```dart
final superContestEffect = await client.superContestEffect(1);
```

</details>

### [Encounters][encounters group]

<details>
<summary>Encounter Methods</summary>

* Get all encounter methods (resource).
```dart
final encounterMethods = await client.encounterMethods();
```
* Get an encounter method (by id).
```dart
final encounterMethod = await client.encounterMethod(1);
```

</details>

<details>
<summary>Encounter Conditions</summary>

* Get all encounter conditions (resource).
```dart
final encounterConditions = await client.encounterConditions();
```
* Get an encounter condition (by id).
```dart
final encounterCondition = await client.encounterCondition(1);
```

</details>

<details>
<summary>Encounter Condition Values</summary>

* Get all encounter condition values (resource).
```dart
final encounterConditionValues = await client.encounterConditionValues();
```
* Get an encounter condition value (by id).
```dart
final encounterConditionValue = await client.encounterConditionValue(1);
```

</details>

### [Evolution][evolution group]

<details>
<summary>Evolution Chains</summary>

* Get all evolution chains (resource).
```dart
final evolutionChains = await client.evolutionChains();
```
* Get an evolution chain (by id).
```dart
final evolutionChain = await client.evolutionChain(1);
```

</details>

<details>
<summary>Evolution Triggers</summary>

* Get all evolution triggers (resource).
```dart
final evolutionTriggers = await client.evolutionTriggers();
```
* Get an evolution trigger (by id).
```dart
final evolutionTrigger = await client.evolutionTrigger(1);
```

</details>

### [Games][games group]

<details>
<summary>Generations</summary>

* Get all generations (resource).
```dart
final generations = await client.generations();
```
* Get a generation (by id).
```dart
final generation = await client.generation(1);
```

</details>

<details>
<summary>Pokedexes</summary>

* Get all pokedexes (resource).
```dart
final pokedexes = await client.pokedexes();
```
* Get a pokedex (by id).
```dart
final pokedex = await client.pokedex(1);
```

</details>

<details>
<summary>Version</summary>

* Get all versions (resource).
```dart
final versions = await client.versions();
```
* Get a version (by id).
```dart
final version = await client.version(1);
```

</details>

<details>
<summary>Version Groups</summary>

* Get all version groups (resource).
```dart
final versionGroups = await client.versionGroups();
```
* Get a version group (by id).
```dart
final versionGroup = await client.versionGroup(1);
```

</details>

### [Items][items group]

<details>
<summary>Item</summary>

* Get all items (resource).
```dart
final items = await client.items();
```
* Get an item (by id).
```dart
final item = await client.item(1);
```

</details>

<details>
<summary>Item Attributes</summary>

* Get all item attributes (resource).
```dart
final itemAttributes = await client.itemAttributes();
```
* Get an item attribute (by id).
```dart
final itemAttribute = await client.itemAttribute(1);
```

</details>

<details>
<summary>Item Categories</summary>

* Get all item categories (resource).
```dart
final itemCategories = await client.itemCategories();
```
* Get an item category (by id).
```dart
final itemCategory = await client.itemCategory(1);
```

</details>

<details>
<summary>Item Fling Effect</summary>

* Get all item fling effects (resource).
```dart
final itemFlingEffects = await client.itemFlingEffects();
```
* Get an item fling effect (by id).
```dart
final itemFlingEffect = await client.itemFlingEffect(1);
```

</details>

<details>
<summary>Item Pockets</summary>

* Get all  item pockets (resource).
```dart
final itemPockets = await client.itemPockets();
```
* Get an  item pocket (by id).
```dart
final itemPocket = await client.itemPocket(1);
```

</details>

### [Locations][locations group]

<details>
<summary>Locations</summary>

* Get all locations (resource).
```dart
final locations = await client.locations();
```
* Get a location (by id).
```dart
final location = await client.location(1);
```

</details>

<details>
<summary>Location Areas</summary>

* Get all locationAreas (resource).
```dart
final locationAreas = await client.locationAreas();
```
* Get a locationArea (by id).
```dart
final locationArea = await client.locationArea(1);
```

</details>

<details>
<summary>Pal Park Areas</summary>

* Get all Pal Park areas (resource).
```dart
final palParkAreas = await client.palParkAreas();
```
* Get a Pal Park area (by id).
```dart
final palParkArea = await client.palParkArea(1);
```

</details>

<details>
<summary>Region</summary>

* Get all regions (resource).
```dart
final regions = await client.regions();
```
* Get a region (by id).
```dart
final region = await client.region(1);
```

</details>

### [Machines][machines group]

<details>
<summary>Machines</summary>

* Get all machines (resource).
```dart
final machines = await client.machines();
```
* Get a machine (by id).
```dart
final machine = await client.machine(1);
```

</details>

### [Moves][moves group]

<details>
<summary>Moves</summary>

* Get all moves (resource).
```dart
final moves = await client.moves();
```
* Get a move (by id).
```dart
final move = await client.move(1);
```

</details>

<details>
<summary>Move Ailments</summary>

* Get all move ailments (resource).
```dart
final moveAilments = await client.moveAilments();
```
* Get a move ailment (by id).
```dart
final moveAilment = await client.moveAilment(1);
```

</details>

<details>
<summary>Move Battle Styles</summary>

* Get all move battle styles (resource).
```dart
final moveBattleStyles = await client.moveBattleStyles();
```
* Get a move battle style (by id).
```dart
final moveBattleStyle = await client.moveBattleStyle(1);
```

</details>

<details>
<summary>Move Categories</summary>

* Get all move categories (resource).
```dart
final moveCategories = await client.moveCategories();
```
* Get a move category (by id).
```dart
final moveCategory = await client.moveCategory(1);
```

</details>

<details>
<summary>Move Damage Class</summary>

* Get all move damage classes (resource).
```dart
final moveDamageClasses = await client.moveDamageClasses();
```
* Get a move damage class (by id).
```dart
final moveDamageClass = await client.moveDamageClass(1);
```

</details>

<details>
<summary>Move Learn Methods</summary>

* Get all move learn methods (resource).
```dart
final moveLearnMethods = await client.moveLearnMethods();
```
* Get a move learn method (by id).
```dart
final moveLearnMethod = await client.moveLearnMethod(1);
```

</details>

<details>
<summary>Move Targets</summary>

* Get all move targets (resource).
```dart
final moveTargets = await client.moveTargets();
```
* Get a move target (by id).
```dart
final moveTarget = await client.moveTarget(1);
```

</details>

### [Pokémon][pokémon group]

<details>
<summary>Abilities</summary>

* Get all abilities (resource).
```dart
final abilities = await client.abilities();
```
* Get an ability (by id).
```dart
final ability = await client.ability(1);
```

</details>

<details>
<summary>Characteristics</summary>

* Get all characteristics (resource).
```dart
final characteristics = await client.characteristics();
```
* Get a characteristic (by id).
```dart
final characteristic = await client.characteristic(1);
```

</details>

<details>
<summary>Egg Groups</summary>

* Get all egg groups (resource).
```dart
final eggGroups = await client.eggGroups();
```
* Get an egg group (by id).
```dart
final eggGroup = await client.eggGroup(1);
```

</details>

<details>
<summary>Genders</summary>

* Get all genders (resource).
```dart
final genders = await client.genders();
```
* Get a gender (by id).
```dart
final gender = await client.gender(1);
```

</details>

<details>
<summary>Growth Rates</summary>

* Get all growth rates (resource).
```dart
final growthRates = await client.growthRates();
```
* Get a growth rate (by id).
```dart
final growthRate = await client.growthRate(1);
```

</details>

<details>
<summary>Natures</summary>

* Get all natures (resource).
```dart
final natures = await client.natures();
```
* Get a nature (by id).
```dart
final nature = await client.nature(1);
```

</details>

<details>
<summary>Pokeathlon Stats</summary>

* Get all Pokeathlon statses (resource).
```dart
final pokeathlonStats = await client.pokeathlonStats();
```
* Get a Pokeathlon stats (by id).
```dart
final pokeathlonStat = await client.pokeathlonStat(1);
```

</details>

<details>
<summary>Pokemon</summary>

* Get all Pokemon (resource).
```dart
final allPokemon = await client.allPokemon();
```
* Get a Pokemon (by id).
```dart
final pokemon = await client.pokemon(1);
```

</details>

<details>
<summary>Pokemon Location Areas</summary>

* Get all pokemon location areas by a Pokemon id.
```dart
final pokemonLocationAreas = await client.pokemonLocationAreas(1);
```

</details>

<details>
<summary>Pokemon Colors</summary>

* Get all Pokemon colors (resource).
```dart
final pokemonColors = await client.pokemonColors();
```
* Get a Pokemon color (by id).
```dart
final pokemonColor = await client.pokemonColor(1);
```

</details>

<details>
<summary>Pokemon Forms</summary>

* Get all Pokemon forms (resource).
```dart
final pokemonForms = await client.pokemonForms();
```
* Get a Pokemon form (by id).
```dart
final pokemonForm = await client.pokemonForm(1);
```

</details>

<details>
<summary>Pokemon Habitats</summary>

* Get all Pokemon habitats (resource).
```dart
final pokemonHabitats = await client.pokemonHabitats();
```
* Get a Pokemon habitat (by id).
```dart
final pokemonHabitat = await client.pokemonHabitat(1);
```

</details>

<details>
<summary>Pokemon Shapes</summary>

* Get all Pokemon shapes (resource).
```dart
final pokemonShapes = await client.pokemonShapes();
```
* Get a Pokemon shape (by id).
```dart
final pokemonShape = await client.pokemonShape(1);
```

</details>

<details>
<summary>Pokemon Species</summary>

* Get all Pokemon species (resource).
```dart
final allPokemonSpecies = await client.allPokemonSpecies();
```
* Get a Pokemon species (by id).
```dart
final pokemonSpecies = await client.pokemonSpecies(1);
```

</details>

<details>
<summary>Stats</summary>

* Get all stats (resource).
```dart
final stats = await client.stats();
```
* Get a stat (by id).
```dart
final stat = await client.stat(1);
```

</details>

<details>
<summary>Types</summary>

* Get all types (resource).
```dart
final types = await client.types();
```
* Get a type (by id).
```dart
final type = await client.type(1);
```

</details>

### [Utility][utility group]

<details>
<summary>Languages</summary>

* Get all languages (resource).
```dart
final languages = await client.languages();
```
* Get a language (by id).
```dart
final language = await client.language(1);
```

</details>

## Accessing models from a URL

Many parts of the API contain URLs to other resources. The client has
`get<T>(Uri)` for accessing those.

```dart
final results = await client.allPokemon();

for (var resource in results.results) {
    final pokemon = await client.get<Pokemon>(Uri.parse(resource.url));
    print(pokemon.name);
}
```

## Features and bugs

Please file feature requests and bugs at the [issue tracker][tracker].

[pokeapi]: https://pokeapi.co
[pokeapi docs]: https://pokeapi.co/docs/v2
[fair use policy]: https://pokeapi.co/docs/v2#fairuse
[berries group]: https://pokeapi.co/docs/v2#berries-section
[contests group]: https://pokeapi.co/docs/v2#contests-section
[encounters group]: https://pokeapi.co/docs/v2#encounters-section
[evolution group]: https://pokeapi.co/docs/v2#evolution-section
[games group]: https://pokeapi.co/docs/v2#games-section
[items group]: https://pokeapi.co/docs/v2#item
[locations group]: https://pokeapi.co/docs/v2#locations-section
[machines group]: https://pokeapi.co/docs/v2#machines-section
[moves group]: https://pokeapi.co/docs/v2#moves-section
[pokémon group]: https://pokeapi.co/docs/v2#pokemon-section
[utility group]: https://pokeapi.co/docs/v2#utility-section
[tracker]: http://gitlab.com/antizealot1337/pokeapi-dart/issues