// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/encounters/encounter_condition_value.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('EncounterConditionValue', () {
    const ID = 1;
    const NAME = 'name';
    const CONDITION_NAME = 'condition';
    const CONDITION = NamedApiResource(CONDITION_NAME, '');
    const NAMES_NAME = 'names';
    const NAME_ = Name(NAMES_NAME, NamedApiResource('', ''));

    test('fromMap', () {
      final e = EncounterConditionValue.fromMap({
        EncounterConditionValue.ID_FIELD_NAME: ID,
        EncounterConditionValue.NAME_FIELD_NAME: NAME,
        EncounterConditionValue.CONDITION_FIELD_NAME: CONDITION.toMap(),
        EncounterConditionValue.NAMES_FIELD_NAME: [NAME_.toMap()],
      });

      expect(e.id, equals(ID));
      expect(e.name, equals(NAME));
      expect(e.condition.name, equals(CONDITION_NAME));
      expect(e.names.length, equals(1));
      expect(e.names.first.name, equals(NAMES_NAME));
    });

    test('toMap', () {
      final e = EncounterConditionValue(ID, NAME, CONDITION, [NAME_]).toMap();

      expect(e[EncounterConditionValue.ID_FIELD_NAME], equals(ID));
      expect(e[EncounterConditionValue.NAME_FIELD_NAME], equals(NAME));
      expect(e[EncounterConditionValue.CONDITION_FIELD_NAME],
          equals(CONDITION.toMap()));
      expect(
          e[EncounterConditionValue.NAMES_FIELD_NAME], equals([NAME_.toMap()]));
    });
  });
}
