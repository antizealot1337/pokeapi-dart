// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/encounters/encounter_condition.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('EncounterCondition', () {
    const ID = 1;
    const NAME = 'name';
    const NAMES_NAME = 'names';
    const NAME_ = Name(NAMES_NAME, NamedApiResource('', ''));
    const VALUE_NAME = 'value';
    const VALUE = NamedApiResource(VALUE_NAME, '');

    test('fromMap', () {
      final e = EncounterCondition.fromMap({
        EncounterCondition.ID_FIELD_NAME: ID,
        EncounterCondition.NAME_FIELD_NAME: NAME,
        EncounterCondition.NAMES_FIELD_NAME: [NAME_.toMap()],
        EncounterCondition.VALUES_FIELD_NAME: [VALUE.toMap()],
      });

      expect(e.id, equals(ID));
      expect(e.name, equals(NAME));
      expect(e.names.length, equals(1));
      expect(e.names.first.name, equals(NAMES_NAME));
      expect(e.values.length, equals(1));
      expect(e.values.first.name, equals(VALUE_NAME));
    });

    test('toMap', () {
      final e = EncounterCondition(ID, NAME, [NAME_], [VALUE]).toMap();

      expect(e[EncounterCondition.ID_FIELD_NAME], equals(ID));
      expect(e[EncounterCondition.NAME_FIELD_NAME], equals(NAME));
      expect(e[EncounterCondition.NAMES_FIELD_NAME], equals([NAME_.toMap()]));
      expect(e[EncounterCondition.VALUES_FIELD_NAME], equals([VALUE.toMap()]));
    });
  });
}
