// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/encounters/encounter_method.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('EncounterMethod', () {
    const ID = 1;
    const NAME = 'name';
    const ORDER = 2;
    const NAMES_NAME = 'names';
    const NAME_ = Name(NAMES_NAME, NamedApiResource('', ''));

    test('fromMap', () {
      final e = EncounterMethod.fromMap({
        EncounterMethod.ID_FIELD_NAME: ID,
        EncounterMethod.NAME_FIELD_NAME: NAME,
        EncounterMethod.ORDER_FIELD_NAME: ORDER,
        EncounterMethod.NAMES_FIELD_NAME: [NAME_.toMap()],
      });

      expect(e.id, equals(ID));
      expect(e.name, equals(NAME));
      expect(e.order, equals(ORDER));
      expect(e.names.length, equals(1));
      expect(e.names.first.name, equals(NAMES_NAME));
    });

    test('toMap', () {
      final e = EncounterMethod(ID, NAME, ORDER, [NAME_]).toMap();

      expect(e[EncounterMethod.ID_FIELD_NAME], equals(ID));
      expect(e[EncounterMethod.NAME_FIELD_NAME], equals(NAME));
      expect(e[EncounterMethod.ORDER_FIELD_NAME], equals(ORDER));
      expect(e[EncounterMethod.NAMES_FIELD_NAME], equals([NAME_.toMap()]));
    });
  });
}
