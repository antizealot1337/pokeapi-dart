// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/pokemon_stat.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('PokemonStat', () {
    const STAT_NAME = 'stat';
    const STAT = NamedApiResource(STAT_NAME, '');
    const EFFORT = 1;
    const BASE_STAT = 2;

    test('fromMap', () {
      final p = PokemonStat.fromMap({
        PokemonStat.STAT_FIELD_NAME: STAT.toMap(),
        PokemonStat.EFFORT_FIELD_NAME: EFFORT,
        PokemonStat.BASE_STAT_FIELD_NAME: BASE_STAT,
      });

      expect(p.stat.name, equals(STAT_NAME));
      expect(p.effort, equals(EFFORT));
      expect(p.baseStat, equals(BASE_STAT));
    });

    test('toMap', () {
      final p = PokemonStat(STAT, EFFORT, BASE_STAT).toMap();

      expect(p[PokemonStat.STAT_FIELD_NAME], equals(STAT.toMap()));
      expect(p[PokemonStat.EFFORT_FIELD_NAME], equals(EFFORT));
      expect(p[PokemonStat.BASE_STAT_FIELD_NAME], equals(BASE_STAT));
    });

    group('listFrom', () {
      test('null', () {
        final p = PokemonStat.listFrom(null);
        expect(p, isEmpty);
      });

      test('not null', () {
        final p = PokemonStat.listFrom([
          {
            PokemonStat.STAT_FIELD_NAME: STAT.toMap(),
            PokemonStat.EFFORT_FIELD_NAME: EFFORT,
            PokemonStat.BASE_STAT_FIELD_NAME: BASE_STAT,
          },
        ]);

        expect(p.first.stat.name, equals(STAT_NAME));
        expect(p.first.effort, equals(EFFORT));
        expect(p.first.baseStat, equals(BASE_STAT));
      });
    });
  });
}
