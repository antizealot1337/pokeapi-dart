// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/ability.dart';
import 'package:pokeapi/src/models/pokemon/ability_effect_change.dart';
import 'package:pokeapi/src/models/pokemon/ability_flavor_text.dart';
import 'package:pokeapi/src/models/pokemon/ability_pokemon.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:pokeapi/src/models/utility/common/verbose_effect.dart';
import 'package:test/test.dart';

void main() {
  group('Ability', () {
    const ID = 1;
    const NAME = 'name';
    const IS_MAIN_SERIES = true;
    const GENERATION_NAME = 'generation';
    const GENERATION = NamedApiResource(GENERATION_NAME, '');
    const NAMES_NAME = 'names';
    const NAME_ = Name(NAMES_NAME, NamedApiResource('', ''));
    const EFFECT_ENTRIES_EFFECT = 'effect entries effect';
    const EFFECT_ENTRY =
        VerboseEffect(EFFECT_ENTRIES_EFFECT, '', NamedApiResource('', ''));
    const EFFECT_CHANGES_VERSION_GROUP_NAME = 'effect changes version group';
    const EFFECT_CHANGE = AbilityEffectChange(
        [], NamedApiResource(EFFECT_CHANGES_VERSION_GROUP_NAME, ''));
    const ABILITY_FLAVOR_TEXT = 'ability flavor text';
    const FLAVOR_TEXT = AbilityFlavorText(ABILITY_FLAVOR_TEXT,
        NamedApiResource('', ''), NamedApiResource('', ''));
    const POKEMON_SLOT = 2;
    const POKEMON =
        AbilityPokemon(false, POKEMON_SLOT, NamedApiResource('', ''));

    test('fromMap', () {
      final a = Ability.fromMap({
        Ability.ID_FIELD_NAME: ID,
        Ability.NAME_FIELD_NAME: NAME,
        Ability.IS_MAIN_SERIES_FIELD_NAME: IS_MAIN_SERIES,
        Ability.GENERATION_FIELD_NAME: GENERATION.toMap(),
        Ability.NAMES_FIELD_NAME: [NAME_.toMap()],
        Ability.EFFECT_ENTRIES_FIELD_NAME: [EFFECT_ENTRY.toMap()],
        Ability.EFFECT_CHANGES_FIELD_NAME: [EFFECT_CHANGE.toMap()],
        Ability.FLAVOR_TEXT_ENTRIES_FIELD_NAME: [FLAVOR_TEXT.toMap()],
        Ability.POKEMON_FIELD_NAME: [POKEMON.toMap()],
      });

      expect(a.id, equals(ID));
      expect(a.name, equals(NAME));
      expect(a.isMainSeries, equals(IS_MAIN_SERIES));
      expect(a.generation.name, equals(GENERATION_NAME));
      expect(a.names.length, equals(1));
      expect(a.names.first.name, equals(NAMES_NAME));
      expect(a.effectEntries.length, equals(1));
      expect(a.effectEntries.first.effect, equals(EFFECT_ENTRIES_EFFECT));
      expect(a.effectChanges.length, equals(1));
      expect(a.effectChanges.first.versionGroup.name,
          equals(EFFECT_CHANGES_VERSION_GROUP_NAME));
      expect(a.flavorTextEntries.length, equals(1));
      expect(a.flavorTextEntries.first.flavorText, equals(ABILITY_FLAVOR_TEXT));
      expect(a.pokemon.length, equals(1));
      expect(a.pokemon.first.slot, equals(POKEMON_SLOT));
    });

    test('toMap', () {
      final a = Ability(ID, NAME, IS_MAIN_SERIES, GENERATION, [NAME_],
          [EFFECT_ENTRY], [EFFECT_CHANGE], [FLAVOR_TEXT], [POKEMON]).toMap();

      expect(a[Ability.ID_FIELD_NAME], equals(ID));
      expect(a[Ability.NAME_FIELD_NAME], equals(NAME));
      expect(a[Ability.IS_MAIN_SERIES_FIELD_NAME], equals(IS_MAIN_SERIES));
      expect(a[Ability.GENERATION_FIELD_NAME], equals(GENERATION.toMap()));
      expect(a[Ability.NAMES_FIELD_NAME], equals([NAME_.toMap()]));
      expect(
          a[Ability.EFFECT_ENTRIES_FIELD_NAME], equals([EFFECT_ENTRY.toMap()]));
      expect(a[Ability.EFFECT_CHANGES_FIELD_NAME],
          equals([EFFECT_CHANGE.toMap()]));
      expect(a[Ability.FLAVOR_TEXT_ENTRIES_FIELD_NAME],
          equals([FLAVOR_TEXT.toMap()]));
      expect(a[Ability.POKEMON_FIELD_NAME], equals([POKEMON.toMap()]));
    });
  });
}
