// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/growth_rate_experience_level.dart';
import 'package:test/test.dart';

void main() {
  group('GrowthRateExperienceLevel', () {
    const LEVEL = 1;
    const EXPERIENCE = 2;

    test('fromMap', () {
      final g = GrowthRateExperienceLevel.fromMap({
        GrowthRateExperienceLevel.LEVEL_FIELD_NAME: LEVEL,
        GrowthRateExperienceLevel.EXPERIENCE_FIELD_NAME: EXPERIENCE,
      });

      expect(g.level, equals(LEVEL));
      expect(g.experience, equals(EXPERIENCE));
    });

    test('toMap', () {
      final g = GrowthRateExperienceLevel(LEVEL, EXPERIENCE).toMap();

      expect(g[GrowthRateExperienceLevel.LEVEL_FIELD_NAME], equals(LEVEL));
      expect(g[GrowthRateExperienceLevel.EXPERIENCE_FIELD_NAME],
          equals(EXPERIENCE));
    });

    group('listFrom', () {
      test('null', () {
        final g = GrowthRateExperienceLevel.listFrom(null);
        expect(g, isEmpty);
      });

      test('not null', () {
        final g = GrowthRateExperienceLevel.listFrom([
          {
            GrowthRateExperienceLevel.LEVEL_FIELD_NAME: LEVEL,
            GrowthRateExperienceLevel.EXPERIENCE_FIELD_NAME: EXPERIENCE,
          },
        ]);

        expect(g.first.level, equals(LEVEL));
        expect(g.first.experience, equals(EXPERIENCE));
      });
    });
  });
}
