// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/growth_rate.dart';
import 'package:pokeapi/src/models/pokemon/growth_rate_experience_level.dart';
import 'package:pokeapi/src/models/utility/common/description.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart' hide Description;

void main() {
  group('GrowthRate', () {
    const ID = 1;
    const NAME = 'name';
    const FORMULA = 'formula';
    const DESCIRPTIONS_DESCRIPTION = 'descriptions';
    const DESCRIPTION =
        Description(DESCIRPTIONS_DESCRIPTION, NamedApiResource('', ''));
    const LEVELS_LEVEL = 2;
    const LEVEL = GrowthRateExperienceLevel(LEVELS_LEVEL, 0);
    const POKEMON_SPECIES_NAME = 'pokemon species';
    const POKEMON_SPECIES = NamedApiResource(POKEMON_SPECIES_NAME, '');

    test('fromMap', () {
      final g = GrowthRate.fromMap({
        GrowthRate.ID_FIELD_NAME: ID,
        GrowthRate.NAME_FIELD_NAME: NAME,
        GrowthRate.FORMULA_FIELD_NAME: FORMULA,
        GrowthRate.DESCRIPTIONS_FIELD_NAME: [DESCRIPTION.toMap()],
        GrowthRate.LEVELS_FIELD_NAME: [LEVEL.toMap()],
        GrowthRate.POKEMON_SPECIES_FIELD_NAME: [POKEMON_SPECIES.toMap()],
      });

      expect(g.id, equals(ID));
      expect(g.name, equals(NAME));
      expect(g.formula, equals(FORMULA));
      expect(g.descriptions.length, equals(1));
      expect(
          g.descriptions.first.description, equals(DESCIRPTIONS_DESCRIPTION));
      expect(g.levels.length, equals(1));
      expect(g.levels.first.level, equals(LEVELS_LEVEL));
      expect(g.pokemonSpecies.length, equals(1));
      expect(g.pokemonSpecies.first.name, equals(POKEMON_SPECIES_NAME));
    });

    test('toMap', () {
      final g = GrowthRate(
          ID, NAME, FORMULA, [DESCRIPTION], [LEVEL], [POKEMON_SPECIES]).toMap();

      expect(g[GrowthRate.ID_FIELD_NAME], equals(ID));
      expect(g[GrowthRate.NAME_FIELD_NAME], equals(NAME));
      expect(g[GrowthRate.FORMULA_FIELD_NAME], equals(FORMULA));
      expect(
          g[GrowthRate.DESCRIPTIONS_FIELD_NAME], equals([DESCRIPTION.toMap()]));
      expect(g[GrowthRate.LEVELS_FIELD_NAME], equals([LEVEL.toMap()]));
      expect(g[GrowthRate.POKEMON_SPECIES_FIELD_NAME],
          equals([POKEMON_SPECIES.toMap()]));
    });
  });
}
