// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/ability_flavor_text.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('AbilityFlavorText', () {
    const FLAVOR_TEXT = 'flavor text';
    const LANGUAGE_NAME = 'langage';
    const LANGUAGE = NamedApiResource(LANGUAGE_NAME, '');
    const VERSION_GROUP_NAME = 'version group';
    const VERSION_GROUP = NamedApiResource(VERSION_GROUP_NAME, '');

    test('fromMap', () {
      final a = AbilityFlavorText.fromMap({
        AbilityFlavorText.FLAVOR_TEXT_FIELD_NAME: FLAVOR_TEXT,
        AbilityFlavorText.LANGUAGE_FIELD_NAME: LANGUAGE.toMap(),
        AbilityFlavorText.VERSION_GROUP_FIELD_NAME: VERSION_GROUP.toMap(),
      });

      expect(a.flavorText, equals(FLAVOR_TEXT));
      expect(a.language.name, equals(LANGUAGE_NAME));
      expect(a.versionGroup.name, equals(VERSION_GROUP_NAME));
    });

    test('toMap', () {
      final a = AbilityFlavorText(FLAVOR_TEXT, LANGUAGE, VERSION_GROUP).toMap();

      expect(a[AbilityFlavorText.FLAVOR_TEXT_FIELD_NAME], equals(FLAVOR_TEXT));
      expect(
          a[AbilityFlavorText.LANGUAGE_FIELD_NAME], equals(LANGUAGE.toMap()));
      expect(a[AbilityFlavorText.VERSION_GROUP_FIELD_NAME],
          equals(VERSION_GROUP.toMap()));
    });

    group('listFrom', () {
      test('null', () {
        final a = AbilityFlavorText.listFrom(null);
        expect(a, isEmpty);
      });

      test('not null', () {
        final a = AbilityFlavorText.listFrom([
          {
            AbilityFlavorText.FLAVOR_TEXT_FIELD_NAME: FLAVOR_TEXT,
            AbilityFlavorText.LANGUAGE_FIELD_NAME: LANGUAGE.toMap(),
            AbilityFlavorText.VERSION_GROUP_FIELD_NAME: VERSION_GROUP.toMap(),
          },
        ]);

        expect(a.first.flavorText, equals(FLAVOR_TEXT));
        expect(a.first.language.name, equals(LANGUAGE_NAME));
        expect(a.first.versionGroup.name, equals(VERSION_GROUP_NAME));
      });
    });
  });
}
