// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/pal_park_encounter_area.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('PalParkEncounterArea', () {
    const BASE_SCORE = 1;
    const RATE = 2;
    const AREA_NAME = 'area';
    const AREA = NamedApiResource(AREA_NAME, '');

    test('fromMap', () {
      final p = PalParkEncounterArea.fromMap({
        PalParkEncounterArea.BASE_SCORE_FIELD_NAME: BASE_SCORE,
        PalParkEncounterArea.RATE_FIELD_NAME: RATE,
        PalParkEncounterArea.AREA_FIELD_NAME: AREA.toMap(),
      });

      expect(p.baseScore, equals(BASE_SCORE));
      expect(p.rate, equals(RATE));
      expect(p.area.name, equals(AREA_NAME));
    });

    test('toMap', () {
      final p = PalParkEncounterArea(BASE_SCORE, RATE, AREA).toMap();

      expect(p[PalParkEncounterArea.BASE_SCORE_FIELD_NAME], equals(BASE_SCORE));
      expect(p[PalParkEncounterArea.RATE_FIELD_NAME], equals(RATE));
      expect(p[PalParkEncounterArea.AREA_FIELD_NAME], equals(AREA.toMap()));
    });

    group('listFrom', () {
      test('null', () {
        final p = PalParkEncounterArea.listFrom(null);
        expect(p, isEmpty);
      });

      test('not null', () {
        final p = PalParkEncounterArea.listFrom([
          {
            PalParkEncounterArea.BASE_SCORE_FIELD_NAME: BASE_SCORE,
            PalParkEncounterArea.RATE_FIELD_NAME: RATE,
            PalParkEncounterArea.AREA_FIELD_NAME: AREA.toMap(),
          },
        ]);

        expect(p.first.baseScore, equals(BASE_SCORE));
        expect(p.first.rate, equals(RATE));
        expect(p.first.area.name, equals(AREA_NAME));
      });
    });
  });
}
