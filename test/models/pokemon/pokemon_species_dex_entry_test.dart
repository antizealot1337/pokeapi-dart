// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/pokemon_species_dex_entry.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('PokemonSpeciesDexEntry', () {
    const ENTRY_NUMBER = 1;
    const POKEDEX_NAME = 'pokedex';
    const POKEDEX = NamedApiResource(POKEDEX_NAME, '');

    test('fromMap', () {
      final p = PokemonSpeciesDexEntry.fromMap({
        PokemonSpeciesDexEntry.ENTRY_NUMBER_FIELD_NAME: ENTRY_NUMBER,
        PokemonSpeciesDexEntry.POKEDEX_FIELD_NAME: POKEDEX.toMap(),
      });

      expect(p.entryNumber, equals(ENTRY_NUMBER));
      expect(p.pokedex.name, equals(POKEDEX_NAME));
    });

    test('toMap', () {
      final p = PokemonSpeciesDexEntry(ENTRY_NUMBER, POKEDEX).toMap();

      expect(p[PokemonSpeciesDexEntry.ENTRY_NUMBER_FIELD_NAME],
          equals(ENTRY_NUMBER));
      expect(p[PokemonSpeciesDexEntry.POKEDEX_FIELD_NAME],
          equals(POKEDEX.toMap()));
    });

    group('listFrom', () {
      test('null', () {
        final p = PokemonSpeciesDexEntry.listFrom(null);
        expect(p, isEmpty);
      });

      test('not null', () {
        final p = PokemonSpeciesDexEntry.listFrom([
          {
            PokemonSpeciesDexEntry.ENTRY_NUMBER_FIELD_NAME: ENTRY_NUMBER,
            PokemonSpeciesDexEntry.POKEDEX_FIELD_NAME: POKEDEX.toMap(),
          },
        ]);

        expect(p.first.entryNumber, equals(ENTRY_NUMBER));
        expect(p.first.pokedex.name, equals(POKEDEX_NAME));
      });
    });
  });
}
