// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/move_stat_affect.dart';
import 'package:pokeapi/src/models/pokemon/move_stat_affect_sets.dart';
import 'package:pokeapi/src/models/pokemon/nature_stat_affect_sets.dart';
import 'package:pokeapi/src/models/pokemon/stat.dart';
import 'package:pokeapi/src/models/utility/common/api_resource.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('Stat', () {
    const ID = 1;
    const NAME = 'name';
    const GAME_INDEX = 2;
    const IS_BATTLE_ONLY = true;
    const AFFECTING_MOVES_INCREASE_CHANGE = 3;
    const AFFECTING_MOVES = MoveStatAffectSets([
      MoveStatAffect(AFFECTING_MOVES_INCREASE_CHANGE, NamedApiResource('', ''))
    ], []);
    const AFFECTING_NATURE_INCREASE_NAME = 'affecting nature increase';
    const AFFECTING_NATURES = NatureStatAffectSets(
        [NamedApiResource(AFFECTING_NATURE_INCREASE_NAME, '')], []);
    const CHARACTERISTICS_URL = 'characteristics';
    const CHARACTERISTIC = ApiResource(CHARACTERISTICS_URL);
    const MOVE_DAMAGE_CLASS_NAME = 'move damage class';
    const MOVE_DAMAGE_CLASS = NamedApiResource(MOVE_DAMAGE_CLASS_NAME, '');
    const NAMES_NAME = 'names';
    const NAME_ = Name(NAMES_NAME, NamedApiResource('', ''));

    test('fromMap', () {
      final s = Stat.fromMap({
        Stat.ID_FIELD_NAME: ID,
        Stat.NAME_FIELD_NAME: NAME,
        Stat.GAME_INDEX_FIELD_NAME: GAME_INDEX,
        Stat.IS_BATTLE_ONLY_FIELD_NAME: true,
        Stat.AFFECTING_MOVES_FIELD_NAME: AFFECTING_MOVES.toMap(),
        Stat.AFFECTING_NATURES_FIELD_NAME: AFFECTING_NATURES.toMap(),
        Stat.CHARACTERISTICS_FIELD_NAME: [CHARACTERISTIC.toMap()],
        Stat.MOVE_DAMAGE_CLASS_FIELD_NAME: MOVE_DAMAGE_CLASS.toMap(),
        Stat.NAMES_FIELD_NAME: [NAME_.toMap()],
      });

      expect(s.id, equals(ID));
      expect(s.name, equals(NAME));
      expect(s.gameIndex, equals(GAME_INDEX));
      expect(s.isBattleOnly, equals(IS_BATTLE_ONLY));
      expect(s.affectingMoves.increase.first.change,
          equals(AFFECTING_MOVES_INCREASE_CHANGE));
      expect(s.affectingNatures.increase.first.name,
          equals(AFFECTING_NATURE_INCREASE_NAME));
      expect(s.characteristics.length, equals(1));
      expect(s.characteristics.first.url, equals(CHARACTERISTICS_URL));
      expect(s.moveDamageClass!.name, equals(MOVE_DAMAGE_CLASS_NAME));
      expect(s.names.length, equals(1));
      expect(s.names.first.name, equals(NAMES_NAME));
    });

    test('toMap', () {
      final s = Stat(
          ID,
          NAME,
          GAME_INDEX,
          IS_BATTLE_ONLY,
          AFFECTING_MOVES,
          AFFECTING_NATURES,
          [CHARACTERISTIC],
          MOVE_DAMAGE_CLASS,
          [NAME_]).toMap();

      expect(s[Stat.ID_FIELD_NAME], equals(ID));
      expect(s[Stat.NAME_FIELD_NAME], equals(NAME));
      expect(s[Stat.GAME_INDEX_FIELD_NAME], equals(GAME_INDEX));
      expect(s[Stat.IS_BATTLE_ONLY_FIELD_NAME], equals(IS_BATTLE_ONLY));
      expect(
          s[Stat.AFFECTING_MOVES_FIELD_NAME], equals(AFFECTING_MOVES.toMap()));
      expect(s[Stat.AFFECTING_NATURES_FIELD_NAME],
          equals(AFFECTING_NATURES.toMap()));
      expect(
          s[Stat.CHARACTERISTICS_FIELD_NAME], equals([CHARACTERISTIC.toMap()]));
      expect(s[Stat.MOVE_DAMAGE_CLASS_FIELD_NAME],
          equals(MOVE_DAMAGE_CLASS.toMap()));
      expect(s[Stat.NAMES_FIELD_NAME], equals([NAME_.toMap()]));
    });
  });
}
