// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/move_battle_style_preference.dart';
import 'package:pokeapi/src/models/pokemon/nature.dart';
import 'package:pokeapi/src/models/pokemon/nature_stat_change.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('Nature', () {
    const ID = 1;
    const NAME = 'name';
    const DECREASED_STAT_NAME = 'decreased stat';
    const DECREASED_STAT = NamedApiResource(DECREASED_STAT_NAME, '');
    const INCREASED_STAT_NAME = 'increased stat';
    const INCREASED_STAT = NamedApiResource(INCREASED_STAT_NAME, '');
    const HATES_FLAVOR_NAME = 'hates flavor';
    const HATES_FLAVOR = NamedApiResource(HATES_FLAVOR_NAME, '');
    const LIKES_FLAVOR_NAME = 'likes flavor';
    const LIKES_FLAVOR = NamedApiResource(LIKES_FLAVOR_NAME, '');
    const POKEATHLON_STAT_CHANGES_MAX_CHANGE = 2;
    const POKEATHLON_STAT_CHANGES = NatureStatChange(
        POKEATHLON_STAT_CHANGES_MAX_CHANGE, NamedApiResource('', ''));
    const MOVE_BATTLE_PREFERENCES_LOW_HP_PREFERENCE = 3;
    const MOVE_BATTLE_PREFERENCE = MoveBattleStylePreference(
        MOVE_BATTLE_PREFERENCES_LOW_HP_PREFERENCE, 0, NamedApiResource('', ''));
    const NAMES_NAME = 'names';
    const NAME_ = Name(NAMES_NAME, NamedApiResource('', ''));

    test('fromMap', () {
      final n = Nature.fromMap({
        Nature.ID_FIELD_NAME: ID,
        Nature.NAME_FIELD_NAME: NAME,
        Nature.DECREASED_STAT_FIELD_NAME: DECREASED_STAT.toMap(),
        Nature.INCREASED_STAT_FIELD_NAME: INCREASED_STAT.toMap(),
        Nature.HATES_FLAVOR_FIELD_NAME: HATES_FLAVOR.toMap(),
        Nature.LIKES_FLAVOR_FIELD_NAME: LIKES_FLAVOR.toMap(),
        Nature.POKEATHLON_STAT_CHANGES_FIELD_NAME: [
          POKEATHLON_STAT_CHANGES.toMap(),
        ],
        Nature.MOVE_BATTLE_STYLE_PREFERENCES_FIELD_NAME: [
          MOVE_BATTLE_PREFERENCE.toMap(),
        ],
        Nature.NAMES_FIELD_NAME: [NAME_.toMap()],
      });

      expect(n.id, equals(ID));
      expect(n.name, equals(NAME));
      expect(n.decreasedStat!.name, equals(DECREASED_STAT_NAME));
      expect(n.increasedStat!.name, equals(INCREASED_STAT_NAME));
      expect(n.hatesFlavor!.name, equals(HATES_FLAVOR_NAME));
      expect(n.likesFlavor!.name, equals(LIKES_FLAVOR_NAME));
      expect(n.pokeathlonStatChanges.length, equals(1));
      expect(n.pokeathlonStatChanges.first.maxChange,
          equals(POKEATHLON_STAT_CHANGES_MAX_CHANGE));
      expect(n.moveBattleStylePreferences.length, equals(1));
      expect(n.moveBattleStylePreferences.first.lowHpPreference,
          equals(MOVE_BATTLE_PREFERENCES_LOW_HP_PREFERENCE));
      expect(n.names.length, equals(1));
      expect(n.names.first.name, equals(NAMES_NAME));
    });

    test('toMap', () {
      final n = Nature(
          ID,
          NAME,
          DECREASED_STAT,
          INCREASED_STAT,
          HATES_FLAVOR,
          LIKES_FLAVOR,
          [POKEATHLON_STAT_CHANGES],
          [MOVE_BATTLE_PREFERENCE],
          [NAME_]).toMap();

      expect(n[Nature.ID_FIELD_NAME], equals(ID));
      expect(n[Nature.NAME_FIELD_NAME], equals(NAME));
      expect(
          n[Nature.DECREASED_STAT_FIELD_NAME], equals(DECREASED_STAT.toMap()));
      expect(
          n[Nature.INCREASED_STAT_FIELD_NAME], equals(INCREASED_STAT.toMap()));
      expect(n[Nature.HATES_FLAVOR_FIELD_NAME], equals(HATES_FLAVOR.toMap()));
      expect(n[Nature.LIKES_FLAVOR_FIELD_NAME], equals(LIKES_FLAVOR.toMap()));
      expect(n[Nature.POKEATHLON_STAT_CHANGES_FIELD_NAME],
          equals([POKEATHLON_STAT_CHANGES.toMap()]));
      expect(n[Nature.MOVE_BATTLE_STYLE_PREFERENCES_FIELD_NAME],
          equals([MOVE_BATTLE_PREFERENCE.toMap()]));
      expect(n[Nature.NAMES_FIELD_NAME], equals([NAME_.toMap()]));
    });
  });
}
