// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/pokemon_sprites.dart';
import 'package:test/test.dart';

void main() {
  group('PokemonSprites', () {
    const FRONT_DEFAULT = 'front default';
    const FRONT_SHINY = 'front shiny';
    const FRONT_FEMALE = 'front female';
    const FRONT_SHINY_FEMALE = 'front shiny female';
    const BACK_DEFAULT = 'back default';
    const BACK_SHINY = 'back shiny';
    const BACK_FEMALE = 'back female';
    const BACK_SHINY_FEMALE = 'back shiny female';

    test('fromMap', () {
      final p = PokemonSprites.fromMap({
        PokemonSprites.FRONT_DEFAULT_FIELD_NAME: FRONT_DEFAULT,
        PokemonSprites.FRONT_SHINY_FIELD_NAME: FRONT_SHINY,
        PokemonSprites.FRONT_FEMALE_FIELD_NAME: FRONT_FEMALE,
        PokemonSprites.FRONT_SHINY_FEMALE_FIELD_NAME: FRONT_SHINY_FEMALE,
        PokemonSprites.BACK_DEFAULT_FIELD_NAME: BACK_DEFAULT,
        PokemonSprites.BACK_SHINY_FIELD_NAME: BACK_SHINY,
        PokemonSprites.BACK_FEMALE_FIELD_NAME: BACK_FEMALE,
        PokemonSprites.BACK_SHINY_FEMALE_FIELD_NAME: BACK_SHINY_FEMALE,
      });

      expect(p.frontDefault, equals(FRONT_DEFAULT));
      expect(p.frontShiny, equals(FRONT_SHINY));
      expect(p.frontFemale, equals(FRONT_FEMALE));
      expect(p.frontShinyFemale, equals(FRONT_SHINY_FEMALE));
      expect(p.backDefault, equals(BACK_DEFAULT));
      expect(p.backShiny, equals(BACK_SHINY));
      expect(p.backFemale, equals(BACK_FEMALE));
      expect(p.backShinyFemale, equals(BACK_SHINY_FEMALE));
    });

    test('toMap', () {
      final p = PokemonSprites(
              FRONT_DEFAULT,
              FRONT_SHINY,
              FRONT_FEMALE,
              FRONT_SHINY_FEMALE,
              BACK_DEFAULT,
              BACK_SHINY,
              BACK_FEMALE,
              BACK_SHINY_FEMALE)
          .toMap();

      expect(p[PokemonSprites.FRONT_DEFAULT_FIELD_NAME], equals(FRONT_DEFAULT));
      expect(p[PokemonSprites.FRONT_SHINY_FIELD_NAME], equals(FRONT_SHINY));
      expect(p[PokemonSprites.FRONT_FEMALE_FIELD_NAME], equals(FRONT_FEMALE));
      expect(p[PokemonSprites.FRONT_SHINY_FEMALE_FIELD_NAME],
          equals(FRONT_SHINY_FEMALE));
      expect(p[PokemonSprites.BACK_DEFAULT_FIELD_NAME], equals(BACK_DEFAULT));
      expect(p[PokemonSprites.BACK_SHINY_FIELD_NAME], equals(BACK_SHINY));
      expect(p[PokemonSprites.BACK_FEMALE_FIELD_NAME], equals(BACK_FEMALE));
      expect(p[PokemonSprites.BACK_SHINY_FEMALE_FIELD_NAME],
          equals(BACK_SHINY_FEMALE));
    });
  });
}
