// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/move_battle_style_preference.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('MoveBattleStylePreference', () {
    const LOW_HP_PREFERENCE = 1;
    const HIGH_HP_PREFERENCE = 2;
    const MOVE_BATTLE_STYLE_NAME = 'move battle style';
    const MOVE_BATTLE_STYLE = NamedApiResource(MOVE_BATTLE_STYLE_NAME, '');

    test('fromMap', () {
      final m = MoveBattleStylePreference.fromMap({
        MoveBattleStylePreference.LOW_HP_PREFERENCE_FIELD_NAME:
            LOW_HP_PREFERENCE,
        MoveBattleStylePreference.HIGH_HP_PREFERENCE_FIELD_NAME:
            HIGH_HP_PREFERENCE,
        MoveBattleStylePreference.MOVE_BATTLE_STYLE_FIELD_NAME:
            MOVE_BATTLE_STYLE.toMap(),
      });

      expect(m.lowHpPreference, equals(LOW_HP_PREFERENCE));
      expect(m.highHpPreference, equals(HIGH_HP_PREFERENCE));
      expect(m.moveBattleStyle.name, equals(MOVE_BATTLE_STYLE_NAME));
    });

    test('toMap', () {
      final m = MoveBattleStylePreference(
              LOW_HP_PREFERENCE, HIGH_HP_PREFERENCE, MOVE_BATTLE_STYLE)
          .toMap();

      expect(m[MoveBattleStylePreference.LOW_HP_PREFERENCE_FIELD_NAME],
          equals(LOW_HP_PREFERENCE));
      expect(m[MoveBattleStylePreference.HIGH_HP_PREFERENCE_FIELD_NAME],
          equals(HIGH_HP_PREFERENCE));
      expect(m[MoveBattleStylePreference.MOVE_BATTLE_STYLE_FIELD_NAME],
          equals(MOVE_BATTLE_STYLE.toMap()));
    });

    group('listFrom', () {
      test('null', () {
        final m = MoveBattleStylePreference.listFrom(null);
        expect(m, isEmpty);
      });

      test('not null', () {
        final m = MoveBattleStylePreference.listFrom([
          {
            MoveBattleStylePreference.LOW_HP_PREFERENCE_FIELD_NAME:
                LOW_HP_PREFERENCE,
            MoveBattleStylePreference.HIGH_HP_PREFERENCE_FIELD_NAME:
                HIGH_HP_PREFERENCE,
            MoveBattleStylePreference.MOVE_BATTLE_STYLE_FIELD_NAME:
                MOVE_BATTLE_STYLE.toMap(),
          },
        ]);

        expect(m.first.lowHpPreference, equals(LOW_HP_PREFERENCE));
        expect(m.first.highHpPreference, equals(HIGH_HP_PREFERENCE));
        expect(m.first.moveBattleStyle.name, equals(MOVE_BATTLE_STYLE_NAME));
      });
    });
  });
}
