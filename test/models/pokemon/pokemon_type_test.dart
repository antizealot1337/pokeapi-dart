// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/pokemon_type.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('PokemonType', () {
    const SLOT = 1;
    const TYPE_NAME = 'type';
    const TYPE = NamedApiResource(TYPE_NAME, '');

    test('fromMap', () {
      final p = PokemonType.fromMap({
        PokemonType.SLOT_FIELD_NAME: SLOT,
        PokemonType.TYPE_FIELD_NAME: TYPE.toMap(),
      });

      expect(p.slot, equals(SLOT));
      expect(p.type.name, equals(TYPE_NAME));
    });

    test('toMap', () {
      final p = PokemonType(SLOT, TYPE).toMap();

      expect(p[PokemonType.SLOT_FIELD_NAME], equals(SLOT));
      expect(p[PokemonType.TYPE_FIELD_NAME], equals(TYPE.toMap()));
    });

    group('listFrom', () {
      test('null', () {
        final p = PokemonType.listFrom(null);
        expect(p, isEmpty);
      });

      test('not null', () {
        final p = PokemonType.listFrom([
          {
            PokemonType.SLOT_FIELD_NAME: SLOT,
            PokemonType.TYPE_FIELD_NAME: TYPE.toMap(),
          },
        ]);

        expect(p.first.slot, equals(SLOT));
        expect(p.first.type.name, equals(TYPE_NAME));
      });
    });
  });
}
