// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/pokemon_form.dart';
import 'package:pokeapi/src/models/pokemon/pokemon_form_sprites.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('PokemonForm', () {
    const ID = 1;
    const NAME = 'name';
    const ORDER = 2;
    const FORM_ORDER = 3;
    const IS_DEFAULT = true;
    const IS_BATTLE_ONLY = false;
    const IS_MEGA = true;
    const FORM_NAME = 'form name';
    const POKEMON_NAME = 'pokemon';
    const POKEMON = NamedApiResource(POKEMON_NAME, '');
    const SPRITE_FRONT_DEFAULT = 'sprite front default';
    const SPRITES = PokemonFormSprites(SPRITE_FRONT_DEFAULT, '', '', '');
    const VERSION_GROUP_NAME = 'version group';
    const VERSION_GROUP = NamedApiResource(VERSION_GROUP_NAME, '');
    const NAMES_NAME = 'names';
    const NAME_ = Name(NAMES_NAME, NamedApiResource('', ''));
    const FORMS_NAMES = 'forms';
    const FORM = Name(FORMS_NAMES, NamedApiResource('', ''));

    test('fromMap', () {
      final p = PokemonForm.fromMap({
        PokemonForm.ID_FIELD_NAME: ID,
        PokemonForm.NAME_FIELD_NAME: NAME,
        PokemonForm.ORDER_FIELD_NAME: ORDER,
        PokemonForm.FORM_ORDER_FIELD_NAME: FORM_ORDER,
        PokemonForm.IS_DEFAULT_FIELD_NAME: IS_DEFAULT,
        PokemonForm.IS_BATTLE_ONLY_FIELD_NAME: IS_BATTLE_ONLY,
        PokemonForm.IS_MEGA_FIELD_NAME: IS_MEGA,
        PokemonForm.FORM_NAME_FIELD_NAME: FORM_NAME,
        PokemonForm.POKEMON_FIELD_NAME: POKEMON.toMap(),
        PokemonForm.SPRITES_FIELD_NAME: SPRITES.toMap(),
        PokemonForm.VERSION_GROUP_FIELD_NAME: VERSION_GROUP.toMap(),
        PokemonForm.NAMES_FIELD_NAME: [NAME_.toMap()],
        PokemonForm.FORM_NAMES_FIELD_NAME: [FORM.toMap()],
      });

      expect(p.id, equals(ID));
      expect(p.name, equals(NAME));
      expect(p.order, equals(ORDER));
      expect(p.formOrder, equals(FORM_ORDER));
      expect(p.isDefault, equals(IS_DEFAULT));
      expect(p.isBattleOnly, equals(IS_BATTLE_ONLY));
      expect(p.isMega, equals(IS_MEGA));
      expect(p.formName, equals(FORM_NAME));
      expect(p.pokemon.name, equals(POKEMON_NAME));
      expect(p.sprites.frontDefault, equals(SPRITE_FRONT_DEFAULT));
      expect(p.versionGroup.name, equals(VERSION_GROUP_NAME));
      expect(p.names.length, equals(1));
      expect(p.names.first.name, equals(NAMES_NAME));
      expect(p.formNames.length, equals(1));
      expect(p.formNames.first.name, equals(FORMS_NAMES));
    });

    test('toMap', () {
      final p = PokemonForm(
          ID,
          NAME,
          ORDER,
          FORM_ORDER,
          IS_DEFAULT,
          IS_BATTLE_ONLY,
          IS_MEGA,
          FORM_NAME,
          POKEMON,
          SPRITES,
          VERSION_GROUP,
          [NAME_],
          [FORM]).toMap();

      expect(p[PokemonForm.ID_FIELD_NAME], equals(ID));
      expect(p[PokemonForm.NAME_FIELD_NAME], equals(NAME));
      expect(p[PokemonForm.ORDER_FIELD_NAME], equals(ORDER));
      expect(p[PokemonForm.FORM_ORDER_FIELD_NAME], equals(FORM_ORDER));
      expect(p[PokemonForm.IS_DEFAULT_FIELD_NAME], equals(IS_DEFAULT));
      expect(p[PokemonForm.IS_BATTLE_ONLY_FIELD_NAME], equals(IS_BATTLE_ONLY));
      expect(p[PokemonForm.IS_MEGA_FIELD_NAME], equals(IS_MEGA));
      expect(p[PokemonForm.FORM_NAME_FIELD_NAME], equals(FORM_NAME));
      expect(p[PokemonForm.POKEMON_FIELD_NAME], equals(POKEMON.toMap()));
      expect(p[PokemonForm.SPRITES_FIELD_NAME], equals(SPRITES.toMap()));
      expect(p[PokemonForm.VERSION_GROUP_FIELD_NAME],
          equals(VERSION_GROUP.toMap()));
      expect(p[PokemonForm.NAMES_FIELD_NAME], equals([NAME_.toMap()]));
      expect(p[PokemonForm.FORM_NAMES_FIELD_NAME], equals([FORM.toMap()]));
    });
  });
}
