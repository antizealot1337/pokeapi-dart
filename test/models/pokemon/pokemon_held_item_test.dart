// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/pokemon_held_item.dart';
import 'package:pokeapi/src/models/pokemon/pokemon_held_item_version.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('PokemonHeldItem', () {
    const ITEM_NAME = 'item';
    const ITEM = NamedApiResource(ITEM_NAME, '');
    const VESION_DETAIL_VERSION_NAME = 'version details version';
    const VERSION_DETAIL = PokemonHeldItemVersion(
        NamedApiResource(VESION_DETAIL_VERSION_NAME, ''), 0);

    test('fromMap', () {
      final p = PokemonHeldItem.fromMap({
        PokemonHeldItem.ITEM_FIELD_NAME: ITEM.toMap(),
        PokemonHeldItem.VERSION_DETAILS_FIELD_NAME: [VERSION_DETAIL.toMap()],
      });

      expect(p.item.name, equals(ITEM_NAME));
      expect(p.versionDetails.length, equals(1));
      expect(p.versionDetails.first.version.name,
          equals(VESION_DETAIL_VERSION_NAME));
    });

    test('toMap', () {
      final p = PokemonHeldItem(ITEM, [VERSION_DETAIL]).toMap();

      expect(p[PokemonHeldItem.ITEM_FIELD_NAME], equals(ITEM.toMap()));
      expect(p[PokemonHeldItem.VERSION_DETAILS_FIELD_NAME],
          equals([VERSION_DETAIL.toMap()]));
    });

    group('listFrom', () {
      test('null', () {
        final p = PokemonHeldItem.listFrom(null);
        expect(p, isEmpty);
      });

      test('not null', () {
        final p = PokemonHeldItem.listFrom([
          {
            PokemonHeldItem.ITEM_FIELD_NAME: ITEM.toMap(),
            PokemonHeldItem.VERSION_DETAILS_FIELD_NAME: [
              VERSION_DETAIL.toMap(),
            ],
          },
        ]);

        expect(p.first.item.name, equals(ITEM_NAME));
        expect(p.first.versionDetails.length, equals(1));
        expect(p.first.versionDetails.first.version.name,
            equals(VESION_DETAIL_VERSION_NAME));
      });
    });
  });
}
