// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/move_stat_affect.dart';
import 'package:pokeapi/src/models/pokemon/move_stat_affect_sets.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('MoveStatAffectSets', () {
    const INCREASE_CHANGE = 1;
    const INCREASE = MoveStatAffect(INCREASE_CHANGE, NamedApiResource('', ''));
    const DECREASE_CHANGE = 2;
    const DECREASE = MoveStatAffect(DECREASE_CHANGE, NamedApiResource('', ''));

    test('fromMap', () {
      final m = MoveStatAffectSets.fromMap({
        MoveStatAffectSets.INCREASE_FIELD_NAME: [INCREASE.toMap()],
        MoveStatAffectSets.DECREASE_FIELD_NAME: [DECREASE.toMap()],
      });

      expect(m.increase.length, equals(1));
      expect(m.increase.first.change, equals(INCREASE_CHANGE));
      expect(m.decrease.length, equals(1));
      expect(m.decrease.first.change, equals(DECREASE_CHANGE));
    });

    test('toMap', () {
      final m = MoveStatAffectSets([INCREASE], [DECREASE]).toMap();

      expect(m[MoveStatAffectSets.INCREASE_FIELD_NAME],
          equals([INCREASE.toMap()]));
      expect(m[MoveStatAffectSets.DECREASE_FIELD_NAME],
          equals([DECREASE.toMap()]));
    });
  });
}
