// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/genus.dart';
import 'package:pokeapi/src/models/pokemon/pal_park_encounter_area.dart';
import 'package:pokeapi/src/models/pokemon/pokemon_species.dart';
import 'package:pokeapi/src/models/pokemon/pokemon_species_dex_entry.dart';
import 'package:pokeapi/src/models/pokemon/pokemon_species_variety.dart';
import 'package:pokeapi/src/models/utility/common/api_resource.dart';
import 'package:pokeapi/src/models/utility/common/description.dart';
import 'package:pokeapi/src/models/utility/common/flavor_text.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart' hide Description;

void main() {
  group('PokemonSpecies', () {
    const ID = 1;
    const NAME = 'name';
    const ORDER = 2;
    const GENDER_RATE = 3;
    const CAPTURE_RATE = 4;
    const BASE_HAPPINESS = 5;
    const IS_BABY = true;
    const IS_LEGENDARY = false;
    const IS_MYTHICAL = true;
    const HATCH_COUNTER = 6;
    const HAS_GENDER_DIFFERENCES = false;
    const FORMS_SWITCHABLE = true;
    const GROWTH_RATE_NAME = 'growth rate';
    const GROWTH_RATE = NamedApiResource(GROWTH_RATE_NAME, '');
    const POKEDEX_NUMBER_ENTRY_NUMBER = 7;
    const POKEDEX_NUMBER = PokemonSpeciesDexEntry(
        POKEDEX_NUMBER_ENTRY_NUMBER, NamedApiResource('', ''));
    const EGG_GROUP_NAME = 'egg group';
    const EGG_GROUP = NamedApiResource(EGG_GROUP_NAME, '');
    const COLOR_NAME = 'color';
    const COLOR = NamedApiResource(COLOR_NAME, '');
    const SHAPE_NAME = 'shape';
    const SHAPE = NamedApiResource(SHAPE_NAME, '');
    const EVOLVES_FROM_SPECIES_NAME = 'evolves from species';
    const EVOLVES_FROM_SPECIES =
        NamedApiResource(EVOLVES_FROM_SPECIES_NAME, '');
    const EVOLUTION_CHAIN_URL = 'evolution chaing url';
    const EVOLUTION_CHAIN = ApiResource(EVOLUTION_CHAIN_URL);
    const HABITAT_NAME = 'habitat';
    const HABITAT = NamedApiResource(HABITAT_NAME, '');
    const GENERATION_NAME = 'generation';
    const GENERATION = NamedApiResource(GENERATION_NAME, '');
    const NAMES_NAME = 'names';
    const NAME_ = Name(NAMES_NAME, NamedApiResource('', ''));
    const PAL_PARK_ENCOUNTER_BASE_SCORE = 8;
    const PAL_PARK_ENCOUNTER = PalParkEncounterArea(
        PAL_PARK_ENCOUNTER_BASE_SCORE, 0, NamedApiResource('', ''));
    const FLAVOR_TEXT_ENTRIES_FLAVOR_TEXT = 'flavor text entries flavor text';
    const FLAVOR_TEXT_ENTRY = FlavorText(FLAVOR_TEXT_ENTRIES_FLAVOR_TEXT,
        NamedApiResource('', ''), NamedApiResource('', ''));
    const FORM_DESCRIPTIONS_DESCRIPTION = 'form descriptions description';
    const FORM_DESCRIPTION =
        Description(FORM_DESCRIPTIONS_DESCRIPTION, NamedApiResource('', ''));
    const GENERA_GENUS = 'genera genus';
    const GENUS = Genus(GENERA_GENUS, NamedApiResource('', ''));
    const POKEMON_SPECIES_VARIETY_POKEMON_NAME =
        'pokemon species variety pokemon name';
    const POKEMON_SPECIES_VARIETY = PokemonSpeciesVariety(
        false, NamedApiResource(POKEMON_SPECIES_VARIETY_POKEMON_NAME, ''));

    test('fromMap', () {
      final p = PokemonSpecies.fromMap({
        PokemonSpecies.ID_FIELD_NAME: ID,
        PokemonSpecies.NAME_FIELD_NAME: NAME,
        PokemonSpecies.ORDER_FIELD_NAME: ORDER,
        PokemonSpecies.GENDER_RATE_FIELD_NAME: GENDER_RATE,
        PokemonSpecies.CAPTURE_RATE_FIELD_NAME: CAPTURE_RATE,
        PokemonSpecies.BASE_HAPPINESS_FIELD_NAME: BASE_HAPPINESS,
        PokemonSpecies.IS_BABY_FIELD_NAME: IS_BABY,
        PokemonSpecies.IS_LEGENDARY_FIELD_NAME: IS_LEGENDARY,
        PokemonSpecies.IS_MYTHICAL_FIELD_NAME: IS_MYTHICAL,
        PokemonSpecies.HATCH_COUNTER_FIELD_NAME: HATCH_COUNTER,
        PokemonSpecies.HAS_GENDER_DIFFERENCES_FIELD_NAME:
            HAS_GENDER_DIFFERENCES,
        PokemonSpecies.FORMS_SWITCHABLE_FIELD_NAME: FORMS_SWITCHABLE,
        PokemonSpecies.GROWTH_RATE_FIELD_NAME: GROWTH_RATE.toMap(),
        PokemonSpecies.POKEDEX_NUMBERS_FIELD_NAME: [POKEDEX_NUMBER.toMap()],
        PokemonSpecies.EGG_GROUPS_FIELD_NAME: [EGG_GROUP.toMap()],
        PokemonSpecies.COLOR_FIELD_NAME: COLOR.toMap(),
        PokemonSpecies.SHAPE_FIELD_NAME: SHAPE.toMap(),
        PokemonSpecies.EVOLVES_FROM_SPECIES_FIELD_NAME:
            EVOLVES_FROM_SPECIES.toMap(),
        PokemonSpecies.EVOLUTION_CHAIN_FIELD_NAME: EVOLUTION_CHAIN.toMap(),
        PokemonSpecies.HABITAT_FIELD_NAME: HABITAT.toMap(),
        PokemonSpecies.GENERATION_FIELD_NAME: GENERATION.toMap(),
        PokemonSpecies.NAMES_FIELD_NAME: [NAME_.toMap()],
        PokemonSpecies.PAL_PARK_ENCOUNTERS_FIELD_NAME: [
          PAL_PARK_ENCOUNTER.toMap(),
        ],
        PokemonSpecies.FLAVOR_TEXT_ENTRIES_FIELD_NAME: [
          FLAVOR_TEXT_ENTRY.toMap(),
        ],
        PokemonSpecies.FORM_DESCRIPTIONS_FIELD_NAME: [
          FORM_DESCRIPTION.toMap(),
        ],
        PokemonSpecies.GENERA_FIELD_NAME: [GENUS.toMap()],
        PokemonSpecies.VARIETIES_FIELD_NAME: [
          POKEMON_SPECIES_VARIETY.toMap(),
        ],
      });

      expect(p.id, equals(ID));
      expect(p.name, equals(NAME));
      expect(p.order, equals(ORDER));
      expect(p.genderRate, equals(GENDER_RATE));
      expect(p.captureRate, equals(CAPTURE_RATE));
      expect(p.baseHappiness, equals(BASE_HAPPINESS));
      expect(p.isBaby, equals(IS_BABY));
      expect(p.isLegendary, equals(IS_LEGENDARY));
      expect(p.isMythical, equals(IS_MYTHICAL));
      expect(p.hatchCounter, equals(HATCH_COUNTER));
      expect(p.hasGenderDifferences, equals(HAS_GENDER_DIFFERENCES));
      expect(p.formsSwitchable, equals(FORMS_SWITCHABLE));
      expect(p.growthRate.name, equals(GROWTH_RATE_NAME));
      expect(p.pokedexNumbers.length, equals(1));
      expect(p.pokedexNumbers.first.entryNumber,
          equals(POKEDEX_NUMBER_ENTRY_NUMBER));
      expect(p.eggGroups.length, equals(1));
      expect(p.eggGroups.first.name, equals(EGG_GROUP_NAME));
      expect(p.color.name, equals(COLOR_NAME));
      expect(p.shape.name, equals(SHAPE_NAME));
      expect(p.evolvesFromSpecies!.name, equals(EVOLVES_FROM_SPECIES_NAME));
      expect(p.evolutionChain.url, equals(EVOLUTION_CHAIN_URL));
      expect(p.habitat.name, equals(HABITAT_NAME));
      expect(p.generation.name, equals(GENERATION_NAME));
      expect(p.names.length, equals(1));
      expect(p.names.first.name, equals(NAMES_NAME));
      expect(p.palParkEncounters.length, equals(1));
      expect(p.palParkEncounters.first.baseScore,
          equals(PAL_PARK_ENCOUNTER_BASE_SCORE));
      expect(p.flavorTextEntries.length, equals(1));
      expect(p.flavorTextEntries.first.flavorText,
          equals(FLAVOR_TEXT_ENTRIES_FLAVOR_TEXT));
      expect(p.formDescriptions.length, equals(1));
      expect(p.formDescriptions.first.description,
          equals(FORM_DESCRIPTIONS_DESCRIPTION));
      expect(p.genera.length, equals(1));
      expect(p.genera.first.genus, equals(GENERA_GENUS));
      expect(p.varieties.length, equals(1));
      expect(p.varieties.first.pokemon.name,
          equals(POKEMON_SPECIES_VARIETY_POKEMON_NAME));
    });

    test('toMap', () {
      final p = PokemonSpecies(
          ID,
          NAME,
          ORDER,
          GENDER_RATE,
          CAPTURE_RATE,
          BASE_HAPPINESS,
          IS_BABY,
          IS_LEGENDARY,
          IS_MYTHICAL,
          HATCH_COUNTER,
          HAS_GENDER_DIFFERENCES,
          FORMS_SWITCHABLE,
          GROWTH_RATE,
          [POKEDEX_NUMBER],
          [EGG_GROUP],
          COLOR,
          SHAPE,
          EVOLVES_FROM_SPECIES,
          EVOLUTION_CHAIN,
          HABITAT,
          GENERATION,
          [NAME_],
          [PAL_PARK_ENCOUNTER],
          [FLAVOR_TEXT_ENTRY],
          [FORM_DESCRIPTION],
          [GENUS],
          [POKEMON_SPECIES_VARIETY]).toMap();

      expect(p[PokemonSpecies.ID_FIELD_NAME], equals(ID));
      expect(p[PokemonSpecies.NAME_FIELD_NAME], equals(NAME));
      expect(p[PokemonSpecies.ORDER_FIELD_NAME], equals(ORDER));
      expect(p[PokemonSpecies.GENDER_RATE_FIELD_NAME], equals(GENDER_RATE));
      expect(p[PokemonSpecies.CAPTURE_RATE_FIELD_NAME], equals(CAPTURE_RATE));
      expect(
          p[PokemonSpecies.BASE_HAPPINESS_FIELD_NAME], equals(BASE_HAPPINESS));
      expect(p[PokemonSpecies.IS_BABY_FIELD_NAME], equals(IS_BABY));
      expect(p[PokemonSpecies.IS_LEGENDARY_FIELD_NAME], equals(IS_LEGENDARY));
      expect(p[PokemonSpecies.IS_MYTHICAL_FIELD_NAME], equals(IS_MYTHICAL));
      expect(p[PokemonSpecies.HATCH_COUNTER_FIELD_NAME], equals(HATCH_COUNTER));
      expect(p[PokemonSpecies.HAS_GENDER_DIFFERENCES_FIELD_NAME],
          equals(HAS_GENDER_DIFFERENCES));
      expect(p[PokemonSpecies.FORMS_SWITCHABLE_FIELD_NAME],
          equals(FORMS_SWITCHABLE));
      expect(p[PokemonSpecies.GROWTH_RATE_FIELD_NAME],
          equals(GROWTH_RATE.toMap()));
      expect(p[PokemonSpecies.POKEDEX_NUMBERS_FIELD_NAME],
          equals([POKEDEX_NUMBER.toMap()]));
      expect(
          p[PokemonSpecies.EGG_GROUPS_FIELD_NAME], equals([EGG_GROUP.toMap()]));
      expect(p[PokemonSpecies.COLOR_FIELD_NAME], equals(COLOR.toMap()));
      expect(p[PokemonSpecies.SHAPE_FIELD_NAME], equals(SHAPE.toMap()));
      expect(p[PokemonSpecies.EVOLVES_FROM_SPECIES_FIELD_NAME],
          equals(EVOLVES_FROM_SPECIES.toMap()));
      expect(p[PokemonSpecies.EVOLUTION_CHAIN_FIELD_NAME],
          equals(EVOLUTION_CHAIN.toMap()));
      expect(p[PokemonSpecies.HABITAT_FIELD_NAME], equals(HABITAT.toMap()));
      expect(
          p[PokemonSpecies.GENERATION_FIELD_NAME], equals(GENERATION.toMap()));
      expect(p[PokemonSpecies.NAMES_FIELD_NAME], equals([NAME_.toMap()]));
      expect(p[PokemonSpecies.PAL_PARK_ENCOUNTERS_FIELD_NAME],
          equals([PAL_PARK_ENCOUNTER.toMap()]));
      expect(p[PokemonSpecies.FLAVOR_TEXT_ENTRIES_FIELD_NAME],
          equals([FLAVOR_TEXT_ENTRY.toMap()]));
      expect(p[PokemonSpecies.FORM_DESCRIPTIONS_FIELD_NAME],
          equals([FORM_DESCRIPTION.toMap()]));
      expect(p[PokemonSpecies.GENERA_FIELD_NAME], equals([GENUS.toMap()]));
      expect(p[PokemonSpecies.VARIETIES_FIELD_NAME],
          equals([POKEMON_SPECIES_VARIETY.toMap()]));
    });
  });
}
