// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/genus.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('Genus', () {
    const GENUS = 'genus';
    const LANGUAGE_NAME = 'language';
    const LANGUAGE = NamedApiResource(LANGUAGE_NAME, '');

    test('fromMap', () {
      final g = Genus.fromMap({
        Genus.GENUS_FIELD_NAME: GENUS,
        Genus.LANGUAGE_FIELD_NAME: LANGUAGE.toMap(),
      });

      expect(g.genus, equals(GENUS));
      expect(g.language.name, equals(LANGUAGE_NAME));
    });

    test('toMap', () {
      final g = Genus(GENUS, LANGUAGE).toMap();

      expect(g[Genus.GENUS_FIELD_NAME], equals(GENUS));
      expect(g[Genus.LANGUAGE_FIELD_NAME], equals(LANGUAGE.toMap()));
    });

    group('listFrom', () {
      test('null', () {
        final g = Genus.listFrom(null);
        expect(g, isEmpty);
      });

      test('not null', () {
        final g = Genus.listFrom([
          {
            Genus.GENUS_FIELD_NAME: GENUS,
            Genus.LANGUAGE_FIELD_NAME: LANGUAGE.toMap(),
          },
        ]);

        expect(g.first.genus, equals(GENUS));
        expect(g.first.language.name, equals(LANGUAGE_NAME));
      });
    });
  });
}
