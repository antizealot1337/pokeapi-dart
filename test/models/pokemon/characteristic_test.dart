// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/characteristic.dart';
import 'package:test/test.dart';

void main() {
  group('Characteristic', () {
    const ID = 1;
    const GENE_MODULO = 2;
    const POSSIBLE_VALUE = 3;

    test('fromMap', () {
      final c = Characteristic.fromMap({
        Characteristic.ID_FIELD_NAME: ID,
        Characteristic.GENE_MODULO_FIELD_NAME: GENE_MODULO,
        Characteristic.POSSIBLE_VALUES_FIELD_NAME: [POSSIBLE_VALUE],
      });

      expect(c.id, equals(ID));
      expect(c.geneModulo, equals(GENE_MODULO));
      expect(c.possibleValues.length, equals(1));
      expect(c.possibleValues.first, equals(POSSIBLE_VALUE));
    });

    test('toMap', () {
      final c = Characteristic(ID, GENE_MODULO, [POSSIBLE_VALUE]).toMap();

      expect(c[Characteristic.ID_FIELD_NAME], equals(ID));
      expect(c[Characteristic.GENE_MODULO_FIELD_NAME], equals(GENE_MODULO));
      expect(c[Characteristic.POSSIBLE_VALUES_FIELD_NAME],
          equals([POSSIBLE_VALUE]));
    });
  });
}
