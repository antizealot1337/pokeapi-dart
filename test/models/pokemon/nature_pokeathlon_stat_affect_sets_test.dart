// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/nature_pokeathlon_stat_affect.dart';
import 'package:pokeapi/src/models/pokemon/nature_pokeathlon_stat_affect_sets.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('NaturePokeathlonStatAffectSets', () {
    const INCREASE_MAX_CHANGE = 1;
    const INCREASE = NaturePokeathlonStatAffect(
        INCREASE_MAX_CHANGE, NamedApiResource('', ''));
    const DECREASE_MAX_CHANGE = 2;
    const DECREASE = NaturePokeathlonStatAffect(
        DECREASE_MAX_CHANGE, NamedApiResource('', ''));

    test('fromMap', () {
      final n = NaturePokeathlonStatAffectSets.fromMap({
        NaturePokeathlonStatAffectSets.INCREASE_FIELD_NAME: [INCREASE.toMap()],
        NaturePokeathlonStatAffectSets.DECREASE_FIELD_NAME: [DECREASE.toMap()],
      });

      expect(n.increase.length, equals(1));
      expect(n.increase.first.maxChange, equals(INCREASE_MAX_CHANGE));
      expect(n.decrease.length, equals(1));
      expect(n.decrease.first.maxChange, equals(DECREASE_MAX_CHANGE));
    });

    test('toMap', () {
      final n = NaturePokeathlonStatAffectSets([INCREASE], [DECREASE]).toMap();

      expect(n[NaturePokeathlonStatAffectSets.INCREASE_FIELD_NAME],
          equals([INCREASE.toMap()]));
      expect(n[NaturePokeathlonStatAffectSets.DECREASE_FIELD_NAME],
          equals([DECREASE.toMap()]));
    });
  });
}
