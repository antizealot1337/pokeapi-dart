// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/type_pokemon.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('TypePokemon', () {
    const SLOT = 1;
    const POKEMON_NAME = 'pokemon';
    const POKEMON = NamedApiResource(POKEMON_NAME, '');

    test('fromMap', () {
      final t = TypePokemon.fromMap({
        TypePokemon.SLOT_FIELD_NAME: SLOT,
        TypePokemon.POKEMON_FIELD_NAME: POKEMON.toMap(),
      });

      expect(t.slot, equals(SLOT));
      expect(t.pokemon.name, equals(POKEMON_NAME));
    });

    test('toMap', () {
      final t = TypePokemon(SLOT, POKEMON).toMap();

      expect(t[TypePokemon.SLOT_FIELD_NAME], equals(SLOT));
      expect(t[TypePokemon.POKEMON_FIELD_NAME], equals(POKEMON.toMap()));
    });

    group('listFrom', () {
      test('null', () {
        final t = TypePokemon.listFrom(null);
        expect(t, isEmpty);
      });

      test('not null', () {
        final t = TypePokemon.listFrom([
          {
            TypePokemon.SLOT_FIELD_NAME: SLOT,
            TypePokemon.POKEMON_FIELD_NAME: POKEMON.toMap(),
          },
        ]);

        expect(t.first.slot, equals(SLOT));
        expect(t.first.pokemon.name, equals(POKEMON_NAME));
      });
    });
  });
}
