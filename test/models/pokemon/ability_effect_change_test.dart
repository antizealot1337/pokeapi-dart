// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/ability_effect_change.dart';
import 'package:pokeapi/src/models/utility/common/effect.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('AbilityEffectChange', () {
    const EFFECT_ENTRIES_EFFECT = 'effect entries effect';
    const EFFECT_ENTRY =
        Effect(EFFECT_ENTRIES_EFFECT, NamedApiResource('', ''));
    const VERSION_GROUP_NAME = 'version group';
    const VERSION_GROUP = NamedApiResource(VERSION_GROUP_NAME, '');

    test('fromMap', () {
      final a = AbilityEffectChange.fromMap({
        AbilityEffectChange.EFFECT_ENTRIES_FIELD_NAME: [EFFECT_ENTRY.toMap()],
        AbilityEffectChange.VERSION_GROUP_FIELD_NAME: VERSION_GROUP.toMap(),
      });

      expect(a.effectEntries.length, equals(1));
      expect(a.effectEntries.first.effect, equals(EFFECT_ENTRIES_EFFECT));
      expect(a.versionGroup.name, equals(VERSION_GROUP_NAME));
    });

    test('toMap', () {
      final a = AbilityEffectChange([EFFECT_ENTRY], VERSION_GROUP).toMap();

      expect(a[AbilityEffectChange.EFFECT_ENTRIES_FIELD_NAME],
          equals([EFFECT_ENTRY.toMap()]));
      expect(a[AbilityEffectChange.VERSION_GROUP_FIELD_NAME],
          equals(VERSION_GROUP.toMap()));
    });

    group('listFrom', () {
      test('null', () {
        final a = AbilityEffectChange.listFrom(null);
        expect(a, isEmpty);
      });

      test('not null', () {
        final a = AbilityEffectChange.listFrom([
          {
            AbilityEffectChange.EFFECT_ENTRIES_FIELD_NAME: [
              EFFECT_ENTRY.toMap(),
            ],
            AbilityEffectChange.VERSION_GROUP_FIELD_NAME: VERSION_GROUP.toMap(),
          },
        ]);

        expect(a.first.effectEntries.length, equals(1));
        expect(
            a.first.effectEntries.first.effect, equals(EFFECT_ENTRIES_EFFECT));
        expect(a.first.versionGroup.name, equals(VERSION_GROUP_NAME));
      });
    });
  });
}
