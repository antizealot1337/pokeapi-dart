// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/pokemon.dart';
import 'package:pokeapi/src/models/pokemon/pokemon_ability.dart';
import 'package:pokeapi/src/models/pokemon/pokemon_held_item.dart';
import 'package:pokeapi/src/models/pokemon/pokemon_move.dart';
import 'package:pokeapi/src/models/pokemon/pokemon_sprites.dart';
import 'package:pokeapi/src/models/pokemon/pokemon_stat.dart';
import 'package:pokeapi/src/models/pokemon/pokemon_type.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:pokeapi/src/models/utility/common/version_game_index.dart';
import 'package:test/test.dart';

void main() {
  group('Pokemon', () {
    const ID = 1;
    const NAME = 'name';
    const BASE_EXPERIENCE = 2;
    const HEIGHT = 3;
    const IS_DEFAULT = true;
    const ORDER = 4;
    const WEIGHT = 5;
    const ABILITY_SLOT = 6;
    const ABILITY =
        PokemonAbility(false, ABILITY_SLOT, NamedApiResource('', ''));
    const FORMS_NAME = 'forms';
    const FORM = NamedApiResource(FORMS_NAME, '');
    const GAME_INDEX = 7;
    const VERSION_GAME_INDEX =
        VersionGameIndex(GAME_INDEX, NamedApiResource('', ''));
    const HELD_ITEM_NAME = 'held item';
    const HELD_ITEM = PokemonHeldItem(NamedApiResource(HELD_ITEM_NAME, ''), []);
    const LOCATION_AREA_ENCOUNTERS = 'location area encounter';
    const MOVE_NAME = 'move';
    const MOVE = PokemonMove(NamedApiResource(MOVE_NAME, ''), []);
    const SPRITES_FRONT_DEFAULT = 'sprites front default';
    const SPRITES =
        PokemonSprites(SPRITES_FRONT_DEFAULT, '', '', '', '', '', '', '');
    const SPECIES_NAME = 'species';
    const SPECIES = NamedApiResource(SPECIES_NAME, '');
    const STAT_NAME = 'stat';
    const STAT = PokemonStat(NamedApiResource(STAT_NAME, ''), 0, 0);
    const TYPE_NAME = 'type';
    const TYPE = PokemonType(0, NamedApiResource(TYPE_NAME, ''));

    test('fromMap', () {
      final p = Pokemon.fromMap({
        Pokemon.ID_FIELD_NAME: ID,
        Pokemon.NAME_FIELD_NAME: NAME,
        Pokemon.BASE_EXPERIENCE_FIELD_NAME: BASE_EXPERIENCE,
        Pokemon.HEIGHT_FIELD_NAME: HEIGHT,
        Pokemon.IS_DEFAULT_FIELD_NAME: IS_DEFAULT,
        Pokemon.ORDER_FIELD_NAME: ORDER,
        Pokemon.WEIGHT_FIELD_NAME: WEIGHT,
        Pokemon.ABILITIES_FIELD_NAME: [ABILITY.toMap()],
        Pokemon.FORMS_FIELD_NAME: [FORM.toMap()],
        Pokemon.GAME_INDICIES_FIELD_NAME: [VERSION_GAME_INDEX.toMap()],
        Pokemon.HELD_ITEM_FIELD_NAME: [HELD_ITEM.toMap()],
        Pokemon.LOCATION_AREA_ENCOUNTERS_FIELD_NAME: LOCATION_AREA_ENCOUNTERS,
        Pokemon.MOVES_FIELD_NAME: [MOVE.toMap()],
        Pokemon.SPRITES_FIELD_NAME: SPRITES.toMap(),
        Pokemon.SPECIES_FIELD_NAME: SPECIES.toMap(),
        Pokemon.STATS_FIELD_NAME: [STAT.toMap()],
        Pokemon.TYPES_FIELD_NAME: [TYPE.toMap()],
      });

      expect(p.id, equals(ID));
      expect(p.name, equals(NAME));
      expect(p.baseExperience, equals(BASE_EXPERIENCE));
      expect(p.height, equals(HEIGHT));
      expect(p.isDefault, equals(IS_DEFAULT));
      expect(p.order, equals(ORDER));
      expect(p.weight, equals(WEIGHT));
      expect(p.abilities.length, equals(1));
      expect(p.abilities.first.slot, equals(ABILITY_SLOT));
      expect(p.forms.length, equals(1));
      expect(p.forms.first.name, equals(FORMS_NAME));
      expect(p.gameIndicies.length, equals(1));
      expect(p.gameIndicies.first.gameIndex, equals(GAME_INDEX));
      expect(p.heldItems.length, equals(1));
      expect(p.heldItems.first.item.name, equals(HELD_ITEM_NAME));
      expect(p.locationAreaEncounters, equals(LOCATION_AREA_ENCOUNTERS));
      expect(p.moves.length, equals(1));
      expect(p.moves.first.move.name, equals(MOVE_NAME));
      expect(p.sprites.frontDefault, equals(SPRITES_FRONT_DEFAULT));
      expect(p.species.name, equals(SPECIES_NAME));
      expect(p.stats.length, equals(1));
      expect(p.stats.first.stat.name, equals(STAT_NAME));
      expect(p.types.length, equals(1));
      expect(p.types.first.type.name, equals(TYPE_NAME));
    });

    test('toMap', () {
      final p = Pokemon(
          ID,
          NAME,
          BASE_EXPERIENCE,
          HEIGHT,
          IS_DEFAULT,
          ORDER,
          WEIGHT,
          [ABILITY],
          [FORM],
          [VERSION_GAME_INDEX],
          [HELD_ITEM],
          LOCATION_AREA_ENCOUNTERS,
          [MOVE],
          SPRITES,
          SPECIES,
          [STAT],
          [TYPE]).toMap();

      expect(p[Pokemon.ID_FIELD_NAME], equals(ID));
      expect(p[Pokemon.NAME_FIELD_NAME], equals(NAME));
      expect(p[Pokemon.BASE_EXPERIENCE_FIELD_NAME], equals(BASE_EXPERIENCE));
      expect(p[Pokemon.HEIGHT_FIELD_NAME], equals(HEIGHT));
      expect(p[Pokemon.IS_DEFAULT_FIELD_NAME], equals(IS_DEFAULT));
      expect(p[Pokemon.ORDER_FIELD_NAME], equals(ORDER));
      expect(p[Pokemon.WEIGHT_FIELD_NAME], equals(WEIGHT));
      expect(p[Pokemon.ABILITIES_FIELD_NAME], equals([ABILITY.toMap()]));
      expect(p[Pokemon.FORMS_FIELD_NAME], equals([FORM.toMap()]));
      expect(p[Pokemon.GAME_INDICIES_FIELD_NAME],
          equals([VERSION_GAME_INDEX.toMap()]));
      expect(p[Pokemon.HELD_ITEM_FIELD_NAME], equals([HELD_ITEM.toMap()]));
      expect(p[Pokemon.LOCATION_AREA_ENCOUNTERS_FIELD_NAME],
          equals(LOCATION_AREA_ENCOUNTERS));
      expect(p[Pokemon.MOVES_FIELD_NAME], equals([MOVE.toMap()]));
      expect(p[Pokemon.SPRITES_FIELD_NAME], equals(SPRITES.toMap()));
      expect(p[Pokemon.SPECIES_FIELD_NAME], equals(SPECIES.toMap()));
      expect(p[Pokemon.STATS_FIELD_NAME], equals([STAT.toMap()]));
      expect(p[Pokemon.TYPES_FIELD_NAME], equals([TYPE.toMap()]));
    });
  });
}
