// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/pokemon_habitat.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('PokemonHabitat', () {
    const ID = 1;
    const NAME = 'name';
    const NAMES_NAME = 'names';
    const NAME_ = Name(NAMES_NAME, NamedApiResource('', ''));
    const POKEMON_SPECIES_NAME = 'pokemon species name';
    const POKEMON_SPECIES = NamedApiResource(POKEMON_SPECIES_NAME, '');

    test('fromMap', () {
      final p = PokemonHabitat.fromMap({
        PokemonHabitat.ID_FIELD_NAME: ID,
        PokemonHabitat.NAME_FIELD_NAME: NAME,
        PokemonHabitat.NAMES_FIELD_NAME: [NAME_.toMap()],
        PokemonHabitat.POKEMON_SPECIES_FIELD_NAME: [POKEMON_SPECIES.toMap()],
      });

      expect(p.id, equals(ID));
      expect(p.name, equals(NAME));
      expect(p.names.length, equals(1));
      expect(p.names.first.name, equals(NAMES_NAME));
      expect(p.pokemonSpecies.length, equals(1));
      expect(p.pokemonSpecies.first.name, equals(POKEMON_SPECIES_NAME));
    });

    test('toMap', () {
      final p = PokemonHabitat(ID, NAME, [NAME_], [POKEMON_SPECIES]).toMap();

      expect(p[PokemonHabitat.ID_FIELD_NAME], equals(ID));
      expect(p[PokemonHabitat.NAME_FIELD_NAME], equals(NAME));
      expect(p[PokemonHabitat.NAMES_FIELD_NAME], equals([NAME_.toMap()]));
      expect(p[PokemonHabitat.POKEMON_SPECIES_FIELD_NAME],
          equals([POKEMON_SPECIES.toMap()]));
    });
  });
}
