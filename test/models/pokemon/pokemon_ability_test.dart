// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/pokemon_ability.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('PokemonAbility', () {
    const IS_HIDDEN = true;
    const SLOT = 1;
    const ABILITY_NAME = 'ability';
    const ABILITY = NamedApiResource(ABILITY_NAME, '');

    test('fromMap', () {
      final p = PokemonAbility.fromMap({
        PokemonAbility.IS_HIDDEN_FIELD_NAME: IS_HIDDEN,
        PokemonAbility.SLOT_FIELD_NAME: SLOT,
        PokemonAbility.ABILITY_FIELD_NAME: ABILITY.toMap(),
      });

      expect(p.isHidden, equals(IS_HIDDEN));
      expect(p.slot, equals(SLOT));
      expect(p.ability.name, equals(ABILITY_NAME));
    });

    test('toMap', () {
      final p = PokemonAbility(IS_HIDDEN, SLOT, ABILITY).toMap();

      expect(p[PokemonAbility.IS_HIDDEN_FIELD_NAME], equals(IS_HIDDEN));
      expect(p[PokemonAbility.SLOT_FIELD_NAME], equals(SLOT));
      expect(p[PokemonAbility.ABILITY_FIELD_NAME], equals(ABILITY.toMap()));
    });

    group('listFrom', () {
      test('null', () {
        final p = PokemonAbility.listFrom(null);
        expect(p, isEmpty);
      });

      test('not null', () {
        final p = PokemonAbility.listFrom([
          {
            PokemonAbility.IS_HIDDEN_FIELD_NAME: IS_HIDDEN,
            PokemonAbility.SLOT_FIELD_NAME: SLOT,
            PokemonAbility.ABILITY_FIELD_NAME: ABILITY.toMap(),
          },
        ]);

        expect(p.first.isHidden, equals(IS_HIDDEN));
        expect(p.first.slot, equals(SLOT));
        expect(p.first.ability.name, equals(ABILITY_NAME));
      });
    });
  });
}
