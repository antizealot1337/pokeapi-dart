// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/move_stat_affect.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('MoveStatAffect', () {
    const CHANGE = 1;
    const MOVE_NAME = 'move';
    const MOVE = NamedApiResource(MOVE_NAME, '');

    test('fromMap', () {
      final m = MoveStatAffect.fromMap({
        MoveStatAffect.CHANGE_FIELD_NAME: CHANGE,
        MoveStatAffect.MOVE_FIELD_NAME: MOVE.toMap(),
      });

      expect(m.change, equals(CHANGE));
      expect(m.move.name, equals(MOVE_NAME));
    });

    test('toMap', () {
      final m = MoveStatAffect(CHANGE, MOVE).toMap();

      expect(m[MoveStatAffect.CHANGE_FIELD_NAME], equals(CHANGE));
      expect(m[MoveStatAffect.MOVE_FIELD_NAME], equals(MOVE.toMap()));
    });

    group('listFrom', () {
      test('null', () {
        final m = MoveStatAffect.listFrom(null);
        expect(m, isEmpty);
      });

      test('not null', () {
        final m = MoveStatAffect.listFrom([
          {
            MoveStatAffect.CHANGE_FIELD_NAME: CHANGE,
            MoveStatAffect.MOVE_FIELD_NAME: MOVE.toMap(),
          },
        ]);

        expect(m.first.change, equals(CHANGE));
        expect(m.first.move.name, equals(MOVE_NAME));
      });
    });
  });
}
