// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/gender.dart';
import 'package:pokeapi/src/models/pokemon/pokemon_species_gender.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('Gender', () {
    const ID = 1;
    const NAME = 'name';
    const POKEMON_SPECIES_DETAIL_RATE = 2;
    const POKEMON_SPECIES_DETAIL = PokemonSpeciesGender(
        POKEMON_SPECIES_DETAIL_RATE, NamedApiResource('', ''));
    const REQUIRED_FOR_EVOLUTION_NAME = 'required for evolution';
    const REQUIRED_FOR_EVOLUTION =
        NamedApiResource(REQUIRED_FOR_EVOLUTION_NAME, '');

    test('fromMap', () {
      final g = Gender.fromMap({
        Gender.ID_FIELD_NAME: ID,
        Gender.NAME_FIELD_NAME: NAME,
        Gender.POKEMON_SPECIES_DETAILS_FIELD_NAME: [
          POKEMON_SPECIES_DETAIL.toMap(),
        ],
        Gender.REQUIRED_FOR_EVOLUTION_FIELD_NAME: [
          REQUIRED_FOR_EVOLUTION.toMap(),
        ],
      });

      expect(g.id, equals(ID));
      expect(g.name, equals(NAME));
      expect(g.pokemonSpeciesDetails.length, equals(1));
      expect(g.pokemonSpeciesDetails.first.rate,
          equals(POKEMON_SPECIES_DETAIL_RATE));
      expect(g.requiredForEvolution.length, equals(1));
      expect(g.requiredForEvolution.first.name,
          equals(REQUIRED_FOR_EVOLUTION_NAME));
    });

    test('toMap', () {
      final g =
          Gender(ID, NAME, [POKEMON_SPECIES_DETAIL], [REQUIRED_FOR_EVOLUTION])
              .toMap();

      expect(g[Gender.ID_FIELD_NAME], equals(ID));
      expect(g[Gender.NAME_FIELD_NAME], equals(NAME));
      expect(g[Gender.POKEMON_SPECIES_DETAILS_FIELD_NAME],
          equals([POKEMON_SPECIES_DETAIL.toMap()]));
      expect(g[Gender.REQUIRED_FOR_EVOLUTION_FIELD_NAME],
          equals([REQUIRED_FOR_EVOLUTION.toMap()]));
    });
  });
}
