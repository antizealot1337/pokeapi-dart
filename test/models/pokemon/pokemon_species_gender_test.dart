// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/pokemon_species_gender.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('PokemonSpeciesGender', () {
    const RATE = 1;
    const POKEMON_SPECIES_NAME = 'pokemon species';
    const POKEMON_SPECIES = NamedApiResource(POKEMON_SPECIES_NAME, '');

    test('fromMap', () {
      final p = PokemonSpeciesGender.fromMap({
        PokemonSpeciesGender.RATE_FIELD_NAME: RATE,
        PokemonSpeciesGender.POKEMON_SPECIES_FIELD_NAME:
            POKEMON_SPECIES.toMap(),
      });

      expect(p.rate, equals(RATE));
      expect(p.pokemonSpecies.name, equals(POKEMON_SPECIES_NAME));
    });

    test('toMap', () {
      final p = PokemonSpeciesGender(RATE, POKEMON_SPECIES).toMap();

      expect(p[PokemonSpeciesGender.RATE_FIELD_NAME], equals(RATE));
      expect(p[PokemonSpeciesGender.POKEMON_SPECIES_FIELD_NAME],
          equals(POKEMON_SPECIES.toMap()));
    });

    group('listFrom', () {
      test('null', () {
        final p = PokemonSpeciesGender.listFrom(null);
        expect(p, isEmpty);
      });

      test('not null', () {
        final p = PokemonSpeciesGender.listFrom([
          {
            PokemonSpeciesGender.RATE_FIELD_NAME: RATE,
            PokemonSpeciesGender.POKEMON_SPECIES_FIELD_NAME:
                POKEMON_SPECIES.toMap(),
          },
        ]);

        expect(p.first.rate, equals(RATE));
        expect(p.first.pokemonSpecies.name, equals(POKEMON_SPECIES_NAME));
      });
    });
  });
}
