// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/type_relations.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('TypeRelations', () {
    const NO_DAMAGE_TO_NAME = 'no damage to';
    const NO_DAMAGE_TO = NamedApiResource(NO_DAMAGE_TO_NAME, '');
    const HALF_DAMAGE_TO_NAME = 'half damage to';
    const HALF_DAMAGE_TO = NamedApiResource(HALF_DAMAGE_TO_NAME, '');
    const DOUBLE_DAMAGE_TO_NAME = 'double damage to';
    const DOUBLE_DAMAGE_TO = NamedApiResource(DOUBLE_DAMAGE_TO_NAME, '');
    const NO_DAMAGE_FROM_NAME = 'no damage from';
    const NO_DAMAGE_FROM = NamedApiResource(NO_DAMAGE_FROM_NAME, '');
    const HALF_DAMAGE_FROM_NAME = 'half damage from';
    const HALF_DAMAGE_FROM = NamedApiResource(HALF_DAMAGE_FROM_NAME, '');
    const DOUBLE_DAMAGE_FROM_NAME = 'double damage from';
    const DOUBLE_DAMAGE_FROM = NamedApiResource(DOUBLE_DAMAGE_FROM_NAME, '');

    test('fromMap', () {
      final t = TypeRelations.fromMap({
        TypeRelations.NO_DAMAGE_TO_FIELD_NAME: [NO_DAMAGE_TO.toMap()],
        TypeRelations.HALF_DAMAGE_TO_FIELD_NAME: [HALF_DAMAGE_TO.toMap()],
        TypeRelations.DOUBLE_DAMAGE_TO_FIELD_NAME: [DOUBLE_DAMAGE_TO.toMap()],
        TypeRelations.NO_DAMAGE_FROM_FIELD_NAME: [NO_DAMAGE_FROM.toMap()],
        TypeRelations.HALF_DAMAGE_FROM_FIELD_NAME: [HALF_DAMAGE_FROM.toMap()],
        TypeRelations.DOUBLE_DAMAGE_FROM_FIELD_NAME: [
          DOUBLE_DAMAGE_FROM.toMap(),
        ],
      });

      expect(t.noDamageTo.length, equals(1));
      expect(t.noDamageTo.first.name, equals(NO_DAMAGE_TO_NAME));
      expect(t.halfDamageTo.length, equals(1));
      expect(t.halfDamageTo.first.name, equals(HALF_DAMAGE_TO_NAME));
      expect(t.doubleDamageTo.length, equals(1));
      expect(t.doubleDamageTo.first.name, equals(DOUBLE_DAMAGE_TO_NAME));
      expect(t.noDamageFrom.length, equals(1));
      expect(t.noDamageFrom.first.name, equals(NO_DAMAGE_FROM_NAME));
      expect(t.halfDamageFrom.length, equals(1));
      expect(t.halfDamageFrom.first.name, equals(HALF_DAMAGE_FROM_NAME));
      expect(t.doubleDamageFrom.first.name, equals(DOUBLE_DAMAGE_FROM_NAME));
    });

    test('toMap', () {
      final t = TypeRelations(
          [NO_DAMAGE_TO],
          [HALF_DAMAGE_TO],
          [DOUBLE_DAMAGE_TO],
          [NO_DAMAGE_FROM],
          [HALF_DAMAGE_FROM],
          [DOUBLE_DAMAGE_FROM]).toMap();

      expect(t[TypeRelations.NO_DAMAGE_TO_FIELD_NAME],
          equals([NO_DAMAGE_TO.toMap()]));
      expect(t[TypeRelations.HALF_DAMAGE_TO_FIELD_NAME],
          equals([HALF_DAMAGE_TO.toMap()]));
      expect(t[TypeRelations.DOUBLE_DAMAGE_TO_FIELD_NAME],
          equals([DOUBLE_DAMAGE_TO.toMap()]));
      expect(t[TypeRelations.NO_DAMAGE_FROM_FIELD_NAME],
          equals([NO_DAMAGE_FROM.toMap()]));
      expect(t[TypeRelations.HALF_DAMAGE_FROM_FIELD_NAME],
          equals([HALF_DAMAGE_FROM.toMap()]));
      expect(t[TypeRelations.DOUBLE_DAMAGE_FROM_FIELD_NAME],
          equals([DOUBLE_DAMAGE_FROM.toMap()]));
    });
  });
}
