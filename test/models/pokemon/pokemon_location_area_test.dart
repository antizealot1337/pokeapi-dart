// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/pokemon_location_area.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:pokeapi/src/models/utility/common/version_encounter_detail.dart';
import 'package:test/test.dart';

void main() {
  group('PokemonLocationArea', () {
    const LOCATION_AREA_NAME = 'location area';
    const LOCATION_AREA = NamedApiResource(LOCATION_AREA_NAME, '');
    const VERSION_DETAILS_VERSION_NAME = 'version details version name';
    const VERSION_DETAIL = VersionEncounterDetail(
        NamedApiResource(VERSION_DETAILS_VERSION_NAME, ''), 0, []);

    test('fromMap', () {
      final p = PokemonLocationArea.fromMap({
        PokemonLocationArea.LOCATION_AREA_FIELD_NAME: LOCATION_AREA.toMap(),
        PokemonLocationArea.VERSION_DETAILS_FIELD_NAME: [
          VERSION_DETAIL.toMap(),
        ],
      });

      expect(p.locationArea.name, equals(LOCATION_AREA_NAME));
      expect(p.versionDetails.length, equals(1));
      expect(p.versionDetails.first.version.name,
          equals(VERSION_DETAILS_VERSION_NAME));
    });

    test('toMap', () {
      final p = PokemonLocationArea(LOCATION_AREA, [VERSION_DETAIL]).toMap();

      expect(p[PokemonLocationArea.LOCATION_AREA_FIELD_NAME],
          equals(LOCATION_AREA.toMap()));
      expect(p[PokemonLocationArea.VERSION_DETAILS_FIELD_NAME],
          equals([VERSION_DETAIL.toMap()]));
    });

    group('listFrom', () {
      test('null', () {
        final p = PokemonLocationArea.listFrom(null);
        expect(p, isEmpty);
      });

      test('not null', () {
        final p = PokemonLocationArea.listFrom([
          {
            PokemonLocationArea.LOCATION_AREA_FIELD_NAME: LOCATION_AREA.toMap(),
            PokemonLocationArea.VERSION_DETAILS_FIELD_NAME: [
              VERSION_DETAIL.toMap(),
            ],
          },
        ]);

        expect(p.first.locationArea.name, equals(LOCATION_AREA_NAME));
        expect(p.first.versionDetails.length, equals(1));
        expect(p.first.versionDetails.first.version.name,
            equals(VERSION_DETAILS_VERSION_NAME));
      });
    });
  });
}
