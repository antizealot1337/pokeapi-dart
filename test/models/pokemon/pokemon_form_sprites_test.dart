// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/pokemon_form_sprites.dart';
import 'package:test/test.dart';

void main() {
  group('PokemonFormSprites', () {
    const FRONT_DEFAULT = 'front default';
    const FRONT_SHINY = 'front shiny';
    const BACK_DEFAULT = 'back default';
    const BACK_SHINY = 'back shiny';

    test('fromMap', () {
      final p = PokemonFormSprites.fromMap({
        PokemonFormSprites.FRONT_DEFAULT_FIELD_NAME: FRONT_DEFAULT,
        PokemonFormSprites.FRONT_SHINY_FIELD_NAME: FRONT_SHINY,
        PokemonFormSprites.BACK_DEFAULT_FIELD_NAME: BACK_DEFAULT,
        PokemonFormSprites.BACK_SHINY_FIELD_NAME: BACK_SHINY,
      });

      expect(p.frontDefault, equals(FRONT_DEFAULT));
      expect(p.frontShiny, equals(FRONT_SHINY));
      expect(p.backDefault, equals(BACK_DEFAULT));
      expect(p.backShiny, equals(BACK_SHINY));
    });

    test('toMap', () {
      final p = PokemonFormSprites(
              FRONT_DEFAULT, FRONT_SHINY, BACK_DEFAULT, BACK_SHINY)
          .toMap();

      expect(p[PokemonFormSprites.FRONT_DEFAULT_FIELD_NAME],
          equals(FRONT_DEFAULT));
      expect(p[PokemonFormSprites.FRONT_SHINY_FIELD_NAME], equals(FRONT_SHINY));
      expect(
          p[PokemonFormSprites.BACK_DEFAULT_FIELD_NAME], equals(BACK_DEFAULT));
      expect(p[PokemonFormSprites.BACK_SHINY_FIELD_NAME], equals(BACK_SHINY));
    });
  });
}
