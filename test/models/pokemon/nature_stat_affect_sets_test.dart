// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/nature_stat_affect_sets.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('NatureStatAffectSets', () {
    const INCREASE_NAME = 'increase';
    const INCREASE = NamedApiResource(INCREASE_NAME, '');
    const DECREASE_NAME = 'decrease';
    const DECREASE = NamedApiResource(DECREASE_NAME, '');

    test('fromMap', () {
      final n = NatureStatAffectSets.fromMap({
        NatureStatAffectSets.INCREASE_FIELD_NAME: [INCREASE.toMap()],
        NatureStatAffectSets.DECREASE_FIELD_NAME: [DECREASE.toMap()],
      });

      expect(n.increase.length, equals(1));
      expect(n.increase.first.name, equals(INCREASE_NAME));
      expect(n.decrease.length, equals(1));
      expect(n.decrease.first.name, equals(DECREASE_NAME));
    });

    test('toMap', () {
      final n = NatureStatAffectSets([INCREASE], [DECREASE]).toMap();

      expect(n[NatureStatAffectSets.INCREASE_FIELD_NAME],
          equals([INCREASE.toMap()]));
      expect(n[NatureStatAffectSets.DECREASE_FIELD_NAME],
          equals([DECREASE.toMap()]));
    });
  });
}
