// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/awesome_name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('AwesomeName', () {
    const AWESOME_NAME = 'awesome name';
    const LANGUAGE_NAME = 'language';
    const LANGUAGE = NamedApiResource(LANGUAGE_NAME, '');

    test('fromMap', () {
      final a = AwesomeName.fromMap({
        AwesomeName.AWESOME_NAME_FIELD_NAME: AWESOME_NAME,
        AwesomeName.LANGAUGE_FIELD_NAME: LANGUAGE.toMap(),
      });

      expect(a.awesomeName, equals(AWESOME_NAME));
      expect(a.langauge!.name, equals(LANGUAGE_NAME));
    });

    test('toMap', () {
      final a = AwesomeName(AWESOME_NAME, LANGUAGE).toMap();

      expect(a[AwesomeName.AWESOME_NAME_FIELD_NAME], equals(AWESOME_NAME));
      expect(a[AwesomeName.LANGAUGE_FIELD_NAME], equals(LANGUAGE.toMap()));
    });

    group('listFrom', () {
      test('null', () {
        final a = AwesomeName.listFrom(null);
        expect(a, isEmpty);
      });

      test('not null', () {
        final a = AwesomeName.listFrom([
          {
            AwesomeName.AWESOME_NAME_FIELD_NAME: AWESOME_NAME,
            AwesomeName.LANGAUGE_FIELD_NAME: LANGUAGE.toMap(),
          },
        ]);

        expect(a.first.awesomeName, equals(AWESOME_NAME));
        expect(a.first.langauge!.name, equals(LANGUAGE_NAME));
      });
    });
  });
}
