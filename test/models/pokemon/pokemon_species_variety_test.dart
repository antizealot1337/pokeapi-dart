// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/pokemon_species_variety.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('PokemonSpeciesVariety', () {
    const IS_DEFAULT = true;
    const POKEMON_NAME = 'pokemon';
    const POKEMON = NamedApiResource(POKEMON_NAME, '');

    test('fromMap', () {
      final p = PokemonSpeciesVariety.fromMap({
        PokemonSpeciesVariety.IS_DEFAULT_FIELD_NAME: IS_DEFAULT,
        PokemonSpeciesVariety.POKEMON_FIELD_NAME: POKEMON.toMap(),
      });

      expect(p.isDefault, equals(IS_DEFAULT));
      expect(p.pokemon.name, equals(POKEMON_NAME));
    });

    test('toMap', () {
      final p = PokemonSpeciesVariety(IS_DEFAULT, POKEMON).toMap();

      expect(
          p[PokemonSpeciesVariety.IS_DEFAULT_FIELD_NAME], equals(IS_DEFAULT));
      expect(
          p[PokemonSpeciesVariety.POKEMON_FIELD_NAME], equals(POKEMON.toMap()));
    });

    group('listFrom', () {
      test('null', () {
        final p = PokemonSpeciesVariety.listFrom(null);
        expect(p, isEmpty);
      });

      test('not null', () {
        final p = PokemonSpeciesVariety.listFrom([
          {
            PokemonSpeciesVariety.IS_DEFAULT_FIELD_NAME: IS_DEFAULT,
            PokemonSpeciesVariety.POKEMON_FIELD_NAME: POKEMON.toMap(),
          },
        ]);

        expect(p.first.isDefault, equals(IS_DEFAULT));
        expect(p.first.pokemon.name, equals(POKEMON_NAME));
      });
    });
  });
}
