// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/ability_pokemon.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('AbilityPokemon', () {
    const IS_HIDDEN = true;
    const SLOT = 1;
    const POKEMON_NAME = 'pokemon';
    const POKEMON = NamedApiResource(POKEMON_NAME, '');

    test('fromMap', () {
      final a = AbilityPokemon.fromMap({
        AbilityPokemon.IS_HIDDEN_FIELD_NAME: IS_HIDDEN,
        AbilityPokemon.SLOT_FIELD_NAME: SLOT,
        AbilityPokemon.POKEMON_FIELD_NAME: POKEMON.toMap(),
      });

      expect(a.isHidden, equals(IS_HIDDEN));
      expect(a.slot, equals(SLOT));
      expect(a.pokemon.name, equals(POKEMON_NAME));
    });

    test('toMap', () {
      final a = AbilityPokemon(IS_HIDDEN, SLOT, POKEMON).toMap();

      expect(a[AbilityPokemon.IS_HIDDEN_FIELD_NAME], equals(IS_HIDDEN));
      expect(a[AbilityPokemon.SLOT_FIELD_NAME], equals(SLOT));
      expect(a[AbilityPokemon.POKEMON_FIELD_NAME], equals(POKEMON.toMap()));
    });

    group('listFrom', () {
      test('null', () {
        final a = AbilityPokemon.listFrom(null);
        expect(a, isEmpty);
      });

      test('not null', () {
        final a = AbilityPokemon.listFrom([
          {
            AbilityPokemon.IS_HIDDEN_FIELD_NAME: IS_HIDDEN,
            AbilityPokemon.SLOT_FIELD_NAME: SLOT,
            AbilityPokemon.POKEMON_FIELD_NAME: POKEMON.toMap(),
          },
        ]);

        expect(a.first.isHidden, equals(IS_HIDDEN));
        expect(a.first.slot, equals(SLOT));
        expect(a.first.pokemon.name, equals(POKEMON_NAME));
      });
    });
  });
}
