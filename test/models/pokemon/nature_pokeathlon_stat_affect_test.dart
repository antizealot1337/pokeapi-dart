// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/nature_pokeathlon_stat_affect.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('NaturePokeathlonStatAffect', () {
    const MAX_CHANGE = 1;
    const NATURE_NAME = 'nature';
    const NATURE = NamedApiResource(NATURE_NAME, '');

    test('fromMap', () {
      final n = NaturePokeathlonStatAffect.fromMap({
        NaturePokeathlonStatAffect.MAX_CHANGE_FIELD_NAME: MAX_CHANGE,
        NaturePokeathlonStatAffect.NATURE_FIELD_NAME: NATURE.toMap(),
      });

      expect(n.maxChange, equals(MAX_CHANGE));
      expect(n.nature.name, equals(NATURE_NAME));
    });

    test('toMap', () {
      final n = NaturePokeathlonStatAffect(MAX_CHANGE, NATURE).toMap();

      expect(n[NaturePokeathlonStatAffect.MAX_CHANGE_FIELD_NAME],
          equals(MAX_CHANGE));
      expect(n[NaturePokeathlonStatAffect.NATURE_FIELD_NAME],
          equals(NATURE.toMap()));
    });

    group('listFrom', () {
      test('null', () {
        final n = NaturePokeathlonStatAffect.listFrom(null);
        expect(n, isEmpty);
      });

      test('not null', () {
        final n = NaturePokeathlonStatAffect.listFrom([
          {
            NaturePokeathlonStatAffect.MAX_CHANGE_FIELD_NAME: MAX_CHANGE,
            NaturePokeathlonStatAffect.NATURE_FIELD_NAME: NATURE.toMap(),
          },
        ]);

        expect(n.first.maxChange, equals(MAX_CHANGE));
        expect(n.first.nature.name, equals(NATURE_NAME));
      });
    });
  });
}
