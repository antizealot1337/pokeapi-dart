// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/pokemon_move.dart';
import 'package:pokeapi/src/models/pokemon/pokemon_move_version.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('PokemonMove', () {
    const MOVE_NAME = 'move';
    const MOVE = NamedApiResource(MOVE_NAME, '');
    const VERSION_GROUP_DETAIL_MOVE_LEARN_METHOD_NAME =
        'version group detail move learn method';
    const VERSION_GROUP_DETAIL = PokemonMoveVersion(
        NamedApiResource(VERSION_GROUP_DETAIL_MOVE_LEARN_METHOD_NAME, ''),
        NamedApiResource('', ''),
        0);

    test('fromMap', () {
      final p = PokemonMove.fromMap({
        PokemonMove.MOVE_FIELD_NAME: MOVE.toMap(),
        PokemonMove.VERSION_GROUP_DETAILS_FIELD_NAME: [
          VERSION_GROUP_DETAIL.toMap(),
        ],
      });

      expect(p.move.name, equals(MOVE_NAME));
      expect(p.versionGroupDetails.length, equals(1));
      expect(p.versionGroupDetails.first.moveLearnMethod.name,
          equals(VERSION_GROUP_DETAIL_MOVE_LEARN_METHOD_NAME));
    });

    test('toMap', () {
      final p = PokemonMove(MOVE, [VERSION_GROUP_DETAIL]).toMap();

      expect(p[PokemonMove.MOVE_FIELD_NAME], equals(MOVE.toMap()));
      expect(p[PokemonMove.VERSION_GROUP_DETAILS_FIELD_NAME],
          equals([VERSION_GROUP_DETAIL.toMap()]));
    });

    group('listFrom', () {
      test('null', () {
        final p = PokemonMove.listFrom(null);
        expect(p, isEmpty);
      });

      test('not null', () {
        final p = PokemonMove.listFrom([
          {
            PokemonMove.MOVE_FIELD_NAME: MOVE.toMap(),
            PokemonMove.VERSION_GROUP_DETAILS_FIELD_NAME: [
              VERSION_GROUP_DETAIL.toMap(),
            ],
          },
        ]);

        expect(p.first.move.name, equals(MOVE_NAME));
        expect(p.first.versionGroupDetails.length, equals(1));
        expect(p.first.versionGroupDetails.first.moveLearnMethod.name,
            equals(VERSION_GROUP_DETAIL_MOVE_LEARN_METHOD_NAME));
      });
    });
  });
}
