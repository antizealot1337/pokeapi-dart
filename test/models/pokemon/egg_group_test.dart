// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/egg_group.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('EggGroup', () {
    const ID = 1;
    const NAME = 'name';
    const NAMES_NAME = 'names';
    const NAME_ = Name(NAMES_NAME, NamedApiResource('', ''));
    const POKEMON_SPECIES_NAME = 'pokemon species';
    const POKEMON_SPECIES = NamedApiResource(POKEMON_SPECIES_NAME, '');

    test('fromMap', () {
      final e = EggGroup.fromMap({
        EggGroup.ID_FIELD_NAME: ID,
        EggGroup.NAME_FIELD_NAME: NAME,
        EggGroup.NAMES_FIELD_NAME: [NAME_.toMap()],
        EggGroup.POKEMON_SPECIES_FIELD_NAME: [POKEMON_SPECIES.toMap()],
      });

      expect(e.id, equals(ID));
      expect(e.name, equals(NAME));
      expect(e.names.length, equals(1));
      expect(e.names.first.name, equals(NAMES_NAME));
      expect(e.pokemonSpecies.length, equals(1));
      expect(e.pokemonSpecies.first.name, equals(POKEMON_SPECIES_NAME));
    });

    test('toMap', () {
      final e = EggGroup(ID, NAME, [NAME_], [POKEMON_SPECIES]).toMap();

      expect(e[EggGroup.ID_FIELD_NAME], equals(ID));
      expect(e[EggGroup.NAME_FIELD_NAME], equals(NAME));
      expect(e[EggGroup.NAMES_FIELD_NAME], equals([NAME_.toMap()]));
      expect(e[EggGroup.POKEMON_SPECIES_FIELD_NAME],
          equals([POKEMON_SPECIES.toMap()]));
    });
  });
}
