// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/awesome_name.dart';
import 'package:pokeapi/src/models/pokemon/pokemon_shape.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('PokemonShape', () {
    const ID = 1;
    const NAME = 'name';
    const AWESOME_NAMES_NAME = 'awesome names';
    const AWESOME_NAME =
        AwesomeName(AWESOME_NAMES_NAME, NamedApiResource('', ''));
    const NAMES_NAME = 'names';
    const NAME_ = Name(NAMES_NAME, NamedApiResource('', ''));
    const POKEMON_SPECIES_NAME = 'pokemon species name';
    const POKEMON_SPECIES = NamedApiResource(POKEMON_SPECIES_NAME, '');

    test('fromMap', () {
      final p = PokemonShape.fromMap({
        PokemonShape.ID_FIELD_NAME: ID,
        PokemonShape.NAME_FIELD_NAME: NAME,
        PokemonShape.AWESOME_NAMES_FIELD_NAME: [AWESOME_NAME.toMap()],
        PokemonShape.NAMES_FIELD_NAME: [NAME_.toMap()],
        PokemonShape.POKEMON_SPECIES_FIELD_NAME: [POKEMON_SPECIES.toMap()],
      });

      expect(p.id, equals(ID));
      expect(p.name, equals(NAME));
      expect(p.awesomeNames.length, equals(1));
      expect(p.awesomeNames.first.awesomeName, equals(AWESOME_NAMES_NAME));
      expect(p.names.length, equals(1));
      expect(p.names.first.name, equals(NAMES_NAME));
      expect(p.pokemonSpecies.length, equals(1));
      expect(p.pokemonSpecies.first.name, equals(POKEMON_SPECIES_NAME));
    });

    test('toMap', () {
      final p =
          PokemonShape(ID, NAME, [AWESOME_NAME], [NAME_], [POKEMON_SPECIES])
              .toMap();

      expect(p[PokemonShape.ID_FIELD_NAME], equals(ID));
      expect(p[PokemonShape.NAME_FIELD_NAME], equals(NAME));
      expect(p[PokemonShape.AWESOME_NAMES_FIELD_NAME],
          equals([AWESOME_NAME.toMap()]));
      expect(p[PokemonShape.NAMES_FIELD_NAME], equals([NAME_.toMap()]));
      expect(p[PokemonShape.POKEMON_SPECIES_FIELD_NAME],
          equals([POKEMON_SPECIES.toMap()]));
    });
  });
}
