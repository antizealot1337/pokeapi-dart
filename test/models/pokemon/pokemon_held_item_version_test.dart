// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/pokemon_held_item_version.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('PokemonHeldItemVersion', () {
    const VERSION_NAME = 'version';
    const VERSION = NamedApiResource(VERSION_NAME, '');
    const RARITY = 1;

    test('fromMap', () {
      final p = PokemonHeldItemVersion.fromMap({
        PokemonHeldItemVersion.VERSION_FIELD_NAME: VERSION.toMap(),
        PokemonHeldItemVersion.RARITY_FIELD_NAME: RARITY,
      });

      expect(p.version.name, equals(VERSION_NAME));
      expect(p.rarity, equals(RARITY));
    });

    test('toMap', () {
      final p = PokemonHeldItemVersion(VERSION, RARITY).toMap();

      expect(p[PokemonHeldItemVersion.VERSION_FIELD_NAME],
          equals(VERSION.toMap()));
      expect(p[PokemonHeldItemVersion.RARITY_FIELD_NAME], equals(RARITY));
    });

    group('listFrom', () {
      test('null', () {
        final p = PokemonHeldItemVersion.listFrom(null);
        expect(p, isEmpty);
      });

      test('not null', () {
        final p = PokemonHeldItemVersion.listFrom([
          {
            PokemonHeldItemVersion.VERSION_FIELD_NAME: VERSION.toMap(),
            PokemonHeldItemVersion.RARITY_FIELD_NAME: RARITY,
          },
        ]);

        expect(p.first.version.name, equals(VERSION_NAME));
        expect(p.first.rarity, equals(RARITY));
      });
    });
  });
}
