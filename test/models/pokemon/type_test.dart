// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/type.dart';
import 'package:pokeapi/src/models/pokemon/type_pokemon.dart';
import 'package:pokeapi/src/models/pokemon/type_relations.dart';
import 'package:pokeapi/src/models/utility/common/generation_game_index.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('Type', () {
    const ID = 1;
    const NAME = 'name';
    const DAMAGE_RELATIONS_NO_DAMAGE_TO_NAME = 'damage relations no damage to';
    const DAMAGE_RELATIONS = TypeRelations(
        [NamedApiResource(DAMAGE_RELATIONS_NO_DAMAGE_TO_NAME, '')],
        [],
        [],
        [],
        [],
        []);
    const GAME_INDICIES_GAME_INDEX = 2;
    const GAME_INDEX =
        GenerationGameIndex(GAME_INDICIES_GAME_INDEX, NamedApiResource('', ''));
    const GENERATION_NAME = 'generation';
    const GENERATION = NamedApiResource(GENERATION_NAME, '');
    const MOVE_DAMAGE_CLASS_NAME = 'move damage class';
    const MOVE_DAMAGE_CLASS = NamedApiResource(MOVE_DAMAGE_CLASS_NAME, '');
    const NAMES_NAME = 'names';
    const NAME_ = Name(NAMES_NAME, NamedApiResource('', ''));
    const POKEMON_SLOT = 3;
    const POKEMON = TypePokemon(POKEMON_SLOT, NamedApiResource('', ''));
    const MOVES_NAME = 'moves';
    const MOVE = NamedApiResource(MOVES_NAME, '');

    test('fromMap', () {
      final t = Type.fromMap({
        Type.ID_FIELD_NAME: ID,
        Type.NAME_FIELD_NAME: NAME,
        Type.DAMAGE_RELATIONS_FIELD_NAME: DAMAGE_RELATIONS.toMap(),
        Type.GAME_INDICIES_FIELD_NAME: [GAME_INDEX.toMap()],
        Type.GENERATION_FIELD_NAME: GENERATION.toMap(),
        Type.MOVE_DAMAGE_CLASS_FIELD_NAME: MOVE_DAMAGE_CLASS.toMap(),
        Type.NAMES_FIELD_NAME: [NAME_.toMap()],
        Type.POKEMON_FIELD_NAME: [POKEMON.toMap()],
        Type.MOVES_FIELD_NAME: [MOVE.toMap()],
      });

      expect(t.id, equals(ID));
      expect(t.name, equals(NAME));
      expect(t.damageRelations.noDamageTo.first.name,
          equals(DAMAGE_RELATIONS_NO_DAMAGE_TO_NAME));
      expect(t.gameIndicies.length, equals(1));
      expect(t.gameIndicies.first.gameIndex, equals(GAME_INDICIES_GAME_INDEX));
      expect(t.generation.name, equals(GENERATION_NAME));
      expect(t.moveDamageClass.name, equals(MOVE_DAMAGE_CLASS_NAME));
      expect(t.names.length, equals(1));
      expect(t.names.first.name, equals(NAMES_NAME));
      expect(t.pokemon.length, equals(1));
      expect(t.pokemon.first.slot, equals(POKEMON_SLOT));
      expect(t.moves.length, equals(1));
      expect(t.moves.first.name, equals(MOVES_NAME));
    });

    test('toMap', () {
      final t = Type(ID, NAME, DAMAGE_RELATIONS, [GAME_INDEX], GENERATION,
          MOVE_DAMAGE_CLASS, [NAME_], [POKEMON], [MOVE]).toMap();

      expect(t[Type.ID_FIELD_NAME], equals(ID));
      expect(t[Type.NAME_FIELD_NAME], equals(NAME));
      expect(t[Type.DAMAGE_RELATIONS_FIELD_NAME],
          equals(DAMAGE_RELATIONS.toMap()));
      expect(t[Type.GAME_INDICIES_FIELD_NAME], equals([GAME_INDEX.toMap()]));
      expect(t[Type.GENERATION_FIELD_NAME], equals(GENERATION.toMap()));
      expect(t[Type.MOVE_DAMAGE_CLASS_FIELD_NAME],
          equals(MOVE_DAMAGE_CLASS.toMap()));
      expect(t[Type.NAMES_FIELD_NAME], equals([NAME_.toMap()]));
      expect(t[Type.POKEMON_FIELD_NAME], equals([POKEMON.toMap()]));
      expect(t[Type.MOVES_FIELD_NAME], equals([MOVE.toMap()]));
    });
  });
}
