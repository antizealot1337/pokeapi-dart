// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/nature_stat_change.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('NatureStatChange', () {
    const MAX_CHANGE = 1;
    const POKEATHLON_STAT_NAME = 'pokeathlon stat';
    const POKEATHLON = NamedApiResource(POKEATHLON_STAT_NAME, '');

    test('fromMap', () {
      final n = NatureStatChange.fromMap({
        NatureStatChange.MAX_CHANGE_FIELD_NAME: MAX_CHANGE,
        NatureStatChange.POKEATHLON_STAT_FIELD_NAME: POKEATHLON.toMap(),
      });

      expect(n.maxChange, equals(MAX_CHANGE));
      expect(n.pokeathlonStat.name, equals(POKEATHLON_STAT_NAME));
    });

    test('toMap', () {
      final n = NatureStatChange(MAX_CHANGE, POKEATHLON).toMap();

      expect(n[NatureStatChange.MAX_CHANGE_FIELD_NAME], equals(MAX_CHANGE));
      expect(n[NatureStatChange.POKEATHLON_STAT_FIELD_NAME],
          equals(POKEATHLON.toMap()));
    });

    group('listFrom', () {
      test('null', () {
        final n = NatureStatChange.listFrom(null);
        expect(n, isEmpty);
      });

      test('not null', () {
        final n = NatureStatChange.listFrom([
          {
            NatureStatChange.MAX_CHANGE_FIELD_NAME: MAX_CHANGE,
            NatureStatChange.POKEATHLON_STAT_FIELD_NAME: POKEATHLON.toMap(),
          },
        ]);

        expect(n.first.maxChange, equals(MAX_CHANGE));
        expect(n.first.pokeathlonStat.name, equals(POKEATHLON_STAT_NAME));
      });
    });
  });
}
