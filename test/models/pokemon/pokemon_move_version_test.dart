// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/pokemon_move_version.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('PokemonMoveVersion', () {
    const MOVE_LEARN_METHOD_NAME = 'move learn method';
    const MOVE_LEARN_METHOD = NamedApiResource(MOVE_LEARN_METHOD_NAME, '');
    const VERSION_GROUP_NAME = 'version group';
    const VERSION_GROUP = NamedApiResource(VERSION_GROUP_NAME, '');
    const LEVEL_LEARNED_AT = 1;

    test('fromMap', () {
      final p = PokemonMoveVersion.fromMap({
        PokemonMoveVersion.MOVE_LEARN_METHOD_FIELD_NAME:
            MOVE_LEARN_METHOD.toMap(),
        PokemonMoveVersion.VERSION_GROUP_FIELD_NAME: VERSION_GROUP.toMap(),
        PokemonMoveVersion.LEVEL_LEARNED_AT_FIELD_NAME: LEVEL_LEARNED_AT,
      });

      expect(p.moveLearnMethod.name, equals(MOVE_LEARN_METHOD_NAME));
      expect(p.versionGroup.name, equals(VERSION_GROUP_NAME));
      expect(p.levelLearnedAt, equals(LEVEL_LEARNED_AT));
    });

    test('toMap', () {
      final p =
          PokemonMoveVersion(MOVE_LEARN_METHOD, VERSION_GROUP, LEVEL_LEARNED_AT)
              .toMap();

      expect(p[PokemonMoveVersion.MOVE_LEARN_METHOD_FIELD_NAME],
          equals(MOVE_LEARN_METHOD.toMap()));
      expect(p[PokemonMoveVersion.VERSION_GROUP_FIELD_NAME],
          equals(VERSION_GROUP.toMap()));
      expect(p[PokemonMoveVersion.LEVEL_LEARNED_AT_FIELD_NAME],
          equals(LEVEL_LEARNED_AT));
    });

    group('listFrom', () {
      test('null', () {
        final p = PokemonMoveVersion.listFrom(null);
        expect(p, isEmpty);
      });

      test('not null', () {
        final p = PokemonMoveVersion.listFrom([
          {
            PokemonMoveVersion.MOVE_LEARN_METHOD_FIELD_NAME:
                MOVE_LEARN_METHOD.toMap(),
            PokemonMoveVersion.VERSION_GROUP_FIELD_NAME: VERSION_GROUP.toMap(),
            PokemonMoveVersion.LEVEL_LEARNED_AT_FIELD_NAME: LEVEL_LEARNED_AT,
          },
        ]);

        expect(p.first.moveLearnMethod.name, equals(MOVE_LEARN_METHOD_NAME));
        expect(p.first.versionGroup.name, equals(VERSION_GROUP_NAME));
        expect(p.first.levelLearnedAt, equals(LEVEL_LEARNED_AT));
      });
    });
  });
}
