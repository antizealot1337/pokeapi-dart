// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/nature_pokeathlon_stat_affect.dart';
import 'package:pokeapi/src/models/pokemon/nature_pokeathlon_stat_affect_sets.dart';
import 'package:pokeapi/src/models/pokemon/pokeatlon_stat.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('PokeathlonStat', () {
    const ID = 1;
    const NAME = 'name';
    const NAMES_NAME = 'names';
    const AFFECTING_NATURES_INCREASE_MAX_CHANGE = 2;
    const NAME_ = Name(NAMES_NAME, NamedApiResource('', ''));
    const AFFECTING_NATURES = NaturePokeathlonStatAffectSets([
      NaturePokeathlonStatAffect(
        AFFECTING_NATURES_INCREASE_MAX_CHANGE,
        NamedApiResource('', ''),
      ),
    ], []);

    test('fromMap', () {
      final p = PokeathlonStat.fromMap({
        PokeathlonStat.ID_FIELD_NAME: ID,
        PokeathlonStat.NAME_FIELD_NAME: NAME,
        PokeathlonStat.NAMES_FIELD_NAME: [NAME_.toMap()],
        PokeathlonStat.AFFECTING_NATURES_FIELD_NAME: AFFECTING_NATURES.toMap(),
      });

      expect(p.id, equals(ID));
      expect(p.name, equals(NAME));
      expect(p.names.length, equals(1));
      expect(p.names.first.name, equals(NAMES_NAME));
      expect(p.affectingNatures.increase.length, equals(1));
      expect(p.affectingNatures.increase.first.maxChange,
          equals(AFFECTING_NATURES_INCREASE_MAX_CHANGE));
    });

    test('toMap', () {
      final p = PokeathlonStat(ID, NAME, [NAME_], AFFECTING_NATURES).toMap();

      expect(p[PokeathlonStat.ID_FIELD_NAME], equals(ID));
      expect(p[PokeathlonStat.NAME_FIELD_NAME], equals(NAME));
      expect(p[PokeathlonStat.NAMES_FIELD_NAME], equals([NAME_.toMap()]));
      expect(p[PokeathlonStat.AFFECTING_NATURES_FIELD_NAME],
          equals(AFFECTING_NATURES.toMap()));
    });
  });
}
