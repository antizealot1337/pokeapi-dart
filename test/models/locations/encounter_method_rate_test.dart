// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/locations/encounter_method_rate.dart';
import 'package:pokeapi/src/models/locations/encounter_version_detail.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('EncounterMethodRate', () {
    const ENCOUNTER_METHOD_NAME = 'name';
    const ENCOUNTER_METHOD = NamedApiResource(ENCOUNTER_METHOD_NAME, '');
    const ENCOUNTER_VERSION_RATE = 1;
    const ENCOUNTER_VERSION = EncounterVersionDetail(
        ENCOUNTER_VERSION_RATE, NamedApiResource('', ''));

    test('fromMap', () {
      final e = EncounterMethodRate.fromMap({
        EncounterMethodRate.ENCOUNTER_METHOD_FIELD_NAME:
            ENCOUNTER_METHOD.toMap(),
        EncounterMethodRate.VERSION_DETAILS_FIELD_NAME: [
          ENCOUNTER_VERSION.toMap(),
        ],
      });

      expect(e.encounterMethod.name, equals(ENCOUNTER_METHOD_NAME));
      expect(e.versionDetails.length, equals(1));
      expect(e.versionDetails.first.rate, equals(ENCOUNTER_VERSION_RATE));
    });

    test('toMap', () {
      final e =
          EncounterMethodRate(ENCOUNTER_METHOD, [ENCOUNTER_VERSION]).toMap();

      expect(e[EncounterMethodRate.ENCOUNTER_METHOD_FIELD_NAME],
          equals(ENCOUNTER_METHOD.toMap()));
      expect(e[EncounterMethodRate.VERSION_DETAILS_FIELD_NAME],
          equals([ENCOUNTER_VERSION.toMap()]));
    });

    group('listFrom', () {
      test('null', () {
        final e = EncounterMethodRate.listFrom(null);
        expect(e, isEmpty);
      });

      test('not null', () {
        final e = EncounterMethodRate.listFrom([
          {
            EncounterMethodRate.ENCOUNTER_METHOD_FIELD_NAME:
                ENCOUNTER_METHOD.toMap(),
            EncounterMethodRate.VERSION_DETAILS_FIELD_NAME: [
              ENCOUNTER_VERSION.toMap(),
            ],
          },
        ]);

        expect(e.first.encounterMethod.name, equals(ENCOUNTER_METHOD_NAME));
        expect(e.first.versionDetails.length, equals(1));
        expect(
            e.first.versionDetails.first.rate, equals(ENCOUNTER_VERSION_RATE));
      });
    });
  });
}
