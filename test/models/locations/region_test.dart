// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/locations/region.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('Region', () {
    const ID = 1;
    const LOCATIONS_NAME = 'location';
    const LOCATION = NamedApiResource(LOCATIONS_NAME, '');
    const NAME = 'name';
    const NAMES_NAME = 'names';
    const NAME_ = Name(NAMES_NAME, NamedApiResource('', ''));
    const GENERATION_NAME = 'generation';
    const GENERATION = NamedApiResource(GENERATION_NAME, '');
    const POKEDEX_NAME = 'pokedex';
    const POKEDEX = NamedApiResource(POKEDEX_NAME, '');
    const VERSION_GROUP_NAME = 'version group';
    const VERSION_GROUP = NamedApiResource(VERSION_GROUP_NAME, '');

    test('fromMap', () {
      final r = Region.fromMap({
        Region.ID_FIELD_NAME: ID,
        Region.LOCATIONS_FIELD_NAME: [LOCATION.toMap()],
        Region.NAME_FIELD_NAME: NAME,
        Region.NAMES_FIELD_NAME: [NAME_.toMap()],
        Region.MAIN_GENERATION_FIELD_NAME: GENERATION.toMap(),
        Region.POKEDEXES_FIELD_NAME: [POKEDEX.toMap()],
        Region.VERSION_GROUPS_FIELD_NAME: [VERSION_GROUP.toMap()],
      });

      expect(r.id, equals(ID));
      expect(r.locations.length, equals(1));
      expect(r.locations.first.name, equals(LOCATIONS_NAME));
      expect(r.name, equals(NAME));
      expect(r.names.length, equals(1));
      expect(r.names.first.name, equals(NAMES_NAME));
      expect(r.mainGeneration.name, equals(GENERATION_NAME));
      expect(r.pokedexes.length, equals(1));
      expect(r.pokedexes.first.name, equals(POKEDEX_NAME));
      expect(r.versionGroups.length, equals(1));
      expect(r.versionGroups.first.name, equals(VERSION_GROUP_NAME));
    });

    test('toMap', () {
      final r = Region(ID, [LOCATION], NAME, [NAME_], GENERATION, [POKEDEX],
          [VERSION_GROUP]).toMap();

      expect(r[Region.ID_FIELD_NAME], equals(ID));
      expect(r[Region.LOCATIONS_FIELD_NAME], equals([LOCATION.toMap()]));
      expect(r[Region.NAME_FIELD_NAME], equals(NAME));
      expect(r[Region.NAMES_FIELD_NAME], equals([NAME_.toMap()]));
      expect(r[Region.MAIN_GENERATION_FIELD_NAME], equals(GENERATION.toMap()));
      expect(r[Region.POKEDEXES_FIELD_NAME], equals([POKEDEX.toMap()]));
      expect(
          r[Region.VERSION_GROUPS_FIELD_NAME], equals([VERSION_GROUP.toMap()]));
    });
  });
}
