// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/locations/encounter_version_detail.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('EncounterVersionDetail', () {
    const RATE = 1;
    const VERSION_NAME = 'name';
    const VERSION = NamedApiResource(VERSION_NAME, '');

    test('fromMap', () {
      final e = EncounterVersionDetail.fromMap({
        EncounterVersionDetail.RATE_FIELD_NAME: RATE,
        EncounterVersionDetail.VERSION_FIELD_NAME: VERSION.toMap(),
      });

      expect(e.rate, equals(RATE));
      expect(e.version.name, equals(VERSION_NAME));
    });

    test('toMap', () {
      final e = EncounterVersionDetail(RATE, VERSION).toMap();

      expect(e[EncounterVersionDetail.RATE_FIELD_NAME], equals(RATE));
      expect(e[EncounterVersionDetail.VERSION_FIELD_NAME],
          equals(VERSION.toMap()));
    });

    group('listFrom', () {
      test('null', () {
        final e = EncounterVersionDetail.listFrom(null);
        expect(e, isEmpty);
      });

      test('not null', () {
        final e = EncounterVersionDetail.listFrom([
          {
            EncounterVersionDetail.RATE_FIELD_NAME: RATE,
            EncounterVersionDetail.VERSION_FIELD_NAME: VERSION.toMap(),
          },
        ]);

        expect(e.first.rate, equals(RATE));
        expect(e.first.version.name, equals(VERSION_NAME));
      });
    });
  });
}
