// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/locations/location.dart';
import 'package:pokeapi/src/models/utility/common/generation_game_index.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('Location', () {
    const ID = 1;
    const NAME = 'name';
    const REGION_NAME = 'region';
    const REGION = NamedApiResource(REGION_NAME, '');
    const NAMES_NAME = 'names';
    const NAME_ = Name(NAMES_NAME, NamedApiResource('', ''));
    const GAME_INDEX = 2;
    const GAME_INDEX_ =
        GenerationGameIndex(GAME_INDEX, NamedApiResource('', ''));
    const AREA_NAME = 'area';
    const AREA = NamedApiResource(AREA_NAME, '');

    test('fromMap', () {
      final l = Location.fromMap({
        Location.ID_FIELD_NAME: ID,
        Location.NAME_FIELD_NAME: NAME,
        Location.REGION_FIELD_NAME: REGION.toMap(),
        Location.NAMES_FIELD_NAME: [NAME_.toMap()],
        Location.GAME_INDICES_FIELD_NAME: [GAME_INDEX_.toMap()],
        Location.AREAS_FIELD_NAME: [AREA.toMap()],
      });

      expect(l.id, equals(ID));
      expect(l.name, equals(NAME));
      expect(l.region.name, equals(REGION_NAME));
      expect(l.names.length, equals(1));
      expect(l.names.first.name, equals(NAMES_NAME));
      expect(l.gameIndices.length, equals(1));
      expect(l.gameIndices.first.gameIndex, equals(GAME_INDEX));
      expect(l.areas.length, equals(1));
      expect(l.areas.first.name, equals(AREA_NAME));
    });

    test('toMap', () {
      final l =
          Location(ID, NAME, REGION, [NAME_], [GAME_INDEX_], [AREA]).toMap();

      expect(l[Location.ID_FIELD_NAME], equals(ID));
      expect(l[Location.NAME_FIELD_NAME], equals(NAME));
      expect(l[Location.REGION_FIELD_NAME], equals(REGION.toMap()));
      expect(l[Location.NAMES_FIELD_NAME], equals([NAME_.toMap()]));
      expect(
          l[Location.GAME_INDICES_FIELD_NAME], equals([GAME_INDEX_.toMap()]));
      expect(l[Location.AREAS_FIELD_NAME], equals([AREA.toMap()]));
    });
  });
}
