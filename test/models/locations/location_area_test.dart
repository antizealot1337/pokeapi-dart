// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/locations/encounter_method_rate.dart';
import 'package:pokeapi/src/models/locations/location_area.dart';
import 'package:pokeapi/src/models/locations/pokemon_encounter.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('LocationArea', () {
    const ID = 1;
    const NAME = 'name';
    const GAME_INDEX = 2;
    const ENCOUNTER_METHOD_RATE_NAME = 'encounter method';
    const ENCOUNTER_METHOD_RATE = EncounterMethodRate(
        NamedApiResource(ENCOUNTER_METHOD_RATE_NAME, ''), []);
    const LOCATION_NAME = 'location';
    const LOCATION = NamedApiResource(LOCATION_NAME, '');
    const NAMES_NAME = 'names';
    const NAME_ = Name(NAMES_NAME, NamedApiResource('', ''));
    const POKEMON_NAME = 'pokemon';
    const POKEMON = PokemonEncounter(NamedApiResource(POKEMON_NAME, ''), []);

    test('fromMap', () {
      final l = LocationArea.fromMap({
        LocationArea.ID_FIELD_NAME: ID,
        LocationArea.NAME_FIELD_NAME: NAME,
        LocationArea.GAME_INDEX_FIELD_NAME: GAME_INDEX,
        LocationArea.ENCOUNTER_METHOD_RATES_FIELD_NAME: [
          ENCOUNTER_METHOD_RATE.toMap(),
        ],
        LocationArea.LOCATION_FIELD_NAME: LOCATION.toMap(),
        LocationArea.NAMES_FIELD_NAME: [NAME_.toMap()],
        LocationArea.POKEMON_ENCOUNTERS_FIELD_NAME: [POKEMON.toMap()],
      });

      expect(l.id, equals(ID));
      expect(l.name, equals(NAME));
      expect(l.gameIndex, equals(GAME_INDEX));
      expect(l.encounterMethodRates.length, equals(1));
      expect(l.encounterMethodRates.first.encounterMethod.name,
          equals(ENCOUNTER_METHOD_RATE_NAME));
      expect(l.location.name, equals(LOCATION_NAME));
      expect(l.names.length, equals(1));
      expect(l.names.first.name, equals(NAMES_NAME));
      expect(l.pokemonEncounters.length, equals(1));
      expect(l.pokemonEncounters.first.pokemon.name, equals(POKEMON_NAME));
    });

    test('toMap', () {
      final l = LocationArea(ID, NAME, GAME_INDEX, [ENCOUNTER_METHOD_RATE],
          LOCATION, [NAME_], [POKEMON]).toMap();

      expect(l[LocationArea.ID_FIELD_NAME], equals(ID));
      expect(l[LocationArea.NAME_FIELD_NAME], equals(NAME));
      expect(l[LocationArea.GAME_INDEX_FIELD_NAME], equals(GAME_INDEX));
      expect(l[LocationArea.ENCOUNTER_METHOD_RATES_FIELD_NAME],
          equals([ENCOUNTER_METHOD_RATE.toMap()]));
      expect(l[LocationArea.LOCATION_FIELD_NAME], equals(LOCATION.toMap()));
      expect(l[LocationArea.NAMES_FIELD_NAME], equals([NAME_.toMap()]));
      expect(l[LocationArea.POKEMON_ENCOUNTERS_FIELD_NAME],
          equals([POKEMON.toMap()]));
    });
  });
}
