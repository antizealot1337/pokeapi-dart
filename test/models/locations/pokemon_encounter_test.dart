// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/locations/pokemon_encounter.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:pokeapi/src/models/utility/common/version_encounter_detail.dart';
import 'package:test/test.dart';

void main() {
  group('PokemonEncounter', () {
    const POKEMON_NAME = 'name';
    const POKEMON = NamedApiResource(POKEMON_NAME, '');
    const VERSION_NAME = 'version';
    const VERSION =
        VersionEncounterDetail(NamedApiResource(VERSION_NAME, ''), 0, []);

    test('fromMap', () {
      final p = PokemonEncounter.fromMap({
        PokemonEncounter.POKEMON_FIELD_NAME: POKEMON.toMap(),
        PokemonEncounter.VERSION_DETAILS_FIELD_NAME: [VERSION.toMap()],
      });

      expect(p.pokemon.name, equals(POKEMON_NAME));
      expect(p.versionDetails.length, equals(1));
      expect(p.versionDetails.first.version.name, equals(VERSION_NAME));
    });

    test('toMap', () {
      final p = PokemonEncounter(POKEMON, [VERSION]).toMap();

      expect(p[PokemonEncounter.POKEMON_FIELD_NAME], equals(POKEMON.toMap()));
      expect(p[PokemonEncounter.VERSION_DETAILS_FIELD_NAME],
          equals([VERSION.toMap()]));
    });

    group('listFrom', () {
      test('null', () {
        final p = PokemonEncounter.listFrom(null);
        expect(p, isEmpty);
      });

      test('not null', () {
        final p = PokemonEncounter.listFrom([
          {
            PokemonEncounter.POKEMON_FIELD_NAME: POKEMON.toMap(),
            PokemonEncounter.VERSION_DETAILS_FIELD_NAME: [VERSION.toMap()],
          },
        ]);

        expect(p.first.pokemon.name, equals(POKEMON_NAME));
        expect(p.first.versionDetails.length, equals(1));
        expect(p.first.versionDetails.first.version.name, equals(VERSION_NAME));
      });
    });
  });
}
