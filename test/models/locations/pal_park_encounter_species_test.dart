// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/locations/pal_park_encounter_species.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('PalParkEncounterSpecies', () {
    const BASE_SCORE = 1;
    const RATE = 2;
    const POKEMON_SPECIES_NAME = 'pokemon species';
    const POKEMON_SPECIES = NamedApiResource(POKEMON_SPECIES_NAME, '');

    test('fromMap', () {
      final p = PalParkEncounterSpecies.fromMap({
        PalParkEncounterSpecies.BASE_SCORE_FIELD_NAME: BASE_SCORE,
        PalParkEncounterSpecies.RATE_FIELD_NAME: RATE,
        PalParkEncounterSpecies.POKEMON_SPECIES: POKEMON_SPECIES.toMap(),
      });

      expect(p.baseScore, equals(BASE_SCORE));
      expect(p.rate, equals(RATE));
      expect(p.pokemonSpecies.name, equals(POKEMON_SPECIES_NAME));
    });

    test('toMap', () {
      final p =
          PalParkEncounterSpecies(BASE_SCORE, RATE, POKEMON_SPECIES).toMap();

      expect(
          p[PalParkEncounterSpecies.BASE_SCORE_FIELD_NAME], equals(BASE_SCORE));
      expect(p[PalParkEncounterSpecies.RATE_FIELD_NAME], equals(RATE));
      expect(p[PalParkEncounterSpecies.POKEMON_SPECIES],
          equals(POKEMON_SPECIES.toMap()));
    });

    group('listFrom', () {
      test('null', () {
        final p = PalParkEncounterSpecies.listFrom(null);
        expect(p, isEmpty);
      });

      test('not null', () {
        final p = PalParkEncounterSpecies.listFrom([
          {
            PalParkEncounterSpecies.BASE_SCORE_FIELD_NAME: BASE_SCORE,
            PalParkEncounterSpecies.RATE_FIELD_NAME: RATE,
            PalParkEncounterSpecies.POKEMON_SPECIES: POKEMON_SPECIES.toMap(),
          },
        ]);

        expect(p.first.baseScore, equals(BASE_SCORE));
        expect(p.first.rate, equals(RATE));
        expect(p.first.pokemonSpecies.name, equals(POKEMON_SPECIES_NAME));
      });
    });
  });
}
