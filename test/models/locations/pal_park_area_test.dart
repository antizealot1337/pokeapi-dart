// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/locations/pal_park_area.dart';
import 'package:pokeapi/src/models/locations/pal_park_encounter_species.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('PalParkArea', () {
    const ID = 1;
    const NAME = 'name';
    const NAMES_NAME = 'names';
    const NAME_ = Name(NAMES_NAME, NamedApiResource('', ''));
    const POKEMON_ENCOUNTER_BASE_SCORE = 2;
    const POKEMON_ENCOUNTER = PalParkEncounterSpecies(
        POKEMON_ENCOUNTER_BASE_SCORE, 0, NamedApiResource('', ''));

    test('fromMap', () {
      final p = PalParkArea.fromMap({
        PalParkArea.ID_FIELD_NAME: ID,
        PalParkArea.NAME_FIELD_NAME: NAME,
        PalParkArea.NAMES_FIELD_NAME: [NAME_.toMap()],
        PalParkArea.POKEMON_ENCOUNERS_FIELD_NAME: [POKEMON_ENCOUNTER.toMap()],
      });

      expect(p.id, equals(ID));
      expect(p.name, equals(NAME));
      expect(p.names.length, equals(1));
      expect(p.names.first.name, equals(NAMES_NAME));
      expect(p.pokemonEncounters.length, equals(1));
      expect(p.pokemonEncounters.first.baseScore,
          equals(POKEMON_ENCOUNTER_BASE_SCORE));
    });

    test('toMap', () {
      final p = PalParkArea(ID, NAME, [NAME_], [POKEMON_ENCOUNTER]).toMap();

      expect(p[PalParkArea.ID_FIELD_NAME], equals(ID));
      expect(p[PalParkArea.NAME_FIELD_NAME], equals(NAME));
      expect(p[PalParkArea.NAMES_FIELD_NAME], equals([NAME_.toMap()]));
      expect(p[PalParkArea.POKEMON_ENCOUNERS_FIELD_NAME],
          equals([POKEMON_ENCOUNTER.toMap()]));
    });
  });
}
