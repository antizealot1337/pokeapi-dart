// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/resourse.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('Resource', () {
    const COUNT = 1;
    const NEXT = 'next';
    const PREVIOUS = 'previous';
    const RESULT = NamedApiResource('result', '');

    test('fromMap', () {
      final r = Resource.fromMap({
        Resource.COUNT_FIELD_NAME: COUNT,
        Resource.NEXT_FIELD_NAME: NEXT,
        Resource.PREVIOUS_FIELD_NAME: PREVIOUS,
        Resource.RESULTS_FIELD_NAME: [RESULT.toMap()]
      });

      expect(r.count, equals(COUNT));
      expect(r.next, equals(NEXT));
      expect(r.previous, equals(PREVIOUS));
      expect(r.results.length, equals(1));
      expect(r.results.first.name, equals(RESULT.name));
    });

    test('toMap', () {
      final r = Resource(COUNT, NEXT, PREVIOUS, [RESULT]).toMap();

      expect(r[Resource.COUNT_FIELD_NAME], equals(COUNT));
      expect(r[Resource.NEXT_FIELD_NAME], equals(NEXT));
      expect(r[Resource.PREVIOUS_FIELD_NAME], equals(PREVIOUS));
      expect(r[Resource.RESULTS_FIELD_NAME], equals([RESULT.toMap()]));
    });
  });
}
