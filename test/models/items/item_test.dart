// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/items/item.dart';
import 'package:pokeapi/src/models/items/item_holder_pokemon.dart';
import 'package:pokeapi/src/models/items/item_sprites.dart';
import 'package:pokeapi/src/models/utility/common/api_resource.dart';
import 'package:pokeapi/src/models/utility/common/generation_game_index.dart';
import 'package:pokeapi/src/models/utility/common/machine_version_detail.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:pokeapi/src/models/utility/common/verbose_effect.dart';
import 'package:pokeapi/src/models/utility/common/version_group_flavor_text.dart';
import 'package:test/test.dart';

void main() {
  group('Item', () {
    const ID = 1;
    const NAME = 'name';
    const COST = 2;
    const FLING_POWER = 3;
    const FLING_EFFECT_NAME = 'fling effect';
    const FLING_EFFECT = NamedApiResource(FLING_EFFECT_NAME, '');
    const ATTRIBUTE_NAME = 'attribute';
    const ATTRIBUTE = NamedApiResource(ATTRIBUTE_NAME, '');
    const CATEGORY_NAME = 'category';
    const CATEGORY = NamedApiResource(CATEGORY_NAME, '');
    const EFFECT_ENTRY_EFFECT = 'verbose effect';
    const EFFECT_ENTRY =
        VerboseEffect(EFFECT_ENTRY_EFFECT, '', NamedApiResource('', ''));
    const FLAVOR_TEXT_ENTRY_TEXT = 'flavor text';
    const FLAVOR_TEXT_ENTRY = VersionGroupFlavorText(FLAVOR_TEXT_ENTRY_TEXT,
        NamedApiResource('', ''), NamedApiResource('', ''));
    const GAME_INDICIES_INDEX = 4;
    const GAME_INDEX =
        GenerationGameIndex(GAME_INDICIES_INDEX, NamedApiResource('', ''));
    const NAMES_NAME = 'names name';
    const NAME_ = Name(NAMES_NAME, NamedApiResource('', ''));
    const SPRITES_DEFAULT = 'sprites default';
    const SPRITES = ItemSprites(SPRITES_DEFAULT);
    const HELD_BY_POKEMON_NAME = 'pokemon name';
    const HELD_BY =
        ItemHolderPokemon(NamedApiResource(HELD_BY_POKEMON_NAME, ''), []);
    const BABY_TRIGGER_FOR_URL = 'baby trigger';
    const BABY_TRIGGER_FOR = ApiResource(BABY_TRIGGER_FOR_URL);
    const MACHINE_URL = 'machine url';
    const MACHINE = MachineVersionDetail(
        ApiResource(MACHINE_URL), NamedApiResource('', ''));

    test('fromMap', () {
      final i = Item.fromMap({
        Item.ID_FIELD_NAME: ID,
        Item.NAME_FIELD_NAME: NAME,
        Item.COST_FIELD_NAME: COST,
        Item.FLING_POWER_FIELD_NAME: FLING_POWER,
        Item.FLING_EFFECT_FIELD_NAME: FLING_EFFECT.toMap(),
        Item.ATTRIBUTES_FIELD_NAME: [ATTRIBUTE.toMap()],
        Item.CATEGORY_FIELD_NAME: CATEGORY.toMap(),
        Item.EFFECT_ENTRIES_FIELD_NAME: [EFFECT_ENTRY.toMap()],
        Item.FLAVOR_TEXT_ENTRIES_FIELD_NAME: [FLAVOR_TEXT_ENTRY.toMap()],
        Item.GAME_INDICES_FIELD_NAME: [GAME_INDEX.toMap()],
        Item.NAMES_FIELD_NAME: [NAME_.toMap()],
        Item.SPRITES_FIELD_NAME: SPRITES.toMap(),
        Item.HELD_BY_POKEMON_FIELD_NAME: [HELD_BY.toMap()],
        Item.BABY_TRIGGER_FOR_FIELD_NAME: BABY_TRIGGER_FOR.toMap(),
        Item.MACHINES_FIELD_NAME: [MACHINE.toMap()],
      });

      expect(i.id, equals(ID));
      expect(i.name, equals(NAME));
      expect(i.cost, equals(COST));
      expect(i.flingPower, equals(FLING_POWER));
      expect(i.flingEffect!.name, equals(FLING_EFFECT_NAME));
      expect(i.attributes.length, equals(1));
      expect(i.attributes.first.name, equals(ATTRIBUTE_NAME));
      expect(i.category.name, equals(CATEGORY_NAME));
      expect(i.effectEntries.length, equals(1));
      expect(i.effectEntries.first.effect, equals(EFFECT_ENTRY_EFFECT));
      expect(i.flavorTextEntries.length, equals(1));
      expect(i.flavorTextEntries.first.text, equals(FLAVOR_TEXT_ENTRY_TEXT));
      expect(i.gameIndicies.length, equals(1));
      expect(i.gameIndicies.first.gameIndex, equals(GAME_INDICIES_INDEX));
      expect(i.names.length, equals(1));
      expect(i.names.first.name, equals(NAMES_NAME));
      expect(i.heldByPokemon.length, equals(1));
      expect(i.heldByPokemon.first.pokemon.name, equals(HELD_BY_POKEMON_NAME));
      expect(i.machines.length, equals(1));
      expect(i.machines.first.machine.url, equals(MACHINE_URL));
    });

    test('toMap', () {
      final i = Item(
          ID,
          NAME,
          COST,
          FLING_POWER,
          FLING_EFFECT,
          [ATTRIBUTE],
          CATEGORY,
          [EFFECT_ENTRY],
          [FLAVOR_TEXT_ENTRY],
          [GAME_INDEX],
          [NAME_],
          SPRITES,
          [HELD_BY],
          BABY_TRIGGER_FOR,
          [MACHINE]).toMap();

      expect(i[Item.ID_FIELD_NAME], equals(ID));
      expect(i[Item.NAME_FIELD_NAME], equals(NAME));
      expect(i[Item.COST_FIELD_NAME], equals(COST));
      expect(i[Item.FLING_POWER_FIELD_NAME], equals(FLING_POWER));
      expect(i[Item.FLING_EFFECT_FIELD_NAME], equals(FLING_EFFECT.toMap()));
      expect(i[Item.ATTRIBUTES_FIELD_NAME], equals([ATTRIBUTE.toMap()]));
      expect(i[Item.CATEGORY_FIELD_NAME], equals(CATEGORY.toMap()));
      expect(i[Item.EFFECT_ENTRIES_FIELD_NAME], equals([EFFECT_ENTRY.toMap()]));
      expect(i[Item.FLAVOR_TEXT_ENTRIES_FIELD_NAME],
          equals([FLAVOR_TEXT_ENTRY.toMap()]));
      expect(i[Item.GAME_INDICES_FIELD_NAME], equals([GAME_INDEX.toMap()]));
      expect(i[Item.NAMES_FIELD_NAME], equals([NAME_.toMap()]));
      expect(i[Item.SPRITES_FIELD_NAME], equals(SPRITES.toMap()));
      expect(i[Item.HELD_BY_POKEMON_FIELD_NAME], equals([HELD_BY.toMap()]));
      expect(i[Item.BABY_TRIGGER_FOR_FIELD_NAME],
          equals(BABY_TRIGGER_FOR.toMap()));
      expect(i[Item.MACHINES_FIELD_NAME], equals([MACHINE.toMap()]));
    });
  });
}
