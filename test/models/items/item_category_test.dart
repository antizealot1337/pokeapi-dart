// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/items/item_category.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('ItemCategory', () {
    const ID = 1;
    const NAME = 'name';
    const ITEMS_NAME = 'item';
    const ITEM = NamedApiResource(ITEMS_NAME, '');
    const NAMES_NAME = 'names';
    const NAME_ = Name(NAMES_NAME, NamedApiResource('', ''));
    const POCKET_NAME = 'pocket';
    const POCKET = NamedApiResource(POCKET_NAME, '');

    test('fromMap', () {
      final i = ItemCategory.fromMap({
        ItemCategory.ID_FIELD_NAME: ID,
        ItemCategory.NAME_FIELD_NAME: NAME,
        ItemCategory.ITEMS_FIELD_NAME: [ITEM.toMap()],
        ItemCategory.NAMES_FIELD_NAME: [NAME_.toMap()],
        ItemCategory.POCKET_FIELD_NAME: POCKET.toMap(),
      });

      expect(i.id, equals(ID));
      expect(i.name, equals(NAME));
      expect(i.items.length, equals(1));
      expect(i.items.first.name, equals(ITEMS_NAME));
      expect(i.names.length, equals(1));
      expect(i.names.first.name, equals(NAMES_NAME));
    });

    test('toMap', () {
      final i = ItemCategory(ID, NAME, [ITEM], [NAME_], POCKET).toMap();

      expect(i[ItemCategory.ID_FIELD_NAME], equals(ID));
      expect(i[ItemCategory.NAME_FIELD_NAME], equals(NAME));
      expect(i[ItemCategory.ITEMS_FIELD_NAME], equals([ITEM.toMap()]));
      expect(i[ItemCategory.NAMES_FIELD_NAME], equals([NAME_.toMap()]));
      expect(i[ItemCategory.POCKET_FIELD_NAME], equals(POCKET.toMap()));
    });
  });
}
