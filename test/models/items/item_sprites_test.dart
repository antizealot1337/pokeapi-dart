// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/items/item_sprites.dart';
import 'package:test/test.dart';

void main() {
  group('ItemSprites', () {
    const DEFAULT = 'default';

    test('fromMap', () {
      final i = ItemSprites.fromMap({
        ItemSprites.DEFAULT_FIELD_NAME: DEFAULT,
      });

      expect(i.default_, equals(DEFAULT));
    });

    test('toMap', () {
      final i = ItemSprites(DEFAULT).toMap();

      expect(i[ItemSprites.DEFAULT_FIELD_NAME], equals(DEFAULT));
    });
  });
}
