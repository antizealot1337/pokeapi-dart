// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/items/item_attribute.dart';
import 'package:pokeapi/src/models/utility/common/description.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart' hide Description;

void main() {
  group('ItemAttribute', () {
    const ID = 1;
    const NAME = 'name';
    const ITEMS_NAME = 'item';
    const ITEM = NamedApiResource(ITEMS_NAME, '');
    const NAMES_NAME = 'names name';
    const NAME_ = Name(NAMES_NAME, NamedApiResource('', ''));
    const DESCRIPTION = 'description';
    const DESCRIPTION_ = Description(DESCRIPTION, NamedApiResource('', ''));

    test('fromMap', () {
      final i = ItemAttribute.fromMap({
        ItemAttribute.ID_FIELD_NAME: ID,
        ItemAttribute.NAME_FIELD_NAME: NAME,
        ItemAttribute.ITEMS_FIELD_NAME: [ITEM.toMap()],
        ItemAttribute.NAMES_FIELD_NAME: [NAME_.toMap()],
        ItemAttribute.DESCRIPTIONS_FIELD_NAME: [DESCRIPTION_.toMap()],
      });

      expect(i.id, equals(ID));
      expect(i.name, equals(NAME));
      expect(i.items.length, equals(1));
      expect(i.items.first.name, equals(ITEMS_NAME));
      expect(i.names.length, equals(1));
      expect(i.names.first.name, equals(NAMES_NAME));
      expect(i.descriptions.length, equals(1));
      expect(i.descriptions.first.description, equals(DESCRIPTION));
    });

    test('toMap', () {
      final i =
          ItemAttribute(ID, NAME, [ITEM], [NAME_], [DESCRIPTION_]).toMap();

      expect(i[ItemAttribute.ID_FIELD_NAME], equals(ID));
      expect(i[ItemAttribute.NAME_FIELD_NAME], equals(NAME));
      expect(i[ItemAttribute.ITEMS_FIELD_NAME], equals([ITEM.toMap()]));
      expect(i[ItemAttribute.NAMES_FIELD_NAME], equals([NAME_.toMap()]));
      expect(i[ItemAttribute.DESCRIPTIONS_FIELD_NAME],
          equals([DESCRIPTION_.toMap()]));
    });
  });
}
