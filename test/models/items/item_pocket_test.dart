// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/items/item_pocket.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('ItemPocket', () {
    const ID = 1;
    const NAME = 'name';
    const CATEGORY_NAME = 'category';
    const CATEGORY = NamedApiResource(CATEGORY_NAME, '');
    const NAMES_NAME = 'names';
    const NAME_ = Name(NAMES_NAME, NamedApiResource('', ''));

    test('fromMap', () {
      final i = ItemPocket.fromMap({
        ItemPocket.ID_FIELD_NAME: ID,
        ItemPocket.NAME_FIELD_NAME: NAME,
        ItemPocket.CATEGORIES_FIELD_NAME: [CATEGORY.toMap()],
        ItemPocket.NAMES_FIELD_NAME: [NAME_.toMap()],
      });

      expect(i.id, equals(ID));
      expect(i.name, equals(NAME));
      expect(i.categories.length, equals(1));
      expect(i.categories.first.name, equals(CATEGORY_NAME));
      expect(i.names.length, equals(1));
      expect(i.names.first.name, equals(NAMES_NAME));
    });

    test('toMap', () {
      final i = ItemPocket(ID, NAME, [CATEGORY], [NAME_]).toMap();

      expect(i[ItemPocket.ID_FIELD_NAME], equals(ID));
      expect(i[ItemPocket.NAME_FIELD_NAME], equals(NAME));
      expect(i[ItemPocket.CATEGORIES_FIELD_NAME], equals([CATEGORY.toMap()]));
      expect(i[ItemPocket.NAMES_FIELD_NAME], equals([NAME_.toMap()]));
    });
  });
}
