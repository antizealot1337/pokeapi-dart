// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/items/item_holder_pokemon_version_detail.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('ItemHolderPokemonVersionDetail', () {
    const RARITY = 1;
    const VERSION_NAME = 'version';
    const VERSION = NamedApiResource(VERSION_NAME, '');

    test('fromMap', () {
      final i = ItemHolderPokemonVersionDetail.fromMap({
        ItemHolderPokemonVersionDetail.RARITY_FIELD_NAME: RARITY,
        ItemHolderPokemonVersionDetail.VERSION_FIELD_NAME: VERSION.toMap(),
      });

      expect(i.rarity, equals(RARITY));
      expect(i.version.name, equals(VERSION_NAME));
    });

    test('toMap', () {
      final i = ItemHolderPokemonVersionDetail(RARITY, VERSION).toMap();

      expect(
          i[ItemHolderPokemonVersionDetail.RARITY_FIELD_NAME], equals(RARITY));
      expect(i[ItemHolderPokemonVersionDetail.VERSION_FIELD_NAME],
          equals(VERSION.toMap()));
    });

    group('listFrom', () {
      test('null', () {
        final i = ItemHolderPokemonVersionDetail.listFrom(null);
        expect(i, isEmpty);
      });

      test('not null', () {
        final i = ItemHolderPokemonVersionDetail.listFrom([
          {
            ItemHolderPokemonVersionDetail.RARITY_FIELD_NAME: RARITY,
            ItemHolderPokemonVersionDetail.VERSION_FIELD_NAME: VERSION.toMap(),
          },
        ]);

        expect(i.first.rarity, equals(RARITY));
        expect(i.first.version.name, equals(VERSION_NAME));
      });
    });
  });
}
