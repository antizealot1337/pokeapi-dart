// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/items/item_fling_effect.dart';
import 'package:pokeapi/src/models/utility/common/effect.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('ItemFlingEffect', () {
    const ID = 1;
    const NAME = 'name';
    const EFFECT_ENTRIES_EFFECT = 'effect';
    const EFFECT = Effect(EFFECT_ENTRIES_EFFECT, NamedApiResource('', ''));
    const ITEMS_NAME = 'item';
    const ITEM = NamedApiResource(ITEMS_NAME, '');

    test('fromMap', () {
      final i = ItemFlingEffect.fromMap({
        ItemFlingEffect.ID_FIELD_NAME: ID,
        ItemFlingEffect.NAME_FIELD_NAME: NAME,
        ItemFlingEffect.EFFECT_ENTRIES_FIELD_NAME: [EFFECT.toMap()],
        ItemFlingEffect.ITEMS_FIELD_NAME: [ITEM.toMap()],
      });

      expect(i.id, equals(ID));
      expect(i.name, equals(NAME));
      expect(i.effectEntries.length, equals(1));
      expect(i.effectEntries.first.effect, equals(EFFECT_ENTRIES_EFFECT));
      expect(i.items.length, equals(1));
      expect(i.items.first.name, equals(ITEMS_NAME));
    });

    test('toMap', () {
      final i = ItemFlingEffect(ID, NAME, [EFFECT], [ITEM]).toMap();

      expect(i[ItemFlingEffect.ID_FIELD_NAME], equals(ID));
      expect(i[ItemFlingEffect.NAME_FIELD_NAME], equals(NAME));
      expect(i[ItemFlingEffect.EFFECT_ENTRIES_FIELD_NAME],
          equals([EFFECT.toMap()]));
      expect(i[ItemFlingEffect.ITEMS_FIELD_NAME], equals([ITEM.toMap()]));
    });
  });
}
