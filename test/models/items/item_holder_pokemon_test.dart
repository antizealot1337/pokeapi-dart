// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/items/item_holder_pokemon.dart';
import 'package:pokeapi/src/models/items/item_holder_pokemon_version_detail.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('ItemHolderPokemon', () {
    const POKEMON_NAME = 'pokemon';
    const POKEMON = NamedApiResource(POKEMON_NAME, '');
    const RARITY = 1;
    const VERSION_DETAIL =
        ItemHolderPokemonVersionDetail(RARITY, NamedApiResource('', ''));

    test('fromMap', () {
      final i = ItemHolderPokemon.fromMap({
        ItemHolderPokemon.POKEMON_FIELD_NAME: POKEMON.toMap(),
        ItemHolderPokemon.VERSION_DETAILS_FIELD_NAME: [VERSION_DETAIL.toMap()],
      });

      expect(i.pokemon.name, equals(POKEMON_NAME));
      expect(i.versionDetails.length, equals(1));
      expect(i.versionDetails.first.rarity, equals(RARITY));
    });

    test('toMap', () {
      final i = ItemHolderPokemon(POKEMON, [VERSION_DETAIL]).toMap();

      expect(i[ItemHolderPokemon.POKEMON_FIELD_NAME], equals(POKEMON.toMap()));
      expect(i[ItemHolderPokemon.VERSION_DETAILS_FIELD_NAME],
          equals([VERSION_DETAIL.toMap()]));
    });

    group('listFrom', () {
      test('null', () {
        final i = ItemHolderPokemon.listFrom(null);
        expect(i, isEmpty);
      });

      test('not null', () {
        final i = ItemHolderPokemon.listFrom([
          {
            ItemHolderPokemon.POKEMON_FIELD_NAME: POKEMON.toMap(),
            ItemHolderPokemon.VERSION_DETAILS_FIELD_NAME: [
              VERSION_DETAIL.toMap(),
            ],
          },
        ]);

        expect(i.first.pokemon.name, equals(POKEMON_NAME));
        expect(i.first.versionDetails.length, equals(1));
        expect(i.first.versionDetails.first.rarity, equals(RARITY));
      });
    });
  });
}
