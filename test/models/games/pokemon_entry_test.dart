// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/games/pokemon_entry.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('PokemonEntry', () {
    const ENTRY_NUMBER = 1;
    const POKEMON_SPECIES_NAME = 'pokemon species';
    const POKEMON_SPECIES = NamedApiResource(POKEMON_SPECIES_NAME, '');

    test('fromMap', () {
      final p = PokemonEntry.fromMap({
        PokemonEntry.ENTRY_NUMBER_FIELD: ENTRY_NUMBER,
        PokemonEntry.POKEMON_SPECIES: POKEMON_SPECIES.toMap(),
      });

      expect(p.entryNumber, equals(ENTRY_NUMBER));
      expect(p.pokemonSpecies.name, equals(POKEMON_SPECIES_NAME));
    });

    test('toMap', () {
      final p = PokemonEntry(ENTRY_NUMBER, POKEMON_SPECIES).toMap();

      expect(p[PokemonEntry.ENTRY_NUMBER_FIELD], equals(ENTRY_NUMBER));
      expect(p[PokemonEntry.POKEMON_SPECIES], equals(POKEMON_SPECIES.toMap()));
    });

    group('listFrom', () {
      test('null', () {
        final p = PokemonEntry.listFrom(null);
        expect(p, isEmpty);
      });

      test('not null', () {
        final p = PokemonEntry.listFrom([
          {
            PokemonEntry.ENTRY_NUMBER_FIELD: ENTRY_NUMBER,
            PokemonEntry.POKEMON_SPECIES: POKEMON_SPECIES.toMap(),
          },
        ]);

        expect(p.first.entryNumber, equals(ENTRY_NUMBER));
        expect(p.first.pokemonSpecies.name, equals(POKEMON_SPECIES_NAME));
      });
    });
  });
}
