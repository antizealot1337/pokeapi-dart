// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/games/version_group.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('VersionGroup', () {
    const ID = 1;
    const NAME = 'name';
    const ORDER = 2;
    const GENERATION_NAME = 'generation';
    const GENERATION = NamedApiResource(GENERATION_NAME, '');
    const MOVE_LEARN_METHOD_NAME = 'move learn method';
    const MOVE_LEARN_METHOD = NamedApiResource(MOVE_LEARN_METHOD_NAME, '');
    const POKEDEX_NAME = 'pokedex';
    const POKEDEX = NamedApiResource(POKEDEX_NAME, '');
    const REGION_NAME = 'region';
    const REGION = NamedApiResource(REGION_NAME, '');
    const VERSION_NAME = 'version';
    const VERSION = NamedApiResource(VERSION_NAME, '');

    test('fromMap', () {
      final v = VersionGroup.fromMap({
        VersionGroup.ID_FIELD_NAME: ID,
        VersionGroup.NAME_FIELD_NAME: NAME,
        VersionGroup.ORDER_FIELD_NAME: ORDER,
        VersionGroup.GENERATION_FIELD_NAME: GENERATION.toMap(),
        VersionGroup.MOVE_LEARN_METHODS_FIELD_NAME: [MOVE_LEARN_METHOD.toMap()],
        VersionGroup.POKEDEXS_FIELD_NAME: [POKEDEX.toMap()],
        VersionGroup.REGIONS_FIELD_NAME: [REGION.toMap()],
        VersionGroup.VERSIONS_FIELD_NAME: [VERSION.toMap()],
      });

      expect(v.id, equals(ID));
      expect(v.name, equals(NAME));
      expect(v.order, equals(ORDER));
      expect(v.generation.name, equals(GENERATION_NAME));
      expect(v.moveLearnMethods.length, equals(1));
      expect(v.moveLearnMethods.first.name, equals(MOVE_LEARN_METHOD_NAME));
      expect(v.pokedexes.length, equals(1));
      expect(v.pokedexes.first.name, equals(POKEDEX_NAME));
      expect(v.regions.length, equals(1));
      expect(v.regions.first.name, equals(REGION_NAME));
      expect(v.versions.length, equals(1));
      expect(v.versions.first.name, equals(VERSION_NAME));
    });

    test('toMap', () {
      final v = VersionGroup(ID, NAME, ORDER, GENERATION, [MOVE_LEARN_METHOD],
          [POKEDEX], [REGION], [VERSION]).toMap();

      expect(v[VersionGroup.ID_FIELD_NAME], equals(ID));
      expect(v[VersionGroup.NAME_FIELD_NAME], equals(NAME));
      expect(v[VersionGroup.ORDER_FIELD_NAME], equals(ORDER));
      expect(v[VersionGroup.GENERATION_FIELD_NAME], equals(GENERATION.toMap()));
      expect(v[VersionGroup.MOVE_LEARN_METHODS_FIELD_NAME],
          equals([MOVE_LEARN_METHOD.toMap()]));
      expect(v[VersionGroup.POKEDEXS_FIELD_NAME], equals([POKEDEX.toMap()]));
      expect(v[VersionGroup.REGIONS_FIELD_NAME], equals([REGION.toMap()]));
      expect(v[VersionGroup.VERSIONS_FIELD_NAME], equals([VERSION.toMap()]));
    });
  });
}
