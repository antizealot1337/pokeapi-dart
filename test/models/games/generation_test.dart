// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/games/generation.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('Generation', () {
    const ID = 1;
    const NAME = 'name';
    const ABILITY_NAME = 'ability';
    const ABILITY = NamedApiResource(ABILITY_NAME, '');
    const GENERATION_NAME = 'generation';
    const GENERATION = Name(GENERATION_NAME, NamedApiResource('', ''));
    const MAIN_REGION_NAME = 'region';
    const MAIN_REGION = NamedApiResource(MAIN_REGION_NAME, '');
    const MOVE_NAME = 'move';
    const MOVE = NamedApiResource(MOVE_NAME, '');
    const POKEMON_SPECIES_NAME = 'pokemon species';
    const POKEMON_SPECIES = NamedApiResource(POKEMON_SPECIES_NAME, '');
    const TYPE_NAME = 'types';
    const TYPE = NamedApiResource(TYPE_NAME, '');
    const VERSION_GROUP_NAME = 'version group';
    const VERSION_GROUP = NamedApiResource(VERSION_GROUP_NAME, '');

    test('fromMap', () {
      final g = Generation.fromMap({
        Generation.ID_FIELD_NAME: ID,
        Generation.NAME_FIELD_NAME: NAME,
        Generation.ABILITIES_FIELD_NAME: [ABILITY.toMap()],
        Generation.NAMES_FIELD_NAME: [GENERATION.toMap()],
        Generation.MAIN_REGION_FIELD_NAME: MAIN_REGION.toMap(),
        Generation.MOVES_FIELD_NAME: [MOVE.toMap()],
        Generation.POKEMON_SPECIES_FIELD_NAME: [POKEMON_SPECIES.toMap()],
        Generation.TYPES_FIELD_NAME: [TYPE.toMap()],
        Generation.VERSION_GROUPS_FIELD_NAME: [VERSION_GROUP.toMap()],
      });

      expect(g.id, equals(ID));
      expect(g.name, equals(NAME));
      expect(g.abilities.length, equals(1));
      expect(g.abilities.first.name, equals(ABILITY_NAME));
      expect(g.names.length, equals(1));
      expect(g.names.first.name, equals(GENERATION_NAME));
      expect(g.mainRegion.name, equals(MAIN_REGION_NAME));
      expect(g.moves.length, equals(1));
      expect(g.moves.first.name, equals(MOVE_NAME));
      expect(g.pokemonSpecies.length, equals(1));
      expect(g.pokemonSpecies.first.name, equals(POKEMON_SPECIES_NAME));
      expect(g.types.length, equals(1));
      expect(g.types.first.name, equals(TYPE_NAME));
      expect(g.versionGroups.first.name, equals(VERSION_GROUP_NAME));
    });

    test('toMap', () {
      final g = Generation(ID, NAME, [ABILITY], [GENERATION], MAIN_REGION,
          [MOVE], [POKEMON_SPECIES], [TYPE], [VERSION_GROUP]).toMap();

      expect(g[Generation.ID_FIELD_NAME], equals(ID));
      expect(g[Generation.NAME_FIELD_NAME], equals(NAME));
      expect(g[Generation.ABILITIES_FIELD_NAME], equals([ABILITY.toMap()]));
      expect(g[Generation.NAMES_FIELD_NAME], equals([GENERATION.toMap()]));
      expect(g[Generation.MAIN_REGION_FIELD_NAME], equals(MAIN_REGION.toMap()));
      expect(g[Generation.MOVES_FIELD_NAME], equals([MOVE.toMap()]));
      expect(g[Generation.POKEMON_SPECIES_FIELD_NAME],
          equals([POKEMON_SPECIES.toMap()]));
      expect(g[Generation.TYPES_FIELD_NAME], equals([TYPE.toMap()]));
      expect(g[Generation.VERSION_GROUPS_FIELD_NAME],
          equals([VERSION_GROUP.toMap()]));
    });
  });
}
