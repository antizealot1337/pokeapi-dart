// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/games/pokedex.dart';
import 'package:pokeapi/src/models/games/pokemon_entry.dart';
import 'package:pokeapi/src/models/utility/common/description.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart' hide Description;

void main() {
  group('Pokedex', () {
    const ID = 1;
    const NAME = 'name';
    const IS_MAIN_SERIES = true;
    const DESCRIPTION_DESCRIPTION = 'description';
    const DESCRIPTION =
        Description(DESCRIPTION_DESCRIPTION, NamedApiResource('', ''));
    const NAMES_NAME = 'names';
    const NAME_ = Name(NAMES_NAME, NamedApiResource('', ''));
    const ENTRY_NUMBER = 2;
    const ENTRY = PokemonEntry(ENTRY_NUMBER, NamedApiResource('', ''));
    const REGION_NAME = 'region';
    const REGION = NamedApiResource(REGION_NAME, '');
    const VERSION_GROUPS_NAME = 'version group';
    const VERSION_GROUP = NamedApiResource(VERSION_GROUPS_NAME, '');

    test('fromMap', () {
      final p = Pokedex.fromMap({
        Pokedex.ID_FIELD_NAME: ID,
        Pokedex.NAME_FIELD_NAME: NAME,
        Pokedex.IS_MAIN_SERIES_FIELD_NAME: IS_MAIN_SERIES,
        Pokedex.DESCRIPTIONS_FIELD_NAME: [DESCRIPTION.toMap()],
        Pokedex.NAMES_FIELD_NAME: [NAME_.toMap()],
        Pokedex.POKEMON_ENTRIES_FIELD_NAME: [ENTRY.toMap()],
        Pokedex.REGION_FIELD_NAME: REGION.toMap(),
        Pokedex.VERSION_GROUPS_FIELD_NAME: [VERSION_GROUP.toMap()],
      });

      expect(p.id, equals(ID));
      expect(p.name, equals(NAME));
      expect(p.isMainSeries, equals(IS_MAIN_SERIES));
      expect(p.descriptions.length, equals(1));
      expect(p.descriptions.first.description, equals(DESCRIPTION_DESCRIPTION));
      expect(p.names.length, equals(1));
      expect(p.names.first.name, equals(NAMES_NAME));
      expect(p.pokemonEntries.length, equals(1));
      expect(p.pokemonEntries.first.entryNumber, equals(ENTRY_NUMBER));
      expect(p.region.name, equals(REGION_NAME));
      expect(p.versionGroups.length, equals(1));
    });

    test('toMap', () {
      final p = Pokedex(ID, NAME, IS_MAIN_SERIES, [DESCRIPTION], [NAME_],
          [ENTRY], REGION, [VERSION_GROUP]).toMap();

      expect(p[Pokedex.ID_FIELD_NAME], equals(ID));
      expect(p[Pokedex.NAME_FIELD_NAME], equals(NAME));
      expect(p[Pokedex.IS_MAIN_SERIES_FIELD_NAME], equals(IS_MAIN_SERIES));
      expect(p[Pokedex.DESCRIPTIONS_FIELD_NAME], equals([DESCRIPTION.toMap()]));
      expect(p[Pokedex.NAMES_FIELD_NAME], equals([NAME_.toMap()]));
      expect(p[Pokedex.POKEMON_ENTRIES_FIELD_NAME], equals([ENTRY.toMap()]));
      expect(p[Pokedex.REGION_FIELD_NAME], equals(REGION.toMap()));
      expect(p[Pokedex.VERSION_GROUPS_FIELD_NAME],
          equals([VERSION_GROUP.toMap()]));
    });
  });
}
