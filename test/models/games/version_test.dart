// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/games/version.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('Version', () {
    const ID = 1;
    const NAME = 'name';
    const NAMES_NAME = 'names';
    const NAME_ = Name(NAMES_NAME, NamedApiResource('', ''));
    const VERSION_GROUP_NAME = 'version group';
    const VERSION_GROUP = NamedApiResource(VERSION_GROUP_NAME, '');

    test('fromMap', () {
      final v = Version.fromMap({
        Version.ID_FIELD_NAME: ID,
        Version.NAME_FIELD_NAME: NAME,
        Version.NAMES_FIELD_NAME: [NAME_.toMap()],
        Version.VERSION_GROUP_FIELD_NAME: VERSION_GROUP.toMap(),
      });

      expect(v.id, equals(ID));
      expect(v.name, equals(NAME));
      expect(v.names.length, equals(1));
      expect(v.names.first.name, equals(NAMES_NAME));
      expect(v.versionGroup.name, equals(VERSION_GROUP_NAME));
    });

    test('toMap', () {
      final v = Version(ID, NAME, [NAME_], VERSION_GROUP).toMap();

      expect(v[Version.ID_FIELD_NAME], equals(ID));
      expect(v[Version.NAME_FIELD_NAME], equals(NAME));
      expect(v[Version.NAMES_FIELD_NAME], equals([NAME_.toMap()]));
      expect(
          v[Version.VERSION_GROUP_FIELD_NAME], equals(VERSION_GROUP.toMap()));
    });
  });
}
