// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/berries/berry.dart';
import 'package:pokeapi/src/models/berries/berry_flavor_map.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('Berry', () {
    const ID = 1;
    const NAME = 'name';
    const GROWTH_TIME = 2;
    const MAX_HARVEST = 3;
    const NATURAL_GIFT_POWER = 4;
    const SIZE = 5;
    const SMOOTHNESS = 6;
    const SOIL_DRYNESS = 7;
    const FIRMNESS_NAME = 'firemness';
    const FIRMNESS = NamedApiResource(FIRMNESS_NAME, '');
    const FLAVOR_POTENCY = 8;
    const FLAVOR = BerryFlavorMap(FLAVOR_POTENCY, NamedApiResource('', ''));
    const ITEM_NAME = 'item';
    const ITEM = NamedApiResource(ITEM_NAME, '');
    const NATURAL_GIFT_TYPE_NAME = 'natuarl gift type';
    const NATURAL_GIFT_TYPE = NamedApiResource(NATURAL_GIFT_TYPE_NAME, '');

    test('fromMap', () {
      final b = Berry.fromMap({
        Berry.ID_FIELD_NAME: ID,
        Berry.NAME_FIELD_NAME: NAME,
        Berry.GROWTH_TIME_FIELD_NAME: GROWTH_TIME,
        Berry.MAX_HARVEST_FIELD_NAME: MAX_HARVEST,
        Berry.NATURAL_GIFT_POWER_FIELD_NAME: NATURAL_GIFT_POWER,
        Berry.SIZE_FIELD_NAME: SIZE,
        Berry.SMOOTHNESS_FIELD_NAME: SMOOTHNESS,
        Berry.SOIL_DRYNESS_FIELD_NAME: SOIL_DRYNESS,
        Berry.FIRMNESS_FIELD_NAME: FIRMNESS.toMap(),
        Berry.FLAVORS_FIELD_NAME: [FLAVOR.toMap()],
        Berry.ITEM_FIELD_NAME: ITEM.toMap(),
        Berry.NATURAL_GIFT_TYPE_FIELD_NAME: NATURAL_GIFT_TYPE.toMap(),
      });

      expect(b.id, equals(ID));
      expect(b.name, equals(NAME));
      expect(b.growthTime, equals(GROWTH_TIME));
      expect(b.maxHarvest, equals(MAX_HARVEST));
      expect(b.naturalGiftPower, equals(NATURAL_GIFT_POWER));
      expect(b.size, equals(SIZE));
      expect(b.smoothness, equals(SMOOTHNESS));
      expect(b.soilDryness, equals(SOIL_DRYNESS));
      expect(b.firmness.name, equals(FIRMNESS_NAME));
      expect(b.flavors.length, equals(1));
      expect(b.flavors.first.potency, equals(FLAVOR_POTENCY));
      expect(b.item.name, equals(ITEM_NAME));
      expect(b.naturalGiftType.name, equals(NATURAL_GIFT_TYPE_NAME));
    });

    test('toMap', () {
      final b = Berry(
              ID,
              NAME,
              GROWTH_TIME,
              MAX_HARVEST,
              NATURAL_GIFT_POWER,
              SIZE,
              SMOOTHNESS,
              FIRMNESS,
              [FLAVOR],
              SOIL_DRYNESS,
              ITEM,
              NATURAL_GIFT_TYPE)
          .toMap();

      expect(b[Berry.ID_FIELD_NAME], equals(ID));
      expect(b[Berry.NAME_FIELD_NAME], equals(NAME));
      expect(b[Berry.GROWTH_TIME_FIELD_NAME], equals(GROWTH_TIME));
      expect(b[Berry.MAX_HARVEST_FIELD_NAME], equals(MAX_HARVEST));
      expect(
          b[Berry.NATURAL_GIFT_POWER_FIELD_NAME], equals(NATURAL_GIFT_POWER));
      expect(b[Berry.SIZE_FIELD_NAME], equals(SIZE));
      expect(b[Berry.SMOOTHNESS_FIELD_NAME], equals(SMOOTHNESS));
      expect(b[Berry.FIRMNESS_FIELD_NAME], equals(FIRMNESS.toMap()));
      expect(b[Berry.SOIL_DRYNESS_FIELD_NAME], equals(SOIL_DRYNESS));
      expect(b[Berry.ITEM_FIELD_NAME], equals(ITEM.toMap()));
      expect(b[Berry.NATURAL_GIFT_TYPE_FIELD_NAME],
          equals(NATURAL_GIFT_TYPE.toMap()));
    });
  });
}
