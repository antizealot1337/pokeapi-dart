// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/berries/berry_flavor.dart';
import 'package:pokeapi/src/models/berries/flavor_berry_map.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('BerryFlavor', () {
    const ID = 1;
    const NAME = 'name';
    const BERRY_POTENCY = 2;
    const BERRY = FlavorBerryMap(BERRY_POTENCY, NamedApiResource('', ''));
    const CONTEST_TYPE_NAME = 'contest type';
    const CONTEST_TYPE = NamedApiResource(CONTEST_TYPE_NAME, '');
    const NAMES_NAME = 'names';
    const NAME_ = Name(NAMES_NAME, NamedApiResource('', ''));

    test('fromMap', () {
      final b = BerryFlavor.fromMap({
        BerryFlavor.ID_FIELD_NAME: ID,
        BerryFlavor.NAME_FIELD_NAME: NAME,
        BerryFlavor.BERRIES_FIELD_NAME: [BERRY.toMap()],
        BerryFlavor.CONTEST_TYPE_FIELD_NAME: CONTEST_TYPE.toMap(),
        BerryFlavor.NAMES_FIELD_NAME: [NAME_.toMap()],
      });

      expect(b.id, equals(ID));
      expect(b.name, equals(NAME));
      expect(b.berries.length, equals(1));
      expect(b.berries.first.potency, equals(BERRY_POTENCY));
      expect(b.contestType.name, equals(CONTEST_TYPE_NAME));
      expect(b.names.length, equals(1));
      expect(b.names.first.name, equals(NAMES_NAME));
    });

    test('toMap', () {
      final b = BerryFlavor(ID, NAME, [BERRY], CONTEST_TYPE, [NAME_]).toMap();

      expect(b[BerryFlavor.ID_FIELD_NAME], equals(ID));
      expect(b[BerryFlavor.NAME_FIELD_NAME], equals(NAME));
      expect(b[BerryFlavor.BERRIES_FIELD_NAME], equals([BERRY.toMap()]));
      expect(
          b[BerryFlavor.CONTEST_TYPE_FIELD_NAME], equals(CONTEST_TYPE.toMap()));
      expect(b[BerryFlavor.NAMES_FIELD_NAME], equals([NAME_.toMap()]));
    });
  });
}
