// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/berries/berry_flavor_map.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('BerryFlavorMap', () {
    const POTENCY = 1;
    const FLAVOR_NAME = 'flavor';
    const FLAVOR = NamedApiResource(FLAVOR_NAME, '');

    test('fromMap', () {
      final b = BerryFlavorMap.fromMap({
        BerryFlavorMap.POTENCY_FIELD_NAME: POTENCY,
        BerryFlavorMap.FLAVOR_FIELD_NAME: FLAVOR.toMap(),
      });

      expect(b.potency, equals(POTENCY));
      expect(b.flavor.name, equals(FLAVOR_NAME));
    });

    test('toMap', () {
      final b = BerryFlavorMap(POTENCY, FLAVOR).toMap();

      expect(b[BerryFlavorMap.POTENCY_FIELD_NAME], equals(POTENCY));
      expect(b[BerryFlavorMap.FLAVOR_FIELD_NAME], equals(FLAVOR.toMap()));
    });

    group('listFrom', () {
      test('null', () {
        final b = BerryFlavorMap.listFrom(null);
        expect(b.length, equals(0));
      });

      test('not null', () {
        final b = BerryFlavorMap.listFrom([
          {
            BerryFlavorMap.POTENCY_FIELD_NAME: POTENCY,
            BerryFlavorMap.FLAVOR_FIELD_NAME: FLAVOR.toMap(),
          },
        ]);

        expect(b.length, equals(1));
        expect(b.first.potency, equals(POTENCY));
        expect(b.first.flavor.name, equals(FLAVOR_NAME));
      });
    });
  });
}
