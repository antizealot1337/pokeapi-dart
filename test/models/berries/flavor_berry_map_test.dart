// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/berries/flavor_berry_map.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('FlavorBerryMap', () {
    const POTENCY = 1;
    const BERRY_NAME = 'berry';
    const BERRY = NamedApiResource(BERRY_NAME, '');

    test('fromMap', () {
      final f = FlavorBerryMap.fromMap({
        FlavorBerryMap.POTENCY_FIELD_NAME: POTENCY,
        FlavorBerryMap.BERRY_FIELD_NAME: BERRY.toMap(),
      });

      expect(f.potency, equals(POTENCY));
      expect(f.berry.name, equals(BERRY_NAME));
    });

    test('toMap', () {
      final f = FlavorBerryMap(POTENCY, BERRY).toMap();

      expect(f[FlavorBerryMap.POTENCY_FIELD_NAME], equals(POTENCY));
      expect(f[FlavorBerryMap.BERRY_FIELD_NAME], equals(BERRY.toMap()));
    });

    group('listFrom', () {
      test('null', () {
        final f = FlavorBerryMap.listFrom(null);
        expect(f, isEmpty);
      });

      test('not null', () {
        final f = FlavorBerryMap.listFrom([
          {
            FlavorBerryMap.POTENCY_FIELD_NAME: POTENCY,
            FlavorBerryMap.BERRY_FIELD_NAME: BERRY.toMap(),
          }
        ]);

        expect(f.first.potency, equals(POTENCY));
        expect(f.first.berry.name, equals(BERRY_NAME));
      });
    });
  });
}
