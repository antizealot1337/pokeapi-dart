// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/berries/berry_firmness.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('BerryFirmness', () {
    const ID = 1;
    const NAME = 'name';
    const BERRY_NAME = 'berry';
    const BERRY = NamedApiResource(BERRY_NAME, '');
    const NAMES_NAME = 'names';
    const NAME_ = Name(NAMES_NAME, NamedApiResource('', ''));

    test('fromMap', () {
      final b = BerryFirmness.fromMap({
        BerryFirmness.ID_FIELD_NAME: ID,
        BerryFirmness.NAME_FIELD_NAME: NAME,
        BerryFirmness.BERRIES_FIELD_NAME: [BERRY.toMap()],
        BerryFirmness.NAMES_FIELD_NAME: [NAME_.toMap()],
      });

      expect(b.id, equals(ID));
      expect(b.name, equals(NAME));
      expect(b.berries.length, equals(1));
      expect(b.berries.first.name, equals(BERRY_NAME));
      expect(b.names.length, equals(1));
      expect(b.names.first.name, equals(NAMES_NAME));
    });

    test('toMap', () {
      final b = BerryFirmness(ID, NAME, [BERRY], [NAME_]).toMap();

      expect(b[BerryFirmness.ID_FIELD_NAME], equals(ID));
      expect(b[BerryFirmness.NAME_FIELD_NAME], equals(NAME));
      expect(b[BerryFirmness.BERRIES_FIELD_NAME], equals([BERRY.toMap()]));
      expect(b[BerryFirmness.NAMES_FIELD_NAME], equals([NAME_.toMap()]));
    });
  });
}
