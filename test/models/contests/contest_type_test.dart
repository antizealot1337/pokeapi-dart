// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/contests/contest_name.dart';
import 'package:pokeapi/src/models/contests/contest_type.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('ContestType', () {
    const ID = 1;
    const NAME = 'name';
    const BERRY_FLAVOR_NAME = 'berry';
    const BERRY_FLAVOR = NamedApiResource(BERRY_FLAVOR_NAME, '');
    const CONTEST_NAME_NAME = 'contest name';
    const CONTEST_NAME =
        ContestName(CONTEST_NAME_NAME, '', NamedApiResource('', ''));

    test('fromMap', () {
      final c = ContestType.fromMap({
        ContestType.ID_FIELD_NAME: ID,
        ContestType.NAME_FIELD_NAME: NAME,
        ContestType.BERRY_FLAVOR_FIELD_NAME: BERRY_FLAVOR.toMap(),
        ContestType.NAMES_FIELD_NAME: [CONTEST_NAME.toMap()],
      });

      expect(c.id, equals(ID));
      expect(c.name, equals(NAME));
      expect(c.berryFlavor.name, equals(BERRY_FLAVOR_NAME));
      expect(c.names.length, equals(1));
      expect(c.names.first.name, equals(CONTEST_NAME_NAME));
    });

    test('toMap', () {
      final c = ContestType(ID, NAME, BERRY_FLAVOR, [CONTEST_NAME]).toMap();

      expect(c[ContestType.ID_FIELD_NAME], equals(ID));
      expect(c[ContestType.NAME_FIELD_NAME], equals(NAME));
      expect(
          c[ContestType.BERRY_FLAVOR_FIELD_NAME], equals(BERRY_FLAVOR.toMap()));
      expect(c[ContestType.NAMES_FIELD_NAME], equals([CONTEST_NAME.toMap()]));
    });
  });
}
