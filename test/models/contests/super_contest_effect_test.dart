// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/contests/super_contest_effect.dart';
import 'package:pokeapi/src/models/utility/common/flavor_text.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('SuperContestEffect', () {
    const ID = 1;
    const APPEAL = 2;
    const FLAVOR_TEXT_TEXT = 'flavor text';
    const FLAVOR_TEXT = FlavorText(
        FLAVOR_TEXT_TEXT, NamedApiResource('', ''), NamedApiResource('', ''));
    const MOVE_NAME = 'move';
    const MOVE = NamedApiResource(MOVE_NAME, '');

    test('fromMap', () {
      final s = SuperContestEffect.fromMap({
        SuperContestEffect.ID_FIELD_NAME: ID,
        SuperContestEffect.APPEAL_FIELD_NAME: APPEAL,
        SuperContestEffect.FLAVOR_TEXT_ENTRIES_FIELD_NAME: [
          FLAVOR_TEXT.toMap()
        ],
        SuperContestEffect.MOVES_FIELD_NAME: [MOVE.toMap()],
      });

      expect(s.id, equals(ID));
      expect(s.appeal, equals(APPEAL));
      expect(s.flavorTextEntries.length, equals(1));
      expect(s.flavorTextEntries.first.flavorText, equals(FLAVOR_TEXT_TEXT));
      expect(s.moves.length, equals(1));
      expect(s.moves.first.name, equals(MOVE_NAME));
    });

    test('toMap', () {
      final s = SuperContestEffect(ID, APPEAL, [FLAVOR_TEXT], [MOVE]).toMap();

      expect(s[SuperContestEffect.ID_FIELD_NAME], equals(ID));
      expect(s[SuperContestEffect.APPEAL_FIELD_NAME], equals(APPEAL));
      expect(s[SuperContestEffect.FLAVOR_TEXT_ENTRIES_FIELD_NAME],
          equals([FLAVOR_TEXT.toMap()]));
      expect(s[SuperContestEffect.MOVES_FIELD_NAME], equals([MOVE.toMap()]));
    });
  });
}
