// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/contests/contest_effect.dart';
import 'package:pokeapi/src/models/utility/common/effect.dart';
import 'package:pokeapi/src/models/utility/common/flavor_text.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('ContestEffect', () {
    const ID = 1;
    const APPEAL = 2;
    const JAM = 3;
    const EFFECT_NAME = 'effect';
    const EFFECT = Effect(EFFECT_NAME, NamedApiResource('', ''));
    const FLAVOR_TEXT_TEXT = 'flavor text';
    const FLAVOR_TEXT = FlavorText(
        FLAVOR_TEXT_TEXT, NamedApiResource('', ''), NamedApiResource('', ''));

    test('fromMap', () {
      final c = ContestEffect.fromMap({
        ContestEffect.ID_FIELD_NAME: ID,
        ContestEffect.APPEAL_FIELD_NAME: APPEAL,
        ContestEffect.JAM_FIELD_NAME: JAM,
        ContestEffect.EFFECT_ENTRIES_FIELD_NAME: [EFFECT.toMap()],
        ContestEffect.FLAVOR_TEXT_ENTRIES_FIELD_NAME: [FLAVOR_TEXT.toMap()],
      });

      expect(c.id, equals(ID));
      expect(c.appeal, equals(APPEAL));
      expect(c.jam, equals(JAM));
      expect(c.effectEntries.length, equals(1));
      expect(c.effectEntries.first.effect, equals(EFFECT_NAME));
      expect(c.flavorTextEntries.length, equals(1));
      expect(c.flavorTextEntries.first.flavorText, equals(FLAVOR_TEXT_TEXT));
    });

    test('toMap', () {
      final c = ContestEffect(ID, APPEAL, JAM, [EFFECT], [FLAVOR_TEXT]).toMap();

      expect(c[ContestEffect.ID_FIELD_NAME], equals(ID));
      expect(c[ContestEffect.APPEAL_FIELD_NAME], equals(APPEAL));
      expect(c[ContestEffect.JAM_FIELD_NAME], equals(JAM));
      expect(
          c[ContestEffect.EFFECT_ENTRIES_FIELD_NAME], equals([EFFECT.toMap()]));
      expect(c[ContestEffect.FLAVOR_TEXT_ENTRIES_FIELD_NAME],
          equals([FLAVOR_TEXT.toMap()]));
    });
  });
}
