// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/contests/contest_name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('ContestName', () {
    const NAME = 'name';
    const COLOR = 'color';
    const LANGUAGE_NAME = 'language';
    const LANGUAGE = NamedApiResource(LANGUAGE_NAME, '');

    test('fromMap', () {
      final c = ContestName.fromMap({
        ContestName.NAME_FIELD_NAME: NAME,
        ContestName.COLOR_FIELD_NAME: COLOR,
        ContestName.LANGUAGE_FIELD_NAME: LANGUAGE.toMap(),
      });

      expect(c.name, equals(NAME));
      expect(c.color, equals(COLOR));
      expect(c.language.name, equals(LANGUAGE_NAME));
    });

    test('toMap', () {
      final c = ContestName(NAME, COLOR, LANGUAGE).toMap();

      expect(c[ContestName.NAME_FIELD_NAME], equals(NAME));
      expect(c[ContestName.COLOR_FIELD_NAME], equals(COLOR));
      expect(c[ContestName.LANGUAGE_FIELD_NAME], equals(LANGUAGE.toMap()));
    });

    group('listFrom', () {
      test('null', () {
        final c = ContestName.listFrom(null);
        expect(c, isEmpty);
      });

      test('not null', () {
        final c = ContestName.listFrom([
          {
            ContestName.NAME_FIELD_NAME: NAME,
            ContestName.COLOR_FIELD_NAME: COLOR,
            ContestName.LANGUAGE_FIELD_NAME: LANGUAGE.toMap(),
          },
        ]);

        expect(c.first.name, equals(NAME));
        expect(c.first.color, equals(COLOR));
        expect(c.first.language.name, equals(LANGUAGE_NAME));
      });
    });
  });
}
