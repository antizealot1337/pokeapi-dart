// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/machines/machine.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('Machine', () {
    const ID = 1;
    const ITEM_NAME = 'item';
    const ITEM = NamedApiResource(ITEM_NAME, '');
    const MOVE_NAME = 'move';
    const MOVE = NamedApiResource(MOVE_NAME, '');
    const VERSION_GROUP_NAME = 'version group';
    const VERSION_GROUP = NamedApiResource(VERSION_GROUP_NAME, '');

    test('fromMap', () {
      final m = Machine.fromMap({
        Machine.ID_FIELD_NAME: ID,
        Machine.ITEM_FIELD_NAME: ITEM.toMap(),
        Machine.MOVE_FIELD_NAME: MOVE.toMap(),
        Machine.VERSION_GROUP_FIELD_NAME: VERSION_GROUP.toMap(),
      });

      expect(m.id, equals(ID));
      expect(m.item.name, equals(ITEM_NAME));
      expect(m.move.name, equals(MOVE_NAME));
      expect(m.versionGroup.name, equals(VERSION_GROUP_NAME));
    });

    test('toMap', () {
      final m = Machine(ID, ITEM, MOVE, VERSION_GROUP).toMap();

      expect(m[Machine.ID_FIELD_NAME], equals(ID));
      expect(m[Machine.ITEM_FIELD_NAME], equals(ITEM.toMap()));
      expect(m[Machine.MOVE_FIELD_NAME], equals(MOVE.toMap()));
      expect(
          m[Machine.VERSION_GROUP_FIELD_NAME], equals(VERSION_GROUP.toMap()));
    });
  });
}
