// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/moves/move_damage_class.dart';
import 'package:pokeapi/src/models/utility/common/description.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart' hide Description;

void main() {
  group('MoveDamageClass', () {
    const ID = 1;
    const NAME = 'name';
    const DESCRIPTIONS_DESCRIPTION = 'description';
    const DESCRIPTION =
        Description(DESCRIPTIONS_DESCRIPTION, NamedApiResource('', ''));
    const MOVES_NAME = 'moves';
    const MOVE = NamedApiResource(MOVES_NAME, '');
    const NAMES_NAME = 'names';
    const NAME_ = Name(NAMES_NAME, NamedApiResource('', ''));

    test('fromMap', () {
      final m = MoveDamageClass.fromMap({
        MoveDamageClass.ID_FIELD_NAME: ID,
        MoveDamageClass.NAME_FIELD_NAME: NAME,
        MoveDamageClass.DESCRIPTIONS_FIELD_NAME: [DESCRIPTION.toMap()],
        MoveDamageClass.MOVES_FIELD_NAME: [MOVE.toMap()],
        MoveDamageClass.NAMES_FIELD_NAME: [NAME_.toMap()],
      });

      expect(m.id, equals(ID));
      expect(m.name, equals(NAME));
      expect(m.descriptions.length, equals(1));
      expect(
          m.descriptions.first.description, equals(DESCRIPTIONS_DESCRIPTION));
      expect(m.moves.length, equals(1));
      expect(m.moves.first.name, equals(MOVES_NAME));
      expect(m.names.length, equals(1));
      expect(m.names.first.name, equals(NAMES_NAME));
    });

    test('toMap', () {
      final m =
          MoveDamageClass(ID, NAME, [DESCRIPTION], [MOVE], [NAME_]).toMap();

      expect(m[MoveDamageClass.ID_FIELD_NAME], equals(ID));
      expect(m[MoveDamageClass.NAME_FIELD_NAME], equals(NAME));
      expect(m[MoveDamageClass.DESCRIPTIONS_FIELD_NAME],
          equals([DESCRIPTION.toMap()]));
      expect(m[MoveDamageClass.MOVES_FIELD_NAME], equals([MOVE.toMap()]));
      expect(m[MoveDamageClass.NAMES_FIELD_NAME], equals([NAME_.toMap()]));
    });
  });
}
