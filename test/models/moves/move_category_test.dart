// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/moves/move_category.dart';
import 'package:pokeapi/src/models/utility/common/description.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart' hide Description;

void main() {
  group('MoveCategory', () {
    const ID = 1;
    const NAME = 'name';
    const MOVES_NAME = 'moves';
    const MOVE = NamedApiResource(MOVES_NAME, '');
    const DESCRIPTIONS_DESCRIPTION = 'descriptions';
    const DESCRIPTION =
        Description(DESCRIPTIONS_DESCRIPTION, NamedApiResource('', ''));

    test('fromMap', () {
      final m = MoveCategory.fromMap({
        MoveCategory.ID_FIELD_NAME: ID,
        MoveCategory.NAME_FIELD_NAME: NAME,
        MoveCategory.MOVES_FIELD_NAME: [MOVE.toMap()],
        MoveCategory.DESCRIPTIONS_FIELD_NAME: [DESCRIPTION.toMap()],
      });

      expect(m.id, equals(ID));
      expect(m.name, equals(NAME));
      expect(m.moves.length, equals(1));
      expect(m.moves.first.name, equals(MOVES_NAME));
      expect(m.descriptions.length, equals(1));
      expect(
          m.descriptions.first.description, equals(DESCRIPTIONS_DESCRIPTION));
    });

    test('toMap', () {
      final m = MoveCategory(ID, NAME, [MOVE], [DESCRIPTION]).toMap();

      expect(m[MoveCategory.ID_FIELD_NAME], equals(ID));
      expect(m[MoveCategory.NAME_FIELD_NAME], equals(NAME));
      expect(m[MoveCategory.MOVES_FIELD_NAME], equals([MOVE.toMap()]));
      expect(m[MoveCategory.DESCRIPTIONS_FIELD_NAME],
          equals([DESCRIPTION.toMap()]));
    });
  });
}
