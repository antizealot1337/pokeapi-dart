// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/moves/contest_combo_detail.dart';
import 'package:pokeapi/src/models/moves/contest_combo_set.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('ContestComboSet', () {
    const NORMAL_USE_BEFORE_NAME = 'normal use before';
    const NORMAL =
        ContestComboDetail([NamedApiResource(NORMAL_USE_BEFORE_NAME, '')], []);
    const SUPER_USE_BEFORE_NAME = 'super use before';
    const SUPER =
        ContestComboDetail([NamedApiResource(SUPER_USE_BEFORE_NAME, '')], []);

    test('fromMap', () {
      final c = ContestComboSet.fromMap({
        ContestComboSet.NORMAL_FIELD_NAME: NORMAL.toMap(),
        ContestComboSet.SUPER_FIELD_NAME: SUPER.toMap(),
      });

      expect(c.normal.useBefore.length, equals(1));
      expect(c.normal.useBefore.first.name, equals(NORMAL_USE_BEFORE_NAME));
      expect(c.super_.useBefore.length, equals(1));
      expect(c.super_.useBefore.first.name, equals(SUPER_USE_BEFORE_NAME));
    });

    test('toMap', () {
      final c = ContestComboSet(NORMAL, SUPER).toMap();

      expect(c[ContestComboSet.NORMAL_FIELD_NAME], equals(NORMAL.toMap()));
      expect(c[ContestComboSet.SUPER_FIELD_NAME], equals(SUPER.toMap()));
    });
  });
}
