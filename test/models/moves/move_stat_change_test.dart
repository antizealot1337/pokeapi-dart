// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/moves/move_stat_change.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('MoveStatChange', () {
    const CHANGE = 1;
    const STAT_NAME = 'stat';
    const STAT = NamedApiResource(STAT_NAME, '');

    test('fromMap', () {
      final m = MoveStatChange.fromMap({
        MoveStatChange.CHANGE_FIELD_NAME: CHANGE,
        MoveStatChange.STAT_FIELD_NAME: STAT.toMap(),
      });

      expect(m.change, equals(CHANGE));
      expect(m.stat.name, equals(STAT_NAME));
    });

    test('toMap', () {
      final m = MoveStatChange(CHANGE, STAT).toMap();

      expect(m[MoveStatChange.CHANGE_FIELD_NAME], equals(CHANGE));
      expect(m[MoveStatChange.STAT_FIELD_NAME], equals(STAT.toMap()));
    });

    group('listFrom', () {
      test('null', () {
        final m = MoveStatChange.listFrom(null);
        expect(m, isEmpty);
      });

      test('not null', () {
        final m = MoveStatChange.listFrom([
          {
            MoveStatChange.CHANGE_FIELD_NAME: CHANGE,
            MoveStatChange.STAT_FIELD_NAME: STAT.toMap(),
          },
        ]);

        expect(m.first.change, equals(CHANGE));
        expect(m.first.stat.name, equals(STAT_NAME));
      });
    });
  });
}
