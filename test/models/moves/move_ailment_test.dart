// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/moves/move_ailment.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('MoveAilment', () {
    const ID = 1;
    const NAME = 'name';
    const MOVES_NAME = 'moves';
    const MOVE = NamedApiResource(MOVES_NAME, '');
    const NAMES_NAME = 'names';
    const NAME_ = Name(NAMES_NAME, NamedApiResource('', ''));

    test('fromMap', () {
      final m = MoveAilment.fromMap({
        MoveAilment.ID_FIELD_NAME: ID,
        MoveAilment.NAME_FIELD_NAME: NAME,
        MoveAilment.MOVES_FIELD_NAME: [MOVE.toMap()],
        MoveAilment.NAMES_FIELD_NAME: [NAME_.toMap()],
      });

      expect(m.id, equals(ID));
      expect(m.name, equals(NAME));
      expect(m.moves.length, equals(1));
      expect(m.moves.first.name, equals(MOVES_NAME));
      expect(m.names.length, equals(1));
      expect(m.names.first.name, equals(NAMES_NAME));
    });

    test('toMap', () {
      final m = MoveAilment(ID, NAME, [MOVE], [NAME_]).toMap();

      expect(m[MoveAilment.ID_FIELD_NAME], equals(ID));
      expect(m[MoveAilment.NAME_FIELD_NAME], equals(NAME));
      expect(m[MoveAilment.MOVES_FIELD_NAME], equals([MOVE.toMap()]));
      expect(m[MoveAilment.NAMES_FIELD_NAME], equals([NAME_.toMap()]));
    });
  });
}
