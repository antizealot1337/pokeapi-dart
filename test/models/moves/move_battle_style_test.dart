// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/moves/move_battle_style.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('MoveBattleStyle', () {
    const ID = 1;
    const NAME = 'name';
    const NAMES_NAME = 'names';
    const NAME_ = Name(NAMES_NAME, NamedApiResource('', ''));

    test('fromMap', () {
      final m = MoveBattleStyle.fromMap({
        MoveBattleStyle.ID_FIELD_NAME: ID,
        MoveBattleStyle.NAME_FIELD_NAME: NAME,
        MoveBattleStyle.NAMES_FIELD_NAME: [NAME_.toMap()],
      });

      expect(m.id, equals(ID));
      expect(m.name, equals(NAME));
      expect(m.names.length, equals(1));
      expect(m.names.first.name, equals(NAMES_NAME));
    });

    test('toMap', () {
      final m = MoveBattleStyle(ID, NAME, [NAME_]).toMap();

      expect(m[MoveBattleStyle.ID_FIELD_NAME], equals(ID));
      expect(m[MoveBattleStyle.NAME_FIELD_NAME], equals(NAME));
      expect(m[MoveBattleStyle.NAMES_FIELD_NAME], equals([NAME_.toMap()]));
    });
  });
}
