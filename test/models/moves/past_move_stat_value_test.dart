// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/moves/past_move_stat_value.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:pokeapi/src/models/utility/common/verbose_effect.dart';
import 'package:test/test.dart';

void main() {
  group('PastMoveStatValue', () {
    const ACCURACY = 1;
    const EFFECT_CHANCE = 2;
    const POWER = 3;
    const PP = 4;
    const EFFECT_ENTRIES_EFFECT = 'effect';
    const EFFECT_ENTRY =
        VerboseEffect(EFFECT_ENTRIES_EFFECT, '', NamedApiResource('', ''));
    const TYPE_NAME = 'type';
    const TYPE = NamedApiResource(TYPE_NAME, '');
    const VERSION_GROUP_NAME = 'version group';
    const VERSION_GROUP = NamedApiResource(VERSION_GROUP_NAME, '');

    test('fromMap', () {
      final p = PastMoveStatValue.fromMap({
        PastMoveStatValue.ACCURACY_FIELD_NAME: ACCURACY,
        PastMoveStatValue.EFFECT_CHANCE_FIELD_NAME: EFFECT_CHANCE,
        PastMoveStatValue.POWER_FIELD_NAME: POWER,
        PastMoveStatValue.PP_FIELD_NAME: PP,
        PastMoveStatValue.EFFECT_ENTRIES_FIELD_NAME: [EFFECT_ENTRY.toMap()],
        PastMoveStatValue.TYPE_FIELD_NAME: TYPE.toMap(),
        PastMoveStatValue.VERSION_GROUP_FIELD_NAME: VERSION_GROUP.toMap(),
      });

      expect(p.accuracy, equals(ACCURACY));
      expect(p.effectChance, equals(EFFECT_CHANCE));
      expect(p.power, equals(POWER));
      expect(p.pp, equals(PP));
      expect(p.effectEntries.length, equals(1));
      expect(p.effectEntries.first.effect, equals(EFFECT_ENTRIES_EFFECT));
      expect(p.type.name, equals(TYPE_NAME));
      expect(p.versionGroup.name, equals(VERSION_GROUP_NAME));
    });

    test('toMap', () {
      final p = PastMoveStatValue(ACCURACY, EFFECT_CHANCE, POWER, PP,
              [EFFECT_ENTRY], TYPE, VERSION_GROUP)
          .toMap();

      expect(p[PastMoveStatValue.ACCURACY_FIELD_NAME], equals(ACCURACY));
      expect(
          p[PastMoveStatValue.EFFECT_CHANCE_FIELD_NAME], equals(EFFECT_CHANCE));
      expect(p[PastMoveStatValue.POWER_FIELD_NAME], equals(POWER));
      expect(p[PastMoveStatValue.PP_FIELD_NAME], equals(PP));
      expect(p[PastMoveStatValue.EFFECT_ENTRIES_FIELD_NAME],
          equals([EFFECT_ENTRY.toMap()]));
      expect(p[PastMoveStatValue.TYPE_FIELD_NAME], equals(TYPE.toMap()));
      expect(p[PastMoveStatValue.VERSION_GROUP_FIELD_NAME],
          equals(VERSION_GROUP.toMap()));
    });

    group('listFrom', () {
      test('null', () {
        final p = PastMoveStatValue.listFrom(null);
        expect(p, isEmpty);
      });

      test('not null', () {
        final p = PastMoveStatValue.listFrom([
          {
            PastMoveStatValue.ACCURACY_FIELD_NAME: ACCURACY,
            PastMoveStatValue.EFFECT_CHANCE_FIELD_NAME: EFFECT_CHANCE,
            PastMoveStatValue.POWER_FIELD_NAME: POWER,
            PastMoveStatValue.PP_FIELD_NAME: PP,
            PastMoveStatValue.EFFECT_ENTRIES_FIELD_NAME: [EFFECT_ENTRY.toMap()],
            PastMoveStatValue.TYPE_FIELD_NAME: TYPE.toMap(),
            PastMoveStatValue.VERSION_GROUP_FIELD_NAME: VERSION_GROUP.toMap(),
          },
        ]);

        expect(p.first.accuracy, equals(ACCURACY));
        expect(p.first.effectChance, equals(EFFECT_CHANCE));
        expect(p.first.power, equals(POWER));
        expect(p.first.pp, equals(PP));
        expect(p.first.effectEntries.length, equals(1));
        expect(
            p.first.effectEntries.first.effect, equals(EFFECT_ENTRIES_EFFECT));
        expect(p.first.type.name, equals(TYPE_NAME));
        expect(p.first.versionGroup.name, equals(VERSION_GROUP_NAME));
      });
    });
  });
}
