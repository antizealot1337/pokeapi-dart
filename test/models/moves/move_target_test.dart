// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/moves/move_target.dart';
import 'package:pokeapi/src/models/utility/common/description.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart' hide Description;

void main() {
  group('MoveTarget', () {
    const ID = 1;
    const NAME = 'name';
    const DESCRIPTIONS_DESCRIPTION = 'description';
    const DESCRIPTION =
        Description(DESCRIPTIONS_DESCRIPTION, NamedApiResource('', ''));
    const MOVES_NAME = 'moves';
    const MOVE = NamedApiResource(MOVES_NAME, '');
    const NAMES_NAME = 'names';
    const NAME_ = Name(NAMES_NAME, NamedApiResource('', ''));

    test('fromMap', () {
      final m = MoveTarget.fromMap({
        MoveTarget.ID_FIELD_NAME: ID,
        MoveTarget.NAME_FIELD_NAME: NAME,
        MoveTarget.DESCRIPTIONS_FIELD_NAME: [DESCRIPTION.toMap()],
        MoveTarget.MOVES_FIELD_NAME: [MOVE.toMap()],
        MoveTarget.NAMES_FIELD_NAME: [NAME_.toMap()],
      });

      expect(m.id, equals(ID));
      expect(m.name, equals(NAME));
      expect(m.descriptions.length, equals(1));
      expect(
          m.descriptions.first.description, equals(DESCRIPTIONS_DESCRIPTION));
      expect(m.moves.length, equals(1));
      expect(m.moves.first.name, equals(MOVES_NAME));
      expect(m.names.length, equals(1));
      expect(m.names.first.name, equals(NAMES_NAME));
    });

    test('toMap', () {
      final m = MoveTarget(ID, NAME, [DESCRIPTION], [MOVE], [NAME_]).toMap();

      expect(m[MoveTarget.ID_FIELD_NAME], equals(ID));
      expect(m[MoveTarget.NAME_FIELD_NAME], equals(NAME));
      expect(
          m[MoveTarget.DESCRIPTIONS_FIELD_NAME], equals([DESCRIPTION.toMap()]));
      expect(m[MoveTarget.MOVES_FIELD_NAME], equals([MOVE.toMap()]));
      expect(m[MoveTarget.NAMES_FIELD_NAME], equals([NAME_.toMap()]));
    });
  });
}
