// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/moves/contest_combo_detail.dart';
import 'package:pokeapi/src/models/moves/contest_combo_set.dart';
import 'package:pokeapi/src/models/moves/move.dart';
import 'package:pokeapi/src/models/moves/move_flavor_text.dart';
import 'package:pokeapi/src/models/moves/move_meta_data.dart';
import 'package:pokeapi/src/models/moves/move_stat_change.dart';
import 'package:pokeapi/src/models/moves/past_move_stat_value.dart';
import 'package:pokeapi/src/models/pokemon/ability_effect_change.dart';
import 'package:pokeapi/src/models/utility/common/api_resource.dart';
import 'package:pokeapi/src/models/utility/common/machine_version_detail.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:pokeapi/src/models/utility/common/verbose_effect.dart';
import 'package:test/test.dart';

void main() {
  group('Move', () {
    const ID = 1;
    const NAME = 'name';
    const ACCURACY = 2;
    const EFFECT_CHANCE = 3;
    const PP = 4;
    const PRIORITY = 5;
    const POWER = 6;
    const CONTEST_COMBOS_NORMAL_USE_BEFORE_NAME =
        'normal contest combo use before';
    const CONTEST_COMBO = ContestComboSet(
        ContestComboDetail(
            [NamedApiResource(CONTEST_COMBOS_NORMAL_USE_BEFORE_NAME, '')], []),
        ContestComboDetail([], []));
    const CONTEST_TYPE_NAME = 'contest type';
    const CONTEST_TYPE = NamedApiResource(CONTEST_TYPE_NAME, '');
    const CONTEST_EFFECT_URL = 'contest effect';
    const CONTEST_EFFECT = ApiResource(CONTEST_EFFECT_URL);
    const DAMAGE_CLASS_NAME = 'damage class';
    const DAMAGE_CLASS = NamedApiResource(DAMAGE_CLASS_NAME, '');
    const EFFECT_ENTRIES_EFFECT = 'effect entries effect';
    const EFFECT_ENTRY =
        VerboseEffect(EFFECT_ENTRIES_EFFECT, '', NamedApiResource('', ''));
    const EFFECT_CHANGES_VERSION_GROUP_NAME = 'effect changes version group';
    const EFFECT_CHANGE = AbilityEffectChange(
        [], NamedApiResource(EFFECT_CHANGES_VERSION_GROUP_NAME, ''));
    const FLAVOR_TEXT_ENTRIES_FLAVOR_TEXT = 'flavor text entries flavor text';
    const FLAVOR_TEXT_ENTRY = MoveFlavorText(FLAVOR_TEXT_ENTRIES_FLAVOR_TEXT,
        NamedApiResource('', ''), NamedApiResource('', ''));
    const GENERATION_NAME = 'generation';
    const GENERATION = NamedApiResource(GENERATION_NAME, '');
    const MACHINES_URL = 'machines url';
    const MACHINE = MachineVersionDetail(
        ApiResource(MACHINES_URL), NamedApiResource('', ''));
    const MOVE_META_DATA_ALIMENT_NAME = 'move meta data aliment';
    const META = MoveMetaData(NamedApiResource(MOVE_META_DATA_ALIMENT_NAME, ''),
        NamedApiResource('', ''), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
    const NAMES_NAME = 'names';
    const NAME_ = Name(NAMES_NAME, NamedApiResource('', ''));
    const PAST_MOVE_ACCURACY = 7;
    const PAST_MOVE = PastMoveStatValue(PAST_MOVE_ACCURACY, EFFECT_CHANCE, 0, 0,
        [], NamedApiResource('', ''), NamedApiResource('', ''));
    const MOVE_STAT_CHANGE = 8;
    const STAT_CHANGE =
        MoveStatChange(MOVE_STAT_CHANGE, NamedApiResource('', ''));
    const SUPER_CONTEST_EFFECT_URL = 'super contest effect url';
    const SUPER_CONTEST_EFFECT = ApiResource(SUPER_CONTEST_EFFECT_URL);
    const TARGET_NAME = 'target';
    const TARGET = NamedApiResource(TARGET_NAME, '');
    const TYPE_NAME = 'type';
    const TYPE = NamedApiResource(TYPE_NAME, '');

    test('fromMap', () {
      final m = Move.fromMap({
        Move.ID_FIELD_NAME: ID,
        Move.NAME_FIELD_NAME: NAME,
        Move.ACCURACY_FIELD_NAME: ACCURACY,
        Move.EFFECT_CHANCE_FIELD_NAME: EFFECT_CHANCE,
        Move.PP_FIELD_NAME: PP,
        Move.PRIORITY_FIELD_NAME: PRIORITY,
        Move.POWER_FIELD_NAME: POWER,
        Move.CONTEST_COMBOS_FIELD_NAME: CONTEST_COMBO.toMap(),
        Move.CONTEST_TYPE_FIELD_NAME: CONTEST_TYPE.toMap(),
        Move.CONTEST_EFFECT_FIELD_NAME: CONTEST_EFFECT.toMap(),
        Move.DAMAGE_CLASS_FIELD_NAME: DAMAGE_CLASS.toMap(),
        Move.EFFECT_ENTRIES_FIELD_NAME: [EFFECT_ENTRY.toMap()],
        Move.EFFECT_CHANGES_FIELD_NAME: [EFFECT_CHANGE.toMap()],
        Move.FLAVOR_TEXT_ENTRIES_FIELD_NAME: [FLAVOR_TEXT_ENTRY.toMap()],
        Move.GENERATION_FIELD_NAME: GENERATION.toMap(),
        Move.MACHINES_FIELD_NAME: [MACHINE.toMap()],
        Move.META_FIELD_NAME: META.toMap(),
        Move.NAMES_FIELD_NAME: [NAME_.toMap()],
        Move.PAST_VALUES_FIELD_NAME: [PAST_MOVE.toMap()],
        Move.STAT_CHANGES_FIELD_NAME: [STAT_CHANGE.toMap()],
        Move.SUPER_CONTEST_EFFECT_FIELD_NAME: SUPER_CONTEST_EFFECT.toMap(),
        Move.TARGET_FIELD_NAME: TARGET.toMap(),
        Move.TYPE_FIELD_NAME: TYPE.toMap(),
      });

      expect(m.id, equals(ID));
      expect(m.name, equals(NAME));
      expect(m.accuracy, equals(ACCURACY));
      expect(m.effectChance, equals(EFFECT_CHANCE));
      expect(m.pp, equals(PP));
      expect(m.priority, equals(PRIORITY));
      expect(m.power, equals(POWER));
      expect(m.contestCombos.normal.useBefore.first.name,
          equals(CONTEST_COMBOS_NORMAL_USE_BEFORE_NAME));
      expect(m.contestType.name, equals(CONTEST_TYPE_NAME));
      expect(m.contestEffect.url, equals(CONTEST_EFFECT_URL));
      expect(m.damageClass.name, equals(DAMAGE_CLASS_NAME));
      expect(m.effectEntries.length, equals(1));
      expect(m.effectEntries.first.effect, equals(EFFECT_ENTRIES_EFFECT));
      expect(m.effectChanges.length, equals(1));
      expect(m.effectChanges.first.versionGroup.name,
          equals(EFFECT_CHANGES_VERSION_GROUP_NAME));
      expect(m.flavorTextEntries.length, equals(1));
      expect(m.flavorTextEntries.first.flavorText,
          equals(FLAVOR_TEXT_ENTRIES_FLAVOR_TEXT));
      expect(m.generation.name, equals(GENERATION_NAME));
      expect(m.machines.length, equals(1));
      expect(m.machines.first.machine.url, equals(MACHINES_URL));
      expect(m.meta.ailment.name, equals(MOVE_META_DATA_ALIMENT_NAME));
      expect(m.names.length, equals(1));
      expect(m.names.first.name, equals(NAMES_NAME));
      expect(m.pastValues.length, equals(1));
      expect(m.pastValues.first.accuracy, equals(PAST_MOVE_ACCURACY));
      expect(m.statChanges.length, equals(1));
      expect(m.statChanges.first.change, equals(MOVE_STAT_CHANGE));
      expect(m.superContestEffect.url, equals(SUPER_CONTEST_EFFECT_URL));
      expect(m.target.name, equals(TARGET_NAME));
      expect(m.type.name, equals(TYPE_NAME));
    });

    test('toMap', () {
      final m = Move(
              ID,
              NAME,
              ACCURACY,
              EFFECT_CHANCE,
              PP,
              PRIORITY,
              POWER,
              CONTEST_COMBO,
              CONTEST_TYPE,
              CONTEST_EFFECT,
              DAMAGE_CLASS,
              [EFFECT_ENTRY],
              [EFFECT_CHANGE],
              [FLAVOR_TEXT_ENTRY],
              GENERATION,
              [MACHINE],
              META,
              [NAME_],
              [PAST_MOVE],
              [STAT_CHANGE],
              SUPER_CONTEST_EFFECT,
              TARGET,
              TYPE)
          .toMap();
      expect(m[Move.ID_FIELD_NAME], equals(ID));
      expect(m[Move.NAME_FIELD_NAME], equals(NAME));
      expect(m[Move.ACCURACY_FIELD_NAME], equals(ACCURACY));
      expect(m[Move.EFFECT_CHANCE_FIELD_NAME], equals(EFFECT_CHANCE));
      expect(m[Move.PP_FIELD_NAME], equals(PP));
      expect(m[Move.PRIORITY_FIELD_NAME], equals(PRIORITY));
      expect(m[Move.POWER_FIELD_NAME], equals(POWER));
      expect(m[Move.CONTEST_COMBOS_FIELD_NAME], equals(CONTEST_COMBO.toMap()));
      expect(m[Move.CONTEST_TYPE_FIELD_NAME], equals(CONTEST_TYPE.toMap()));
      expect(m[Move.CONTEST_EFFECT_FIELD_NAME], equals(CONTEST_EFFECT.toMap()));
      expect(m[Move.DAMAGE_CLASS_FIELD_NAME], equals(DAMAGE_CLASS.toMap()));
      expect(m[Move.EFFECT_ENTRIES_FIELD_NAME], equals([EFFECT_ENTRY.toMap()]));
      expect(
          m[Move.EFFECT_CHANGES_FIELD_NAME], equals([EFFECT_CHANGE.toMap()]));
      expect(m[Move.FLAVOR_TEXT_ENTRIES_FIELD_NAME],
          equals([FLAVOR_TEXT_ENTRY.toMap()]));
      expect(m[Move.GENERATION_FIELD_NAME], equals(GENERATION.toMap()));
      expect(m[Move.MACHINES_FIELD_NAME], equals([MACHINE.toMap()]));
      expect(m[Move.META_FIELD_NAME], equals(META.toMap()));
      expect(m[Move.NAMES_FIELD_NAME], equals([NAME_.toMap()]));
      expect(m[Move.PAST_VALUES_FIELD_NAME], equals([PAST_MOVE.toMap()]));
      expect(m[Move.STAT_CHANGES_FIELD_NAME], equals([STAT_CHANGE.toMap()]));
      expect(m[Move.SUPER_CONTEST_EFFECT_FIELD_NAME],
          equals(SUPER_CONTEST_EFFECT.toMap()));
      expect(m[Move.TARGET_FIELD_NAME], equals(TARGET.toMap()));
      expect(m[Move.TYPE_FIELD_NAME], equals(TYPE.toMap()));
    });
  });
}
