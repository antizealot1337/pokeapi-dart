// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/moves/move_learn_method.dart';
import 'package:pokeapi/src/models/utility/common/description.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart' hide Description;

void main() {
  group('MoveLearnMethod', () {
    const ID = 1;
    const NAME = 'name';
    const DESCRIPTIONS_DESCRIPTION = 'descriptions';
    const DESCRIPTION =
        Description(DESCRIPTIONS_DESCRIPTION, NamedApiResource('', ''));
    const NAMES_NAME = 'names';
    const NAME_ = Name(NAMES_NAME, NamedApiResource('', ''));
    const VERSION_GROUPS_NAME = 'version groups';
    const VERSION_GROUP = NamedApiResource(VERSION_GROUPS_NAME, '');

    test('fromMap', () {
      final m = MoveLearnMethod.fromMap({
        MoveLearnMethod.ID_FIELD_NAME: ID,
        MoveLearnMethod.NAME_FIELD_NAME: NAME,
        MoveLearnMethod.DESCRIPTIONS_FIELD_NAME: [DESCRIPTION.toMap()],
        MoveLearnMethod.NAMES_FIELD_NAME: [NAME_.toMap()],
        MoveLearnMethod.VERSION_GROUPS_FIELD_NAME: [VERSION_GROUP.toMap()],
      });

      expect(m.id, equals(ID));
      expect(m.name, equals(NAME));
      expect(m.descriptions.length, equals(1));
      expect(
          m.descriptions.first.description, equals(DESCRIPTIONS_DESCRIPTION));
      expect(m.names.length, equals(1));
      expect(m.names.first.name, equals(NAMES_NAME));
      expect(m.versionGroups.length, equals(1));
      expect(m.versionGroups.first.name, equals(VERSION_GROUPS_NAME));
    });

    test('toMap', () {
      final m =
          MoveLearnMethod(ID, NAME, [DESCRIPTION], [NAME_], [VERSION_GROUP])
              .toMap();

      expect(m[MoveLearnMethod.ID_FIELD_NAME], equals(ID));
      expect(m[MoveLearnMethod.NAME_FIELD_NAME], equals(NAME));
      expect(m[MoveLearnMethod.DESCRIPTIONS_FIELD_NAME],
          equals([DESCRIPTION.toMap()]));
      expect(m[MoveLearnMethod.NAMES_FIELD_NAME], equals([NAME_.toMap()]));
      expect(m[MoveLearnMethod.VERSION_GROUPS_FIELD_NAME],
          equals([VERSION_GROUP.toMap()]));
    });
  });
}
