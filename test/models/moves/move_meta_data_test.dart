// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/moves/move_meta_data.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('MoveMetaDAta', () {
    const AILMENT_NAME = 'ailment';
    const AILMENT = NamedApiResource(AILMENT_NAME, '');
    const CATEGORY_NAME = 'category';
    const CATEGORY = NamedApiResource(CATEGORY_NAME, '');
    const MIN_HITS = 1;
    const MAX_HITS = 2;
    const MIN_TURNS = 3;
    const MAX_TURNS = 4;
    const DRAIN = 5;
    const HEALING = 6;
    const CRIT_RATE = 7;
    const AILMENT_CHANCE = 8;
    const FLINCH_CHANCE = 9;
    const STAT_CHANCE = 10;

    test('fromMap', () {
      final m = MoveMetaData.fromMap({
        MoveMetaData.AILMENT_FIELD_NAME: AILMENT.toMap(),
        MoveMetaData.CATEGORY_FIELD_NAME: CATEGORY.toMap(),
        MoveMetaData.MIN_HITS_FIELD_NAME: MIN_HITS,
        MoveMetaData.MAX_HITS_FIELD_NAME: MAX_HITS,
        MoveMetaData.MIN_TURNS_FIELD_NAME: MIN_TURNS,
        MoveMetaData.MAX_TURNS_FIELD_NAME: MAX_TURNS,
        MoveMetaData.DRAIN_FIELD_NAME: DRAIN,
        MoveMetaData.HEALING_FIELD_NAME: HEALING,
        MoveMetaData.CRIT_RATE_FIELD_NAME: CRIT_RATE,
        MoveMetaData.AILMENT_CHANCE_FIELD_NAME: AILMENT_CHANCE,
        MoveMetaData.FLINCH_CHANCE_FIELD_NAME: FLINCH_CHANCE,
        MoveMetaData.STAT_CHANCE_FIELD_NAME: STAT_CHANCE,
      });

      expect(m.ailment.name, equals(AILMENT_NAME));
      expect(m.category.name, equals(CATEGORY_NAME));
      expect(m.minHits, equals(MIN_HITS));
      expect(m.maxHits, equals(MAX_HITS));
      expect(m.minTurns, equals(MIN_TURNS));
      expect(m.maxTurns, equals(MAX_TURNS));
      expect(m.drain, equals(DRAIN));
      expect(m.healing, equals(HEALING));
      expect(m.critRate, equals(CRIT_RATE));
      expect(m.ailmentChance, equals(AILMENT_CHANCE));
      expect(m.flinchChance, equals(FLINCH_CHANCE));
      expect(m.statChance, equals(STAT_CHANCE));
    });

    test('toMap', () {
      final m = MoveMetaData(
              AILMENT,
              CATEGORY,
              MIN_HITS,
              MAX_HITS,
              MIN_TURNS,
              MAX_TURNS,
              DRAIN,
              HEALING,
              CRIT_RATE,
              AILMENT_CHANCE,
              FLINCH_CHANCE,
              STAT_CHANCE)
          .toMap();

      expect(m[MoveMetaData.AILMENT_FIELD_NAME], equals(AILMENT.toMap()));
      expect(m[MoveMetaData.CATEGORY_FIELD_NAME], equals(CATEGORY.toMap()));
      expect(m[MoveMetaData.MIN_HITS_FIELD_NAME], equals(MIN_HITS));
      expect(m[MoveMetaData.MAX_HITS_FIELD_NAME], equals(MAX_HITS));
      expect(m[MoveMetaData.MIN_TURNS_FIELD_NAME], equals(MIN_TURNS));
      expect(m[MoveMetaData.MAX_TURNS_FIELD_NAME], equals(MAX_TURNS));
      expect(m[MoveMetaData.DRAIN_FIELD_NAME], equals(DRAIN));
      expect(m[MoveMetaData.HEALING_FIELD_NAME], equals(HEALING));
      expect(m[MoveMetaData.CRIT_RATE_FIELD_NAME], equals(CRIT_RATE));
      expect(m[MoveMetaData.AILMENT_CHANCE_FIELD_NAME], equals(AILMENT_CHANCE));
      expect(m[MoveMetaData.FLINCH_CHANCE_FIELD_NAME], equals(FLINCH_CHANCE));
      expect(m[MoveMetaData.STAT_CHANCE_FIELD_NAME], equals(STAT_CHANCE));
    });
  });
}
