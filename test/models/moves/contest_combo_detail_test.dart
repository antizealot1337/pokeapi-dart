// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/moves/contest_combo_detail.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('ContestComboDetail', () {
    const USE_BEFORE_NAME = 'before';
    const USE_BEFORE = NamedApiResource(USE_BEFORE_NAME, '');
    const USE_AFTER_NAME = 'after';
    const USE_AFTER = NamedApiResource(USE_AFTER_NAME, '');

    test('fromMap', () {
      final c = ContestComboDetail.fromMap({
        ContestComboDetail.USE_BEFORE_FIELD_NAME: [USE_BEFORE.toMap()],
        ContestComboDetail.USE_AFTER_FIELD_NAME: [USE_AFTER.toMap()],
      });

      expect(c.useBefore.length, equals(1));
      expect(c.useBefore.first.name, equals(USE_BEFORE_NAME));
      expect(c.useAfter.length, equals(1));
      expect(c.useAfter.first.name, equals(USE_AFTER_NAME));
    });

    test('toMap', () {
      final c = ContestComboDetail([USE_BEFORE], [USE_AFTER]).toMap();

      expect(c[ContestComboDetail.USE_BEFORE_FIELD_NAME],
          equals([USE_BEFORE.toMap()]));
      expect(c[ContestComboDetail.USE_AFTER_FIELD_NAME],
          equals([USE_AFTER.toMap()]));
    });
  });
}
