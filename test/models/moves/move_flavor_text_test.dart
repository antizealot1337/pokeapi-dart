// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/moves/move_flavor_text.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('MoveFlavorText', () {
    const FLAVOR_TEXT = 'flavor text';
    const LANGUAGE_NAME = 'language';
    const LANGUAGE = NamedApiResource(LANGUAGE_NAME, '');
    const VERSION_GROUP_NAME = 'version group';
    const VERSION_GROUP = NamedApiResource(VERSION_GROUP_NAME, '');

    test('fromMap', () {
      final m = MoveFlavorText.fromMap({
        MoveFlavorText.FLAVOR_TEXT_FIELD_NAME: FLAVOR_TEXT,
        MoveFlavorText.LANGUAGE_FIELD_NAME: LANGUAGE.toMap(),
        MoveFlavorText.VERSION_GROUP_FIELD_NAME: VERSION_GROUP.toMap(),
      });

      expect(m.flavorText, equals(FLAVOR_TEXT));
      expect(m.language.name, equals(LANGUAGE_NAME));
    });

    test('toMap', () {
      final m = MoveFlavorText(FLAVOR_TEXT, LANGUAGE, VERSION_GROUP).toMap();

      expect(m[MoveFlavorText.FLAVOR_TEXT_FIELD_NAME], equals(FLAVOR_TEXT));
      expect(m[MoveFlavorText.LANGUAGE_FIELD_NAME], equals(LANGUAGE.toMap()));
      expect(m[MoveFlavorText.VERSION_GROUP_FIELD_NAME],
          equals(VERSION_GROUP.toMap()));
    });

    group('listFrom', () {
      test('null', () {
        final m = MoveFlavorText.listFrom(null);
        expect(m, isEmpty);
      });

      test('not null', () {
        final m = MoveFlavorText.listFrom([
          {
            MoveFlavorText.FLAVOR_TEXT_FIELD_NAME: FLAVOR_TEXT,
            MoveFlavorText.LANGUAGE_FIELD_NAME: LANGUAGE.toMap(),
            MoveFlavorText.VERSION_GROUP_FIELD_NAME: VERSION_GROUP.toMap(),
          },
        ]);

        expect(m.first.flavorText, equals(FLAVOR_TEXT));
        expect(m.first.language.name, equals(LANGUAGE_NAME));
      });
    });
  });
}
