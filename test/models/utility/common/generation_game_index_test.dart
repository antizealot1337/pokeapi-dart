// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/generation_game_index.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('GenerationGameIndex', () {
    const GAME_INDEX = 1;
    const GENERATION_NAME = 'generation';
    const GENERATION = NamedApiResource(GENERATION_NAME, '');

    test('fromMap', () {
      final g = GenerationGameIndex.fromMap({
        GenerationGameIndex.GAME_INDEX_FIELD_NAME: GAME_INDEX,
        GenerationGameIndex.GENERATION_FIELD_NAME: GENERATION.toMap(),
      });

      expect(g.gameIndex, equals(GAME_INDEX));
      expect(g.generation.name, equals(GENERATION_NAME));
    });

    test('toMap', () {
      final g = GenerationGameIndex(GAME_INDEX, GENERATION).toMap();

      expect(g[GenerationGameIndex.GAME_INDEX_FIELD_NAME], equals(GAME_INDEX));
      expect(g[GenerationGameIndex.GENERATION_FIELD_NAME],
          equals(GENERATION.toMap()));
    });
  });
}
