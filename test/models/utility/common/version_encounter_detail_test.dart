// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/encounter.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:pokeapi/src/models/utility/common/version_encounter_detail.dart';
import 'package:test/test.dart';

void main() {
  group('VersionEncounterDetail', () {
    const MAX_CHANCE = 1;
    const VERSION_NAME = 'version';
    const VERSION = NamedApiResource(VERSION_NAME, '');
    const ENCOUNTER = Encounter(0, 0, [], 0, NamedApiResource('', ''));

    test('fromMap', () {
      final v = VersionEncounterDetail.fromMap({
        VersionEncounterDetail.VERSION_FIELD_NAME: VERSION.toMap(),
        VersionEncounterDetail.MAX_CHANCE_FIELD_NAME: MAX_CHANCE,
        VersionEncounterDetail.ENCOUNTER_DETAILS_FIELD_NAME: [
          ENCOUNTER.toMap(),
        ],
      });

      expect(v.version.name, equals(VERSION_NAME));
      expect(v.maxChance, equals(MAX_CHANCE));
      expect(v.encounterDetails.length, equals(1));
    });

    test('toMap', () {
      final v =
          VersionEncounterDetail(VERSION, MAX_CHANCE, [ENCOUNTER]).toMap();

      expect(v[VersionEncounterDetail.VERSION_FIELD_NAME],
          equals(VERSION.toMap()));
      expect(
          v[VersionEncounterDetail.MAX_CHANCE_FIELD_NAME], equals(MAX_CHANCE));
      expect(v[VersionEncounterDetail.ENCOUNTER_DETAILS_FIELD_NAME],
          equals([ENCOUNTER.toMap()]));
    });

    group('listFrom', () {
      test('null', () {
        final v = VersionEncounterDetail.listFrom(null);
        expect(v, isEmpty);
      });

      test('not null', () {
        final v = VersionEncounterDetail.listFrom([
          {
            VersionEncounterDetail.VERSION_FIELD_NAME: VERSION.toMap(),
            VersionEncounterDetail.MAX_CHANCE_FIELD_NAME: MAX_CHANCE,
            VersionEncounterDetail.ENCOUNTER_DETAILS_FIELD_NAME: [
              Encounter(0, 0, [], 0, NamedApiResource('', '')).toMap(),
            ],
          },
        ]);

        expect(v.first.version.name, equals(VERSION_NAME));
        expect(v.first.maxChance, equals(MAX_CHANCE));
        expect(v.first.encounterDetails.length, equals(1));
      });
    });
  });
}
