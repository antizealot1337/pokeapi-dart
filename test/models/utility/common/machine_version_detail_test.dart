// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/api_resource.dart';
import 'package:pokeapi/src/models/utility/common/machine_version_detail.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('MachineVersionDetail', () {
    const MACHINE_URL = 'url';
    const MACHINE = ApiResource(MACHINE_URL);
    const VERSION_GROUP_NAME = 'version';
    const VERSION_GROUP = NamedApiResource(VERSION_GROUP_NAME, '');

    test('fromMap', () {
      final m = MachineVersionDetail.fromMap({
        MachineVersionDetail.MACHINE_FIELD_NAME: MACHINE.toMap(),
        MachineVersionDetail.VERSION_GROUP_FIELD_NAME: VERSION_GROUP.toMap(),
      });

      expect(m.machine.url, equals(MACHINE_URL));
      expect(m.versionGroup.name, equals(VERSION_GROUP_NAME));
    });

    test('toMap', () {
      final m = MachineVersionDetail(MACHINE, VERSION_GROUP).toMap();

      expect(
          m[MachineVersionDetail.MACHINE_FIELD_NAME], equals(MACHINE.toMap()));
      expect(m[MachineVersionDetail.VERSION_GROUP_FIELD_NAME],
          equals(VERSION_GROUP.toMap()));
    });

    group('listFrom', () {
      test('null', () {
        final m = MachineVersionDetail.listFrom(null);
        expect(m, isEmpty);
      });

      test('not null', () {
        final m = MachineVersionDetail.listFrom([
          {
            MachineVersionDetail.MACHINE_FIELD_NAME: MACHINE.toMap(),
            MachineVersionDetail.VERSION_GROUP_FIELD_NAME:
                VERSION_GROUP.toMap(),
          },
        ]);

        expect(m.first.machine.url, equals(MACHINE_URL));
        expect(m.first.versionGroup.name, equals(VERSION_GROUP_NAME));
      });
    });
  });
}
