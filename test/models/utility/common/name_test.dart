// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('Name', () {
    const NAME = 'name';
    const LANGUAGE_NAME = 'language';
    const LANGUAGE = NamedApiResource(LANGUAGE_NAME, '');

    test('fromMap', () {
      final n = Name.fromMap({
        Name.NAME_FIELD_NAME: NAME,
        Name.LAGUNAGE_FIELD_NAME: LANGUAGE.toMap(),
      });

      expect(n.name, equals(NAME));
      expect(n.language.name, equals(LANGUAGE_NAME));
    });

    test('toMap', () {
      final n = Name(NAME, LANGUAGE).toMap();

      expect(n[Name.NAME_FIELD_NAME], equals(NAME));
      expect(n[Name.LAGUNAGE_FIELD_NAME], equals(LANGUAGE.toMap()));
    });

    group('listFrom', () {
      test('null', () {
        final n = Name.listFrom(null);
        expect(n, isEmpty);
      });

      test('not null', () {
        final n = Name.listFrom([
          {
            Name.NAME_FIELD_NAME: NAME,
            Name.LAGUNAGE_FIELD_NAME: LANGUAGE.toMap(),
          }
        ]);
        expect(n.length, equals(1));
        expect(n.first.name, equals(NAME));
        expect(n.first.language.name, equals(LANGUAGE_NAME));
      });
    });
  });
}
