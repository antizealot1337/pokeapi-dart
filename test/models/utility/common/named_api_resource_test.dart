// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('NamedApiResource', () {
    const NAME = 'name';
    const URL = 'url';

    test('fromMap', () {
      final n = NamedApiResource.fromMap({
        NamedApiResource.NAME_FIELD_NAME: NAME,
        NamedApiResource.URL_FIELD_NAME: URL,
      });

      expect(n.name, equals(NAME));
      expect(n.url, equals(URL));
    });

    test('toMap', () {
      final map = NamedApiResource(NAME, URL).toMap();
      expect(map[NamedApiResource.NAME_FIELD_NAME], equals(NAME));
      expect(map[NamedApiResource.URL_FIELD_NAME], equals(URL));
    });

    group('tryFromMap', () {
      test('null', () {
        final n = NamedApiResource.tryFromMap(null);
        expect(n, isNull);
      });

      test('not null', () {
        final n = NamedApiResource.tryFromMap({
          NamedApiResource.NAME_FIELD_NAME: NAME,
          NamedApiResource.URL_FIELD_NAME: URL,
        });

        expect(n, isNotNull);
      });
    });

    group('listFrom', () {
      test('null', () {
        var namedApiResources = NamedApiResource.listFrom(null);

        expect(namedApiResources, isEmpty);
      });

      test('not null', () {
        var namedApiResources = NamedApiResource.listFrom([
          {
            NamedApiResource.NAME_FIELD_NAME: NAME,
            NamedApiResource.URL_FIELD_NAME: URL,
          }
        ]);

        expect(namedApiResources.length, equals(1));
        expect(namedApiResources.first.name, equals(NAME));
        expect(namedApiResources.first.url, equals(URL));
      });
    });
  });
}
