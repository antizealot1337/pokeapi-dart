// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:pokeapi/src/models/utility/common/version_group_flavor_text.dart';
import 'package:test/test.dart';

void main() {
  group('VersionGroupFlavorText', () {
    const TEXT = 'text';
    const LANGUAGE_NAME = 'language';
    const LANGUAGE = NamedApiResource(LANGUAGE_NAME, '');
    const VERSION_GROUP_NAME = 'version group';
    const VERSION_GROUP = NamedApiResource(VERSION_GROUP_NAME, '');

    test('fromMap', () {
      final v = VersionGroupFlavorText.fromMap({
        VersionGroupFlavorText.TEXT_FIELD_NAME: TEXT,
        VersionGroupFlavorText.LANGUAGE_FIELD_NAME: LANGUAGE.toMap(),
        VersionGroupFlavorText.VERSION_GROUP_FIELD_NAME: VERSION_GROUP.toMap(),
      });

      expect(v.text, equals(TEXT));
      expect(v.language.name, equals(LANGUAGE_NAME));
      expect(v.versionGroup.name, equals(VERSION_GROUP_NAME));
    });

    test('toMap', () {
      final v = VersionGroupFlavorText(TEXT, LANGUAGE, VERSION_GROUP).toMap();

      expect(v[VersionGroupFlavorText.TEXT_FIELD_NAME], equals(TEXT));
      expect(v[VersionGroupFlavorText.LANGUAGE_FIELD_NAME],
          equals(LANGUAGE.toMap()));
      expect(v[VersionGroupFlavorText.VERSION_GROUP_FIELD_NAME],
          equals(VERSION_GROUP.toMap()));
    });

    group('listFrom', () {
      test('null', () {
        final v = VersionGroupFlavorText.listFrom(null);
        expect(v, isEmpty);
      });

      test('not null', () {
        final v = VersionGroupFlavorText.listFrom([
          {
            VersionGroupFlavorText.TEXT_FIELD_NAME: TEXT,
            VersionGroupFlavorText.LANGUAGE_FIELD_NAME: LANGUAGE.toMap(),
            VersionGroupFlavorText.VERSION_GROUP_FIELD_NAME:
                VERSION_GROUP.toMap(),
          },
        ]);

        expect(v.first.text, equals(TEXT));
        expect(v.first.language.name, equals(LANGUAGE_NAME));
        expect(v.first.versionGroup.name, equals(VERSION_GROUP_NAME));
      });
    });
  });
}
