// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('ApiResources', () {
    const URL = 'url';

    test('fromMap', () {
      final a = ApiResource.fromMap({
        ApiResource.URL_FIELD_NAME: URL,
      });

      expect(a.url, equals(URL));
    });

    test('toMap', () {
      final map = ApiResource(URL).toMap();
      expect(map[ApiResource.URL_FIELD_NAME], equals(URL));
    });

    group('tryFromMap', () {
      test('null', () {
        final a = ApiResource.tryFromMap(null);
        expect(a, isNull);
      });

      test('not null', () {
        final a = ApiResource.tryFromMap({
          ApiResource.URL_FIELD_NAME: URL,
        });

        expect(a, isNotNull);
        expect(a!.url, equals(URL));
      });
    });

    group('listFrom', () {
      test('null', () {
        final a = ApiResource.listFrom(null);
        expect(a, isEmpty);
      });

      test('not null', () {
        final a = ApiResource.listFrom([
          {
            ApiResource.URL_FIELD_NAME: URL,
          },
        ]);

        expect(a.first.url, equals(URL));
      });
    });
  });
}
