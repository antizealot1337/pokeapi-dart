// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/description.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart' hide Description;

void main() {
  group('Description', () {
    const DESCRIPTION = 'description';
    const LANGUAGE_NAME = 'language';
    const LANGUAGE = NamedApiResource(LANGUAGE_NAME, '');

    test('fromMap', () {
      final d = Description.fromMap({
        Description.DESCRIPTION_FIELD_NAME: DESCRIPTION,
        Description.LANGUAGE_FIELD_NAME: LANGUAGE.toMap(),
      });

      expect(d.description, equals(DESCRIPTION));
      expect(d.language.name, equals(LANGUAGE_NAME));
    });

    test('toMap', () {
      final d = Description(DESCRIPTION, LANGUAGE).toMap();

      expect(d[Description.DESCRIPTION_FIELD_NAME], equals(DESCRIPTION));
      expect(d[Description.LANGUAGE_FIELD_NAME], equals(LANGUAGE.toMap()));
    });

    group('listFrom', () {
      test('null', () {
        final d = Description.listFrom(null);
        expect(d, isEmpty);
      });

      test('not null', () {
        final d = Description.listFrom([
          {
            Description.DESCRIPTION_FIELD_NAME: DESCRIPTION,
            Description.LANGUAGE_FIELD_NAME: LANGUAGE.toMap(),
          },
        ]);

        expect(d.first.description, equals(DESCRIPTION));
        expect(d.first.language.name, equals(LANGUAGE_NAME));
      });
    });
  });
}
