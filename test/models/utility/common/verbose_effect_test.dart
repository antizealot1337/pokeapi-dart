// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:pokeapi/src/models/utility/common/verbose_effect.dart';
import 'package:test/test.dart';

void main() {
  group('VerboseEffect', () {
    const EFFECT = 'effect';
    const SHORT_EFFECT = 'short_effect';
    const LANGUAGE_NAME = 'language';
    const LANGUAGE = NamedApiResource(LANGUAGE_NAME, '');

    test('fromMap', () {
      final v = VerboseEffect.fromMap({
        VerboseEffect.EFFECT_FIELD_NAME: EFFECT,
        VerboseEffect.SHORT_EFFECT_FIELD_NAME: SHORT_EFFECT,
        VerboseEffect.LANGUAGE_FIELD_NAME: LANGUAGE.toMap(),
      });

      expect(v.effect, equals(EFFECT));
      expect(v.shortEffect, equals(SHORT_EFFECT));
      expect(v.language.name, equals(LANGUAGE_NAME));
    });

    test('toMap', () {
      final v = VerboseEffect(EFFECT, SHORT_EFFECT, LANGUAGE).toMap();

      expect(v[VerboseEffect.EFFECT_FIELD_NAME], equals(EFFECT));
      expect(v[VerboseEffect.SHORT_EFFECT_FIELD_NAME], equals(SHORT_EFFECT));
      expect(v[VerboseEffect.LANGUAGE_FIELD_NAME], equals(LANGUAGE.toMap()));
    });

    group('listFrom', () {
      test('null', () {
        final v = VerboseEffect.listFrom(null);
        expect(v, isEmpty);
      });

      test('not null', () {
        final v = VerboseEffect.listFrom([
          {
            VerboseEffect.EFFECT_FIELD_NAME: EFFECT,
            VerboseEffect.SHORT_EFFECT_FIELD_NAME: SHORT_EFFECT,
            VerboseEffect.LANGUAGE_FIELD_NAME: LANGUAGE.toMap(),
          },
        ]);

        expect(v.first.effect, equals(EFFECT));
        expect(v.first.shortEffect, equals(SHORT_EFFECT));
        expect(v.first.language.name, equals(LANGUAGE_NAME));
      });
    });
  });
}
