// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/effect.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('Effect', () {
    const EFFECT = 'effect';
    const LANGUAGE_NAME = 'laguage';
    const LANGUAGE = NamedApiResource(LANGUAGE_NAME, '');

    test('fromMap', () {
      final e = Effect.fromMap({
        Effect.EFFECT_FIELD_NAME: EFFECT,
        Effect.LANGUAGE_FIELD_NAME: LANGUAGE.toMap(),
      });

      expect(e.effect, equals(EFFECT));
      expect(e.language.name, equals(LANGUAGE_NAME));
    });

    test('toMap', () {
      final e = Effect(EFFECT, LANGUAGE).toMap();

      expect(e[Effect.EFFECT_FIELD_NAME], equals(EFFECT));
      expect(e[Effect.LANGUAGE_FIELD_NAME], equals(LANGUAGE.toMap()));
    });

    group('listFrom', () {
      test('null', () {
        final e = Effect.listFrom(null);
        expect(e, isEmpty);
      });

      test('not null', () {
        final e = Effect.listFrom([
          {
            Effect.EFFECT_FIELD_NAME: EFFECT,
            Effect.LANGUAGE_FIELD_NAME: LANGUAGE.toMap(),
          },
        ]);

        expect(e.first.effect, equals(EFFECT));
        expect(e.first.language.name, equals(LANGUAGE_NAME));
      });
    });
  });
}
