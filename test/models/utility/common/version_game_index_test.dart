// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:pokeapi/src/models/utility/common/version_game_index.dart';
import 'package:test/test.dart';

void main() {
  group('VersionGameIndex', () {
    const GAME_INDEX = 1;
    const VERSION_NAME = 'version';
    const VERSION = NamedApiResource(VERSION_NAME, '');

    test('fromMap', () {
      final v = VersionGameIndex.fromMap({
        VersionGameIndex.GAME_INDEX_FIELD_NAME: GAME_INDEX,
        VersionGameIndex.VERION_FIELD_NAME: VERSION.toMap(),
      });

      expect(v.gameIndex, equals(GAME_INDEX));
      expect(v.version.name, equals(VERSION_NAME));
    });

    test('toMap', () {
      final v = VersionGameIndex(GAME_INDEX, VERSION).toMap();

      expect(v[VersionGameIndex.GAME_INDEX_FIELD_NAME], equals(GAME_INDEX));
      expect(v[VersionGameIndex.VERION_FIELD_NAME], equals(VERSION.toMap()));
    });

    group('listFrom', () {
      test('null', () {
        final v = VersionGameIndex.listFrom(null);
        expect(v, isEmpty);
      });

      test('not null', () {
        final v = VersionGameIndex.listFrom([
          {
            VersionGameIndex.GAME_INDEX_FIELD_NAME: GAME_INDEX,
            VersionGameIndex.VERION_FIELD_NAME: VERSION.toMap(),
          },
        ]);

        expect(v.first.gameIndex, equals(GAME_INDEX));
        expect(v.first.version.name, equals(VERSION_NAME));
      });
    });
  });
}
