// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/encounter.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('Encounter', () {
    const MIN_LEVEL = 1;
    const MAX_LEVEL = 2;
    const CHANCE = 3;
    const METHOD_NAME = 'method';
    const METHOD = NamedApiResource(METHOD_NAME, '');

    test('fromMap', () {
      final e = Encounter.fromMap({
        Encounter.MIN_LEVEL_FIELD_NAME: MIN_LEVEL,
        Encounter.MAX_LEVEL_FIELD_NAME: MAX_LEVEL,
        Encounter.CONDITION_VALUES_FIELD_NAME: [
          NamedApiResource('', '').toMap(),
        ],
        Encounter.CHANCE_FIELD_NAME: CHANCE,
        Encounter.METHOD_FIELD_NAME: METHOD.toMap(),
      });

      expect(e.minLevel, equals(MIN_LEVEL));
      expect(e.maxLevel, equals(MAX_LEVEL));
      expect(e.conditionValues.length, equals(1));
      expect(e.chance, equals(CHANCE));
      expect(e.method.name, equals(METHOD_NAME));
    });

    test('toMap', () {
      final e = Encounter(
              MIN_LEVEL, MAX_LEVEL, [NamedApiResource('', '')], CHANCE, METHOD)
          .toMap();

      expect(e[Encounter.MIN_LEVEL_FIELD_NAME], equals(MIN_LEVEL));
      expect(e[Encounter.MAX_LEVEL_FIELD_NAME], equals(MAX_LEVEL));
      expect(e[Encounter.CONDITION_VALUES_FIELD_NAME], isList);
      expect(e[Encounter.CHANCE_FIELD_NAME], equals(CHANCE));
      expect(e[Encounter.METHOD_FIELD_NAME], equals(METHOD.toMap()));
    });

    group('listFrom', () {
      test('null', () {
        final e = Encounter.listFrom(null);
        expect(e, isEmpty);
      });

      test('not null', () {
        final e = Encounter.listFrom([
          {
            Encounter.MIN_LEVEL_FIELD_NAME: MIN_LEVEL,
            Encounter.MAX_LEVEL_FIELD_NAME: MAX_LEVEL,
            Encounter.CONDITION_VALUES_FIELD_NAME: [
              NamedApiResource('', '').toMap(),
            ],
            Encounter.CHANCE_FIELD_NAME: CHANCE,
            Encounter.METHOD_FIELD_NAME: METHOD.toMap(),
          },
        ]);

        expect(e.first.minLevel, equals(MIN_LEVEL));
        expect(e.first.maxLevel, equals(MAX_LEVEL));
        expect(e.first.conditionValues.length, equals(1));
        expect(e.first.chance, equals(CHANCE));
        expect(e.first.method.name, equals(METHOD_NAME));
      });
    });
  });
}
