// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/flavor_text.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('FlavorText', () {
    const FLAVOR_TEXT = 'flavor text';
    const LANGUAGE_NAME = 'language';
    const LANGUAGE = NamedApiResource(LANGUAGE_NAME, '');
    const VERSION_NAME = 'version';
    const VERSION = NamedApiResource(VERSION_NAME, '');

    test('fromMap', () {
      final f = FlavorText.fromMap({
        FlavorText.FLAVOR_TEXT_FIELD_NAME: FLAVOR_TEXT,
        FlavorText.LANGUAGE_FIELD_NAME: LANGUAGE.toMap(),
        FlavorText.VERSION_FIELD_NAME: VERSION.toMap(),
      });

      expect(f.flavorText, equals(FLAVOR_TEXT));
      expect(f.language.name, equals(LANGUAGE_NAME));
      expect(f.version!.name, equals(VERSION_NAME));
    });

    test('toMap', () {
      final f = FlavorText(FLAVOR_TEXT, LANGUAGE, VERSION).toMap();

      expect(f[FlavorText.FLAVOR_TEXT_FIELD_NAME], equals(FLAVOR_TEXT));
      expect(f[FlavorText.LANGUAGE_FIELD_NAME], equals(LANGUAGE.toMap()));
      expect(f[FlavorText.VERSION_FIELD_NAME], equals(VERSION.toMap()));
    });

    group('listFrom', () {
      test('null', () {
        final f = FlavorText.listFrom(null);
        expect(f, isEmpty);
      });

      test('not null', () {
        final f = FlavorText.listFrom([
          {
            FlavorText.FLAVOR_TEXT_FIELD_NAME: FLAVOR_TEXT,
            FlavorText.LANGUAGE_FIELD_NAME: LANGUAGE.toMap(),
            FlavorText.VERSION_FIELD_NAME: VERSION.toMap(),
          },
        ]);

        expect(f.first.flavorText, equals(FLAVOR_TEXT));
        expect(f.first.language.name, equals(LANGUAGE_NAME));
        expect(f.first.version!.name, equals(VERSION_NAME));
      });
    });
  });
}
