// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:pokeapi/src/models/utility/languages/language.dart';
import 'package:test/test.dart';

void main() {
  group('Language', () {
    const ID = 1;
    const NAME = 'name';
    const OFFICIAL = true;
    const ISO639 = 'iso639';
    const ISO3166 = 'iso3166';
    const NAMES_NAME = 'names';
    const NAME_ = Name(NAMES_NAME, NamedApiResource('', ''));

    test('fromMap', () {
      final language = Language.fromMap({
        Language.ID_FIELD_NAME: ID,
        Language.NAME_FIELD_NAME: NAME,
        Language.OFFICIAL_FIELD_NAME: OFFICIAL,
        Language.ISO639_FIELD_NAME: ISO639,
        Language.ISO3166_FIELD_NAME: ISO3166,
        Language.NAMES_FIELD_NAME: [
          NAME_.toMap(),
        ],
      });

      expect(language.id, equals(ID));
      expect(language.name, equals(NAME));
      expect(language.official, equals(OFFICIAL));
      expect(language.iso639, equals(ISO639));
      expect(language.iso3166, equals(ISO3166));
      expect(language.names.length, equals(1));
      expect(language.names.first.name, equals(NAMES_NAME));
    });

    test('toMap', () {
      final l = Language(ID, NAME, OFFICIAL, ISO639, ISO3166, [NAME_]).toMap();

      expect(l[Language.ID_FIELD_NAME], equals(ID));
      expect(l[Language.NAME_FIELD_NAME], equals(NAME));
      expect(l[Language.OFFICIAL_FIELD_NAME], equals(OFFICIAL));
      expect(l[Language.ISO639_FIELD_NAME], equals(ISO639));
      expect(l[Language.ISO3166_FIELD_NAME], equals(ISO3166));
      expect(l[Language.NAMES_FIELD_NAME], equals([NAME_.toMap()]));
    });
  });
}
