// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/evolution/evolution_detail.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('EvolutionDetail', () {
    const ITEM_NAME = 'item';
    const ITEM = NamedApiResource(ITEM_NAME, '');
    const TRIGGER_NAME = 'trigger';
    const TRIGGER = NamedApiResource(TRIGGER_NAME, '');
    const GENDER = 1;
    const HELD_ITEM_NAME = 'held item';
    const HELD_ITEM = NamedApiResource(HELD_ITEM_NAME, '');
    const KNOWN_MOVE_NAME = 'known move';
    const KNOWN_MOVE = NamedApiResource(KNOWN_MOVE_NAME, '');
    const KNOWN_MOVE_TYPE_NAME = 'known move type';
    const KNOWN_MOVE_TYPE = NamedApiResource(KNOWN_MOVE_TYPE_NAME, '');
    const LOCATION_NAME = 'location';
    const LOCATION = NamedApiResource(LOCATION_NAME, '');
    const MIN_LEVEL = 2;
    const MIN_HAPPINESS = 3;
    const MIN_BEAUTY = 4;
    const MIN_AFFECTION = 5;
    const NEEDS_OVERWORLD_RAIN = true;
    const PARTY_SPECIES_NAME = 'party species';
    const PARTY_SPECIES = NamedApiResource(PARTY_SPECIES_NAME, '');
    const PARTY_TYPE_NAME = 'party type';
    const PARTY_TYPE = NamedApiResource(PARTY_TYPE_NAME, '');
    const RELATIVE_PHYSICAL_STATS = 6;
    const TIME_OF_DAY = 'time of day';
    const TRADE_SPECIES_NAME = 'trade species';
    const TRADE_SPECIES = NamedApiResource(TRADE_SPECIES_NAME, '');
    const TURN_UPSIDE_DOWN = false;

    test('fromMap', () {
      final e = EvolutionDetail.fromMap({
        EvolutionDetail.ITEM_FIELD_NAME: ITEM.toMap(),
        EvolutionDetail.TRIGGER_FIELD_NAME: TRIGGER.toMap(),
        EvolutionDetail.GENDER_FIELD_NAME: GENDER,
        EvolutionDetail.HELD_ITEM_FIELD_NAME: HELD_ITEM.toMap(),
        EvolutionDetail.KNOWN_MOVE_FIELD_NAME: KNOWN_MOVE.toMap(),
        EvolutionDetail.KNOWN_MOVE_TYPE_FIELD_NAME: KNOWN_MOVE_TYPE.toMap(),
        EvolutionDetail.LOCATION_FIELD_NAME: LOCATION.toMap(),
        EvolutionDetail.MIN_LEVEL_FIELD_NAME: MIN_LEVEL,
        EvolutionDetail.MIN_HAPPINESS_FIELD_NAME: MIN_HAPPINESS,
        EvolutionDetail.MIN_BEAUTY_FIELD_NAME: MIN_BEAUTY,
        EvolutionDetail.MIN_AFFECTION_FIELD_NAME: MIN_AFFECTION,
        EvolutionDetail.NEEDS_OVERWORLD_RAIN_FIELD_NAME: NEEDS_OVERWORLD_RAIN,
        EvolutionDetail.PARTY_SPECIES_FIELD_NAME: PARTY_SPECIES.toMap(),
        EvolutionDetail.PARTY_TYPE_FIELD_NAME: PARTY_TYPE.toMap(),
        EvolutionDetail.RELATIVE_PHYSICAL_STATS_FIELD_NAME:
            RELATIVE_PHYSICAL_STATS,
        EvolutionDetail.TIME_OF_DAY_FIELD_NAME: TIME_OF_DAY,
        EvolutionDetail.TRADE_SPECIES_FIELD_NAME: TRADE_SPECIES.toMap(),
        EvolutionDetail.TURN_UPSIDE_DOWN_FIELD_NAME: TURN_UPSIDE_DOWN,
      });

      expect(e.item!.name, equals(ITEM_NAME));
      expect(e.trigger.name, equals(TRIGGER_NAME));
      expect(e.gender, equals(GENDER));
      expect(e.heldItem!.name, equals(HELD_ITEM_NAME));
      expect(e.knownMove!.name, equals(KNOWN_MOVE_NAME));
      expect(e.location!.name, equals(LOCATION_NAME));
      expect(e.minLevel, equals(MIN_LEVEL));
      expect(e.minHappiness, equals(MIN_HAPPINESS));
      expect(e.minBeauty, equals(MIN_BEAUTY));
      expect(e.minAffection, equals(MIN_AFFECTION));
      expect(e.needsOverworldRain, equals(NEEDS_OVERWORLD_RAIN));
      expect(e.partySpecies!.name, equals(PARTY_SPECIES_NAME));
      expect(e.partyType!.name, equals(PARTY_TYPE_NAME));
      expect(e.relativePhysicalStats, equals(RELATIVE_PHYSICAL_STATS));
      expect(e.timeOfDay, equals(TIME_OF_DAY));
      expect(e.tradeSpecies!.name, equals(TRADE_SPECIES_NAME));
      expect(e.turnUpsideDown, equals(TURN_UPSIDE_DOWN));
    });

    test('toMap', () {
      final e = EvolutionDetail(
              ITEM,
              TRIGGER,
              GENDER,
              HELD_ITEM,
              KNOWN_MOVE,
              KNOWN_MOVE_TYPE,
              LOCATION,
              MIN_LEVEL,
              MIN_HAPPINESS,
              MIN_BEAUTY,
              MIN_AFFECTION,
              NEEDS_OVERWORLD_RAIN,
              PARTY_SPECIES,
              PARTY_TYPE,
              RELATIVE_PHYSICAL_STATS,
              TIME_OF_DAY,
              TRADE_SPECIES,
              TURN_UPSIDE_DOWN)
          .toMap();

      expect(e[EvolutionDetail.ITEM_FIELD_NAME], equals(ITEM.toMap()));
      expect(e[EvolutionDetail.TRIGGER_FIELD_NAME], equals(TRIGGER.toMap()));
      expect(e[EvolutionDetail.GENDER_FIELD_NAME], equals(GENDER));
      expect(
          e[EvolutionDetail.HELD_ITEM_FIELD_NAME], equals(HELD_ITEM.toMap()));
      expect(
          e[EvolutionDetail.KNOWN_MOVE_FIELD_NAME], equals(KNOWN_MOVE.toMap()));
      expect(e[EvolutionDetail.KNOWN_MOVE_TYPE_FIELD_NAME],
          equals(KNOWN_MOVE_TYPE.toMap()));
      expect(e[EvolutionDetail.LOCATION_FIELD_NAME], equals(LOCATION.toMap()));
      expect(e[EvolutionDetail.MIN_LEVEL_FIELD_NAME], equals(MIN_LEVEL));
      expect(
          e[EvolutionDetail.MIN_HAPPINESS_FIELD_NAME], equals(MIN_HAPPINESS));
      expect(e[EvolutionDetail.MIN_BEAUTY_FIELD_NAME], equals(MIN_BEAUTY));
      expect(
          e[EvolutionDetail.MIN_AFFECTION_FIELD_NAME], equals(MIN_AFFECTION));
      expect(e[EvolutionDetail.NEEDS_OVERWORLD_RAIN_FIELD_NAME],
          equals(NEEDS_OVERWORLD_RAIN));
      expect(e[EvolutionDetail.PARTY_SPECIES_FIELD_NAME],
          equals(PARTY_SPECIES.toMap()));
      expect(
          e[EvolutionDetail.PARTY_TYPE_FIELD_NAME], equals(PARTY_TYPE.toMap()));
      expect(e[EvolutionDetail.RELATIVE_PHYSICAL_STATS_FIELD_NAME],
          equals(RELATIVE_PHYSICAL_STATS));
      expect(e[EvolutionDetail.TIME_OF_DAY_FIELD_NAME], equals(TIME_OF_DAY));
      expect(e[EvolutionDetail.TRADE_SPECIES_FIELD_NAME],
          equals(TRADE_SPECIES.toMap()));
      expect(e[EvolutionDetail.TURN_UPSIDE_DOWN_FIELD_NAME],
          equals(TURN_UPSIDE_DOWN));
    });

    group('listFrom', () {
      test('null', () {
        final e = EvolutionDetail.listFrom(null);
        expect(e, isEmpty);
      });

      test('not null', () {
        final e = EvolutionDetail.listFrom([
          {
            EvolutionDetail.ITEM_FIELD_NAME: ITEM.toMap(),
            EvolutionDetail.TRIGGER_FIELD_NAME: TRIGGER.toMap(),
            EvolutionDetail.GENDER_FIELD_NAME: GENDER,
            EvolutionDetail.HELD_ITEM_FIELD_NAME: HELD_ITEM.toMap(),
            EvolutionDetail.KNOWN_MOVE_FIELD_NAME: KNOWN_MOVE.toMap(),
            EvolutionDetail.KNOWN_MOVE_TYPE_FIELD_NAME: KNOWN_MOVE_TYPE.toMap(),
            EvolutionDetail.LOCATION_FIELD_NAME: LOCATION.toMap(),
            EvolutionDetail.MIN_LEVEL_FIELD_NAME: MIN_LEVEL,
            EvolutionDetail.MIN_HAPPINESS_FIELD_NAME: MIN_HAPPINESS,
            EvolutionDetail.MIN_BEAUTY_FIELD_NAME: MIN_BEAUTY,
            EvolutionDetail.MIN_AFFECTION_FIELD_NAME: MIN_AFFECTION,
            EvolutionDetail.NEEDS_OVERWORLD_RAIN_FIELD_NAME:
                NEEDS_OVERWORLD_RAIN,
            EvolutionDetail.PARTY_SPECIES_FIELD_NAME: PARTY_SPECIES.toMap(),
            EvolutionDetail.PARTY_TYPE_FIELD_NAME: PARTY_TYPE.toMap(),
            EvolutionDetail.RELATIVE_PHYSICAL_STATS_FIELD_NAME:
                RELATIVE_PHYSICAL_STATS,
            EvolutionDetail.TIME_OF_DAY_FIELD_NAME: TIME_OF_DAY,
            EvolutionDetail.TRADE_SPECIES_FIELD_NAME: TRADE_SPECIES.toMap(),
            EvolutionDetail.TURN_UPSIDE_DOWN_FIELD_NAME: TURN_UPSIDE_DOWN,
          }
        ]);

        expect(e.first.item!.name, equals(ITEM_NAME));
        expect(e.first.trigger.name, equals(TRIGGER_NAME));
        expect(e.first.gender, equals(GENDER));
        expect(e.first.heldItem!.name, equals(HELD_ITEM_NAME));
        expect(e.first.knownMove!.name, equals(KNOWN_MOVE_NAME));
        expect(e.first.location!.name, equals(LOCATION_NAME));
        expect(e.first.minLevel, equals(MIN_LEVEL));
        expect(e.first.minHappiness, equals(MIN_HAPPINESS));
        expect(e.first.minBeauty, equals(MIN_BEAUTY));
        expect(e.first.minAffection, equals(MIN_AFFECTION));
        expect(e.first.needsOverworldRain, equals(NEEDS_OVERWORLD_RAIN));
        expect(e.first.partySpecies!.name, equals(PARTY_SPECIES_NAME));
        expect(e.first.partyType!.name, equals(PARTY_TYPE_NAME));
        expect(e.first.relativePhysicalStats, equals(RELATIVE_PHYSICAL_STATS));
        expect(e.first.timeOfDay, equals(TIME_OF_DAY));
        expect(e.first.tradeSpecies!.name, equals(TRADE_SPECIES_NAME));
        expect(e.first.turnUpsideDown, equals(TURN_UPSIDE_DOWN));
      });
    });
  });
}
