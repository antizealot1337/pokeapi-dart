// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/evolution/chain_link.dart';
import 'package:pokeapi/src/models/evolution/evolution_detail.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('ChainLink', () {
    const IS_BABY = true;
    const SPECIES_NAME = 'species';
    const SPECIES = NamedApiResource(SPECIES_NAME, '');
    const EVOLUTION_DETAIL_ITEM_NAME = 'evolution detail item';
    const EVOLUTION_DETAIL = EvolutionDetail(
        NamedApiResource(EVOLUTION_DETAIL_ITEM_NAME, ''),
        NamedApiResource('', ''),
        1,
        NamedApiResource('', ''),
        NamedApiResource('', ''),
        NamedApiResource('', ''),
        NamedApiResource('', ''),
        0,
        0,
        0,
        0,
        false,
        NamedApiResource('', ''),
        NamedApiResource('', ''),
        0,
        '',
        NamedApiResource('', ''),
        false);

    test('fromMap', () {
      final c = ChainLink.fromMap({
        ChainLink.IS_BABY_FIELD_NAME: IS_BABY,
        ChainLink.SPECIES_FIELD_NAME: SPECIES.toMap(),
        ChainLink.EVOLUTION_DETAILS_FIELD_NAME: [EVOLUTION_DETAIL.toMap()],
        ChainLink.EVOLVES_TO_FIELD_NAME: [
          {
            ChainLink.IS_BABY_FIELD_NAME: false,
            ChainLink.SPECIES_FIELD_NAME: NamedApiResource('', '').toMap(),
            ChainLink.EVOLUTION_DETAILS_FIELD_NAME: [],
            ChainLink.EVOLVES_TO_FIELD_NAME: [],
          },
        ],
      });

      expect(c.isBaby, equals(IS_BABY));
      expect(c.species.name, equals(SPECIES_NAME));
      expect(c.evolutionDetails.length, equals(1));
      expect(c.evolutionDetails.first.item!.name,
          equals(EVOLUTION_DETAIL_ITEM_NAME));
      expect(c.evolvesTo.length, equals(1));
    });

    test('toMap', () {
      final c = ChainLink(IS_BABY, SPECIES, [EVOLUTION_DETAIL], []).toMap();

      expect(c[ChainLink.IS_BABY_FIELD_NAME], equals(IS_BABY));
      expect(c[ChainLink.SPECIES_FIELD_NAME], equals(SPECIES.toMap()));
      expect(c[ChainLink.EVOLUTION_DETAILS_FIELD_NAME],
          equals([EVOLUTION_DETAIL.toMap()]));
      expect(c[ChainLink.EVOLVES_TO_FIELD_NAME], equals([]));
    });

    group('listFrom', () {
      test('null', () {
        final c = ChainLink.listFrom(null);
        expect(c, isEmpty);
      });

      test('not null', () {
        final c = ChainLink.listFrom([
          {
            ChainLink.IS_BABY_FIELD_NAME: IS_BABY,
            ChainLink.SPECIES_FIELD_NAME: SPECIES.toMap(),
            ChainLink.EVOLUTION_DETAILS_FIELD_NAME: [EVOLUTION_DETAIL.toMap()],
            ChainLink.EVOLVES_TO_FIELD_NAME: [
              {
                ChainLink.IS_BABY_FIELD_NAME: false,
                ChainLink.SPECIES_FIELD_NAME: NamedApiResource('', '').toMap(),
                ChainLink.EVOLUTION_DETAILS_FIELD_NAME: [],
                ChainLink.EVOLVES_TO_FIELD_NAME: [],
              },
            ],
          },
        ]);

        expect(c.first.isBaby, equals(IS_BABY));
        expect(c.first.species.name, equals(SPECIES_NAME));
        expect(c.first.evolutionDetails.length, equals(1));
        expect(c.first.evolutionDetails.first.item!.name,
            equals(EVOLUTION_DETAIL_ITEM_NAME));
        expect(c.first.evolvesTo.length, equals(1));
      });
    });
  });
}
