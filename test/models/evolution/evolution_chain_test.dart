// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/evolution/chain_link.dart';
import 'package:pokeapi/src/models/evolution/evolution_chain.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('EvolutionChain', () {
    const ID = 1;
    const BABY_TRIGGER_NAME = 'baby trigger';
    const BABY_TRIGGER = NamedApiResource(BABY_TRIGGER_NAME, '');
    const CHAIN_SPECIES_NAME = 'chain species';
    const CHAIN =
        ChainLink(false, NamedApiResource(CHAIN_SPECIES_NAME, ''), [], []);

    test('fromMap', () {
      final e = EvolutionChain.fromMap({
        EvolutionChain.ID_FIELD_NAME: ID,
        EvolutionChain.BABY_TRIGGER_ITEM_FIELD_NAME: BABY_TRIGGER.toMap(),
        EvolutionChain.CHAIN_FIELD_NAME: CHAIN.toMap(),
      });

      expect(e.id, equals(ID));
      expect(e.babyTriggerItem!.name, equals(BABY_TRIGGER_NAME));
      expect(e.chain.species.name, equals(CHAIN_SPECIES_NAME));
    });

    test('toMap', () {
      final e = EvolutionChain(ID, BABY_TRIGGER, CHAIN).toMap();

      expect(e[EvolutionChain.ID_FIELD_NAME], equals(ID));
      expect(e[EvolutionChain.BABY_TRIGGER_ITEM_FIELD_NAME],
          equals(BABY_TRIGGER.toMap()));
      expect(e[EvolutionChain.CHAIN_FIELD_NAME], equals(CHAIN.toMap()));
    });
  });
}
