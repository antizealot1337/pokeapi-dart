// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/evolution/evolution_trigger.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:test/test.dart';

void main() {
  group('EvolutionTrigger', () {
    const ID = 1;
    const NAME = 'name';
    const NAME_NAME = 'name name';
    const NAME_ = Name(NAME_NAME, NamedApiResource('', ''));
    const POKEMON_SPECIES_NAME = 'pokemon species';
    const POKEMON_SPECIES = NamedApiResource(POKEMON_SPECIES_NAME, '');

    test('fromMap', () {
      final e = EvolutionTrigger.fromMap({
        EvolutionTrigger.ID_FIELD_NAME: ID,
        EvolutionTrigger.NAME_FIELD_NAME: NAME,
        EvolutionTrigger.NAMES_FIELD_NAME: [NAME_.toMap()],
        EvolutionTrigger.POKEMON_SPECIES_FIELD_NAME: [POKEMON_SPECIES.toMap()],
      });

      expect(e.id, equals(ID));
      expect(e.name, equals(NAME));
      expect(e.names.length, equals(1));
      expect(e.names.first.name, equals(NAME_NAME));
      expect(e.pokemonSpecies.length, equals(1));
      expect(e.pokemonSpecies.first.name, equals(POKEMON_SPECIES_NAME));
    });

    test('toMap', () {
      final e = EvolutionTrigger(ID, NAME, [NAME_], [POKEMON_SPECIES]).toMap();

      expect(e[EvolutionTrigger.ID_FIELD_NAME], equals(ID));
      expect(e[EvolutionTrigger.NAME_FIELD_NAME], equals(NAME));
      expect(e[EvolutionTrigger.NAMES_FIELD_NAME], equals([NAME_.toMap()]));
      expect(e[EvolutionTrigger.POKEMON_SPECIES_FIELD_NAME],
          equals([POKEMON_SPECIES.toMap()]));
    });
  });
}
