// SPDX-License-Identifier: MIT

import 'package:pokeapi/pokeapi.dart';
import 'package:pokeapi/src/models/models.dart';
import 'package:test/test.dart';

void main() {
  group('PokeAPIClient', () {
    group('(HTTP request)', () {
      group('get', () {
        final client = PokeAPIClient.defaultClient();
        test('Berry', () async {
          final berry = await client
              .get<Berry>(Uri.parse('https://pokeapi.co/api/v2/berry/1'));
          expect(berry.id, equals(1));
        });
      });

      group('resources', () {
        final client = PokeAPIClient.defaultClient();

        test('berries', () async {
          final results = await client.berries();
          expect(results.count, isNonZero);
        });

        test('berryFirmnesses', () async {
          final results = await client.berryFirmnesses();
          expect(results.count, isNonZero);
        });

        test('berryFlavors', () async {
          final results = await client.berryFlavors();
          expect(results.count, isNonZero);
        });

        test('contestTypes', () async {
          final results = await client.contestTypes();
          expect(results.count, isNonZero);
        });

        test('contestEffects', () async {
          final results = await client.contestEffects();
          expect(results.count, isNonZero);
        });

        test('superContestEffects', () async {
          final results = await client.superContestEffects();
          expect(results.count, isNonZero);
        });

        test('encounterMethods', () async {
          final results = await client.encounterMethods();
          expect(results.count, isNonZero);
        });

        test('encounterConditions', () async {
          final results = await client.encounterConditions();
          expect(results.count, isNonZero);
        });

        test('encounterConditionValues', () async {
          final results = await client.encounterConditionValues();
          expect(results.count, isNonZero);
        });

        test('evolutionChains', () async {
          final results = await client.evolutionChains();
          expect(results.count, isNonZero);
        });

        test('evolutionTriggers', () async {
          final results = await client.evolutionTriggers();
          expect(results.count, isNonZero);
        });

        test('generations', () async {
          final results = await client.generations();
          expect(results.count, isNonZero);
        });

        test('pokedexes', () async {
          final results = await client.pokedexes();
          expect(results.count, isNonZero);
        });

        test('versions', () async {
          final results = await client.versions();
          expect(results.count, isNonZero);
        });

        test('versionGroups', () async {
          final results = await client.versionGroups();
          expect(results.count, isNonZero);
        });

        test('items', () async {
          final results = await client.items();
          expect(results.count, isNonZero);
        });

        test('itemAttributes', () async {
          final results = await client.itemAttributes();
          expect(results.count, isNonZero);
        });

        test('itemCategories', () async {
          final results = await client.itemCategories();
          expect(results.count, isNonZero);
        });

        test('itemFlingEffects', () async {
          final results = await client.itemFlingEffects();
          expect(results.count, isNonZero);
        });

        test('itemPockets', () async {
          final results = await client.itemPockets();
          expect(results.count, isNonZero);
        });

        test('locations', () async {
          final results = await client.locations();
          expect(results.count, isNonZero);
        });

        test('locationAreas', () async {
          final results = await client.locationAreas();
          expect(results.count, isNonZero);
        });

        test('regions', () async {
          final results = await client.regions();
          expect(results.count, isNonZero);
        });

        test('machines', () async {
          final results = await client.machines();
          expect(results.count, isNonZero);
        });

        test('moves', () async {
          final results = await client.moves();
          expect(results.count, isNonZero);
        });

        test('moveAilments', () async {
          final results = await client.moveAilments();
          expect(results.count, isNonZero);
        });

        test('moveBattleStyles', () async {
          final results = await client.moveBattleStyles();
          expect(results.count, isNonZero);
        });

        test('moveCategories', () async {
          final results = await client.moveCategories();
          expect(results.count, isNonZero);
        });

        test('moveDamageClasses', () async {
          final results = await client.moveDamageClasses();
          expect(results.count, isNonZero);
        });

        test('moveLearnMethods', () async {
          final results = await client.moveLearnMethods();
          expect(results.count, isNonZero);
        });

        test('moveTargets', () async {
          final results = await client.moveTargets();
          expect(results.count, isNonZero);
        });

        test('abilities', () async {
          final results = await client.abilities();
          expect(results.count, isNonZero);
        });

        test('characteristics', () async {
          final results = await client.characteristics();
          expect(results.count, isNonZero);
        });

        test('eggGroups', () async {
          final results = await client.eggGroups();
          expect(results.count, isNonZero);
        });

        test('genders', () async {
          final results = await client.genders();
          expect(results.count, isNonZero);
        });

        test('growthRates', () async {
          final results = await client.growthRates();
          expect(results.count, isNonZero);
        });

        test('natures', () async {
          final results = await client.natures();
          expect(results.count, isNonZero);
        });

        test('pokeathlonStats', () async {
          final results = await client.pokeathlonStats();
          expect(results.count, isNonZero);
        });

        test('allPokemon', () async {
          final results = await client.allPokemon();
          expect(results.count, isNonZero);
        });

        test('pokemonColors', () async {
          final results = await client.pokemonColors();
          expect(results.count, isNonZero);
        });

        test('pokemonForms', () async {
          final results = await client.pokemonForms();
          expect(results.count, isNonZero);
        });

        test('pokemonHabitats', () async {
          final results = await client.pokemonHabitats();
          expect(results.count, isNonZero);
        });

        test('pokemonShapes', () async {
          final results = await client.pokemonShapes();
          expect(results.count, isNonZero);
        });

        test('allPokemonSpecies', () async {
          final results = await client.allPokemonSpecies();
          expect(results.count, isNonZero);
        });

        test('allPokemonSpecies', () async {
          final results = await client.allPokemonSpecies();
          expect(results.count, isNonZero);
        });

        test('stats', () async {
          final results = await client.stats();
          expect(results.count, isNonZero);
        });

        test('types', () async {
          final results = await client.types();
          expect(results.count, isNonZero);
        });

        test('languages', () async {
          final results = await client.languages();
          expect(results.count, isNonZero);
        });
      });

      group('models', () {
        final client = PokeAPIClient.defaultClient();

        group('berry', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.berry(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('berryFirmness', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.berryFirmness(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('berryFlavor', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.berryFlavor(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('contestType', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.contestType(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('contestEffect', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.berry(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('superContestEffect', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.superContestEffect(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('encounterMethod', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.encounterMethod(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('encounterCondition', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.encounterCondition(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('encounterConditionValue', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.encounterConditionValue(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('evolutionChain', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.evolutionChain(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('evolutionTrigger', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.evolutionTrigger(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('generation', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.generation(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('pokedex', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.generation(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('version', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.version(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('versionGroup', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.versionGroup(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('item', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.item(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('itemAttribute', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.itemAttribute(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('itemCategory', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.itemCategory(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('itemFlingEffect', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.itemFlingEffect(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('itemPocket', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.itemPocket(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('location', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.location(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('locationArea', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.locationArea(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('palParkArea', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.palParkArea(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('region', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.region(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('machine', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.machine(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('move', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.move(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('moveAilment', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.moveAilment(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('moveBattleStyle', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.moveBattleStyle(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('moveCategory', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.moveCategory(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('moveDamageClass', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.moveDamageClass(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('moveLearnMethod', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.moveLearnMethod(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('moveTarget', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.moveTarget(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('ability', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.ability(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('characteristic', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.characteristic(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('eggGroup', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.eggGroup(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('gender', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.gender(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('growthRate', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.growthRate(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('nature', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.nature(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('pokeathlonStat', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.pokeathlonStat(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('pokemon', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.pokemon(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('pokemonLocationArea', () {
          group('(pokemon) id', () {
            test('1', () async {
              const ID = 1;
              const LOCATION_AREA_NAME = 'cerulean-city-area';
              final results = await client.pokemonLocationAreas(ID);
              expect(
                  results.first.locationArea.name, equals(LOCATION_AREA_NAME));
            });
          });
        });

        group('pokemonColor', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.pokemonColor(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('pokemonForm', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.pokemonForm(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('pokemonHabitat', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.pokemonHabitat(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('pokemonShape', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.pokemonShape(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('pokemonSpecies', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.pokemonSpecies(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('stat', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.stat(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('type', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.type(ID);
              expect(results.id, equals(ID));
            });
          });
        });

        group('language', () {
          group('id', () {
            test('1', () async {
              const ID = 1;
              final results = await client.language(ID);
              expect(results.id, equals(ID));
            });
          });
        });
      });
    }, skip: 'Requests to https://pokeapi.co and should be run deliberately');
  });
}
