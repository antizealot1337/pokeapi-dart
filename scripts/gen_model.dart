// SPDX-License-Identifier: MIT
//
// This was used to generate some of the models used in the project. Since all
// the models are done it is not longer needed. It is only to be preserved in
// this project.
//
// This is a command line program to help create models and tests for them. It
// can be used in an "interactive" mode or strictly from the command line via
// flags. While operating in interactive mode the program will prompt the user
// for the required information. It is also possible to provide default
// information via the command line. If defaults are provided then the program
// will still prompt, but will accept the default in the event invalid
// (or empty) values are provided. When not in interative mode valid values
// are required for the module and class name at a minimum.
//
// The distiction between a "main" or "support" class really only affects the
// comments generated at the top.
import 'dart:io';

import 'package:args/args.dart';

const MODEL_DIRECTORIES = [
  'berries',
  'contests',
  'encounters',
  'evolution',
  'games',
  'items',
  'locations',
  'machines',
  'moves',
  'pokemon',
  'language',
  'common',
];

extension StringFirstChar on String {
  String first() => substring(0, 1);
}

extension Rune on int {
  static final _UPPER_START = 'A'.codeUnitAt(0);
  static final _LOWER_START = 'a'.codeUnitAt(0);
  static final _UPPER_END = 'Z'.codeUnitAt(0);
  static final _DIFFERENCE = _LOWER_START - _UPPER_START;

  bool get isUpper => this >= _UPPER_START && this <= _UPPER_END;

  int toLower() => this + _DIFFERENCE;

  String toRuneString() => String.fromCharCode(this);
}

extension StringToSnakeCase on String {
  String toSnakeCase() {
    if (isEmpty) {
      return this;
    }

    var first = true;

    return runes
        .map((rune) {
          if (first) {
            first = false;
            if (rune.isUpper) {
              rune = rune.toLower();
            }
            return rune.toRuneString();
          }

          return rune.isUpper
              ? '_${rune.toLower().toRuneString()}'
              : rune.toRuneString();
        })
        .toList()
        .join();
  }
}

extension FieldString on String {
  String get staticName => '${keyValue.toUpperCase()}_FIELD_NAME';
  String get keyValue => toSnakeCase();
}

extension TypeString on String {
  static const _TYPE_DEFAULT_VALUES = {
    'bool': 'false',
    'int': '0',
    'double': '0.0',
    'String': "''",
  };

  bool get isBuiltin => _TYPE_DEFAULT_VALUES.containsKey(this);
  bool get isList => startsWith('List<');
  String get listType => substring(5, length - 1);
  String get defaultValue =>
      isList ? '[]' : _TYPE_DEFAULT_VALUES[this] ?? 'null';
}

class Field {
  late final String name;
  late final String type;

  Field({required this.name, required this.type});
  Field.fromLine(String line) {
    var parts = line.split(' ');
    if (parts.length < 2) throw Exception('invalid field line: $line');

    var typeTmp = parts[0].trim();

    // Fix some common typing mistakes (pun intended)
    switch (typeTmp) {
      case '':
        throw Exception('invalid field line: $line');
      case 'b':
      case 'boolean':
        type = 'bool';
        break;
      case 'i':
      case 'Int':
      case 'integer':
        type = 'int';
        break;
      case 'd':
      case 'float':
        type = 'double';
        break;
      case 's':
      case 'str':
      case 'string':
        type = 'String';
        break;
      default:
        type = typeTmp;
        break;
    }

    name = parts[1].trim();
    if (name.isEmpty) throw Exception('invalid field line: $line');
  }
}

class Context {
  final String className;
  final String modelDir;
  final bool isSupport;
  final bool generateClass;
  final bool generateTest;
  final bool dryRun;
  final bool verbose;
  final List<Field> fields;

  Context({
    required this.className,
    required this.modelDir,
    this.fields = const [],
    this.isSupport = false,
    this.generateClass = true,
    this.generateTest = true,
    this.dryRun = false,
    this.verbose = false,
  });

  Context.empty()
      : className = '',
        modelDir = '',
        fields = [],
        isSupport = false,
        generateClass = false,
        generateTest = true,
        dryRun = false,
        verbose = true;

  Context.copyWith(Context original,
      {String? className,
      String? modelDir,
      List<Field>? fields,
      bool? isSupport,
      bool? generateClass,
      bool? generateTest,
      bool? dryRun,
      bool? verbose})
      : className = className ?? original.className,
        modelDir = modelDir ?? original.modelDir,
        fields = fields ?? original.fields,
        isSupport = isSupport ?? original.isSupport,
        generateClass = generateClass ?? original.generateClass,
        generateTest = generateTest ?? original.generateTest,
        dryRun = dryRun ?? original.dryRun,
        verbose = verbose ?? original.verbose;

  bool get isVerbose => verbose || dryRun;

  Context updateWith(
          {String? className,
          String? modelDir,
          List<Field>? fields,
          bool? isSupport,
          bool? generateClass,
          bool? generateTest,
          bool? dryRun,
          bool? verbose}) =>
      Context(
        className: className ?? this.className,
        modelDir: modelDir ?? this.modelDir,
        fields: fields ?? this.fields,
        isSupport: isSupport ?? this.isSupport,
        generateClass: generateClass ?? this.generateClass,
        generateTest: generateTest ?? this.generateTest,
        dryRun: dryRun ?? this.dryRun,
        verbose: verbose ?? this.verbose,
      );

  void generate() async {
    if (className.isEmpty) throw Exception('Class name is empty');
    if (modelDir.isEmpty) throw Exception('Module directory is emtpy');
    if (generateClass) await _writeClass();
    if (generateTest) await _writeTest();
  }

  Future<void> _writeClass() async {
    final outPath = _modelDirPath();

    if (isVerbose) {
      print('Writing (${isSupport ? 'support' : 'main'}) file $outPath');
    } //if

    final output = isSupport ? _supportClassTemplate() : _mainClassTemplate();

    if (dryRun) {
      print(output);
      return;
    }

    final f = File(outPath);
    await f.writeAsString(output);
  }

  Future<void> _writeTest() async {
    final outPath = _testDirPath();

    if (isVerbose) {
      print('Writing test file $outPath');
    } //if

    final output = _testTemplate();

    if (dryRun) {
      print(output);
      return;
    }

    final f = File(outPath);
    await f.writeAsString(output);
  }

  String _modelDirPath() => _outputPath('lib/src', className.toSnakeCase());

  String _testDirPath() =>
      _outputPath('test', '${className.toSnakeCase()}_test');

  String _outputPath(String parent, String filename) {
    switch (modelDir) {
      case 'languages':
      case 'common':
        return '$parent/models/utility/$modelDir/$filename.dart';
      default:
        return '$parent/models/$modelDir/$filename.dart';
    }
  }

  String _mainClassTemplate() =>
      '''${_mainCommentsTemplate()}${_classDefinitionTemplate()}
''';

  String _supportClassTemplate() =>
      '''${_supportCommentsTemplate()}${_classDefinitionTemplate()}
''';

  String _mainCommentsTemplate() => '''/// From PokeAPI documentation:
///
///
/// The fields provide information from the PokeAPI documentation.
''';

  String _supportCommentsTemplate() =>
      '''/// This contains information about a $className.
///
/// The fields provide information from the PokeAPI documentation.
''';

  String _classDefinitionTemplate() => '''class $className {
${_staticFieldNames()}
${_classFields()}

  /// Create a new $className.
  $className(${fields.map((field) => 'this.${field.name}').join(', ')});

  /// Create a new $className from a Map<String, dynamic>.
  $className.fromMap(Map<String, dynamic> data)
    : ${_classFromMapInitializers()};
}
''';

  String _staticFieldNames() => fields
      .map((field) =>
          '''  /// The name of the ${field.name.replaceAll('_', ' ')} field in the JSON data.
  static const ${field.name.staticName} = '${field.name.keyValue}';
''')
      .join('\n');

  String _classFields() => fields.map((field) => '''  ///
  final ${field.type} ${field.name};
''').join('\n');

  String _classFromMapInitializers() => fields
      .map((field) => field.type.isList
          ? '${field.name} = (data[${field.name.staticName}] as List<dynamic>).map((e) => ${field.type.listType}.fromMap(e)).toList()'
          : field.type.isBuiltin
              ? '${field.name} = data[${field.name.staticName}]'
              : '${field.name} = ${field.type}.fromMap(data[${field.name.staticName}])')
      .join(', ');

  String _testTemplate() =>
      '''import 'package:pokeapi/${_modelDirPath().substring(4)}';
import 'package:test/test.dart';

void main() {
  group('$className', () {
    test('fromMap', () {
      final ${className.first().toLowerCase()} = $className.fromMap({
${fields.map((field) => '        $className.${field.name.staticName}: ${field.type.defaultValue}, // TODO').join('\n')}
      });

${fields.map((field) => '      expect(${className.first().toLowerCase()}.${field.name}, equals(${field.type.defaultValue})); // TODO').join('\n')}
    });
  });
}''';
}

void main(List<String> args) {
  const SUPPORT_CLASS = 'support-class';
  const MODEL_DIRECTORY = 'model-directory';
  const DRY_RUN = 'dry-run';
  const VERBOSE = 'verbose';
  const GENERATE_CLASS = 'generate-class';
  const GENERATE_TEST = 'generate-test';
  const FIELD = 'field';
  const INTERACTIVE = 'interactive';

  final parser = ArgParser()
    ..addFlag(SUPPORT_CLASS,
        abbr: SUPPORT_CLASS.first(),
        help: 'Generate a name for a support class')
    ..addOption(MODEL_DIRECTORY,
        abbr: MODEL_DIRECTORY.first(),
        allowed: MODEL_DIRECTORIES,
        help: 'The name of the model directory')
    ..addFlag(VERBOSE, abbr: VERBOSE.first(), help: 'Print more information')
    ..addFlag(DRY_RUN,
        abbr: DRY_RUN.first().toUpperCase(),
        help: "Don't actually write anything")
    ..addFlag(GENERATE_CLASS,
        abbr: 'C',
        negatable: true,
        help: 'Generate a model main/support class file')
    ..addFlag(GENERATE_TEST,
        abbr: 'T', negatable: true, help: 'Generatea a test file')
    ..addMultiOption(FIELD,
        abbr: FIELD.first(),
        splitCommas: true,
        help: 'Field names for the class in the form of name=type')
    ..addFlag(INTERACTIVE, help: 'Prompt for values');

  final results = parser.parse(args);

  if (results.rest.isEmpty && !results[INTERACTIVE]) {
    throw ArgumentError('class name must be provided as an argument');
  }

  final context = results[INTERACTIVE] as bool
      ? interactive(Context(
          className: results.rest.first,
          modelDir: results[MODEL_DIRECTORY] ?? '',
          dryRun: results[DRY_RUN],
          verbose: results[VERBOSE],
        ))
      : Context(
          className: results.rest.first,
          modelDir: results[MODEL_DIRECTORY],
          isSupport: results[SUPPORT_CLASS],
          generateClass: results[GENERATE_CLASS],
          generateTest: results[GENERATE_TEST],
          dryRun: results[DRY_RUN],
          verbose: results[VERBOSE],
        );

  context.generate();
}

Context interactive(final Context defaults) => defaults.updateWith(
      className: promptStr('Enter the class name (${defaults.className}): ',
          defaultValue: defaults.className),
      modelDir: promptStr(
          'Enter the model directory${defaults.modelDir.isEmpty ? '' : ' (${defaults.modelDir})'}: ',
          defaultValue: defaults.modelDir,
          validator: (line) => MODEL_DIRECTORIES.contains(line),
          validHint: 'expecting on of ${MODEL_DIRECTORIES.join(', ')}'),
      fields: promptMulti(
          'Enter one field per line in the form of "type name" (emtpy line ends input):\n',
          converter: (line) => Field.fromLine(line),
          hint: 'field must be in the form of "type name"'),
      isSupport: promptBool(
          'Is this a support class (${defaults.isSupport ? 'Y/n' : 'N/y'})? ',
          defaultValue: defaults.isSupport),
      generateClass: promptBool(
          'Generate the class file (${defaults.generateClass ? 'Y/n' : 'N/y'})? ',
          defaultValue: defaults.generateClass),
      generateTest: promptBool(
          'Generate the test file (${defaults.generateClass ? 'Y/n' : 'N/y'})? ',
          defaultValue: defaults.generateTest),
      verbose: true,
    );

String promptStr(String message,
    {String? defaultValue,
    bool Function(String?)? validator,
    String? validHint}) {
  var value = '';
  if (defaultValue?.isEmpty ?? false) {
    defaultValue = null;
  }

  validator ??= (s) => !(s == null || s.isEmpty);
  validHint ??= 'value must not be an empty string';

  if (defaultValue == null) {
    while (value.isEmpty) {
      stdout.write(message);
      value = stdin.readLineSync() ?? '';

      if (!validator(value)) {
        print('error: invalid value provided (hint: $validHint)');
      }
    }
  } else {
    stdout.write('$message');
    value = stdin.readLineSync() ?? defaultValue;
    if (value.isEmpty) {
      value = defaultValue;
    }
  }

  return value;
}

bool promptBool(String message, {bool? defaultValue}) {
  bool? value;

  while (value == null) {
    switch (promptStr(message, defaultValue: defaultValue?.toString())) {
      case 'true':
      case 't':
      case 'yes':
      case 'y':
        value = true;
        break;
      case 'false':
      case 'f':
      case 'no':
      case 'n':
        value = false;
        break;
      default:
        if (defaultValue != null) {
          value = defaultValue;
        } else {
          value = null;
        }
    }
  }

  return value;
}

List<String> promptLines(String message) {
  stdout.write(message);

  final values = <String>[];

  while (true) {
    final value = stdin.readLineSync();
    if (value == null || value.isEmpty) {
      break;
    }
    values.add(value);
  }

  return values;
}

List<T> promptMulti<T>(String message,
    {required T Function(String) converter, String? hint}) {
  stdout.write(message);

  final values = <T>[];

  while (true) {
    final line = stdin.readLineSync();

    if (line == null || line.isEmpty) {
      break;
    }

    try {
      final value = converter(line);
      values.add(value);
    } catch (e) {
      print('error: $e');
      if (hint != null) print('hint: $hint');
    }
  }

  return values;
}
