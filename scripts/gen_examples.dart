// SPDX-License-Identifier: MIT
//
// This was used to generate some of the examples in the project. It is included
// in the project for preservation purposes.

import 'dart:io';
import 'package:args/args.dart';

abstract class Info {
  final String method;
  final String? commentName;

  const Info(this.method, [this.commentName]);

  String get variable => method;

  String source();
}

class ObjectInfo extends Info {
  const ObjectInfo(String method, [String? comment]) : super(method, comment);

  @override
  String source() => '''
// SPDX-License-Identifier: MIT

// ignore_for_file: unused_local_variable

import 'package:pokeapi/pokeapi.dart';

void main() async {
  const ID = 1;

  // Get the default client.
  final client = PokeAPIClient.defaultClient();

  // Get the ${commentName ?? variable} by id
  final $variable = await client.$method(ID);

  // Use results for stuff...
}
''';
}

class ResourceInfo extends Info {
  const ResourceInfo(String method, [String? comment]) : super(method, comment);

  @override
  String source() => '''
// SPDX-License-Identifier: MIT

// ignore_for_file: unused_local_variable

import 'package:pokeapi/pokeapi.dart';

void main() async {
  // Get the default client.
  final client = PokeAPIClient.defaultClient();

  // Get the ${commentName ?? variable}
  final $variable = await client.$method();

  // Use results for stuff...
}
''';
}

class Model {
  final String dirName;
  final ObjectInfo object;
  final ResourceInfo resource;

  const Model(this.dirName, {required this.object, required this.resource});
}

const sections = <String, List<Model>>{
  'berries': [
    Model(
      'berry',
      object: ObjectInfo('berry'),
      resource: ResourceInfo('berries'),
    ),
    Model(
      'berry_firmness',
      object: ObjectInfo('berryFirmness', 'berry firmness'),
      resource: ResourceInfo('berryFirmnesses', 'berry firmnesses'),
    ),
    Model(
      'berry_flavor',
      object: ObjectInfo('berryFlavor', 'berry flabor'),
      resource: ResourceInfo('berryFirmnesses', 'berry firmnesses'),
    ),
  ],
  'contests': [
    Model(
      'contest_effect',
      object: ObjectInfo('contestEffect', 'contest effect'),
      resource: ResourceInfo('contestEffects', 'contest effects'),
    ),
    Model(
      'contest_type',
      object: ObjectInfo('contestType', 'contest type'),
      resource: ResourceInfo('contestTypes', 'contest types'),
    ),
    Model(
      'super_contest_type',
      object: ObjectInfo('superContestType', 'super contest type'),
      resource: ResourceInfo('superContestTypes', 'super contest types'),
    ),
  ],
  'encounters': [
    Model(
      'encounter_condition_values',
      object:
          ObjectInfo('encounterConditionValue', 'encounter condition value'),
      resource: ResourceInfo(
          'ecnounterConditionValues', 'encounter condition values'),
    ),
    Model(
      'encounter_conditions',
      object: ObjectInfo('encounterCondition', 'encounter condition'),
      resource: ResourceInfo('encounterConditions', 'encounter conditions'),
    ),
    Model(
      'encounter_methods',
      object: ObjectInfo('encounterMethod', 'encounter method'),
      resource: ResourceInfo('encounterMethods', 'encounter methods'),
    ),
  ],
  'evolution': [
    Model(
      'evolution_chains',
      object: ObjectInfo('evolutionChain', 'evolution chain'),
      resource: ResourceInfo('evolutionChains', 'evolution chains'),
    ),
    Model(
      'evolution_triggers',
      object: ObjectInfo('evolutionTrigger', 'evolution trigger'),
      resource: ResourceInfo('evolutionTriggers', 'evolution triggers'),
    ),
  ],
  'games': [
    Model(
      'generations',
      object: ObjectInfo('generation'),
      resource: ResourceInfo('generations'),
    ),
    Model(
      'pokedexes',
      object: ObjectInfo('pokedex'),
      resource: ResourceInfo('pokedexes'),
    ),
    Model(
      'version_groups',
      object: ObjectInfo('versionGroup', 'version group'),
      resource: ResourceInfo('versionGroups', 'version groups'),
    ),
    Model(
      'versions',
      object: ObjectInfo('version'),
      resource: ResourceInfo('versions'),
    ),
  ],
  'items': [
    Model(
      'item',
      object: ObjectInfo('item'),
      resource: ResourceInfo('items'),
    ),
    Model(
      'item_attribute',
      object: ObjectInfo('itemAttribute', 'item attribute'),
      resource: ResourceInfo('itemAttributes', 'item attributes'),
    ),
    Model(
      'item_category',
      object: ObjectInfo('itemCategory', 'item category'),
      resource: ResourceInfo('itemCategories', 'item categories'),
    ),
    Model(
      'item_fling_effect',
      object: ObjectInfo('itemFlingEffect', 'item fling effect'),
      resource: ResourceInfo('itemFlingEffects', 'item fling effects'),
    ),
    Model(
      'item_pocket',
      object: ObjectInfo('itemPocket', 'item pocket'),
      resource: ResourceInfo('itemPockets', 'item pockets'),
    ),
  ],
  'locations': [
    Model(
      'location',
      object: ObjectInfo('location'),
      resource: ResourceInfo('locations'),
    ),
    Model(
      'location_area',
      object: ObjectInfo('locationArea', 'location area'),
      resource: ResourceInfo('locationAreas', 'location areas'),
    ),
    Model(
      'pal_park_area',
      object: ObjectInfo('palParkArea', 'PalPark area'),
      resource: ResourceInfo('palParkAreas', 'PalPark areas'),
    ),
    Model(
      'region',
      object: ObjectInfo('region'),
      resource: ResourceInfo('regions'),
    ),
  ],
  'machines': [
    Model(
      'machines',
      object: ObjectInfo('machine'),
      resource: ResourceInfo('machines'),
    ),
  ],
  'moves': [
    Model(
      'move',
      object: ObjectInfo('move'),
      resource: ResourceInfo('moves'),
    ),
    Model(
      'move_aliment',
      object: ObjectInfo('moveAilment', 'move ailment'),
      resource: ResourceInfo('moveAilments', 'move ailments'),
    ),
    Model(
      'move_battle_style',
      object: ObjectInfo('moveBattleStyle', 'move battle style'),
      resource: ResourceInfo('moveBattleStyles', 'move battle styles'),
    ),
    Model(
      'move_category',
      object: ObjectInfo('moveCategory', 'move category'),
      resource: ResourceInfo('moveCategories', 'move categories'),
    ),
    Model(
      'move_damage_class',
      object: ObjectInfo('moveDamageClass', 'move damage class'),
      resource: ResourceInfo('moveDamageClasses', 'move damage classes'),
    ),
    Model(
      'move_learn_method',
      object: ObjectInfo('moveLearnMethod', 'move learn method'),
      resource: ResourceInfo('moveLearnMethods', 'move learn methods'),
    ),
    Model(
      'move_target',
      object: ObjectInfo('moveTarget', 'move target'),
      resource: ResourceInfo('moveTargets', 'move targets'),
    ),
  ],
  'pokemon': [
    Model(
      'abilities',
      object: ObjectInfo('ability'),
      resource: ResourceInfo('abilities'),
    ),
    Model(
      'characteristics',
      object: ObjectInfo('characteristic'),
      resource: ResourceInfo('characteristics'),
    ),
    Model(
      'egg_groups',
      object: ObjectInfo('eggGroup', 'egg group'),
      resource: ResourceInfo('eggGroups', 'egg groups'),
    ),
    Model(
      'genders',
      object: ObjectInfo('gender'),
      resource: ResourceInfo('genders'),
    ),
    Model(
      'growth_rates',
      object: ObjectInfo('growthRate', 'growth rate'),
      resource: ResourceInfo('growthRates', 'growth rates'),
    ),
    Model(
      'natures',
      object: ObjectInfo('nature'),
      resource: ResourceInfo('natures'),
    ),
    Model(
      'pokeathlon_stats',
      object: ObjectInfo('pokeathlonStat', 'Pokeathlon stat'),
      resource: ResourceInfo('pokeathlonStats', 'Pokeathlon stats'),
    ),
    Model(
      'pokemon',
      object: ObjectInfo('pokemon'),
      resource: ResourceInfo('allPokemon', 'pokemon'),
    ),
    Model(
      'pokemon_location_areas',
      object: ObjectInfo('pokemonLocationAreas', 'pokemon encounter'),
      resource: ResourceInfo('pokemonLocationAreas'),
    ), // This generates bad examples that need to be fixed manually
    Model(
      'pokemon_colors',
      object: ObjectInfo('pokemonColor', 'pokemon color'),
      resource: ResourceInfo('pokemonColors', 'pokemon colors'),
    ),
    Model(
      'pokemon_forms',
      object: ObjectInfo('pokemonForm', 'pokemon form'),
      resource: ResourceInfo('pokemonForms', 'pokemon forms'),
    ),
    Model(
      'pokemon_habitats',
      object: ObjectInfo('pokemonHabitat', 'pokemon habitat'),
      resource: ResourceInfo('pokemonHabitats', 'pokemon habitats'),
    ),
    Model(
      'pokemon_shapes',
      object: ObjectInfo('pokemonShape', 'pokemon shape'),
      resource: ResourceInfo('pokemonShapes', 'pokemon shapes'),
    ),
    Model(
      'pokemon_species',
      object: ObjectInfo('pokemonSpecies', 'pokemon species'),
      resource: ResourceInfo('allPokemonSpecies', 'pokemon species'),
    ),
    Model(
      'stats',
      object: ObjectInfo('stat'),
      resource: ResourceInfo('stats'),
    ),
    Model(
      'types',
      object: ObjectInfo('type'),
      resource: ResourceInfo('types'),
    ),
  ],
  'utility': [
    Model(
      'languages',
      object: ObjectInfo('language'),
      resource: ResourceInfo('languages'),
    ),
  ],
};

void main(List<String> args) {
  const FORCE = 'force';
  const VERBOSE = 'verbose';
  const DIRECTORY = 'directory';
  const SHOW_USAGE = 'help';

  final parser = ArgParser()
    ..addFlag(FORCE,
        abbr: 'f',
        negatable: false,
        defaultsTo: false,
        help: 'Overwrite and existing files')
    ..addFlag(VERBOSE,
        abbr: 'v',
        negatable: false,
        defaultsTo: false,
        help: 'Be more informative')
    ..addOption(DIRECTORY,
        abbr: 'd', help: 'The root directory to run the command in')
    ..addFlag(SHOW_USAGE, negatable: false, help: 'Print the usage and exit');

  final results = parser.parse(args);

  if (results[SHOW_USAGE]) {
    print(parser.usage);
    return;
  }

  if (results[DIRECTORY] != null) {
    Directory.current = results[DIRECTORY] as String;
    if (results[VERBOSE]) {
      print('Changing current directory to ${results[DIRECTORY] as String}');
    }
  }

  sections.forEach((groupDir, models) => models.forEach((model) =>
      createExamples(groupDir, model, results[VERBOSE], results[FORCE])));
}

void createExamples(String parent, Model model, bool verbose, bool force) {
  final path = createDir(parent, model.dirName, verbose);
  createObjectExample(path, model.object, verbose, force);
  createResourceExample(path, model.resource, verbose, force);
}

String createDir(String parent, String dirName, bool verbose) {
  final path = 'example/endpoints/$parent/$dirName';
  final d = Directory(path);

  if (d.existsSync()) {
    if (verbose) print('dir exists $path');
  } else {
    d.createSync(recursive: true);
    if (verbose) print('dir created $path');
  }
  return path;
}

void createObjectExample(
    String parent, ObjectInfo object, bool verbose, bool force) {
  final file = '$parent/object.dart';
  writeFile(file, object, verbose, force);
}

void createResourceExample(
    String parent, ResourceInfo resource, bool verbose, bool force) {
  final file = '$parent/resource.dart';
  writeFile(file, resource, verbose, force);
}

void writeFile(String name, Info info, bool verbose, bool force) {
  final file = File(name);
  if (!force && file.existsSync()) {
    if (verbose) print('file exists $name');
    return;
  }

  file.writeAsStringSync(info.source());
  if (verbose) print('file created $name');
}
