// SPDX-License-Identifier: MIT
//
// This file was used to generate the README. It is stored in the project for
// preservation.

import 'dart:io';

import 'package:args/args.dart';

extension Plural on String {
  String get plural => endsWith('y')
      ? '${substring(0, length - 1)}ies'
      : endsWith('s') || endsWith('x')
          ? '${this}es'
          : '${this}s';
}

extension BeginsWithVowel on String {
  static final regexp = RegExp('[aA|eE|iI|oO|uU]');

  bool get beginsWithVowel => startsWith(regexp);
}

class Section {
  final String name;
  final String endpoint;
  final List<Endpoint> endpoints;

  const Section(this.name, this.endpoint, this.endpoints);

  String get groupName => '${name.toLowerCase()} group';
  String generate() => '''### [$name][$groupName]

${endpoints.map((endpoint) => endpoint.generate()).join('\n')}''';
}

class Endpoint {
  final String title;
  final String name;
  final String? pluralOverride;
  final String? comment;
  final String? commentPluralOverride;

  const Endpoint(
    this.title, {
    required this.name,
    this.pluralOverride,
    this.comment,
    this.commentPluralOverride,
  });

  String get plural => pluralOverride ?? name.plural;
  String get commentPlural =>
      commentPluralOverride ?? comment?.plural ?? name.plural;

  String get _a => name.beginsWithVowel ? 'an' : 'a';

  String generate() => '''<details>
<summary>$title</summary>

* Get all $commentPlural (resource).
```dart
final $plural = await client.$plural();
```
* Get $_a ${comment ?? name} (by id).
```dart
final $name = await client.$name(1);
```

</details>
''';
}

class PokemonLocationAreaEndpoint extends Endpoint {
  const PokemonLocationAreaEndpoint(String title)
      : super(title, name: 'pokemonLocationAreas');

  @override
  String generate() => '''<details>
<summary>$title</summary>

* Get all pokemon location areas by a Pokemon id.
```dart
final $name = await client.$name(1);
```

</details>
''';
}

const sections = <Section>[
  Section('Berries', 'https://pokeapi.co/docs/v2#berries-section', [
    Endpoint('Berries', name: 'berry', pluralOverride: 'berries'),
    Endpoint(
      'Berry Firmnesses',
      name: 'berryFirmness',
      comment: 'berry firmness',
    ),
    Endpoint('Berry Flavors', name: 'berryFlavor'),
  ]),
  Section('Contests', 'https://pokeapi.co/docs/v2#contests-section', [
    Endpoint(
      'Contest Types',
      name: 'contestType',
      comment: 'contest type',
    ),
    Endpoint(
      'Contest Effects',
      name: 'contestEffect',
      comment: 'contest effect',
    ),
    Endpoint(
      'Super Contest Effects',
      name: 'superContestEffect',
      comment: 'super contest effect',
    ),
  ]),
  Section('Encounters', 'https://pokeapi.co/docs/v2#encounters-section', [
    Endpoint(
      'Encounter Methods',
      name: 'encounterMethod',
      comment: 'encounter method',
    ),
    Endpoint(
      'Encounter Conditions',
      name: 'encounterCondition',
      comment: 'encounter condition',
    ),
    Endpoint(
      'Encounter Condition Values',
      name: 'encounterConditionValue',
      comment: 'encounter condition value',
    ),
  ]),
  Section('Evolution', 'https://pokeapi.co/docs/v2#evolution-section', [
    Endpoint(
      'Evolution Chains',
      name: 'evolutionChain',
      comment: 'evolution chain',
    ),
    Endpoint(
      'Evolution Triggers',
      name: 'evolutionTrigger',
      comment: 'evolution trigger',
    ),
  ]),
  Section('Games', 'https://pokeapi.co/docs/v2#games-section', [
    Endpoint('Generations', name: 'generation'),
    Endpoint('Pokedexes', name: 'pokedex'),
    Endpoint('Version', name: 'version'),
    Endpoint('Version Groups', name: 'versionGroup', comment: 'version group'),
  ]),
  Section('Items', 'https://pokeapi.co/docs/v2#item', [
    Endpoint('Item', name: 'item'),
    Endpoint(
      'Item Attributes',
      name: 'itemAttribute',
      comment: 'item attribute',
    ),
    Endpoint(
      'Item Categories',
      name: 'itemCategory',
      comment: 'item category',
    ),
    Endpoint(
      'Item Fling Effect',
      name: 'itemFlingEffect',
      comment: 'item fling effect',
    ),
    Endpoint(
      'Item Pockets',
      name: 'itemPocket',
      comment: ' item pocket',
    ),
  ]),
  Section('Locations', 'https://pokeapi.co/docs/v2#locations-section', [
    Endpoint('Locations', name: 'location'),
    Endpoint(
      'Location Areas',
      name: 'locationArea',
      comment: 'locationArea',
    ),
    Endpoint(
      'Pal Park Areas',
      name: 'palParkArea',
      comment: 'Pal Park area',
    ),
    Endpoint('Region', name: 'region'),
  ]),
  Section('Machines', 'https://pokeapi.co/docs/v2#machines-section', [
    Endpoint('Machines', name: 'machine'),
  ]),
  Section('Moves', 'https://pokeapi.co/docs/v2#moves-section', [
    Endpoint('Moves', name: 'move'),
    Endpoint('Move Ailments', name: 'moveAilment', comment: 'move ailment'),
    Endpoint(
      'Move Battle Styles',
      name: 'moveBattleStyle',
      comment: 'move battle style',
    ),
    Endpoint('Move Categories', name: 'moveCategory', comment: 'move category'),
    Endpoint(
      'Move Damage Class',
      name: 'moveDamageClass',
      comment: 'move damage class',
    ),
    Endpoint(
      'Move Learn Methods',
      name: 'moveLearnMethod',
      comment: 'move learn method',
    ),
    Endpoint('Move Targets', name: 'moveTarget', comment: 'move target'),
  ]),
  Section('Pokémon', 'https://pokeapi.co/docs/v2#pokemon-section', [
    Endpoint('Abilities', name: 'ability'),
    Endpoint('Characteristics', name: 'characteristic'),
    Endpoint('Egg Groups', name: 'eggGroup', comment: 'egg group'),
    Endpoint('Genders', name: 'gender'),
    Endpoint('Growth Rates', name: 'growthRate', comment: 'growth rate'),
    Endpoint('Natures', name: 'nature'),
    Endpoint(
      'Pokeathlon Stats',
      name: 'pokeathlonStat',
      comment: 'Pokeathlon stats',
    ),
    Endpoint(
      'Pokemon',
      name: 'pokemon',
      pluralOverride: 'allPokemon',
      comment: 'Pokemon',
      commentPluralOverride: 'Pokemon',
    ),
    PokemonLocationAreaEndpoint('Pokemon Location Areas'),
    Endpoint('Pokemon Colors', name: 'pokemonColor', comment: 'Pokemon color'),
    Endpoint('Pokemon Forms', name: 'pokemonForm', comment: 'Pokemon form'),
    Endpoint(
      'Pokemon Habitats',
      name: 'pokemonHabitat',
      comment: 'Pokemon habitat',
    ),
    Endpoint('Pokemon Shapes', name: 'pokemonShape', comment: 'Pokemon shape'),
    Endpoint(
      'Pokemon Species',
      name: 'pokemonSpecies',
      comment: 'Pokemon species',
      pluralOverride: 'allPokemonSpecies',
      commentPluralOverride: 'Pokemon species',
    ),
    Endpoint('Stats', name: 'stat'),
    Endpoint('Types', name: 'type'),
  ]),
  Section('Utility', 'https://pokeapi.co/docs/v2#utility-section', [
    Endpoint('Languages', name: 'language'),
  ]),
];

void main(List<String> args) {
  const HELP = 'help';
  const OUTPUT = 'output';

  final parser = ArgParser()
    ..addFlag(HELP, abbr: 'h', help: 'Print the help and exit')
    ..addOption(OUTPUT,
        abbr: 'o', help: 'Filename out output instead of the console');

  final results = parser.parse(args);

  if (results[HELP]) {
    print(parser.usage);
    return;
  }

  final readme = genReadme();

  if (results.wasParsed(OUTPUT)) {
    final f = File(results[OUTPUT]);
    f.writeAsStringSync(readme);
  } else {
    print(readme);
  }
}

String genReadme() => '''# PokeAPI

A dart library for access the excellent API ([v2][pokeapi docs]) from [PokeAPI][pokeapi].

## Fair Use Policy

PokeAPI is free  and open to use to use and views itself as primarliy an
educational tool. They ask every developer to abide by their
[Fair Use Policy][fair use policy]. They do not tolerate abuse (DoS, etc). They
also ask developers:
* Locally cache resources whenever you request them.
* Be nice and friendly to your fellow PokéAPI developers.

## Caching

This library does not do caching natively. Use a compatible HttpClient that has
cachine or implement it manually.

## Endpoints

${sections.map((section) => section.generate()).join('\n')}
## Features and bugs

Please file feature requests and bugs at the [issue tracker][tracker].

[pokeapi]: https://pokeapi.co
[pokeapi docs]: https://pokeapi.co/docs/v2
[fair use policy]: https://pokeapi.co/docs/v2#fairuse
${sections.map((section) => '[${section.groupName}]: ${section.endpoint}').join('\n')}
[tracker]: http://gitlab.com/antizealot1337/pokeapi-dart/issues''';
