// SPDX-License-Identifier: MIT

/// This constains information about an ItemSprites.
///
/// The fields provide information from the PokeAPI documentation.
class ItemSprites {
  /// The name of the default field in the JSON data.
  static const DEFAULT_FIELD_NAME = 'default';

  /// The default depiction of this item.
  final String default_;

  /// Create a new ItemSprites.
  const ItemSprites(this.default_);

  /// Create a new ItemSprites from a Map<String, dynamic>.
  ItemSprites.fromMap(Map<String, dynamic> data)
      : default_ = data[DEFAULT_FIELD_NAME];

  /// Convert an ItemSprites to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {DEFAULT_FIELD_NAME: default_};
}
