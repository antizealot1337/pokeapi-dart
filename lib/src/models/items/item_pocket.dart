// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// From PokeAPI documentation:
/// Pockets within the players bag used for storing items by category.
///
/// The fields provide information from the PokeAPI documentation.
class ItemPocket {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the name field in the JSON data.
  static const NAME_FIELD_NAME = 'name';

  /// The name of the categories field in the JSON data.
  static const CATEGORIES_FIELD_NAME = 'categories';

  /// The name of the names field in the JSON data.
  static const NAMES_FIELD_NAME = 'names';

  /// The identifier for this resource.
  final int id;

  /// The name for this resource.
  final String name;

  /// A list of item categories that are relevant to this item pocket.
  final List<NamedApiResource> categories;

  /// The name of this resource listed in different languages.
  final List<Name> names;

  /// Create a new ItemPocket.
  const ItemPocket(this.id, this.name, this.categories, this.names);

  /// Create a new ItemPocket from a Map<String, dynamic>.
  ItemPocket.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        name = data[NAME_FIELD_NAME],
        categories = NamedApiResource.listFrom(data[CATEGORIES_FIELD_NAME]),
        names = Name.listFrom(data[NAMES_FIELD_NAME]);

  /// Convert an ItemPocket to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        NAME_FIELD_NAME: name,
        CATEGORIES_FIELD_NAME: categories.map((e) => e.toMap()).toList(),
        NAMES_FIELD_NAME: names.map((e) => e.toMap()).toList(),
      };
}
