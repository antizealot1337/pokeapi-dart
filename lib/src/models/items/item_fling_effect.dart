// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/effect.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// From PokeAPI documentation:
/// The various effects of the move "Fling" when used with different items.
///
/// The fields provide information from the PokeAPI documentation.
class ItemFlingEffect {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the name field in the JSON data.
  static const NAME_FIELD_NAME = 'name';

  /// The name of the effect entries field in the JSON data.
  static const EFFECT_ENTRIES_FIELD_NAME = 'effect_entries';

  /// The name of the items field in the JSON data.
  static const ITEMS_FIELD_NAME = 'items';

  /// The identifier for this resource.
  final int id;

  /// The name for this resource.
  final String name;

  /// The result of this fling effect listed in different languages.
  final List<Effect> effectEntries;

  /// A list of items that have this fling effect.
  final List<NamedApiResource> items;

  /// Create a new ItemFlingEffect.
  const ItemFlingEffect(this.id, this.name, this.effectEntries, this.items);

  /// Create a new ItemFlingEffect from a Map<String, dynamic>.
  ItemFlingEffect.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        name = data[NAME_FIELD_NAME],
        effectEntries = Effect.listFrom(data[EFFECT_ENTRIES_FIELD_NAME]),
        items = NamedApiResource.listFrom(data[ITEMS_FIELD_NAME]);

  /// Convert an ItemFlingEffect to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        NAME_FIELD_NAME: name,
        EFFECT_ENTRIES_FIELD_NAME: effectEntries.map((e) => e.toMap()).toList(),
        ITEMS_FIELD_NAME: items.map((e) => e.toMap()).toList(),
      };
}
