// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/items/item_holder_pokemon.dart';
import 'package:pokeapi/src/models/items/item_sprites.dart';
import 'package:pokeapi/src/models/utility/common/api_resource.dart';
import 'package:pokeapi/src/models/utility/common/generation_game_index.dart';
import 'package:pokeapi/src/models/utility/common/machine_version_detail.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:pokeapi/src/models/utility/common/verbose_effect.dart';
import 'package:pokeapi/src/models/utility/common/version_group_flavor_text.dart';

/// From PokeAPI documentation:
/// An item is an object in the games which the player can pick up, keep in
/// their bag, and use in some manner. They have various uses, including
/// healing, powering up, helping catch Pokémon, or to access a new area.
///
/// The fields provide information from the PokeAPI documentation.
class Item {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the name field in the JSON data.
  static const NAME_FIELD_NAME = 'name';

  /// The name of the cost field in the JSON data.
  static const COST_FIELD_NAME = 'cost';

  /// The name of the fling power field in the JSON data.
  static const FLING_POWER_FIELD_NAME = 'fling_power';

  /// The name of the fling effect field in the JSON data.
  static const FLING_EFFECT_FIELD_NAME = 'fling_effect';

  /// The name of the attributes field in the JSON data.
  static const ATTRIBUTES_FIELD_NAME = 'attributes';

  /// The name of the category field in the JSON data.
  static const CATEGORY_FIELD_NAME = 'category';

  /// The name of the effect entries field in the JSON data.
  static const EFFECT_ENTRIES_FIELD_NAME = 'effect_entries';

  /// The name of the flavor text entries field in the JSON data.
  static const FLAVOR_TEXT_ENTRIES_FIELD_NAME = 'flavor_text_entries';

  /// The name of the game indicies field in the JSON data.
  static const GAME_INDICES_FIELD_NAME = 'game_indices';

  /// the name of the names field in the JSON data.
  static const NAMES_FIELD_NAME = 'names';

  /// The name of the sprites field in the JSON data.
  static const SPRITES_FIELD_NAME = 'sprites';

  /// The name of the held by pokemon field in the JSON data.
  static const HELD_BY_POKEMON_FIELD_NAME = 'held_by_pokemon';

  /// The name of the baby trigger field in the JSON data.
  static const BABY_TRIGGER_FOR_FIELD_NAME = 'baby_trigger_for';

  /// The name of the machines field in the JSON data.
  static const MACHINES_FIELD_NAME = 'machines';

  /// The identifier for this resource.
  final int id;

  /// The name for this resource.
  final String name;

  /// The price of this item in stores.
  final int cost;

  /// The power of the move Fling when used with this item.
  final int? flingPower;

  /// The effect of the move Fling when used with this item.
  final NamedApiResource? flingEffect;

  /// A list of attributes this item has.
  final List<NamedApiResource> attributes;

  /// The category of items this item falls into.
  final NamedApiResource category;

  /// The effect of this ability listed in different languages.
  final List<VerboseEffect> effectEntries;

  /// The flavor text of this ability listed in different languages.
  final List<VersionGroupFlavorText> flavorTextEntries;

  /// A list of game indices relevent to this item by generation.
  final List<GenerationGameIndex> gameIndicies;

  /// The name of this item listed in different languages.
  final List<Name> names;

  /// A set of sprites used to depict this item in the game.
  final ItemSprites sprites;

  /// A list of Pokémon that might be found in the wild holding this item.
  final List<ItemHolderPokemon> heldByPokemon;

  /// An evolution chain this item requires to produce a bay during mating.
  final ApiResource? babyTriggerFor;

  /// A list of the machines related to this item.
  final List<MachineVersionDetail> machines;

  /// Create a new Item.
  const Item(
      this.id,
      this.name,
      this.cost,
      this.flingPower,
      this.flingEffect,
      this.attributes,
      this.category,
      this.effectEntries,
      this.flavorTextEntries,
      this.gameIndicies,
      this.names,
      this.sprites,
      this.heldByPokemon,
      this.babyTriggerFor,
      this.machines);

  /// Create a new Item from a Map<String, dynamic>.
  Item.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        name = data[NAME_FIELD_NAME],
        cost = data[COST_FIELD_NAME],
        flingPower = data[FLING_POWER_FIELD_NAME],
        flingEffect =
            NamedApiResource.tryFromMap(data[FLING_EFFECT_FIELD_NAME]),
        attributes = NamedApiResource.listFrom(data[ATTRIBUTES_FIELD_NAME]),
        category = NamedApiResource.fromMap(data[CATEGORY_FIELD_NAME]),
        effectEntries = VerboseEffect.listFrom(data[EFFECT_ENTRIES_FIELD_NAME]),
        flavorTextEntries = VersionGroupFlavorText.listFrom(
            data[FLAVOR_TEXT_ENTRIES_FIELD_NAME]),
        gameIndicies =
            GenerationGameIndex.listFrom(data[GAME_INDICES_FIELD_NAME]),
        names = Name.listFrom(data[NAMES_FIELD_NAME]),
        sprites = ItemSprites.fromMap(data[SPRITES_FIELD_NAME]),
        heldByPokemon =
            ItemHolderPokemon.listFrom(data[HELD_BY_POKEMON_FIELD_NAME]),
        babyTriggerFor =
            ApiResource.tryFromMap(data[BABY_TRIGGER_FOR_FIELD_NAME]),
        machines = MachineVersionDetail.listFrom(data[MACHINES_FIELD_NAME]);

  /// Convert an Item to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        NAME_FIELD_NAME: name,
        COST_FIELD_NAME: cost,
        FLING_POWER_FIELD_NAME: flingPower,
        FLING_EFFECT_FIELD_NAME: flingEffect?.toMap(),
        ATTRIBUTES_FIELD_NAME: attributes.map((e) => e.toMap()).toList(),
        CATEGORY_FIELD_NAME: category.toMap(),
        EFFECT_ENTRIES_FIELD_NAME: effectEntries.map((e) => e.toMap()).toList(),
        FLAVOR_TEXT_ENTRIES_FIELD_NAME:
            flavorTextEntries.map((e) => e.toMap()).toList(),
        GAME_INDICES_FIELD_NAME: gameIndicies.map((e) => e.toMap()).toList(),
        NAMES_FIELD_NAME: names.map((e) => e.toMap()).toList(),
        SPRITES_FIELD_NAME: sprites.toMap(),
        HELD_BY_POKEMON_FIELD_NAME:
            heldByPokemon.map((e) => e.toMap()).toList(),
        BABY_TRIGGER_FOR_FIELD_NAME: babyTriggerFor?.toMap(),
        MACHINES_FIELD_NAME: machines.map((e) => e.toMap()).toList(),
      };
}
