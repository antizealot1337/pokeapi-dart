// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This contains data about an ItemHolderPokemonVersionDetail.
///
/// The fields provide information from the PokeAPI documentation.
class ItemHolderPokemonVersionDetail {
  /// The name of the rarity field in the JSON data.
  static const RARITY_FIELD_NAME = 'rarity';

  /// The name of the version field in the JSON data.
  static const VERSION_FIELD_NAME = 'version';

  /// How often this Pokémon holds this item in this version.
  final int rarity;

  /// The version that this item is held in by the Pokémon.
  final NamedApiResource version;

  /// Create a new ItemHolderPokemonVersionDetail.
  const ItemHolderPokemonVersionDetail(this.rarity, this.version);

  /// Create a new ItemHolderPokemonVersionDetail from a Map<String, dynamic>.
  ItemHolderPokemonVersionDetail.fromMap(Map<String, dynamic> data)
      : rarity = data[RARITY_FIELD_NAME],
        version = NamedApiResource.fromMap(data[VERSION_FIELD_NAME]);

  /// Convert an ItemHolderPokemonVersionDetail to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        RARITY_FIELD_NAME: rarity,
        VERSION_FIELD_NAME: version.toMap(),
      };

  /// Create a List of ItemHolderPokemonVersionDetail objects from the provided
  /// data. If the data is null an empty List is returned.
  static List<ItemHolderPokemonVersionDetail> listFrom(List<dynamic>? data) =>
      data?.map((e) => ItemHolderPokemonVersionDetail.fromMap(e)).toList() ??
      [];
}
