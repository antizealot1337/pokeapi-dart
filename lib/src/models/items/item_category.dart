// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// From PokeAPI documentation:
/// Item categories determine where items will be placed in the players bag.
///
/// The fields provide information from the PokeAPI documentation.
class ItemCategory {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the name field in the JSON data.
  static const NAME_FIELD_NAME = 'name';

  /// The name of the items field in the JSON data.
  static const ITEMS_FIELD_NAME = 'items';

  /// The name of the names field in the JSON data.
  static const NAMES_FIELD_NAME = 'names';

  /// The name of the pocket field in the JSON data.
  static const POCKET_FIELD_NAME = 'pocket';

  /// The identifier for this resource.
  final int id;

  /// The name for this resource.
  final String name;

  /// A list of items that are a part of this category.
  final List<NamedApiResource> items;

  /// The name of this item category listed in different languages.
  final List<Name> names;

  /// The pocket items in this category would be put in.
  final NamedApiResource pocket;

  /// Create a new ItemCategory.
  const ItemCategory(this.id, this.name, this.items, this.names, this.pocket);

  /// Create a new ItemCategory from a Map<String, dynamic>.
  ItemCategory.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        name = data[NAME_FIELD_NAME],
        items = NamedApiResource.listFrom(data[ITEMS_FIELD_NAME]),
        names = Name.listFrom(data[NAMES_FIELD_NAME]),
        pocket = NamedApiResource.fromMap(data[POCKET_FIELD_NAME]);

  /// Convert an ItemCategory to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        NAME_FIELD_NAME: name,
        ITEMS_FIELD_NAME: items.map((e) => e.toMap()).toList(),
        NAMES_FIELD_NAME: names.map((e) => e.toMap()).toList(),
        POCKET_FIELD_NAME: pocket.toMap(),
      };
}
