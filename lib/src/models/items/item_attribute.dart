// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/description.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// From PokeAPI documentation:
/// Item attributes define particular aspects of items, e.g. "usable in battle"
/// or "consumable".
///
/// The fields provide information from the PokeAPI documentation.
class ItemAttribute {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the name field id the JSON data.
  static const NAME_FIELD_NAME = 'name';

  /// The name of the items field in the JSON data.
  static const ITEMS_FIELD_NAME = 'items';

  /// The name of the names field in the JSON data.
  static const NAMES_FIELD_NAME = 'names';

  /// The name of the descriptions field in the JSON data.
  static const DESCRIPTIONS_FIELD_NAME = 'descriptions';

  /// The identifier for this resource.
  final int id;

  /// The name for this resource.
  final String name;

  /// A list of items that have this attribute.
  final List<NamedApiResource> items;

  /// The name of this item attribute listed in different languages.
  final List<Name> names;

  /// The description of this item attribute listed in different languages.
  final List<Description> descriptions;

  /// Create a new ItemAttribute.
  const ItemAttribute(
      this.id, this.name, this.items, this.names, this.descriptions);

  /// Create a new ItemAttribute from a Map<String, dynamic>.
  ItemAttribute.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        name = data[NAME_FIELD_NAME],
        items = NamedApiResource.listFrom(data[ITEMS_FIELD_NAME]),
        names = Name.listFrom(data[NAMES_FIELD_NAME]),
        descriptions = Description.listFrom(data[DESCRIPTIONS_FIELD_NAME]);

  /// Convert an ItemAttribute to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        NAME_FIELD_NAME: name,
        ITEMS_FIELD_NAME: items.map((e) => e.toMap()).toList(),
        NAMES_FIELD_NAME: names.map((e) => e.toMap()).toList(),
        DESCRIPTIONS_FIELD_NAME: descriptions.map((e) => e.toMap()).toList(),
      };
}
