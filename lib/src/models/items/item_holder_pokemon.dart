// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/items/item_holder_pokemon_version_detail.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This contains information about an ItemHolderPokemon.
///
/// The fields provide information from the PokeAPI documentation.
class ItemHolderPokemon {
  /// The name of the pokemon field in the JSON data.
  static const POKEMON_FIELD_NAME = 'pokemon';

  /// The name of the version details field in the JSON data.
  static const VERSION_DETAILS_FIELD_NAME = 'version_details';

  /// The Pokémon that holds this item.
  final NamedApiResource pokemon;

  /// The details for the version that this item is held in by the Pokémon.
  final List<ItemHolderPokemonVersionDetail> versionDetails;

  /// Create a new ItemHolderPokemon.
  const ItemHolderPokemon(this.pokemon, this.versionDetails);

  /// Create a new ItemHolderPokemon from a Map<String, dynamic>.
  ItemHolderPokemon.fromMap(Map<String, dynamic> data)
      : pokemon = NamedApiResource.fromMap(data[POKEMON_FIELD_NAME]),
        versionDetails = ItemHolderPokemonVersionDetail.listFrom(
            data[VERSION_DETAILS_FIELD_NAME]);

  /// Convert a ItemHolderPokemon to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        POKEMON_FIELD_NAME: pokemon.toMap(),
        VERSION_DETAILS_FIELD_NAME:
            versionDetails.map((e) => e.toMap()).toList(),
      };

  /// Create a List of ItemHolderPokemon objects from the provided data. If the
  /// data is null an empty List is returned.
  static List<ItemHolderPokemon> listFrom(List<dynamic>? data) =>
      data?.map((e) => ItemHolderPokemon.fromMap(e)).toList() ?? [];
}
