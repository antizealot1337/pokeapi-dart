// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// Resource is the data returned from any number of resource endpoints.
///
/// The fields provide information from the PokeAPI documentation.
class Resource {
  /// The name of the count field in the JSON data.
  static const COUNT_FIELD_NAME = 'count';

  /// The name of the next field in the JSON data.
  static const NEXT_FIELD_NAME = 'next';

  /// The name of the previous field in the JSON data.
  static const PREVIOUS_FIELD_NAME = 'previous';

  /// The name of the results field in the JSON data.
  static const RESULTS_FIELD_NAME = 'results';

  /// The total number of resources available from this API.
  final int count;

  /// The URL for the next page in the list.
  final String? next;

  /// The URL for the previous page in the list.
  final String? previous;

  /// A list of named API resources.
  final List<NamedApiResource> results;

  /// Creates a new Resource.
  const Resource(this.count, this.next, this.previous, this.results);

  /// Creates a Resource from a Map<String, dynamic>.
  Resource.fromMap(Map<String, dynamic> data)
      : count = data[COUNT_FIELD_NAME],
        next = data[NEXT_FIELD_NAME],
        previous = data[PREVIOUS_FIELD_NAME],
        results = NamedApiResource.listFrom(data[RESULTS_FIELD_NAME]);

  /// Convert a Resource to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        COUNT_FIELD_NAME: count,
        NEXT_FIELD_NAME: next,
        PREVIOUS_FIELD_NAME: previous,
        RESULTS_FIELD_NAME: results.map((e) => e.toMap()).toList(),
      };
}
