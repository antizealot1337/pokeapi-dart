// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/description.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// From PokeAPI documentation:
/// Targets moves can be directed at during battle. Targets can be Pokémon,
/// environments or even other moves.
///
/// The fields provide information from the PokeAPI documentation.
class MoveTarget {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the name field in the JSON data.
  static const NAME_FIELD_NAME = 'name';

  /// The name of the descriptions field in the JSON data.
  static const DESCRIPTIONS_FIELD_NAME = 'descriptions';

  /// The name of the moves field in the JSON data.
  static const MOVES_FIELD_NAME = 'moves';

  /// The name of the names field in the JSON data.
  static const NAMES_FIELD_NAME = 'names';

  /// The identifier for this resource.
  final int id;

  /// The name for this resource.
  final String name;

  /// The description of this resource listed in different languages.
  final List<Description> descriptions;

  /// A list of moves that fall into this damage class.
  final List<NamedApiResource> moves;

  /// The name of this resource listed in different languages.
  final List<Name> names;

  /// Create a new MoveTarget.
  const MoveTarget(
      this.id, this.name, this.descriptions, this.moves, this.names);

  /// Create a new MoveTarget from a Map<String, dynamic>.
  MoveTarget.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        name = data[NAME_FIELD_NAME],
        descriptions = Description.listFrom(data[DESCRIPTIONS_FIELD_NAME]),
        moves = NamedApiResource.listFrom(data[MOVES_FIELD_NAME]),
        names = Name.listFrom(data[NAMES_FIELD_NAME]);

  /// Convert a MoveTarget to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        NAME_FIELD_NAME: name,
        DESCRIPTIONS_FIELD_NAME: descriptions.map((e) => e.toMap()).toList(),
        MOVES_FIELD_NAME: moves.map((e) => e.toMap()).toList(),
        NAMES_FIELD_NAME: names.map((e) => e.toMap()).toList(),
      };
}
