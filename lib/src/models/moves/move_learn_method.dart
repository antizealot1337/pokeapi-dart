// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/description.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// From PokeAPI documentation:
/// Methods by which Pokémon can learn moves.
///
/// The fields provide information from the PokeAPI documentation.
class MoveLearnMethod {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the name field in the JSON data.
  static const NAME_FIELD_NAME = 'name';

  /// The name of the descriptions field in the JSON data.
  static const DESCRIPTIONS_FIELD_NAME = 'descriptions';

  /// The name of the names field in the JSON data.
  static const NAMES_FIELD_NAME = 'names';

  /// The name of the version groups field in the JSON data.
  static const VERSION_GROUPS_FIELD_NAME = 'version_groups';

  /// The identifier for this resource.
  final int id;

  /// The name for this resource.
  final String name;

  /// The description of this resource listed in different languages.
  final List<Description> descriptions;

  /// The name of this resource listed in different languages.
  final List<Name> names;

  /// A list of version groups where moves can be learned through this method.
  final List<NamedApiResource> versionGroups;

  /// Create a new MoveLearnMethod.
  const MoveLearnMethod(
      this.id, this.name, this.descriptions, this.names, this.versionGroups);

  /// Create a new MoveLearnMethod from a Map<String, dynamic>.
  MoveLearnMethod.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        name = data[NAME_FIELD_NAME],
        descriptions = Description.listFrom(data[DESCRIPTIONS_FIELD_NAME]),
        names = Name.listFrom(data[NAMES_FIELD_NAME]),
        versionGroups =
            NamedApiResource.listFrom(data[VERSION_GROUPS_FIELD_NAME]);

  /// Convert a MoveLearnMethod to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        NAME_FIELD_NAME: name,
        DESCRIPTIONS_FIELD_NAME: descriptions.map((e) => e.toMap()).toList(),
        NAMES_FIELD_NAME: names.map((e) => e.toMap()).toList(),
        VERSION_GROUPS_FIELD_NAME: versionGroups.map((e) => e.toMap()).toList(),
      };
}
