// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/description.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// From PokeAPI documentation:
/// Very general categories that loosely group move effects.
///
/// The fields provide information from the PokeAPI documentation.
class MoveCategory {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the name field in the JSON data.
  static const NAME_FIELD_NAME = 'name';

  /// The name of the moves field in the JSON data.
  static const MOVES_FIELD_NAME = 'moves';

  /// The name of the descriptions field in the JSON data.
  static const DESCRIPTIONS_FIELD_NAME = 'descriptions';

  /// The identifier for this resource.
  final int id;

  /// The name for this resource.
  final String name;

  /// A list of moves that fall into this category.
  final List<NamedApiResource> moves;

  /// The description of this resource listed in different languages.
  final List<Description> descriptions;

  /// Create a new MoveCategory.
  const MoveCategory(this.id, this.name, this.moves, this.descriptions);

  /// Create a new MoveCategory from a Map<String, dynamic>.
  MoveCategory.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        name = data[NAME_FIELD_NAME],
        moves = NamedApiResource.listFrom(data[MOVES_FIELD_NAME]),
        descriptions = Description.listFrom(data[DESCRIPTIONS_FIELD_NAME]);

  /// Convert a MoveCategory to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        NAME_FIELD_NAME: name,
        MOVES_FIELD_NAME: moves.map((e) => e.toMap()).toList(),
        DESCRIPTIONS_FIELD_NAME: descriptions.map((e) => e.toMap()).toList(),
      };
}
