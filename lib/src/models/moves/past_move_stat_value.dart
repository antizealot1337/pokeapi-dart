// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:pokeapi/src/models/utility/common/verbose_effect.dart';

/// This contains information about a PastMoveStatValue.
///
/// The fields provide information from the PokeAPI documentation.
class PastMoveStatValue {
  /// The name of the accuracy field in the JSON data.
  static const ACCURACY_FIELD_NAME = 'accuracy';

  /// The name of the effect_chance field in the JSON data.
  static const EFFECT_CHANCE_FIELD_NAME = 'effect_chance';

  /// The name of the power field in the JSON data.
  static const POWER_FIELD_NAME = 'power';

  /// The name of the pp field in the JSON data.
  static const PP_FIELD_NAME = 'pp';

  /// The name of the effect_entries field in the JSON data.
  static const EFFECT_ENTRIES_FIELD_NAME = 'effect_entries';

  /// The name of the type field in the JSON data.
  static const TYPE_FIELD_NAME = 'type';

  /// The name of the version group field in the JSON data.
  static const VERSION_GROUP_FIELD_NAME = 'version_group';

  /// The percent value of how likely this move is to be successful.
  final int accuracy;

  /// The percent value of how likely it is this moves effect will take effect.
  final int effectChance;

  /// The base power of this move with a value of 0 if it does not have a base
  /// power.
  final int power;

  /// Power points. The number of times this move can be used.
  final int pp;

  /// The effect of this move listed in different languages.
  final List<VerboseEffect> effectEntries;

  /// The elemental type of this move.
  final NamedApiResource type;

  /// The version group in which these move stat values were in effect.
  final NamedApiResource versionGroup;

  /// Create a new PastMoveStatValue.
  const PastMoveStatValue(this.accuracy, this.effectChance, this.power, this.pp,
      this.effectEntries, this.type, this.versionGroup);

  /// Create a new PastMoveStatValue from a Map<String, dynamic>.
  PastMoveStatValue.fromMap(Map<String, dynamic> data)
      : accuracy = data[ACCURACY_FIELD_NAME],
        effectChance = data[EFFECT_CHANCE_FIELD_NAME],
        power = data[POWER_FIELD_NAME],
        pp = data[PP_FIELD_NAME],
        effectEntries = VerboseEffect.listFrom(data[EFFECT_ENTRIES_FIELD_NAME]),
        type = NamedApiResource.fromMap(data[TYPE_FIELD_NAME]),
        versionGroup = NamedApiResource.fromMap(data[VERSION_GROUP_FIELD_NAME]);

  /// Convert a PastMoveStatValue to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ACCURACY_FIELD_NAME: accuracy,
        EFFECT_CHANCE_FIELD_NAME: effectChance,
        POWER_FIELD_NAME: power,
        PP_FIELD_NAME: pp,
        EFFECT_ENTRIES_FIELD_NAME: effectEntries.map((e) => e.toMap()).toList(),
        TYPE_FIELD_NAME: type.toMap(),
        VERSION_GROUP_FIELD_NAME: versionGroup.toMap(),
      };

  /// Create a List of PokemonEncounter objects from the provided data. If the
  /// data is null an empty List is returned.
  static List<PastMoveStatValue> listFrom(List<dynamic>? data) =>
      data?.map((e) => PastMoveStatValue.fromMap(e)).toList() ?? [];
}
