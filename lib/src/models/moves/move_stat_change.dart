// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This contains information about a MoveStatChange.
///
/// The fields provide information from the PokeAPI documentation.
class MoveStatChange {
  /// The name of the change field in the JSON data.
  static const CHANGE_FIELD_NAME = 'change';

  /// The name of the stat field in the JSON data.
  static const STAT_FIELD_NAME = 'stat';

  /// The amount of change.
  final int change;

  /// The stat being affected.
  final NamedApiResource stat;

  /// Create a new MoveStatChange.
  const MoveStatChange(this.change, this.stat);

  /// Create a new MoveStatChange from a Map<String, dynamic>.
  MoveStatChange.fromMap(Map<String, dynamic> data)
      : change = data[CHANGE_FIELD_NAME],
        stat = NamedApiResource.fromMap(data[STAT_FIELD_NAME]);

  /// Convert a MoveStatChange to a Map<String, dynamic>.
  Map<String, dynamic> toMap() =>
      {CHANGE_FIELD_NAME: change, STAT_FIELD_NAME: stat.toMap()};

  /// Create a List of MoveStatChange objects from the provided data. If the
  /// data is null an empty List is returned.
  static List<MoveStatChange> listFrom(List<dynamic>? data) =>
      data?.map((e) => MoveStatChange.fromMap(e)).toList() ?? [];
}
