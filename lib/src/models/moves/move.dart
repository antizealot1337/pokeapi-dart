// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/moves/contest_combo_set.dart';
import 'package:pokeapi/src/models/moves/move_flavor_text.dart';
import 'package:pokeapi/src/models/moves/move_meta_data.dart';
import 'package:pokeapi/src/models/moves/move_stat_change.dart';
import 'package:pokeapi/src/models/moves/past_move_stat_value.dart';
import 'package:pokeapi/src/models/pokemon/ability_effect_change.dart';
import 'package:pokeapi/src/models/utility/common/api_resource.dart';
import 'package:pokeapi/src/models/utility/common/machine_version_detail.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:pokeapi/src/models/utility/common/verbose_effect.dart';

/// From PokeAPI documentation:
/// Moves are the skills of Pokémon in battle. In battle, a Pokémon uses one
/// move each turn. Some moves (including those learned by Hidden Machine) can
/// be used outside of battle as well, usually for the purpose of removing
/// obstacles or exploring new areas.
///
/// The fields provide information from the PokeAPI documentation.
class Move {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the name field in the JSON data.
  static const NAME_FIELD_NAME = 'name';

  /// The name of the accuracy field in the JSON data.
  static const ACCURACY_FIELD_NAME = 'accuracy';

  /// The name of the effect chance field in the JSON data.
  static const EFFECT_CHANCE_FIELD_NAME = 'effect_chance';

  /// The name of the pp field in the JSON data.
  static const PP_FIELD_NAME = 'pp';

  /// The name of the priority field in the JSON data.
  static const PRIORITY_FIELD_NAME = 'priority';

  /// The name of the power field in the JSON data.
  static const POWER_FIELD_NAME = 'power';

  /// The name of the contest combos field in the JSON data.
  static const CONTEST_COMBOS_FIELD_NAME = 'contest_combos';

  /// The name of the contest type field in the JSON data.
  static const CONTEST_TYPE_FIELD_NAME = 'contest_type';

  /// The name of the contest effect field in the JSON data.
  static const CONTEST_EFFECT_FIELD_NAME = 'contest_effect';

  /// The name of the damage class field in the JSON data.
  static const DAMAGE_CLASS_FIELD_NAME = 'damage_class';

  /// The name of the effect entries field in the JSON data.
  static const EFFECT_ENTRIES_FIELD_NAME = 'effect_entries';

  /// The name of the effect changes field in the JSON data.
  static const EFFECT_CHANGES_FIELD_NAME = 'effect_changes';

  /// The name of the flavor text entries field in the JSON data.
  static const FLAVOR_TEXT_ENTRIES_FIELD_NAME = 'flavor_text_entries';

  /// The name of the generation field in the JSON data.
  static const GENERATION_FIELD_NAME = 'generation';

  /// The name of the machines field in the JSON data.
  static const MACHINES_FIELD_NAME = 'machines';

  /// The name of the meta field in the JSON data.
  static const META_FIELD_NAME = 'meta';

  /// The name of the names field in the JSON data.
  static const NAMES_FIELD_NAME = 'names';

  /// The name of the past values field in the JSON data.
  static const PAST_VALUES_FIELD_NAME = 'past_values';

  /// The name of the stat changes field in the JSON data.
  static const STAT_CHANGES_FIELD_NAME = 'stat_changes';

  /// The name of the super contest effect field in the JSON data.
  static const SUPER_CONTEST_EFFECT_FIELD_NAME = 'super_contest_effect';

  /// The name of the target field in the JSON data.
  static const TARGET_FIELD_NAME = 'target';

  /// The name of the type field in the JSON data.
  static const TYPE_FIELD_NAME = 'type';

  /// The identifier for this resource.
  final int id;

  /// The name for this resource.
  final String name;

  /// The percent value of how likely this move is to be successful.
  final int accuracy;

  /// The percent value of how likely it is this moves effect will happen.
  final int? effectChance;

  /// Power points. The number of times this move can be used.
  final int pp;

  /// A value between -8 and 8. Sets the order in which moves are executed
  /// during battle. See
  /// [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/Priority) for greater
  /// detail.
  final int priority;

  /// The base power of this move with a value of 0 if it does not have a base
  /// power.
  final int power;

  /// A detail of normal and super contest combos that require this move.
  final ContestComboSet contestCombos;

  /// The type of appeal this move gives a Pokémon when used in a contest.
  final NamedApiResource contestType;

  /// The effect the move has when used in a contest.
  final ApiResource contestEffect;

  /// The type of damage the move inflicts on the target, e.g. physical.
  final NamedApiResource damageClass;

  /// The effect of this move listed in different languages.
  final List<VerboseEffect> effectEntries;

  /// The list of previous effects this move has had across version groups of
  /// the games.
  final List<AbilityEffectChange> effectChanges;

  /// The flavor text of this move listed in different languages.
  final List<MoveFlavorText> flavorTextEntries;

  /// The generation in which this move was introduced.
  final NamedApiResource generation;

  /// A list of the machines that teach this move.
  final List<MachineVersionDetail> machines;

  /// Metadata about this move
  final MoveMetaData meta;

  /// The name of this resource listed in different languages.
  final List<Name> names;

  /// A list of move resource value changes across version groups of the game.
  final List<PastMoveStatValue> pastValues;

  /// A list of stats this moves effects and how much it effects them.
  final List<MoveStatChange> statChanges;

  /// The effect the move has when used in a super contest.
  final ApiResource superContestEffect;

  /// The type of target that will receive the effects of the attack.
  final NamedApiResource target;

  /// The elemental type of this move.
  final NamedApiResource type;

  /// Create a new Move.
  const Move(
      this.id,
      this.name,
      this.accuracy,
      this.effectChance,
      this.pp,
      this.priority,
      this.power,
      this.contestCombos,
      this.contestType,
      this.contestEffect,
      this.damageClass,
      this.effectEntries,
      this.effectChanges,
      this.flavorTextEntries,
      this.generation,
      this.machines,
      this.meta,
      this.names,
      this.pastValues,
      this.statChanges,
      this.superContestEffect,
      this.target,
      this.type);

  /// Create a new Move from a Map<String, dynamic>.
  Move.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        name = data[NAME_FIELD_NAME],
        accuracy = data[ACCURACY_FIELD_NAME],
        effectChance = data[EFFECT_CHANCE_FIELD_NAME],
        pp = data[PP_FIELD_NAME],
        priority = data[PRIORITY_FIELD_NAME],
        power = data[POWER_FIELD_NAME],
        contestCombos =
            ContestComboSet.fromMap(data[CONTEST_COMBOS_FIELD_NAME]),
        contestType = NamedApiResource.fromMap(data[CONTEST_TYPE_FIELD_NAME]),
        contestEffect = ApiResource.fromMap(data[CONTEST_EFFECT_FIELD_NAME]),
        damageClass = NamedApiResource.fromMap(data[DAMAGE_CLASS_FIELD_NAME]),
        effectEntries = VerboseEffect.listFrom(data[EFFECT_ENTRIES_FIELD_NAME]),
        effectChanges =
            AbilityEffectChange.listFrom(data[EFFECT_CHANGES_FIELD_NAME]),
        flavorTextEntries =
            MoveFlavorText.listFrom(data[FLAVOR_TEXT_ENTRIES_FIELD_NAME]),
        generation = NamedApiResource.fromMap(data[GENERATION_FIELD_NAME]),
        machines = MachineVersionDetail.listFrom(data[MACHINES_FIELD_NAME]),
        meta = MoveMetaData.fromMap(data[META_FIELD_NAME]),
        names = Name.listFrom(data[NAMES_FIELD_NAME]),
        pastValues = PastMoveStatValue.listFrom(data[PAST_VALUES_FIELD_NAME]),
        statChanges = MoveStatChange.listFrom(data[STAT_CHANGES_FIELD_NAME]),
        superContestEffect =
            ApiResource.fromMap(data[SUPER_CONTEST_EFFECT_FIELD_NAME]),
        target = NamedApiResource.fromMap(data[TARGET_FIELD_NAME]),
        type = NamedApiResource.fromMap(data[TYPE_FIELD_NAME]);

  /// Convert a Move to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        NAME_FIELD_NAME: name,
        ACCURACY_FIELD_NAME: accuracy,
        EFFECT_CHANCE_FIELD_NAME: effectChance,
        PP_FIELD_NAME: pp,
        PRIORITY_FIELD_NAME: priority,
        POWER_FIELD_NAME: power,
        CONTEST_COMBOS_FIELD_NAME: contestCombos.toMap(),
        CONTEST_TYPE_FIELD_NAME: contestType.toMap(),
        CONTEST_EFFECT_FIELD_NAME: contestEffect.toMap(),
        DAMAGE_CLASS_FIELD_NAME: damageClass.toMap(),
        EFFECT_ENTRIES_FIELD_NAME: effectEntries.map((e) => e.toMap()).toList(),
        EFFECT_CHANGES_FIELD_NAME: effectChanges.map((e) => e.toMap()).toList(),
        FLAVOR_TEXT_ENTRIES_FIELD_NAME:
            flavorTextEntries.map((e) => e.toMap()).toList(),
        GENERATION_FIELD_NAME: generation.toMap(),
        MACHINES_FIELD_NAME: machines.map((e) => e.toMap()),
        META_FIELD_NAME: meta.toMap(),
        NAMES_FIELD_NAME: names.map((e) => e.toMap()).toList(),
        PAST_VALUES_FIELD_NAME: pastValues.map((e) => e.toMap()).toList(),
        STAT_CHANGES_FIELD_NAME: statChanges.map((e) => e.toMap()).toList(),
        SUPER_CONTEST_EFFECT_FIELD_NAME: superContestEffect.toMap(),
        TARGET_FIELD_NAME: target.toMap(),
        TYPE_FIELD_NAME: type.toMap(),
      };
}
