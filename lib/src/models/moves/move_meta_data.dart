// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This contains information about a MoveMetaData.
///
/// The fields provide information from the PokeAPI documentation.
class MoveMetaData {
  /// The name of the aliment field in the JSON data.
  static const AILMENT_FIELD_NAME = 'ailment';

  /// The name of the category field in the JSON data.
  static const CATEGORY_FIELD_NAME = 'category';

  /// The name of the min hits field in the JSON data.
  static const MIN_HITS_FIELD_NAME = 'min_hits';

  /// The name of the max hits field in the JSON data.
  static const MAX_HITS_FIELD_NAME = 'max_hits';

  /// The name of the min turns field in the JSON data.
  static const MIN_TURNS_FIELD_NAME = 'min_turns';

  /// The name of the max turns field in the JSON data.
  static const MAX_TURNS_FIELD_NAME = 'max_turns';

  /// The name of the drain field in the JSON data.
  static const DRAIN_FIELD_NAME = 'drain';

  /// The name of the healing field in the JSON data.
  static const HEALING_FIELD_NAME = 'healing';

  /// The name of the crit rate field in the JSON data.
  static const CRIT_RATE_FIELD_NAME = 'crit_rate';

  /// The name of the ailment chance field in the JSON data.
  static const AILMENT_CHANCE_FIELD_NAME = 'ailment_chance';

  /// The name of the flinch chance field in the JSON data.
  static const FLINCH_CHANCE_FIELD_NAME = 'flinch_chance';

  /// The name of the stat chance field in the JSON data.
  static const STAT_CHANCE_FIELD_NAME = 'stat_chance';

  /// The status ailment this move inflicts on its target.
  final NamedApiResource ailment;

  /// The category of move this move falls under, e.g. damage or ailment.
  final NamedApiResource category;

  /// The minimum number of times this move hits. Null if it always only hits
  /// once.
  final int? minHits;

  /// The maximum number of times this move hits. Null if it always only hits
  /// once.
  final int? maxHits;

  /// The minimum number of turns this move continues to take effect. Null if it
  /// always only lasts one turn.
  final int? minTurns;

  /// The maximum number of turns this move continues to take effect. Null if it
  /// always only lasts one turn.
  final int? maxTurns;

  /// HP drain (if positive) or Recoil damage (if negative), in percent of
  /// damage done.
  final int drain;

  /// The amount of hp gained by the attacking Pokemon, in percent of it's
  /// maximum HP.
  final int healing;

  /// Critical hit rate bonus.
  final int critRate;

  /// The likelihood this attack will cause an ailment.
  final int ailmentChance;

  /// The likelihood this attack will cause the target Pokémon to flinch.
  final int flinchChance;

  /// The likelihood this attack will cause a stat change in the target Pokémon.
  final int statChance;

  /// Create a new MoveMetaData.
  const MoveMetaData(
      this.ailment,
      this.category,
      this.minHits,
      this.maxHits,
      this.minTurns,
      this.maxTurns,
      this.drain,
      this.healing,
      this.critRate,
      this.ailmentChance,
      this.flinchChance,
      this.statChance);

  /// Create a new MoveMetaData from a Map<String, dynamic>.
  MoveMetaData.fromMap(Map<String, dynamic> data)
      : ailment = NamedApiResource.fromMap(data[AILMENT_FIELD_NAME]),
        category = NamedApiResource.fromMap(data[CATEGORY_FIELD_NAME]),
        minHits = data[MIN_HITS_FIELD_NAME],
        maxHits = data[MAX_HITS_FIELD_NAME],
        minTurns = data[MIN_TURNS_FIELD_NAME],
        maxTurns = data[MAX_TURNS_FIELD_NAME],
        drain = data[DRAIN_FIELD_NAME],
        healing = data[HEALING_FIELD_NAME],
        critRate = data[CRIT_RATE_FIELD_NAME],
        ailmentChance = data[AILMENT_CHANCE_FIELD_NAME],
        flinchChance = data[FLINCH_CHANCE_FIELD_NAME],
        statChance = data[STAT_CHANCE_FIELD_NAME];

  /// Convert a MoveMetaData to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        AILMENT_FIELD_NAME: ailment.toMap(),
        CATEGORY_FIELD_NAME: category.toMap(),
        MIN_HITS_FIELD_NAME: minHits,
        MAX_HITS_FIELD_NAME: maxHits,
        MIN_TURNS_FIELD_NAME: minTurns,
        MAX_TURNS_FIELD_NAME: maxTurns,
        DRAIN_FIELD_NAME: drain,
        HEALING_FIELD_NAME: healing,
        CRIT_RATE_FIELD_NAME: critRate,
        AILMENT_CHANCE_FIELD_NAME: ailmentChance,
        FLINCH_CHANCE_FIELD_NAME: flinchChance,
        STAT_CHANCE_FIELD_NAME: statChance,
      };
}
