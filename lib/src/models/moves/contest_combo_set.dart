// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/moves/contest_combo_detail.dart';

/// This contains information about a ContestComboSet.
///
/// The fields provide information from the PokeAPI documentation.
class ContestComboSet {
  /// The name of the normal field in the JSON data.
  static const NORMAL_FIELD_NAME = 'normal';

  /// The name of the super field in the JSON data.
  static const SUPER_FIELD_NAME = 'super';

  /// A detail of moves this move can be used before or after, granting
  /// additional appeal points in contests.
  final ContestComboDetail normal;

  /// A detail of moves this move can be used before or after, granting
  /// additional appeal points in super contests.
  final ContestComboDetail super_;

  /// Create a new ContestComboSet.
  const ContestComboSet(this.normal, this.super_);

  /// Create a new ContestComboSet from a Map<String, dynamic>.
  ContestComboSet.fromMap(Map<String, dynamic> data)
      : normal = ContestComboDetail.fromMap(data[NORMAL_FIELD_NAME]),
        super_ = ContestComboDetail.fromMap(data[SUPER_FIELD_NAME]);

  /// Convert a ContestComboSt to a Map<String, dynamic>.
  Map<String, dynamic> toMap() =>
      {NORMAL_FIELD_NAME: normal.toMap(), SUPER_FIELD_NAME: super_.toMap()};
}
