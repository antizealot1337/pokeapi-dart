// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// From PokeAPI documentation:
/// Move Ailments are status conditions caused by moves used during battle. See
/// [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/http://bulbapedia.bulbagarden.net/wiki/Status_condition)
/// for greater detail.
///
/// The fields provide information from the PokeAPI documentation.
class MoveAilment {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the name field in the JSON data.
  static const NAME_FIELD_NAME = 'name';

  /// The name of the moves field in the JSON data.
  static const MOVES_FIELD_NAME = 'moves';

  /// The name of the names field in the JSON data.
  static const NAMES_FIELD_NAME = 'names';

  /// The identifier for this resource.
  final int id;

  /// The name for this resource.
  final String name;

  /// A list of moves that cause this ailment.
  final List<NamedApiResource> moves;

  /// The name of this resource listed in different languages.
  final List<Name> names;

  /// Create a new MoveAilment.
  const MoveAilment(this.id, this.name, this.moves, this.names);

  /// Create a new MoveAilment from a Map<String, dynamic>.
  MoveAilment.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        name = data[NAME_FIELD_NAME],
        moves = NamedApiResource.listFrom(data[MOVES_FIELD_NAME]),
        names = Name.listFrom(data[NAMES_FIELD_NAME]);

  /// Convert a MoveAilment to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        NAME_FIELD_NAME: name,
        MOVES_FIELD_NAME: moves.map((e) => e.toMap()).toList(),
        NAMES_FIELD_NAME: names.map((e) => e.toMap()).toList(),
      };
}
