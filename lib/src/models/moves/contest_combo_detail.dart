// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This contains information about a ContestComboDetail.
///
/// The fields provide information from the PokeAPI documentation.
class ContestComboDetail {
  /// The name of the use before field in the JSON data.
  static const USE_BEFORE_FIELD_NAME = 'use_before';

  /// The name of the use after field in the JSON data.
  static const USE_AFTER_FIELD_NAME = 'use_after';

  /// A list of moves to use before this move.
  final List<NamedApiResource> useBefore;

  /// A list of moves to use after this move.
  final List<NamedApiResource> useAfter;

  /// Create a new ContestComboDetail.
  const ContestComboDetail(this.useBefore, this.useAfter);

  /// Create a new ContestComboDetail from a Map<String, dynamic>.
  ContestComboDetail.fromMap(Map<String, dynamic> data)
      : useBefore = NamedApiResource.listFrom(data[USE_BEFORE_FIELD_NAME]),
        useAfter = NamedApiResource.listFrom(data[USE_AFTER_FIELD_NAME]);

  /// Convert a ContestComboDetail to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        USE_BEFORE_FIELD_NAME: useBefore.map((e) => e.toMap()).toList(),
        USE_AFTER_FIELD_NAME: useAfter.map((e) => e.toMap()).toList(),
      };
}
