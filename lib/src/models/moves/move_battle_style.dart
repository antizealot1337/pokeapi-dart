// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/name.dart';

/// From PokeAPI documentation:
/// Styles of moves when used in the Battle Palace. See
/// [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/Battle_Frontier_(Generation_III))
/// for greater detail.
///
/// The fields provide information from the PokeAPI documentation.
class MoveBattleStyle {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the name field in the JSON data.
  static const NAME_FIELD_NAME = 'name';

  /// The name of the names field in the JSON data.
  static const NAMES_FIELD_NAME = 'names';

  /// The identifier for this resource.
  final int id;

  /// The name for this resource.
  final String name;

  /// The name of this resource listed in different languages.
  final List<Name> names;

  /// Create a new MoveBattleStyle.
  const MoveBattleStyle(this.id, this.name, this.names);

  /// Create a new MoveBattleStyle from a Map<String, dynamic>.
  MoveBattleStyle.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        name = data[NAME_FIELD_NAME],
        names = Name.listFrom(data[NAMES_FIELD_NAME]);

  /// Convert a MoveBattleStyle to a Map<String, dynaimc>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        NAME_FIELD_NAME: name,
        NAMES_FIELD_NAME: names.map((e) => e.toMap()).toList(),
      };
}
