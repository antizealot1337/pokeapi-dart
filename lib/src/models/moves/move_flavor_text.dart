// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This contains information about a MoveFlavorText.
///
/// The fields provide information from the PokeAPI documentation.
class MoveFlavorText {
  /// The name of the flavor text field in the JSON data.
  static const FLAVOR_TEXT_FIELD_NAME = 'flavor_text';

  /// The name of the language field in the JSON data.
  static const LANGUAGE_FIELD_NAME = 'language';

  /// The name of the version group field in the JSON data.
  static const VERSION_GROUP_FIELD_NAME = 'version_group';

  /// The localized flavor text for an api resource in a specific language.
  final String flavorText;

  /// The language this name is in.
  final NamedApiResource language;

  /// The version group that uses this flavor text.
  final NamedApiResource versionGroup;

  /// Create a new MoveFlavorText.
  const MoveFlavorText(this.flavorText, this.language, this.versionGroup);

  /// Create a new MoveFlavorText from a Map<String, dynamic>.
  MoveFlavorText.fromMap(Map<String, dynamic> data)
      : flavorText = data[FLAVOR_TEXT_FIELD_NAME],
        language = NamedApiResource.fromMap(data[LANGUAGE_FIELD_NAME]),
        versionGroup = NamedApiResource.fromMap(data[VERSION_GROUP_FIELD_NAME]);

  /// Convert a MoveFlavorText to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        FLAVOR_TEXT_FIELD_NAME: flavorText,
        LANGUAGE_FIELD_NAME: language.toMap(),
        VERSION_GROUP_FIELD_NAME: versionGroup.toMap(),
      };

  /// Create a List of MoveFlavorText objects from the provided data. If the
  /// data is null an empty List is returned.
  static List<MoveFlavorText> listFrom(List<dynamic>? data) =>
      data?.map((e) => MoveFlavorText.fromMap(e)).toList() ?? [];
}
