// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// From PokeAPI documentation:
/// Machines are the representation of items that teach moves to Pokémon. They
/// vary from version to version, so it is not certain that one specific TM or
/// HM corresponds to a single Machine.
///
/// The fields provide information from the PokeAPI documentation.
class Machine {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the item field in the JSON data.
  static const ITEM_FIELD_NAME = 'item';

  /// The name of the move field in the JSON data.
  static const MOVE_FIELD_NAME = 'move';

  /// The name of the version group field in the JSON data.
  static const VERSION_GROUP_FIELD_NAME = 'version_group';

  /// The identifier for this resource.
  final int id;

  /// The TM or HM item that corresponds to this machine.
  final NamedApiResource item;

  /// The move that is taught by this machine.
  final NamedApiResource move;

  /// The version group that this machine applies to.
  final NamedApiResource versionGroup;

  /// Create a new Machine.
  const Machine(this.id, this.item, this.move, this.versionGroup);

  /// Create a new Machine from a Map<String, dynamic>.
  Machine.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        item = NamedApiResource.fromMap(data[ITEM_FIELD_NAME]),
        move = NamedApiResource.fromMap(data[MOVE_FIELD_NAME]),
        versionGroup = NamedApiResource.fromMap(data[VERSION_GROUP_FIELD_NAME]);

  /// Convert a Machine to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        ITEM_FIELD_NAME: item.toMap(),
        MOVE_FIELD_NAME: move.toMap(),
        VERSION_GROUP_FIELD_NAME: versionGroup.toMap(),
      };
}
