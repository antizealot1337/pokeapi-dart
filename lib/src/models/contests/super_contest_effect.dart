// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/flavor_text.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// From PokeAPI documentation:
/// Super contest effects refer to the effects of moves when used in super
/// contests.
///
/// The fields provide information from the PokeAPI documentation.
class SuperContestEffect {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the appeal field in the JSON data.
  static const APPEAL_FIELD_NAME = 'appeal';

  /// The name of the flavor text entries field in the JSON data.
  static const FLAVOR_TEXT_ENTRIES_FIELD_NAME = 'flavor_text_entries';

  /// The name of the moves field in the JSON data.
  static const MOVES_FIELD_NAME = 'moves';

  /// The identifier for this resource.
  final int id;

  /// The level of appeal this super contest effect has.
  final int appeal;

  /// The flavor text of this super contest effect listed in different
  /// languages.
  final List<FlavorText> flavorTextEntries;

  /// A list of moves that have the effect when used in super contests.
  final List<NamedApiResource> moves;

  /// Create a new SuperContestEffect.
  const SuperContestEffect(
      this.id, this.appeal, this.flavorTextEntries, this.moves);

  /// Create a new SuperContestEffect from a Map<String, dynamic>.
  SuperContestEffect.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        appeal = data[APPEAL_FIELD_NAME],
        flavorTextEntries =
            FlavorText.listFrom(data[FLAVOR_TEXT_ENTRIES_FIELD_NAME]),
        moves = NamedApiResource.listFrom(data[MOVES_FIELD_NAME]);

  /// Convert a SuperContestEffect to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        APPEAL_FIELD_NAME: appeal,
        FLAVOR_TEXT_ENTRIES_FIELD_NAME:
            flavorTextEntries.map((e) => e.toMap()).toList(),
        MOVES_FIELD_NAME: moves.map((e) => e.toMap()).toList(),
      };
}
