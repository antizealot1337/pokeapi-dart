// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/contests/contest_name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// From PokeAPI documentation:
/// Contest types are categories judges used to weigh a Pokémon's condition in
/// Pokémon contests. Check out
/// [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/Contest_condition) for
/// greater detail.
///
/// The fields provide information from the PokeAPI documentation.
class ContestType {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the name field in the JSON data.
  static const NAME_FIELD_NAME = 'name';

  /// The name of the berry flavor field in the JSON data.
  static const BERRY_FLAVOR_FIELD_NAME = 'berry_flavor';

  /// The name of the names field in the JSON data.
  static const NAMES_FIELD_NAME = 'names';

  /// The identifier for this resource.
  final int id;

  /// The name for this resource.
  final String name;

  /// The berry flavor that correlates with this contest type.
  final NamedApiResource berryFlavor;

  /// The name of this contest type listed in different languages.
  final List<ContestName> names;

  /// Create a new ContestType.
  const ContestType(this.id, this.name, this.berryFlavor, this.names);

  /// Create a new ContestType from a Map<String, dynamic>.
  ContestType.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        name = data[NAME_FIELD_NAME],
        berryFlavor = NamedApiResource.fromMap(data[BERRY_FLAVOR_FIELD_NAME]),
        names = ContestName.listFrom(data[NAMES_FIELD_NAME]);

  /// Convert a ContestType to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        NAME_FIELD_NAME: name,
        BERRY_FLAVOR_FIELD_NAME: berryFlavor.toMap(),
        NAMES_FIELD_NAME: names.map((e) => e.toMap()).toList(),
      };
}
