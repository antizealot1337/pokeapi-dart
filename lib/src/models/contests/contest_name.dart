// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This is a ContestName from the PokeAPI.
///
/// The fields provide information from the PokeAPI documentation.
class ContestName {
  /// The name of the name field in the JSON data.
  static const NAME_FIELD_NAME = 'name';

  /// The name of the color field in the JSON data.
  static const COLOR_FIELD_NAME = 'color';

  /// The name of the language field in the JSON data.
  static const LANGUAGE_FIELD_NAME = 'language';

  /// The name for this contest.
  final String name;

  /// The color associated with this contest's name.
  final String color;

  /// The language that this name is in.
  final NamedApiResource language;

  /// Create a new ContestName
  const ContestName(this.name, this.color, this.language);

  /// Create a new ContestName from a Map<String, dynamic>.
  ContestName.fromMap(Map<String, dynamic> data)
      : name = data[NAME_FIELD_NAME],
        color = data[COLOR_FIELD_NAME],
        language = NamedApiResource.fromMap(data[LANGUAGE_FIELD_NAME]);

  /// Convert ContestName to Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        NAME_FIELD_NAME: name,
        COLOR_FIELD_NAME: color,
        LANGUAGE_FIELD_NAME: language.toMap(),
      };

  /// Create a List of ContestName objects from the provided data. If the data
  /// is null an empty List is returned.
  static List<ContestName> listFrom(List<dynamic>? data) =>
      data?.map((e) => ContestName.fromMap(e)).toList() ?? [];
}
