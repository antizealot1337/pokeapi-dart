// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/flavor_text.dart';
import 'package:pokeapi/src/models/utility/common/effect.dart';

/// From PokeAPI documentation:
/// Contest effects refer to the effects of moves when used in contests.
///
/// The fields provide information from the PokeAPI documentation.
class ContestEffect {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the appeal field in the JSON data.
  static const APPEAL_FIELD_NAME = 'appeal';

  /// The name of the jam field in the JSON data.
  static const JAM_FIELD_NAME = 'jam';

  /// The name of the effect enries field in the JSON data.
  static const EFFECT_ENTRIES_FIELD_NAME = 'effect_entries';

  /// The name of the flavor text entries in the JSON data.
  static const FLAVOR_TEXT_ENTRIES_FIELD_NAME = 'flavor_text_entries';

  /// The identifier for this resource.
  final int id;

  /// The base number of hearts the user of this move gets.
  final int appeal;

  /// The base number of hearts the user's opponent loses.
  final int jam;

  /// The result of this contest effect listed in different languages.
  final List<Effect> effectEntries;

  /// The flavor text of this contest effect listed in different languages.
  final List<FlavorText> flavorTextEntries;

  /// Create a new ContestEffect.
  const ContestEffect(this.id, this.appeal, this.jam, this.effectEntries,
      this.flavorTextEntries);

  /// Create a new ContestEffect from a Map<String, dynamic>.
  ContestEffect.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        appeal = data[APPEAL_FIELD_NAME],
        jam = data[JAM_FIELD_NAME],
        effectEntries = Effect.listFrom(data[EFFECT_ENTRIES_FIELD_NAME]),
        flavorTextEntries =
            FlavorText.listFrom(data[FLAVOR_TEXT_ENTRIES_FIELD_NAME]);

  /// Convert a ContestEffect to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        APPEAL_FIELD_NAME: appeal,
        JAM_FIELD_NAME: jam,
        EFFECT_ENTRIES_FIELD_NAME: effectEntries.map((e) => e.toMap()).toList(),
        FLAVOR_TEXT_ENTRIES_FIELD_NAME:
            flavorTextEntries.map((e) => e.toMap()).toList(),
      };
}
