// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This constains information about a Name.
///
/// The fields provide information from the PokeAPI documentation.
class Name {
  /// The name of the name field in the JSON data.
  static const NAME_FIELD_NAME = 'name';

  /// The name of the language field in the JSON data.
  static const LAGUNAGE_FIELD_NAME = 'language';

  /// The localized name for an API resource in a specific language.
  final String name;

  /// The language this name is in.
  final NamedApiResource language;

  /// Create a new Name.
  const Name(this.name, this.language);

  /// Creates a Name from a Map<String, dynamic>.
  Name.fromMap(Map<String, dynamic> data)
      : name = data[NAME_FIELD_NAME],
        language = NamedApiResource.fromMap(data[LAGUNAGE_FIELD_NAME]);

  /// Convert a Name to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        NAME_FIELD_NAME: name,
        LAGUNAGE_FIELD_NAME: language.toMap(),
      };

  /// Create a List of Name objects from the provided data. If the data is null
  /// an empty List is returned.
  static List<Name> listFrom(List<dynamic>? data) =>
      data?.map((e) => Name.fromMap(e)).toList() ?? [];
}
