// SPDX-License-Identifier: MIT

/// This constains information about an APIResource.
///
/// The fields provide information from the PokeAPI documentation.
class ApiResource {
  /// The name of the URL field in the JSON data.
  static const URL_FIELD_NAME = 'url';

  /// The URL of the referenced resource.
  final String url;

  /// Create a new ApiResource.
  const ApiResource(this.url);

  /// Create a new ApiResource from a Map<String, dynamic>.
  ApiResource.fromMap(Map<String, dynamic> data) : url = data[URL_FIELD_NAME];

  /// Convert an ApiResource to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {URL_FIELD_NAME: url};

  /// Try to make an ApiResource from possible null data. If the data is null
  /// then null is returned. If the data isn't null then an ApiResource is
  /// returned.
  static ApiResource? tryFromMap(Map<String, dynamic>? data) =>
      data == null ? null : ApiResource.fromMap(data);

  /// Create a List of ApiResource objects from the provided data. If the data
  /// is null an empty List is returned.
  static List<ApiResource> listFrom(List<dynamic>? data) =>
      data?.map((e) => ApiResource.fromMap(e)).toList() ?? [];
}
