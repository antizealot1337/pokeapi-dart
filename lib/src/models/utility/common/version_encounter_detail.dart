// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/encounter.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This constains information about a VersionEncounterDetail.
///
/// The fields provide information from the PokeAPI documentation.
class VersionEncounterDetail {
  /// The name of the version field in the JSON data.
  static const VERSION_FIELD_NAME = 'version';

  /// The name of the max chance field in the JSON data.
  static const MAX_CHANCE_FIELD_NAME = 'max_chance';

  /// The name of the encounter details field in the JSON data.
  static const ENCOUNTER_DETAILS_FIELD_NAME = 'encounter_details';

  /// The game version this encounter happens in.
  final NamedApiResource version;

  /// The total percentage of all encounter potential.
  final int maxChance;

  /// A list of encounters and their specifics.
  final List<Encounter> encounterDetails;

  /// Create a new VersionEncounterDetail.
  const VersionEncounterDetail(
      this.version, this.maxChance, this.encounterDetails);

  /// Create a new VersionEncounterDetail from a Map<String, dynamic>.
  VersionEncounterDetail.fromMap(Map<String, dynamic> data)
      : version = NamedApiResource.fromMap(data[VERSION_FIELD_NAME]),
        maxChance = data[MAX_CHANCE_FIELD_NAME],
        encounterDetails =
            Encounter.listFrom(data[ENCOUNTER_DETAILS_FIELD_NAME]);

  /// Convert a VersionEncounterDetail to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        VERSION_FIELD_NAME: version.toMap(),
        MAX_CHANCE_FIELD_NAME: maxChance,
        ENCOUNTER_DETAILS_FIELD_NAME:
            encounterDetails.map((e) => e.toMap()).toList(),
      };

  /// Create a List of VersionEncounterDetail objects from the provided data.
  /// If the data is null an empty List is returned.
  static List<VersionEncounterDetail> listFrom(List<dynamic>? data) =>
      data?.map((e) => VersionEncounterDetail.fromMap(e)).toList() ?? [];
}
