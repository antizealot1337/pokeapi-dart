// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This constains information about a FlavorText.
///
/// The fields provide information from the PokeAPI documentation.
class FlavorText {
  /// The name of the flavor text field in the JSON data.
  static const FLAVOR_TEXT_FIELD_NAME = 'flavor_text';

  /// The name of the language field in the JSON data.
  static const LANGUAGE_FIELD_NAME = 'language';

  /// The name of the version field in the JSON data.
  static const VERSION_FIELD_NAME = 'version';

  /// The localized flavor text for an API resource in a specific language.
  final String flavorText;

  ///The language this name is in.
  final NamedApiResource language;

  /// The game version this flavor text is extracted from.
  final NamedApiResource? version;

  /// Create a new FlavorText.
  const FlavorText(this.flavorText, this.language, this.version);

  /// Create a new FlavorText from a Map<String, dynamic>.
  FlavorText.fromMap(Map<String, dynamic> data)
      : flavorText = data[FLAVOR_TEXT_FIELD_NAME],
        language = NamedApiResource.fromMap(data[LANGUAGE_FIELD_NAME]),
        version = NamedApiResource.tryFromMap(data[VERSION_FIELD_NAME]);

  /// Convert a FlavorText to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        FLAVOR_TEXT_FIELD_NAME: flavorText,
        LANGUAGE_FIELD_NAME: language.toMap(),
        VERSION_FIELD_NAME: version?.toMap(),
      };

  /// Create a List of FlavorText objects from the provided data. If the data
  /// is null an empty List is returned.
  static List<FlavorText> listFrom(List<dynamic>? data) =>
      data?.map((e) => FlavorText.fromMap(e)).toList() ?? [];
}
