// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This constains information about a Description.
///
/// The fields provide information from the PokeAPI documentation.
class Description {
  /// The name of the description field in the JSON data.
  static const DESCRIPTION_FIELD_NAME = 'description';

  /// The name of the language field in the JSON data.
  static const LANGUAGE_FIELD_NAME = 'language';

  /// The localized description for an API resource in a specific language.
  final String description;

  /// The language this name is in.
  final NamedApiResource language;

  /// Create a new Description.
  const Description(this.description, this.language);

  /// Create a new Description from a Map<String, dynamic>.
  Description.fromMap(Map<String, dynamic> data)
      : description = data[DESCRIPTION_FIELD_NAME],
        language = NamedApiResource.fromMap(data[LANGUAGE_FIELD_NAME]);

  /// Convert a Description to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        DESCRIPTION_FIELD_NAME: description,
        LANGUAGE_FIELD_NAME: language.toMap(),
      };

  /// Create a List of Description objects from the provided data. If the data
  /// is null an empty List is returned.
  static List<Description> listFrom(List<dynamic>? data) =>
      data?.map((e) => Description.fromMap(e)).toList() ?? [];
}
