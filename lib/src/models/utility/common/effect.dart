// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This constains information about an Effect.
///
/// The fields provide information from the PokeAPI documentation.
class Effect {
  /// The name of the effect field in the JSON data.
  static const EFFECT_FIELD_NAME = 'effect';

  /// The name of the language field in the JSON data.
  static const LANGUAGE_FIELD_NAME = 'language';

  /// The localized effect text for an API resource in a specific language.
  final String effect;

  /// The language this effect is in.
  final NamedApiResource language;

  /// Create a new Effect.
  const Effect(this.effect, this.language);

  /// Create a new Effect from a Map<String, dynamic>.
  Effect.fromMap(Map<String, dynamic> data)
      : effect = data[EFFECT_FIELD_NAME],
        language = NamedApiResource.fromMap(data[LANGUAGE_FIELD_NAME]);

  /// Convert an Effect to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        EFFECT_FIELD_NAME: effect,
        LANGUAGE_FIELD_NAME: language.toMap(),
      };

  /// Create a List of Effect objects from the provided data. If the data is
  /// null an empty List is returned.
  static List<Effect> listFrom(List<dynamic>? data) =>
      data?.map((e) => Effect.fromMap(e)).toList() ?? [];
}
