// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This constains information about a VersionGameIndex.
///
/// The fields provide information from the PokeAPI documentation.
class VersionGameIndex {
  /// The name of the game index field in the JSON data.
  static const GAME_INDEX_FIELD_NAME = 'game_index';

  /// The name of the version field in the JSON data.
  static const VERION_FIELD_NAME = 'version';

  /// The internal id of an API resource within game data.
  final int gameIndex;

  /// The version relevent to this game index.
  final NamedApiResource version;

  /// Create a new VersionGameIndex.
  const VersionGameIndex(this.gameIndex, this.version);

  /// Create a new VersionGameIndex from a Map<String, dynamic>.
  VersionGameIndex.fromMap(Map<String, dynamic> data)
      : gameIndex = data[GAME_INDEX_FIELD_NAME],
        version = NamedApiResource.fromMap(data[VERION_FIELD_NAME]);

  /// Convert a VersionGameIndex to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        GAME_INDEX_FIELD_NAME: gameIndex,
        VERION_FIELD_NAME: version.toMap(),
      };

  /// Create a List of VersionGameIndex objects from the provided data. If the
  /// data is null an empty List is returned.
  static List<VersionGameIndex> listFrom(List<dynamic>? data) =>
      data == null ? [] : data.map((e) => VersionGameIndex.fromMap(e)).toList();
}
