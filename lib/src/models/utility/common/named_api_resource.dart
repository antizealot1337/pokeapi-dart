// SPDX-License-Identifier: MIT

/// NamedResource is a link to another part of the API with a name.
///
/// This class contains documentation from PokeAPI.
class NamedApiResource {
  /// The name of the name field in the JSON data.
  static const NAME_FIELD_NAME = 'name';

  /// The name of the url field in the JSON data.
  static const URL_FIELD_NAME = 'url';

  /// The name of the referenced resource.
  final String name;

  /// The URL of the referenced resource.
  final String url;

  /// Create a new NamedApiResource.
  const NamedApiResource(this.name, this.url);

  /// Creates a NamedApiResource from a Map<String, dynamic>. If a key is
  /// missing from the data then an empty string is substituted.
  NamedApiResource.fromMap(Map<String, dynamic> data)
      : name = data[NAME_FIELD_NAME] ?? '',
        url = data[URL_FIELD_NAME] ?? '';

  /// Convert a NamedApiResource to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {NAME_FIELD_NAME: name, URL_FIELD_NAME: url};

  /// Try to make a NamedApiResource from possible null data. If the data is
  /// null then null is returned. If the data isn't null then a NamedApiResource
  /// is returned even if the data doesn't contain the expected keys.
  static NamedApiResource? tryFromMap(Map<String, dynamic>? data) =>
      data == null ? null : NamedApiResource.fromMap(data);

  /// Create a List of NamedApiResource objects from the provided data. If the
  /// data is null an empty List is returned.
  static List<NamedApiResource> listFrom(List<dynamic>? data) =>
      data?.map((e) => NamedApiResource.fromMap(e)).toList() ?? [];
}
