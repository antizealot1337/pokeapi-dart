// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This constains information about a GenerationGameIndex.
///
/// The fields provide information from the PokeAPI documentation.
class GenerationGameIndex {
  /// The name of the game index field in the JSON data.
  static const GAME_INDEX_FIELD_NAME = 'game_index';

  /// The name of the generation field in the JSON data.
  static const GENERATION_FIELD_NAME = 'generation';

  /// The internal id of an API resource within game data.
  final int gameIndex;

  /// The generation relevent to this game index.
  final NamedApiResource generation;

  /// Create a new GenerationGameIndex.
  const GenerationGameIndex(this.gameIndex, this.generation);

  /// Ceate a new ApiResource from a Map<String, dynamic>.
  GenerationGameIndex.fromMap(Map<String, dynamic> data)
      : gameIndex = data[GAME_INDEX_FIELD_NAME],
        generation = NamedApiResource.fromMap(data[GENERATION_FIELD_NAME]);

  /// Convert a GenerationGameIndex to Map<String, dyanamic>.
  Map<String, dynamic> toMap() => {
        GAME_INDEX_FIELD_NAME: gameIndex,
        GENERATION_FIELD_NAME: generation.toMap(),
      };

  /// Create a List of GenerationGameIndex objects from the provided data. If
  /// the data is null an empty List is returned.
  static List<GenerationGameIndex> listFrom(List<dynamic>? data) =>
      data?.map((e) => GenerationGameIndex.fromMap(e)).toList() ?? [];
}
