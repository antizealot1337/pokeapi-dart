// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This constains information about an VersionGroupFlavorText.
///
/// The fields provide information from the PokeAPI documentation.
class VersionGroupFlavorText {
  /// The name of the text field in the JSON data.
  static const TEXT_FIELD_NAME = 'text';

  /// The name of the language field in the JSON data.
  static const LANGUAGE_FIELD_NAME = 'language';

  /// The name of the version group field in the JSON data.
  static const VERSION_GROUP_FIELD_NAME = 'version_group';

  /// The localized name for an API resource in a specific language.
  final String text;

  /// The language this name is in.
  final NamedApiResource language;

  /// The version group which uses this flavor text.
  final NamedApiResource versionGroup;

  /// Create a new VersionGroupFlavorText.
  const VersionGroupFlavorText(this.text, this.language, this.versionGroup);

  /// Ceate a new VersionGroupFlavorText from a Map<String, dynamic>.
  VersionGroupFlavorText.fromMap(Map<String, dynamic> data)
      : text = data[TEXT_FIELD_NAME],
        language = NamedApiResource.fromMap(data[LANGUAGE_FIELD_NAME]),
        versionGroup = NamedApiResource.fromMap(data[VERSION_GROUP_FIELD_NAME]);

  /// Convert VersionGroupFlavorText to Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        TEXT_FIELD_NAME: text,
        LANGUAGE_FIELD_NAME: language.toMap(),
        VERSION_GROUP_FIELD_NAME: versionGroup.toMap(),
      };

  /// Create a List of VersionGroupFlavorText objects from the provided data.
  /// If the data is null an empty List is returned.
  static List<VersionGroupFlavorText> listFrom(List<dynamic>? data) =>
      data?.map((e) => VersionGroupFlavorText.fromMap(e)).toList() ?? [];
}
