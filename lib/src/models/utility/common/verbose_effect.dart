// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This constains information about a VerboseEffect.
///
/// The fields provide information from the PokeAPI documentation.
class VerboseEffect {
  /// The name of the effect field in the JSON data.
  static const EFFECT_FIELD_NAME = 'effect';

  /// The name of the short effect field in the JSON data.
  static const SHORT_EFFECT_FIELD_NAME = 'short_effect';

  /// The nem of the language field in the JSON data.
  static const LANGUAGE_FIELD_NAME = 'language';

  /// The localized effect text for an API resource in a specific language.
  final String effect;

  /// The localized effect text in brief.
  final String shortEffect;

  /// The language this effect is in.
  final NamedApiResource language;

  /// Create a new VerboseEffect.
  const VerboseEffect(this.effect, this.shortEffect, this.language);

  /// Create a new VerboseEffect from a Map<String, dynamic>.
  VerboseEffect.fromMap(Map<String, dynamic> data)
      : effect = data[EFFECT_FIELD_NAME],
        shortEffect = data[SHORT_EFFECT_FIELD_NAME],
        language = NamedApiResource.fromMap(data[LANGUAGE_FIELD_NAME]);

  /// Convert a VerboseEffect to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        EFFECT_FIELD_NAME: effect,
        SHORT_EFFECT_FIELD_NAME: shortEffect,
        LANGUAGE_FIELD_NAME: language.toMap(),
      };

  /// Create a List of VerboseEffect objects from the provided data. If the
  /// data is null an empty List is returned.
  static List<VerboseEffect> listFrom(List<dynamic>? data) =>
      data?.map((e) => VerboseEffect.fromMap(e)).toList() ?? [];
}
