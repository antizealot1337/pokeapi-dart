// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/api_resource.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This constains information about a MachineVersionDetail.
///
/// The fields provide information from the PokeAPI documentation.
class MachineVersionDetail {
  /// The name of the machine field in the JSON data.
  static const MACHINE_FIELD_NAME = 'machine';

  /// The name of the version group field in the JSON data.
  static const VERSION_GROUP_FIELD_NAME = 'version_group';

  /// The URL of the referenced resource.
  final ApiResource machine;

  /// The version group of this specific machine.
  final NamedApiResource versionGroup;

  /// Create a new MachineVersionDetail.
  const MachineVersionDetail(this.machine, this.versionGroup);

  /// Ceate a new MachineVersionDetail from a Map<String, dynamic>.
  MachineVersionDetail.fromMap(Map<String, dynamic> data)
      : machine = ApiResource.fromMap(data[MACHINE_FIELD_NAME]),
        versionGroup = NamedApiResource.fromMap(data[VERSION_GROUP_FIELD_NAME]);

  /// Convert a MachineVersionDetail to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        MACHINE_FIELD_NAME: machine.toMap(),
        VERSION_GROUP_FIELD_NAME: versionGroup.toMap(),
      };

  /// Create a List of MachineVersionDetail objects from the provided data. If
  /// the data is null an empty List is returned.
  static List<MachineVersionDetail> listFrom(List<dynamic>? data) =>
      data?.map((e) => MachineVersionDetail.fromMap(e)).toList() ?? [];
}
