// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This constains information about an Encounter.
///
/// The fields provide information from the PokeAPI documentation.
class Encounter {
  /// The name of the minimum level field in the JSON data.
  static const MIN_LEVEL_FIELD_NAME = 'min_level';

  /// The name of the maximum level field in the JSON data.
  static const MAX_LEVEL_FIELD_NAME = 'max_level';

  /// The name of the condition values field in the JSON data.
  static const CONDITION_VALUES_FIELD_NAME = 'condition_values';

  /// The name of the chance field in the JSON data.
  static const CHANCE_FIELD_NAME = 'chance';

  /// The name of the method field in the JSON data.
  static const METHOD_FIELD_NAME = 'method';

  /// The lowest level the Pokémon could be encountered at.
  final int minLevel;

  /// The highest level the Pokémon could be encountered at.
  final int maxLevel;

  /// A list of condition values that must be in effect for this encounter to
  /// occur.
  final List<NamedApiResource> conditionValues;

  /// Percent chance that this encounter will occur.
  final int chance;

  /// The method by which this encounter happens.
  final NamedApiResource method;

  /// Create a new Encounter.
  const Encounter(this.minLevel, this.maxLevel, this.conditionValues,
      this.chance, this.method);

  /// Create a new Encounter from a Map<String, dynamic>.
  Encounter.fromMap(Map<String, dynamic> data)
      : minLevel = data[MIN_LEVEL_FIELD_NAME],
        maxLevel = data[MAX_LEVEL_FIELD_NAME],
        conditionValues =
            NamedApiResource.listFrom(data[CONDITION_VALUES_FIELD_NAME]),
        chance = data[CHANCE_FIELD_NAME],
        method = NamedApiResource.fromMap(data[METHOD_FIELD_NAME]);

  /// Convert an Encounter to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        MIN_LEVEL_FIELD_NAME: minLevel,
        MAX_LEVEL_FIELD_NAME: maxLevel,
        CONDITION_VALUES_FIELD_NAME:
            conditionValues.map((e) => e.toMap()).toList(),
        CHANCE_FIELD_NAME: chance,
        METHOD_FIELD_NAME: method.toMap(),
      };

  /// Create a List of Encounter objects from the provided data. If the data is
  /// null an empty List is returned.
  static List<Encounter> listFrom(List<dynamic>? data) =>
      data?.map((e) => Encounter.fromMap(e)).toList() ?? [];
}
