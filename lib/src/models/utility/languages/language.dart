// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/name.dart';

/// From PokeAPI documentation:
/// Languages for translations of API resource information.
///
/// The fields provide information from the PokeAPI documentation.
class Language {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the name field in the JSON data.
  static const NAME_FIELD_NAME = 'name';

  /// The name of the official field in the JSON data.
  static const OFFICIAL_FIELD_NAME = 'official';

  /// The name of the iso639 field in the JSON data.
  static const ISO639_FIELD_NAME = 'iso639';

  /// The name of the iso3166 field in the JSON data.
  static const ISO3166_FIELD_NAME = 'iso3166';

  /// The name of the names field in the JSON data.
  static const NAMES_FIELD_NAME = 'names';

  ///The identifier for this resource.
  final int id;

  /// The name for this resource.
  final String name;

  /// Whether or not the games are published in this language.
  final bool official;

  /// The two-letter code of the country where this language is spoken. Note
  /// that it is not unique.
  final String iso639;

  /// The two-letter code of the language. Note that it is not unique.
  final String iso3166;

  /// The name of this resource listed in different languages.
  final List<Name> names;

  /// Create a new Language.
  const Language(
      this.id, this.name, this.official, this.iso639, this.iso3166, this.names);

  /// Create a new Language from a Map<String, dynamic>.
  Language.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        name = data[NAME_FIELD_NAME],
        official = data[OFFICIAL_FIELD_NAME],
        iso639 = data[ISO639_FIELD_NAME],
        iso3166 = data[ISO3166_FIELD_NAME],
        names = Name.listFrom(data[NAMES_FIELD_NAME]);

  /// Convert Language to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        NAME_FIELD_NAME: name,
        OFFICIAL_FIELD_NAME: official,
        ISO639_FIELD_NAME: iso639,
        ISO3166_FIELD_NAME: iso3166,
        NAMES_FIELD_NAME: names.map((e) => e.toMap()).toList(),
      };
}
