// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/generation_game_index.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// From PokeAPI documentation:
/// Locations that can be visited within the games. Locations make up sizable
/// portions of regions, like cities or routes.
///
/// The fields provide information from the PokeAPI documentation.
class Location {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the name field in the JSON data.
  static const NAME_FIELD_NAME = 'name';

  /// The name of the region field in the JSON data.
  static const REGION_FIELD_NAME = 'region';

  /// The name of the names field in the JSON data.
  static const NAMES_FIELD_NAME = 'names';

  /// The name of the game indices field in the JSON data.
  static const GAME_INDICES_FIELD_NAME = 'game_indices';

  /// The name of the id field in the JSON data.
  static const AREAS_FIELD_NAME = 'areas';

  /// The identifier for this resource.
  final int id;

  /// The name for this resource.
  final String name;

  /// The region this location can be found in.
  final NamedApiResource region;

  /// The name of this resource listed in different languages.
  final List<Name> names;

  /// A list of game indices relevent to this location by generation.
  final List<GenerationGameIndex> gameIndices;

  /// Areas that can be found within this location.
  final List<NamedApiResource> areas;

  /// Create a new Location.
  const Location(this.id, this.name, this.region, this.names, this.gameIndices,
      this.areas);

  /// Create a new Location from a Map<String, dynamic>.
  Location.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        name = data[NAME_FIELD_NAME],
        region = NamedApiResource.fromMap(data[REGION_FIELD_NAME]),
        names = Name.listFrom(data[NAMES_FIELD_NAME]),
        gameIndices =
            GenerationGameIndex.listFrom(data[GAME_INDICES_FIELD_NAME]),
        areas = NamedApiResource.listFrom(data[AREAS_FIELD_NAME]);

  /// Convert a Location to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        NAME_FIELD_NAME: name,
        REGION_FIELD_NAME: region.toMap(),
        NAMES_FIELD_NAME: names.map((e) => e.toMap()).toList(),
        GAME_INDICES_FIELD_NAME: gameIndices.map((e) => e.toMap()).toList(),
        AREAS_FIELD_NAME: areas.map((e) => e.toMap()).toList(),
      };
}
