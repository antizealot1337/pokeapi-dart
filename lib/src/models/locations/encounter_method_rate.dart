// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/locations/encounter_version_detail.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This contains information about an EncounterMethodRate.
///
///
/// The fields provide information from the PokeAPI documentation.
class EncounterMethodRate {
  /// The name of the encounter method field in the JSON data.
  static const ENCOUNTER_METHOD_FIELD_NAME = 'encounter_method';

  /// The name of the version details method field in the JSON data.
  static const VERSION_DETAILS_FIELD_NAME = 'version_details';

  /// The method in which Pokémon may be encountered in an area..
  final NamedApiResource encounterMethod;

  /// The chance of the encounter to occur on a version of the game.
  final List<EncounterVersionDetail> versionDetails;

  /// Create a new EncounterMethodRate.
  const EncounterMethodRate(this.encounterMethod, this.versionDetails);

  /// Create a new EncounterMethodRate from a Map<String, dynamic>.
  EncounterMethodRate.fromMap(Map<String, dynamic> data)
      : encounterMethod =
            NamedApiResource.fromMap(data[ENCOUNTER_METHOD_FIELD_NAME]),
        versionDetails =
            EncounterVersionDetail.listFrom(data[VERSION_DETAILS_FIELD_NAME]);

  /// Convert an EncounterMethodRate to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ENCOUNTER_METHOD_FIELD_NAME: encounterMethod.toMap(),
        VERSION_DETAILS_FIELD_NAME:
            versionDetails.map((e) => e.toMap()).toList(),
      };

  /// Create a List of EncounterMethodRate objects from the provided data. If
  /// the data is null an empty List is returned.
  static List<EncounterMethodRate> listFrom(List<dynamic>? data) =>
      data?.map((e) => EncounterMethodRate.fromMap(e)).toList() ?? [];
}
