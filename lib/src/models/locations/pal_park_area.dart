// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/locations/pal_park_encounter_species.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';

/// From PokeAPI documentation:
/// Areas used for grouping Pokémon encounters in Pal Park. They're like
/// habitats that are specific to
/// [Pal Park](https://bulbapedia.bulbagarden.net/wiki/Pal_Park).
///
/// The fields provide information from the PokeAPI documentation.
class PalParkArea {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the name field in the JSON data.
  static const NAME_FIELD_NAME = 'name';

  /// The name of the names field in the JSON data.
  static const NAMES_FIELD_NAME = 'names';

  /// The name of the pokemon encounters field in the JSON data.
  static const POKEMON_ENCOUNERS_FIELD_NAME = 'pokemon_encounters';

  /// The identifier for this resource.
  final int id;

  /// The name for this resource.
  final String name;

  /// The name of this resource listed in different languages.
  final List<Name> names;

  /// A list of Pokémon encountered in thi pal park area along with details.
  final List<PalParkEncounterSpecies> pokemonEncounters;

  /// Create a new PalParkArea.
  const PalParkArea(this.id, this.name, this.names, this.pokemonEncounters);

  /// Create a new PalParkArea from a Map<String, dynamic>.
  PalParkArea.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        name = data[NAME_FIELD_NAME],
        names = Name.listFrom(data[NAMES_FIELD_NAME]),
        pokemonEncounters = PalParkEncounterSpecies.listFrom(
            data[POKEMON_ENCOUNERS_FIELD_NAME]);

  /// Convert a PalParkArea to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        NAME_FIELD_NAME: name,
        NAMES_FIELD_NAME: names.map((e) => e.toMap()).toList(),
        POKEMON_ENCOUNERS_FIELD_NAME:
            pokemonEncounters.map((e) => e.toMap()).toList(),
      };
}
