// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This contains information about a PalParkEncounterSpecies.
///
/// The fields provide information from the PokeAPI documentation.
class PalParkEncounterSpecies {
  /// The name of the base score field in the JSON data.
  static const BASE_SCORE_FIELD_NAME = 'base_score';

  /// The name of the rate field in the JSON data.
  static const RATE_FIELD_NAME = 'rate';

  /// The name of the pokemon species in the JSON data.
  static const POKEMON_SPECIES = 'pokemon_species';

  /// The base score given to the player when this Pokémon is caught during a
  /// pal park run.
  final int baseScore;

  /// The base rate for encountering this Pokémon in this pal park area.
  final int rate;

  /// The Pokémon species being encountered.
  final NamedApiResource pokemonSpecies;

  /// Create a new PalParkEncounterSpecies.
  const PalParkEncounterSpecies(this.baseScore, this.rate, this.pokemonSpecies);

  /// Create a new PalParkEncounterSpecies from a Map<String, dynamic>.
  PalParkEncounterSpecies.fromMap(Map<String, dynamic> data)
      : baseScore = data[BASE_SCORE_FIELD_NAME],
        rate = data[RATE_FIELD_NAME],
        pokemonSpecies = NamedApiResource.fromMap(data[POKEMON_SPECIES]);

  /// Convert a PalParkEncounterSpecies to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        BASE_SCORE_FIELD_NAME: baseScore,
        RATE_FIELD_NAME: rate,
        POKEMON_SPECIES: pokemonSpecies.toMap(),
      };

  /// Create a List of PalParkEncounterSpecies objects from the provided data.
  /// If the data is null an empty List is returned.
  static List<PalParkEncounterSpecies> listFrom(List<dynamic>? data) =>
      data?.map((e) => PalParkEncounterSpecies.fromMap(e)).toList() ?? [];
}
