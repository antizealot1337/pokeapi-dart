// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// From PokeAPI documentation:
/// A region is an organized area of the Pokémon world. Most often, the main
/// difference between regions is the species of Pokémon that can be encountered
/// within them.
///
/// The fields provide information from the PokeAPI documentation.
class Region {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the locations field in the JSON data.
  static const LOCATIONS_FIELD_NAME = 'locations';

  /// The name of the name field in the JSON data.
  static const NAME_FIELD_NAME = 'name';

  /// The name of the names field in the JSON data.
  static const NAMES_FIELD_NAME = 'names';

  /// The name of the main generation field in the JSON data.
  static const MAIN_GENERATION_FIELD_NAME = 'main_generation';

  /// The name of the pokedexes field in the JSON data.
  static const POKEDEXES_FIELD_NAME = 'pokedexes';

  /// The name of the version groups field in the JSON data.
  static const VERSION_GROUPS_FIELD_NAME = 'version_groups';

  /// The identifier for this resource.
  final int id;

  /// A list of locations that can be found in this region.
  final List<NamedApiResource> locations;

  /// The name for this resource.
  final String name;

  /// The name of this resource listed in different languages.
  final List<Name> names;

  /// The generation this region was introduced in.
  final NamedApiResource mainGeneration;

  /// A list of pokédexes that catalogue Pokémon in this region.
  final List<NamedApiResource> pokedexes;

  /// A list of version groups where this region can be visited.
  final List<NamedApiResource> versionGroups;

  /// Create a new Region.
  const Region(this.id, this.locations, this.name, this.names,
      this.mainGeneration, this.pokedexes, this.versionGroups);

  /// Create a new Region from a Map<String, dynamic>.
  Region.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        locations = NamedApiResource.listFrom(data[LOCATIONS_FIELD_NAME]),
        name = data[NAME_FIELD_NAME],
        names = Name.listFrom(data[NAMES_FIELD_NAME]),
        mainGeneration =
            NamedApiResource.fromMap(data[MAIN_GENERATION_FIELD_NAME]),
        pokedexes = NamedApiResource.listFrom(data[POKEDEXES_FIELD_NAME]),
        versionGroups =
            NamedApiResource.listFrom(data[VERSION_GROUPS_FIELD_NAME]);

  /// Convert a Region to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        LOCATIONS_FIELD_NAME: locations.map((e) => e.toMap()).toList(),
        NAME_FIELD_NAME: name,
        NAMES_FIELD_NAME: names.map((e) => e.toMap()).toList(),
        MAIN_GENERATION_FIELD_NAME: mainGeneration.toMap(),
        POKEDEXES_FIELD_NAME: pokedexes.map((e) => e.toMap()).toList(),
        VERSION_GROUPS_FIELD_NAME: versionGroups.map((e) => e.toMap()).toList(),
      };
}
