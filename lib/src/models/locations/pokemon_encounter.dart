// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:pokeapi/src/models/utility/common/version_encounter_detail.dart';

/// This contains information about a PokemonEncounter.
///
/// The fields provide information from the PokeAPI documentation.
class PokemonEncounter {
  /// The name of the pokemon field in the JSON data.
  static const POKEMON_FIELD_NAME = 'pokemon';

  /// The name of the version details field in the JSON data.
  static const VERSION_DETAILS_FIELD_NAME = 'version_details';

  /// The Pokémon being encountered.
  final NamedApiResource pokemon;

  /// A list of versions and encounters with Pokémon that might happen in the
  /// referenced location area.
  final List<VersionEncounterDetail> versionDetails;

  /// Create a new PokemonEncounter.
  const PokemonEncounter(this.pokemon, this.versionDetails);

  /// Create a new PokemonEncounter from a Map<String, dynamic>.
  PokemonEncounter.fromMap(Map<String, dynamic> data)
      : pokemon = NamedApiResource.fromMap(data[POKEMON_FIELD_NAME]),
        versionDetails =
            VersionEncounterDetail.listFrom(data[VERSION_DETAILS_FIELD_NAME]);

  /// Convert a PokemonEncounter to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        POKEMON_FIELD_NAME: pokemon.toMap(),
        VERSION_DETAILS_FIELD_NAME:
            versionDetails.map((e) => e.toMap()).toList(),
      };

  /// Create a List of PokemonEncounter objects from the provided data. If the
  /// data is null an empty List is returned.
  static List<PokemonEncounter> listFrom(List<dynamic>? data) =>
      data?.map((e) => PokemonEncounter.fromMap(e)).toList() ?? [];
}
