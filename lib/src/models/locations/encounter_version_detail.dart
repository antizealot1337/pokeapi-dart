// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This cotains details about an EcnounterVersionDetail.
///
/// The fields provide information from the PokeAPI documentation.
class EncounterVersionDetail {
  /// The name of the rate field in the JSON dta.
  static const RATE_FIELD_NAME = 'rate';

  /// The name of the version field in the JSON data.
  static const VERSION_FIELD_NAME = 'version';

  /// The chance of an encounter to occur.
  final int rate;

  /// The version of the game in which the encounter can occur with the given
  /// chance.
  final NamedApiResource version;

  /// Create a new EncounterVersionDetail.
  const EncounterVersionDetail(this.rate, this.version);

  /// Create a new EncounterVersionDetail from a Map<String, dynamic>.
  EncounterVersionDetail.fromMap(Map<String, dynamic> data)
      : rate = data[RATE_FIELD_NAME],
        version = NamedApiResource.fromMap(data[VERSION_FIELD_NAME]);

  /// Convert an EncounterVersionDetail to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        RATE_FIELD_NAME: rate,
        VERSION_FIELD_NAME: version.toMap(),
      };

  /// Create a List of EncounterVersionDetail objects from the provided data. If
  /// the data is null an empty List is returned.
  static List<EncounterVersionDetail> listFrom(List<dynamic>? data) =>
      data?.map((e) => EncounterVersionDetail.fromMap(e)).toList() ?? [];
}
