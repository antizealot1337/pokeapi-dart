// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/locations/encounter_method_rate.dart';
import 'package:pokeapi/src/models/locations/pokemon_encounter.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// From PokeAPI documentation:
/// Location areas are sections of areas, such as floors in a building or cave.
/// Each area has its own set of possible Pokémon encounters.
///
/// The fields provide information from the PokeAPI documentation.
class LocationArea {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the name field in the JSON data.
  static const NAME_FIELD_NAME = 'name';

  /// The name of the game index field in the JSON data.
  static const GAME_INDEX_FIELD_NAME = 'game_index';

  /// The name of the encounter method rates field in the JSON data.
  static const ENCOUNTER_METHOD_RATES_FIELD_NAME = 'encounter_method_rates';

  /// The name of the locataion field in the JSON data.
  static const LOCATION_FIELD_NAME = 'location';

  /// The name of the names field in the JSON data.
  static const NAMES_FIELD_NAME = 'names';

  /// The name of the pokemon encounters field in the JSON data.
  static const POKEMON_ENCOUNTERS_FIELD_NAME = 'pokemon_encounters';

  /// The identifier for this resource.
  final int id;

  /// The name for this resource.
  final String name;

  /// The internal id of an API resource within game data.
  final int gameIndex;

  /// A list of methods in which Pokémon may be encountered in this area and how
  /// likely the method will occur depending on the version of the game.
  final List<EncounterMethodRate> encounterMethodRates;

  /// The region this location area can be found in.
  final NamedApiResource location;

  /// The name of this resource listed in different languages.
  final List<Name> names;

  /// A list of Pokémon that can be encountered in this area along with version
  /// specific details about the encounter.
  final List<PokemonEncounter> pokemonEncounters;

  /// Create a new LocationArea.
  const LocationArea(
      this.id,
      this.name,
      this.gameIndex,
      this.encounterMethodRates,
      this.location,
      this.names,
      this.pokemonEncounters);

  /// Create a new LocationArea from a Map<String, dynamic>.
  LocationArea.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        name = data[NAME_FIELD_NAME],
        gameIndex = data[GAME_INDEX_FIELD_NAME],
        encounterMethodRates = EncounterMethodRate.listFrom(
            data[ENCOUNTER_METHOD_RATES_FIELD_NAME]),
        location = NamedApiResource.fromMap(data[LOCATION_FIELD_NAME]),
        names = Name.listFrom(data[NAMES_FIELD_NAME]),
        pokemonEncounters =
            PokemonEncounter.listFrom(data[POKEMON_ENCOUNTERS_FIELD_NAME]);

  /// Convert a LocationArea to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        NAME_FIELD_NAME: name,
        GAME_INDEX_FIELD_NAME: gameIndex,
        ENCOUNTER_METHOD_RATES_FIELD_NAME:
            encounterMethodRates.map((e) => e.toMap()).toList(),
        LOCATION_FIELD_NAME: location.toMap(),
        NAMES_FIELD_NAME: names.map((e) => e.toMap()).toList(),
        POKEMON_ENCOUNTERS_FIELD_NAME:
            pokemonEncounters.map((e) => e.toMap()).toList(),
      };
}
