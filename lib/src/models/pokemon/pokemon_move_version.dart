// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This contains information about a PokemonMoveVersion.
///
/// The fields provide information from the PokeAPI documentation.
class PokemonMoveVersion {
  /// The name of the move learn method field in the JSON data.
  static const MOVE_LEARN_METHOD_FIELD_NAME = 'move_learn_method';

  /// The name of the version group field in the JSON data.
  static const VERSION_GROUP_FIELD_NAME = 'version_group';

  /// The name of the level learned at field in the JSON data.
  static const LEVEL_LEARNED_AT_FIELD_NAME = 'level_learned_at';

  /// The method by which the move is learned.
  final NamedApiResource moveLearnMethod;

  /// The version group in which the move is learned.
  final NamedApiResource versionGroup;

  /// The minimum level to learn the move.
  final int levelLearnedAt;

  /// Create a new PokemonMoveVersion.
  const PokemonMoveVersion(
      this.moveLearnMethod, this.versionGroup, this.levelLearnedAt);

  /// Create a new PokemonMoveVersion from a Map<String, dynamic>.
  PokemonMoveVersion.fromMap(Map<String, dynamic> data)
      : moveLearnMethod =
            NamedApiResource.fromMap(data[MOVE_LEARN_METHOD_FIELD_NAME]),
        versionGroup = NamedApiResource.fromMap(data[VERSION_GROUP_FIELD_NAME]),
        levelLearnedAt = data[LEVEL_LEARNED_AT_FIELD_NAME];

  /// Convert a PokemonMoveVersion to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        MOVE_LEARN_METHOD_FIELD_NAME: moveLearnMethod.toMap(),
        VERSION_GROUP_FIELD_NAME: versionGroup.toMap(),
        LEVEL_LEARNED_AT_FIELD_NAME: levelLearnedAt,
      };

  /// Create a List of PokemonMoveVersion objects from the provided data. If
  /// the data is null an empty List is returned.
  static List<PokemonMoveVersion> listFrom(List<dynamic>? data) =>
      data?.map((e) => PokemonMoveVersion.fromMap(e)).toList() ?? [];
}
