// SPDX-License-Identifier: MIT

/// This contains information about a GrowthRateExperienceLevel.
///
/// The fields provide information from the PokeAPI documentation.
class GrowthRateExperienceLevel {
  /// The name of the level field in the JSON data.
  static const LEVEL_FIELD_NAME = 'level';

  /// The name of the experience field in the JSON data.
  static const EXPERIENCE_FIELD_NAME = 'experience';

  /// The level gained.
  final int level;

  /// The amount of experience required to reach the referenced level.
  final int experience;

  /// Create a new GrowthRateExperienceLevel.
  const GrowthRateExperienceLevel(this.level, this.experience);

  /// Create a new GrowthRateExperienceLevel from a Map<String, dynamic>.
  GrowthRateExperienceLevel.fromMap(Map<String, dynamic> data)
      : level = data[LEVEL_FIELD_NAME],
        experience = data[EXPERIENCE_FIELD_NAME];

  /// Convert a GrowthRateExperienceLevel to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        LEVEL_FIELD_NAME: level,
        EXPERIENCE_FIELD_NAME: experience,
      };

  /// Create a List of GrowthRateExperienceLevel objects from the provided
  /// data. If the data is null an empty List is returned.
  static List<GrowthRateExperienceLevel> listFrom(List<dynamic>? data) =>
      data?.map((e) => GrowthRateExperienceLevel.fromMap(e)).toList() ?? [];
}
