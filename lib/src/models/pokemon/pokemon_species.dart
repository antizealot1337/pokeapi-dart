// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/genus.dart';
import 'package:pokeapi/src/models/pokemon/pal_park_encounter_area.dart';
import 'package:pokeapi/src/models/pokemon/pokemon_species_dex_entry.dart';
import 'package:pokeapi/src/models/pokemon/pokemon_species_variety.dart';
import 'package:pokeapi/src/models/utility/common/api_resource.dart';
import 'package:pokeapi/src/models/utility/common/description.dart';
import 'package:pokeapi/src/models/utility/common/flavor_text.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// From PokeAPI documentation:
/// A Pokémon Species forms the basis for at least one Pokémon. Attributes of a
/// Pokémon species are shared across all varieties of Pokémon within the
/// species. A good example is Wormadam; Wormadam is the species which can be
/// found in three different varieties, Wormadam-Trash, Wormadam-Sandy and
/// Wormadam-Plant.
///
/// The fields provide information from the PokeAPI documentation.
class PokemonSpecies {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the name field in the JSON data.
  static const NAME_FIELD_NAME = 'name';

  /// The name of the order field in the JSON data.
  static const ORDER_FIELD_NAME = 'order';

  /// The name of the genderRate field in the JSON data.
  static const GENDER_RATE_FIELD_NAME = 'gender_rate';

  /// The name of the captureRate field in the JSON data.
  static const CAPTURE_RATE_FIELD_NAME = 'capture_rate';

  /// The name of the baseHappiness field in the JSON data.
  static const BASE_HAPPINESS_FIELD_NAME = 'base_happiness';

  /// The name of the isBaby field in the JSON data.
  static const IS_BABY_FIELD_NAME = 'is_baby';

  /// The name of the isLegendary field in the JSON data.
  static const IS_LEGENDARY_FIELD_NAME = 'is_legendary';

  /// The name of the isMythical field in the JSON data.
  static const IS_MYTHICAL_FIELD_NAME = 'is_mythical';

  /// The name of the hatchCounter field in the JSON data.
  static const HATCH_COUNTER_FIELD_NAME = 'hatch_counter';

  /// The name of the hasGenderDifferences field in the JSON data.
  static const HAS_GENDER_DIFFERENCES_FIELD_NAME = 'has_gender_differences';

  /// The name of the formsSwitchable field in the JSON data.
  static const FORMS_SWITCHABLE_FIELD_NAME = 'forms_switchable';

  /// The name of the growthRate field in the JSON data.
  static const GROWTH_RATE_FIELD_NAME = 'growth_rate';

  /// The name of the pokedexNumbers field in the JSON data.
  static const POKEDEX_NUMBERS_FIELD_NAME = 'pokedex_numbers';

  /// The name of the eggGroups field in the JSON data.
  static const EGG_GROUPS_FIELD_NAME = 'egg_groups';

  /// The name of the color field in the JSON data.
  static const COLOR_FIELD_NAME = 'color';

  /// The name of the shape field in the JSON data.
  static const SHAPE_FIELD_NAME = 'shape';

  /// The name of the evolvesFromSpecies field in the JSON data.
  static const EVOLVES_FROM_SPECIES_FIELD_NAME = 'evolves_from_species';

  /// The name of the evolutionChain field in the JSON data.
  static const EVOLUTION_CHAIN_FIELD_NAME = 'evolution_chain';

  /// The name of the habitat field in the JSON data.
  static const HABITAT_FIELD_NAME = 'habitat';

  /// The name of the generation field in the JSON data.
  static const GENERATION_FIELD_NAME = 'generation';

  /// The name of the names field in the JSON data.
  static const NAMES_FIELD_NAME = 'names';

  /// The name of the palParkEncounters field in the JSON data.
  static const PAL_PARK_ENCOUNTERS_FIELD_NAME = 'pal_park_encounters';

  /// The name of the flavorTextEntries field in the JSON data.
  static const FLAVOR_TEXT_ENTRIES_FIELD_NAME = 'flavor_text_entries';

  /// The name of the formDescriptions field in the JSON data.
  static const FORM_DESCRIPTIONS_FIELD_NAME = 'form_descriptions';

  /// The name of the genera field in the JSON data.
  static const GENERA_FIELD_NAME = 'genera';

  /// The name of the varieties field in the JSON data.
  static const VARIETIES_FIELD_NAME = 'varieties';

  /// The identifier for this resource.
  final int id;

  /// The name for this resource.
  final String name;

  /// The order in which species should be sorted. Based on National Dex order,
  /// except families are grouped together and sorted by stage.
  final int order;

  /// The chance of this Pokémon being female, in eighths; or -1 for genderless.
  final int genderRate;

  /// The base capture rate; up to 255. The higher the number, the easier the
  /// catch.
  final int captureRate;

  /// The happiness when caught by a normal Pokéball; up to 255. The higher the
  /// number, the happier the Pokémon.
  final int baseHappiness;

  /// Whether or not this is a baby Pokémon.
  final bool isBaby;

  /// Whether or not this is a legendary Pokémon.
  final bool isLegendary;

  /// Whether or not this is a mythical Pokémon.
  final bool isMythical;

  /// Initial hatch counter: one must walk 255 × (hatch_counter + 1) steps
  /// before this Pokémon's egg hatches, unless utilizing bonuses like Flame
  /// Body's.
  final int hatchCounter;

  /// Whether or not this Pokémon has visual gender differences.
  final bool hasGenderDifferences;

  /// Whether or not this Pokémon has multiple forms and can switch between
  /// them.
  final bool formsSwitchable;

  /// The rate at which this Pokémon species gains levels.
  final NamedApiResource growthRate;

  /// A list of Pokedexes and the indexes reserved within them for this Pokémon
  /// species.
  final List<PokemonSpeciesDexEntry> pokedexNumbers;

  /// A list of egg groups this Pokémon species is a member of.
  final List<NamedApiResource> eggGroups;

  /// The color of this Pokémon for Pokédex search.
  final NamedApiResource color;

  /// The shape of this Pokémon for Pokédex search.
  final NamedApiResource shape;

  /// The Pokémon species that evolves into this Pokemon_species.
  final NamedApiResource? evolvesFromSpecies;

  /// The evolution chain this Pokémon species is a member of.
  final ApiResource evolutionChain;

  /// The habitat this Pokémon species can be encountered in.
  final NamedApiResource habitat;

  /// The generation this Pokémon species was introduced in.
  final NamedApiResource generation;

  /// The name of this resource listed in different languages.
  final List<Name> names;

  /// A list of encounters that can be had with this Pokémon species in pal
  /// park.
  final List<PalParkEncounterArea> palParkEncounters;

  /// A list of flavor text entries for this Pokémon species.
  final List<FlavorText> flavorTextEntries;

  /// Descriptions of different forms Pokémon take on within the Pokémon
  /// species.
  final List<Description> formDescriptions;

  /// The genus of this Pokémon species listed in multiple languages.
  final List<Genus> genera;

  /// A list of the Pokémon that exist within this Pokémon species.
  final List<PokemonSpeciesVariety> varieties;

  /// Create a new PokemonSpecies.
  const PokemonSpecies(
      this.id,
      this.name,
      this.order,
      this.genderRate,
      this.captureRate,
      this.baseHappiness,
      this.isBaby,
      this.isLegendary,
      this.isMythical,
      this.hatchCounter,
      this.hasGenderDifferences,
      this.formsSwitchable,
      this.growthRate,
      this.pokedexNumbers,
      this.eggGroups,
      this.color,
      this.shape,
      this.evolvesFromSpecies,
      this.evolutionChain,
      this.habitat,
      this.generation,
      this.names,
      this.palParkEncounters,
      this.flavorTextEntries,
      this.formDescriptions,
      this.genera,
      this.varieties);

  /// Create a new PokemonSpecies from a Map<String, dynamic>.
  PokemonSpecies.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        name = data[NAME_FIELD_NAME],
        order = data[ORDER_FIELD_NAME],
        genderRate = data[GENDER_RATE_FIELD_NAME],
        captureRate = data[CAPTURE_RATE_FIELD_NAME],
        baseHappiness = data[BASE_HAPPINESS_FIELD_NAME],
        isBaby = data[IS_BABY_FIELD_NAME],
        isLegendary = data[IS_LEGENDARY_FIELD_NAME],
        isMythical = data[IS_MYTHICAL_FIELD_NAME],
        hatchCounter = data[HATCH_COUNTER_FIELD_NAME],
        hasGenderDifferences = data[HAS_GENDER_DIFFERENCES_FIELD_NAME],
        formsSwitchable = data[FORMS_SWITCHABLE_FIELD_NAME],
        growthRate = NamedApiResource.fromMap(data[GROWTH_RATE_FIELD_NAME]),
        pokedexNumbers =
            PokemonSpeciesDexEntry.listFrom(data[POKEDEX_NUMBERS_FIELD_NAME]),
        eggGroups = NamedApiResource.listFrom(data[EGG_GROUPS_FIELD_NAME]),
        color = NamedApiResource.fromMap(data[COLOR_FIELD_NAME]),
        shape = NamedApiResource.fromMap(data[SHAPE_FIELD_NAME]),
        evolvesFromSpecies =
            NamedApiResource.tryFromMap(data[EVOLVES_FROM_SPECIES_FIELD_NAME]),
        evolutionChain = ApiResource.fromMap(data[EVOLUTION_CHAIN_FIELD_NAME]),
        habitat = NamedApiResource.fromMap(data[HABITAT_FIELD_NAME]),
        generation = NamedApiResource.fromMap(data[GENERATION_FIELD_NAME]),
        names = Name.listFrom(data[NAMES_FIELD_NAME]),
        palParkEncounters =
            PalParkEncounterArea.listFrom(data[PAL_PARK_ENCOUNTERS_FIELD_NAME]),
        flavorTextEntries =
            FlavorText.listFrom(data[FLAVOR_TEXT_ENTRIES_FIELD_NAME]),
        formDescriptions =
            Description.listFrom(data[FORM_DESCRIPTIONS_FIELD_NAME]),
        genera = Genus.listFrom(data[GENERA_FIELD_NAME]),
        varieties = PokemonSpeciesVariety.listFrom(data[VARIETIES_FIELD_NAME]);

  /// Convert a PokemonSpecies to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        NAME_FIELD_NAME: name,
        ORDER_FIELD_NAME: order,
        GENDER_RATE_FIELD_NAME: genderRate,
        CAPTURE_RATE_FIELD_NAME: captureRate,
        BASE_HAPPINESS_FIELD_NAME: baseHappiness,
        IS_BABY_FIELD_NAME: isBaby,
        IS_LEGENDARY_FIELD_NAME: isLegendary,
        IS_MYTHICAL_FIELD_NAME: isMythical,
        HATCH_COUNTER_FIELD_NAME: hatchCounter,
        HAS_GENDER_DIFFERENCES_FIELD_NAME: hasGenderDifferences,
        FORMS_SWITCHABLE_FIELD_NAME: formsSwitchable,
        GROWTH_RATE_FIELD_NAME: growthRate.toMap(),
        POKEDEX_NUMBERS_FIELD_NAME:
            pokedexNumbers.map((e) => e.toMap()).toList(),
        EGG_GROUPS_FIELD_NAME: eggGroups.map((e) => e.toMap()).toList(),
        COLOR_FIELD_NAME: color.toMap(),
        SHAPE_FIELD_NAME: shape.toMap(),
        EVOLVES_FROM_SPECIES_FIELD_NAME: evolvesFromSpecies?.toMap(),
        EVOLUTION_CHAIN_FIELD_NAME: evolutionChain.toMap(),
        HABITAT_FIELD_NAME: habitat.toMap(),
        GENERATION_FIELD_NAME: generation.toMap(),
        NAMES_FIELD_NAME: names.map((e) => e.toMap()).toList(),
        PAL_PARK_ENCOUNTERS_FIELD_NAME:
            palParkEncounters.map((e) => e.toMap()).toList(),
        FLAVOR_TEXT_ENTRIES_FIELD_NAME:
            flavorTextEntries.map((e) => e.toMap()).toList(),
        FORM_DESCRIPTIONS_FIELD_NAME:
            formDescriptions.map((e) => e.toMap()).toList(),
        GENERA_FIELD_NAME: genera.map((e) => e.toMap()).toList(),
        VARIETIES_FIELD_NAME: varieties.map((e) => e.toMap()).toList(),
      };
}
