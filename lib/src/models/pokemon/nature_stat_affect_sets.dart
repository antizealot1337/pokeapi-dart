// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This contains information about a NatureStatAffectSets.
///
/// The fields provide information from the PokeAPI documentation.
class NatureStatAffectSets {
  /// The name of the increase field in the JSON data.
  static const INCREASE_FIELD_NAME = 'increase';

  /// The name of the decrease field in the JSON data.
  static const DECREASE_FIELD_NAME = 'decrease';

  /// A list of natures and how they change the referenced stat.
  final List<NamedApiResource> increase;

  /// A list of nature sand how they change the referenced stat.
  final List<NamedApiResource> decrease;

  /// Create a new NatureStatAffectSets.
  const NatureStatAffectSets(this.increase, this.decrease);

  /// Create a new NatureStatAffectSets from a Map<String, dynamic>.
  NatureStatAffectSets.fromMap(Map<String, dynamic> data)
      : increase = NamedApiResource.listFrom(data[INCREASE_FIELD_NAME]),
        decrease = NamedApiResource.listFrom(data[DECREASE_FIELD_NAME]);

  /// Convert a NatureStatAffectSets to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        INCREASE_FIELD_NAME: increase.map((e) => e.toMap()).toList(),
        DECREASE_FIELD_NAME: decrease.map((e) => e.toMap()).toList(),
      };
}
