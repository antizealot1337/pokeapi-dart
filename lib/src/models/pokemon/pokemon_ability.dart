// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This contains information about a PokemonAbility.
///
/// The fields provide information from the PokeAPI documentation.
class PokemonAbility {
  /// The name of the is hidden field in the JSON data.
  static const IS_HIDDEN_FIELD_NAME = 'is_hidden';

  /// The name of the slot field in the JSON data.
  static const SLOT_FIELD_NAME = 'slot';

  /// The name of the ability field in the JSON data.
  static const ABILITY_FIELD_NAME = 'ability';

  /// Whether or not this is a hidden ability.
  final bool isHidden;

  /// The slot this ability occupies in this Pokémon species.
  final int slot;

  /// The ability the Pokémon may have.
  final NamedApiResource ability;

  /// Create a new PokemonAbility.
  const PokemonAbility(this.isHidden, this.slot, this.ability);

  /// Create a new PokemonAbility from a Map<String, dynamic>.
  PokemonAbility.fromMap(Map<String, dynamic> data)
      : isHidden = data[IS_HIDDEN_FIELD_NAME],
        slot = data[SLOT_FIELD_NAME],
        ability = NamedApiResource.fromMap(data[ABILITY_FIELD_NAME]);

  /// Convert a PokemonAbility to Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        IS_HIDDEN_FIELD_NAME: isHidden,
        SLOT_FIELD_NAME: slot,
        ABILITY_FIELD_NAME: ability.toMap(),
      };

  /// Create a List of PokemonAbility objects from the provided data. If the
  /// data is null an empty List is returned.
  static List<PokemonAbility> listFrom(List<dynamic>? data) =>
      data?.map((e) => PokemonAbility.fromMap(e)).toList() ?? [];
}
