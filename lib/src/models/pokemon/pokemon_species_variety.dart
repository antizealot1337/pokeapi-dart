// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This contains information about a PokemonSpeciesVariety.
///
/// The fields provide information from the PokeAPI documentation.
class PokemonSpeciesVariety {
  /// The name of the isDefault field in the JSON data.
  static const IS_DEFAULT_FIELD_NAME = 'is_default';

  /// The name of the pokemon field in the JSON data.
  static const POKEMON_FIELD_NAME = 'pokemon';

  /// Whether this variety is the default variety.
  final bool isDefault;

  /// The Pokémon variety.
  final NamedApiResource pokemon;

  /// Create a new PokemonSpeciesVariety.
  const PokemonSpeciesVariety(this.isDefault, this.pokemon);

  /// Create a new PokemonSpeciesVariety from a Map<String, dynamic>.
  PokemonSpeciesVariety.fromMap(Map<String, dynamic> data)
      : isDefault = data[IS_DEFAULT_FIELD_NAME],
        pokemon = NamedApiResource.fromMap(data[POKEMON_FIELD_NAME]);

  /// Convert a PokemonSpeciesVariety to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        IS_DEFAULT_FIELD_NAME: isDefault,
        POKEMON_FIELD_NAME: pokemon.toMap(),
      };

  /// Create a List of PokemonSpeciesVariety objects from the provided data. If
  /// the data is null an empty List is returned.
  static List<PokemonSpeciesVariety> listFrom(List<dynamic>? data) =>
      data?.map((e) => PokemonSpeciesVariety.fromMap(e)).toList() ?? [];
}
