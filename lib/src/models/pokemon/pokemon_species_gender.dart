// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This contains information about a PokemonSpeciesGender.
///
/// The fields provide information from the PokeAPI documentation.
class PokemonSpeciesGender {
  /// The name of the rate field in the JSON data.
  static const RATE_FIELD_NAME = 'rate';

  /// The name of the pokemon species field in the JSON data.
  static const POKEMON_SPECIES_FIELD_NAME = 'pokemon_species';

  /// The chance of this Pokémon being female, in eighths; or -1 for genderless.
  final int rate;

  /// A Pokémon species that can be the referenced gender.
  final NamedApiResource pokemonSpecies;

  /// Create a new PokemonSpeciesGender.
  const PokemonSpeciesGender(this.rate, this.pokemonSpecies);

  /// Create a new PokemonSpeciesGender from a Map<String, dynamic>.
  PokemonSpeciesGender.fromMap(Map<String, dynamic> data)
      : rate = data[RATE_FIELD_NAME],
        pokemonSpecies =
            NamedApiResource.fromMap(data[POKEMON_SPECIES_FIELD_NAME]);

  /// Convert a PokemonSpeciesGender to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        RATE_FIELD_NAME: rate,
        POKEMON_SPECIES_FIELD_NAME: pokemonSpecies.toMap(),
      };

  /// Create a List of PokemonSpeciesGender objects from the provided data. If
  /// the data is null an empty List is returned.
  static List<PokemonSpeciesGender> listFrom(List<dynamic>? data) =>
      data?.map((e) => PokemonSpeciesGender.fromMap(e)).toList() ?? [];
}
