// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This contains information about an AbilityFlavorText.
///
/// The fields provide information from the PokeAPI documentation.
class AbilityFlavorText {
  /// The name of the flavor text field in the JSON data.
  static const FLAVOR_TEXT_FIELD_NAME = 'flavor_text';

  /// The name of the language field in the JSON data.
  static const LANGUAGE_FIELD_NAME = 'language';

  /// The name of the version group text field in the JSON data.
  static const VERSION_GROUP_FIELD_NAME = 'version_group';

  /// The localized name for an API resource in a specific language.
  final String flavorText;

  /// The language this text resource is in.
  final NamedApiResource language;

  /// The version group that uses this flavor text.
  final NamedApiResource versionGroup;

  /// Create a new AbilityFlavorText.
  const AbilityFlavorText(this.flavorText, this.language, this.versionGroup);

  /// Create a new AbilityFlavorText from a Map<String, dynamic>.
  AbilityFlavorText.fromMap(Map<String, dynamic> data)
      : flavorText = data[FLAVOR_TEXT_FIELD_NAME],
        language = NamedApiResource.fromMap(data[LANGUAGE_FIELD_NAME]),
        versionGroup = NamedApiResource.fromMap(data[VERSION_GROUP_FIELD_NAME]);

  /// Convert an AbilityFlavorText to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        FLAVOR_TEXT_FIELD_NAME: flavorText,
        LANGUAGE_FIELD_NAME: language.toMap(),
        VERSION_GROUP_FIELD_NAME: versionGroup.toMap(),
      };

  /// Create a List of AbilityFlavorText objects from the provided data. If the
  /// data is null an empty List is returned.
  static List<AbilityFlavorText> listFrom(List<dynamic>? data) =>
      data?.map((e) => AbilityFlavorText.fromMap(e)).toList() ?? [];
}
