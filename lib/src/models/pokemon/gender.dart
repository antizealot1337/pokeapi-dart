// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/pokemon_species_gender.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// From PokeAPI documentation:
/// Genders were introduced in Generation II for the purposes of breeding
/// Pokémon but can also result in visual differences or even different
/// evolutionary lines. Check out
/// [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/Gender) for greater
/// detail.
///
/// The fields provide information from the PokeAPI documentation.
class Gender {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the name field in the JSON data.
  static const NAME_FIELD_NAME = 'name';

  /// The name of the pokemon species details field in the JSON data.
  static const POKEMON_SPECIES_DETAILS_FIELD_NAME = 'pokemon_species_details';

  /// The name of the required for evolution field in the JSON data.
  static const REQUIRED_FOR_EVOLUTION_FIELD_NAME = 'required_for_evolution';

  /// The identifier for this resource.
  final int id;

  /// The name for this resource.
  final String name;

  /// A list of Pokémon species that can be this gender and how likely it is
  /// that they will be.
  final List<PokemonSpeciesGender> pokemonSpeciesDetails;

  /// A list of Pokémon species that required this gender in order for a Pokémon
  /// to evolve into them.
  final List<NamedApiResource> requiredForEvolution;

  /// Create a new Gender.
  const Gender(this.id, this.name, this.pokemonSpeciesDetails,
      this.requiredForEvolution);

  /// Create a new Gender from a Map<String, dynamic>.
  Gender.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        name = data[NAME_FIELD_NAME],
        pokemonSpeciesDetails = PokemonSpeciesGender.listFrom(
            data[POKEMON_SPECIES_DETAILS_FIELD_NAME]),
        requiredForEvolution =
            NamedApiResource.listFrom(data[REQUIRED_FOR_EVOLUTION_FIELD_NAME]);

  /// Convert a Gender to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        NAME_FIELD_NAME: name,
        POKEMON_SPECIES_DETAILS_FIELD_NAME:
            pokemonSpeciesDetails.map((e) => e.toMap()).toList(),
        REQUIRED_FOR_EVOLUTION_FIELD_NAME:
            requiredForEvolution.map((e) => e.toMap()).toList(),
      };
}
