// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/nature_pokeathlon_stat_affect_sets.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';

/// From PokeAPI documentation:
/// Pokeathlon Stats are different attributes of a Pokémon's performance in
/// Pokéathlons. In Pokéathlons, competitions happen on different courses; one
/// for each of the different Pokéathlon stats. See
/// [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/Pok%C3%A9athlon) for
/// greater detail.
///
/// The fields provide information from the PokeAPI documentation.
class PokeathlonStat {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the name field in the JSON data.
  static const NAME_FIELD_NAME = 'name';

  /// The name of the names field in the JSON data.
  static const NAMES_FIELD_NAME = 'names';

  /// The name of the affecting natures field in the JSON data.
  static const AFFECTING_NATURES_FIELD_NAME = 'affecting_natures';

  /// The identifier for this resource.
  final int id;

  /// The name for this resource.
  final String name;

  /// The name of this resource listed in different languages.
  final List<Name> names;

  /// A detail of natures which affect this Pokéathlon stat positively or
  /// negatively.
  final NaturePokeathlonStatAffectSets affectingNatures;

  /// Create a new PokeathlonStat.
  const PokeathlonStat(this.id, this.name, this.names, this.affectingNatures);

  /// Create a new PokeathlonStat from a Map<String, dynamic>.
  PokeathlonStat.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        name = data[NAME_FIELD_NAME],
        names = Name.listFrom(data[NAMES_FIELD_NAME]),
        affectingNatures = NaturePokeathlonStatAffectSets.fromMap(
            data[AFFECTING_NATURES_FIELD_NAME]);

  /// Convert a PokeathlonStat to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        NAME_FIELD_NAME: name,
        NAMES_FIELD_NAME: names.map((e) => e.toMap()).toList(),
        AFFECTING_NATURES_FIELD_NAME: affectingNatures.toMap(),
      };
}
