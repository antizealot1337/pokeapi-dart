// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This contains information about a PokemonSpeciesDexEntry.
///
/// The fields provide information from the PokeAPI documentation.
class PokemonSpeciesDexEntry {
  /// The name of the entryNumber field in the JSON data.
  static const ENTRY_NUMBER_FIELD_NAME = 'entry_number';

  /// The name of the pokedex field in the JSON data.
  static const POKEDEX_FIELD_NAME = 'pokedex';

  /// The index number within the Pokédex.
  final int entryNumber;

  /// The Pokédex the referenced Pokémon species can be found in.
  final NamedApiResource pokedex;

  /// Create a new PokemonSpeciesDexEntry.
  const PokemonSpeciesDexEntry(this.entryNumber, this.pokedex);

  /// Create a new PokemonSpeciesDexEntry from a Map<String, dynamic>.
  PokemonSpeciesDexEntry.fromMap(Map<String, dynamic> data)
      : entryNumber = data[ENTRY_NUMBER_FIELD_NAME],
        pokedex = NamedApiResource.fromMap(data[POKEDEX_FIELD_NAME]);

  /// Convert a PokemonSpeciesDexEntry to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ENTRY_NUMBER_FIELD_NAME: entryNumber,
        POKEDEX_FIELD_NAME: pokedex.toMap(),
      };

  /// Create a List of PokemonSpeciesDexEntry objects from the provided data.
  /// If the data is null an empty List is returned.
  static List<PokemonSpeciesDexEntry> listFrom(List<dynamic>? data) =>
      data?.map((e) => PokemonSpeciesDexEntry.fromMap(e)).toList() ?? [];
}
