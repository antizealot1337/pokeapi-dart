// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/move_stat_affect_sets.dart';
import 'package:pokeapi/src/models/pokemon/nature_stat_affect_sets.dart';
import 'package:pokeapi/src/models/utility/common/api_resource.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// From PokeAPI documentation:
/// Stats determine certain aspects of battles. Each Pokémon has a value for
/// each stat which grows as they gain levels and can be altered momentarily by
/// effects in battles.
///
/// The fields provide information from the PokeAPI documentation.
class Stat {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the name field in the JSON data.
  static const NAME_FIELD_NAME = 'name';

  /// The name of the gameIndex field in the JSON data.
  static const GAME_INDEX_FIELD_NAME = 'game_index';

  /// The name of the isBattleOnly field in the JSON data.
  static const IS_BATTLE_ONLY_FIELD_NAME = 'is_battle_only';

  /// The name of the affectingMoves field in the JSON data.
  static const AFFECTING_MOVES_FIELD_NAME = 'affecting_moves';

  /// The name of the affectingNatures field in the JSON data.
  static const AFFECTING_NATURES_FIELD_NAME = 'affecting_natures';

  /// The name of the characteristics field in the JSON data.
  static const CHARACTERISTICS_FIELD_NAME = 'characteristics';

  /// The name of the moveDamageClass field in the JSON data.
  static const MOVE_DAMAGE_CLASS_FIELD_NAME = 'move_damage_class';

  /// The name of the names field in the JSON data.
  static const NAMES_FIELD_NAME = 'names';

  /// The identifier for this resource.
  final int id;

  /// The name for this resource.
  final String name;

  /// ID the games use for this stat.
  final int gameIndex;

  /// Whether this stat only exists within a battle.
  final bool isBattleOnly;

  /// A detail of moves which affect this stat positively or negatively.
  final MoveStatAffectSets affectingMoves;

  /// A detail of natures which affect this stat positively or negatively.
  final NatureStatAffectSets affectingNatures;

  /// A list of characteristics that are set on a Pokémon when its highest base
  /// stat is this stat.
  final List<ApiResource> characteristics;

  /// The class of damage this stat is directly related to.
  final NamedApiResource? moveDamageClass;

  /// The name of this resource listed in different languages.
  final List<Name> names;

  /// Create a new Stat.
  const Stat(
      this.id,
      this.name,
      this.gameIndex,
      this.isBattleOnly,
      this.affectingMoves,
      this.affectingNatures,
      this.characteristics,
      this.moveDamageClass,
      this.names);

  /// Create a new Stat from a Map<String, dynamic>.
  Stat.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        name = data[NAME_FIELD_NAME],
        gameIndex = data[GAME_INDEX_FIELD_NAME],
        isBattleOnly = data[IS_BATTLE_ONLY_FIELD_NAME],
        affectingMoves =
            MoveStatAffectSets.fromMap(data[AFFECTING_MOVES_FIELD_NAME]),
        affectingNatures =
            NatureStatAffectSets.fromMap(data[AFFECTING_NATURES_FIELD_NAME]),
        characteristics =
            ApiResource.listFrom(data[CHARACTERISTICS_FIELD_NAME]),
        moveDamageClass =
            NamedApiResource.tryFromMap(data[MOVE_DAMAGE_CLASS_FIELD_NAME]),
        names = Name.listFrom(data[NAMES_FIELD_NAME]);

  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        NAME_FIELD_NAME: name,
        GAME_INDEX_FIELD_NAME: gameIndex,
        IS_BATTLE_ONLY_FIELD_NAME: isBattleOnly,
        AFFECTING_MOVES_FIELD_NAME: affectingMoves.toMap(),
        AFFECTING_NATURES_FIELD_NAME: affectingNatures.toMap(),
        CHARACTERISTICS_FIELD_NAME:
            characteristics.map((e) => e.toMap()).toList(),
        MOVE_DAMAGE_CLASS_FIELD_NAME: moveDamageClass?.toMap(),
        NAMES_FIELD_NAME: names.map((e) => e.toMap()).toList(),
      };
}
