// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/awesome_name.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// From PokeAPI documentation:
/// Shapes used for sorting Pokémon in a Pokédex.
///
/// The fields provide information from the PokeAPI documentation.
class PokemonShape {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the name field in the JSON data.
  static const NAME_FIELD_NAME = 'name';

  /// The name of the awesomeNames field in the JSON data.
  static const AWESOME_NAMES_FIELD_NAME = 'awesome_names';

  /// The name of the names field in the JSON data.
  static const NAMES_FIELD_NAME = 'names';

  /// The name of the pokemonSpecies field in the JSON data.
  static const POKEMON_SPECIES_FIELD_NAME = 'pokemon_species';

  /// The identifier for this resource.
  final int id;

  /// The name for this resource.
  final String name;

  /// The "scientific" name of this Pokémon shape listed in different languages.
  final List<AwesomeName> awesomeNames;

  /// The name of this resource listed in different languages.
  final List<Name> names;

  /// A list of the Pokémon species that have this shape.
  final List<NamedApiResource> pokemonSpecies;

  /// Create a new PokemonShape.
  const PokemonShape(
      this.id, this.name, this.awesomeNames, this.names, this.pokemonSpecies);

  /// Create a new PokemonShape from a Map<String, dynamic>.
  PokemonShape.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        name = data[NAME_FIELD_NAME],
        awesomeNames = AwesomeName.listFrom(data[AWESOME_NAMES_FIELD_NAME]),
        names = Name.listFrom(data[NAMES_FIELD_NAME]),
        pokemonSpecies =
            NamedApiResource.listFrom(data[POKEMON_SPECIES_FIELD_NAME]);

  /// Convert a PokemonShape to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        NAME_FIELD_NAME: name,
        AWESOME_NAMES_FIELD_NAME: awesomeNames.map((e) => e.toMap()).toList(),
        NAMES_FIELD_NAME: names.map((e) => e.toMap()).toList(),
        POKEMON_SPECIES_FIELD_NAME:
            pokemonSpecies.map((e) => e.toMap()).toList(),
      };
}
