// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/effect.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This contains information about an AbilityEffectChange.
///
/// The fields provide information from the PokeAPI documentation.
class AbilityEffectChange {
  /// The name of the effect entries field in the JSON data.
  static const EFFECT_ENTRIES_FIELD_NAME = 'effect_entries';

  /// The name of the version group field in the JSON data.
  static const VERSION_GROUP_FIELD_NAME = 'version_group';

  /// The previous effect of this ability listed in different languages.
  final List<Effect> effectEntries;

  /// The version group in which the previous effect of this ability originated.
  final NamedApiResource versionGroup;

  /// Create a new AbilityEffectChange.
  const AbilityEffectChange(this.effectEntries, this.versionGroup);

  /// Create a new AbilityEffectChange from a Map<String, dynamic>.
  AbilityEffectChange.fromMap(Map<String, dynamic> data)
      : effectEntries = Effect.listFrom(data[EFFECT_ENTRIES_FIELD_NAME]),
        versionGroup = NamedApiResource.fromMap(data[VERSION_GROUP_FIELD_NAME]);

  /// Convert an AbilityEffectChange to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        EFFECT_ENTRIES_FIELD_NAME: effectEntries.map((e) => e.toMap()).toList(),
        VERSION_GROUP_FIELD_NAME: versionGroup.toMap(),
      };

  /// Create a List of AbilityEffectChange objects from the provided data. If
  /// the data is null an empty List is returned.
  static List<AbilityEffectChange> listFrom(List<dynamic>? data) =>
      data?.map((e) => AbilityEffectChange.fromMap(e)).toList() ?? [];
}
