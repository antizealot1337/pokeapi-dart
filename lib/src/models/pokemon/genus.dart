// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This contains information about a Genus.
///
/// The fields provide information from the PokeAPI documentation.
class Genus {
  /// The name of the genus field in the JSON data.
  static const GENUS_FIELD_NAME = 'genus';

  /// The name of the language field in the JSON data.
  static const LANGUAGE_FIELD_NAME = 'language';

  /// The localized genus for the referenced Pokémon species
  final String genus;

  /// The language this genus is in.
  final NamedApiResource language;

  /// Create a new Genus.
  const Genus(this.genus, this.language);

  /// Create a new Genus from a Map<String, dynamic>.
  Genus.fromMap(Map<String, dynamic> data)
      : genus = data[GENUS_FIELD_NAME],
        language = NamedApiResource.fromMap(data[LANGUAGE_FIELD_NAME]);

  /// Convert a Genus to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        GENUS_FIELD_NAME: genus,
        LANGUAGE_FIELD_NAME: language.toMap(),
      };

  /// Create a List of Genus objects from the provided data. If the data is
  /// null an empty List is returned.
  static List<Genus> listFrom(List<dynamic>? data) =>
      data?.map((e) => Genus.fromMap(e)).toList() ?? [];
}
