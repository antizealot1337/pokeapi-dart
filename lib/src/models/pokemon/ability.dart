// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/ability_effect_change.dart';
import 'package:pokeapi/src/models/pokemon/ability_flavor_text.dart';
import 'package:pokeapi/src/models/pokemon/ability_pokemon.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:pokeapi/src/models/utility/common/verbose_effect.dart';

/// From PokeAPI documentation:
/// Abilities provide passive effects for Pokémon in battle or in the overworld.
/// Pokémon have multiple possible abilities but can have only one ability at a
/// time. Check out [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/Ability)
/// for greater detail.
///
/// The fields provide information from the PokeAPI documentation.
class Ability {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the name field in the JSON data.
  static const NAME_FIELD_NAME = 'name';

  /// The name of the is_main_series field in the JSON data.
  static const IS_MAIN_SERIES_FIELD_NAME = 'is_main_series';

  /// The name of the generation field in the JSON data.
  static const GENERATION_FIELD_NAME = 'generation';

  /// The name of the names field in the JSON data.
  static const NAMES_FIELD_NAME = 'names';

  /// The name of the effect entries field in the JSON data.
  static const EFFECT_ENTRIES_FIELD_NAME = 'effect_entries';

  /// The name of the effect changes field in the JSON data.
  static const EFFECT_CHANGES_FIELD_NAME = 'effect_changes';

  /// The name of the flavor text entries field in the JSON data.
  static const FLAVOR_TEXT_ENTRIES_FIELD_NAME = 'flavor_text_entries';

  /// The name of the pokemon field in the JSON data.
  static const POKEMON_FIELD_NAME = 'pokemon';

  /// The identifier for this resource.
  final int id;

  /// The name for this resource.
  final String name;

  /// Whether or not this ability originated in the main series of the video
  /// games.
  final bool isMainSeries;

  /// The generation this ability originated in.
  final NamedApiResource generation;

  /// The name of this resource listed in different languages.
  final List<Name> names;

  /// The effect of this ability listed in different languages.
  final List<VerboseEffect> effectEntries;

  /// The list of previous effects this ability has had across version groups.
  final List<AbilityEffectChange> effectChanges;

  /// The flavor text of this ability listed in different languages.
  final List<AbilityFlavorText> flavorTextEntries;

  /// A list of Pokémon that could potentially have this ability.
  final List<AbilityPokemon> pokemon;

  /// Create a new Ability.
  const Ability(
      this.id,
      this.name,
      this.isMainSeries,
      this.generation,
      this.names,
      this.effectEntries,
      this.effectChanges,
      this.flavorTextEntries,
      this.pokemon);

  /// Create a new Ability from a Map<String, dynamic>.
  Ability.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        name = data[NAME_FIELD_NAME],
        isMainSeries = data[IS_MAIN_SERIES_FIELD_NAME],
        generation = NamedApiResource.fromMap(data[GENERATION_FIELD_NAME]),
        names = Name.listFrom(data[NAMES_FIELD_NAME]),
        effectEntries = VerboseEffect.listFrom(data[EFFECT_ENTRIES_FIELD_NAME]),
        effectChanges =
            AbilityEffectChange.listFrom(data[EFFECT_CHANGES_FIELD_NAME]),
        flavorTextEntries =
            AbilityFlavorText.listFrom(data[FLAVOR_TEXT_ENTRIES_FIELD_NAME]),
        pokemon = AbilityPokemon.listFrom(data[POKEMON_FIELD_NAME]);

  /// Convert an Ability to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        NAME_FIELD_NAME: name,
        IS_MAIN_SERIES_FIELD_NAME: isMainSeries,
        GENERATION_FIELD_NAME: generation.toMap(),
        NAMES_FIELD_NAME: names.map((e) => e.toMap()).toList(),
        EFFECT_ENTRIES_FIELD_NAME: effectEntries.map((e) => e.toMap()).toList(),
        EFFECT_CHANGES_FIELD_NAME: effectChanges.map((e) => e.toMap()).toList(),
        FLAVOR_TEXT_ENTRIES_FIELD_NAME:
            flavorTextEntries.map((e) => e.toMap()).toList(),
        POKEMON_FIELD_NAME: pokemon.map((e) => e.toMap()).toList(),
      };
}
