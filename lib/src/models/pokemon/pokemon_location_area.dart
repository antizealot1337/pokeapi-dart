// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:pokeapi/src/models/utility/common/version_encounter_detail.dart';

/// From PokeAPI documentation:
/// Pokémon Location Areas are ares where Pokémon can be found.
///
/// The fields provide information from the PokeAPI documentation.
class PokemonLocationArea {
  /// The name of the locationArea field in the JSON data.
  static const LOCATION_AREA_FIELD_NAME = 'location_area';

  /// The name of the versionDetails field in the JSON data.
  static const VERSION_DETAILS_FIELD_NAME = 'version_details';

  /// The location area the referenced Pokémon can be encountered in.
  final NamedApiResource locationArea;

  /// A list of versions and encounters with the referenced Pokémon that might
  /// happen.
  final List<VersionEncounterDetail> versionDetails;

  /// Create a new PokemonLocationArea.
  const PokemonLocationArea(this.locationArea, this.versionDetails);

  /// Create a new PokemonLocationArea from a Map<String, dynamic>.
  PokemonLocationArea.fromMap(Map<String, dynamic> data)
      : locationArea = NamedApiResource.fromMap(data[LOCATION_AREA_FIELD_NAME]),
        versionDetails =
            VersionEncounterDetail.listFrom(data[VERSION_DETAILS_FIELD_NAME]);

  /// Convert a PokemonLocationArea to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        LOCATION_AREA_FIELD_NAME: locationArea.toMap(),
        VERSION_DETAILS_FIELD_NAME:
            versionDetails.map((e) => e.toMap()).toList(),
      };

  /// Create a List of PokemonEncounter objects from the provided data. If the
  /// data is null an empty List is returned.
  static List<PokemonLocationArea> listFrom(List<dynamic>? data) =>
      data?.map((e) => PokemonLocationArea.fromMap(e)).toList() ?? [];
}
