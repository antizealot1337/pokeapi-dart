// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// From PokeAPI documentation:
/// Habitats are generally different terrain Pokémon can be found in but can
/// also be areas designated for rare or legendary Pokémon.
///
/// The fields provide information from the PokeAPI documentation.
class PokemonHabitat {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the name field in the JSON data.
  static const NAME_FIELD_NAME = 'name';

  /// The name of the names field in the JSON data.
  static const NAMES_FIELD_NAME = 'names';

  /// The name of the pokemonSpecies field in the JSON data.
  static const POKEMON_SPECIES_FIELD_NAME = 'pokemon_species';

  /// The identifier for this resource.
  final int id;

  /// The name for this resource.
  final String name;

  /// The name of this resource listed in different languages.
  final List<Name> names;

  /// A list of the Pokémon species that can be found in this habitat.
  final List<NamedApiResource> pokemonSpecies;

  /// Create a new PokemonHabitat.
  const PokemonHabitat(this.id, this.name, this.names, this.pokemonSpecies);

  /// Create a new PokemonHabitat from a Map<String, dynamic>.
  PokemonHabitat.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        name = data[NAME_FIELD_NAME],
        names = Name.listFrom(data[NAMES_FIELD_NAME]),
        pokemonSpecies =
            NamedApiResource.listFrom(data[POKEMON_SPECIES_FIELD_NAME]);

  /// Convert a PokemonHabitat to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        NAME_FIELD_NAME: name,
        NAMES_FIELD_NAME: names.map((e) => e.toMap()).toList(),
        POKEMON_SPECIES_FIELD_NAME:
            pokemonSpecies.map((e) => e.toMap()).toList(),
      };
}
