// SPDX-License-Identifier: MIT

/// This contains information about a PokemonFormSprites.
///
/// The fields provide information from the PokeAPI documentation.
class PokemonFormSprites {
  /// The name of the frontDefault field in the JSON data.
  static const FRONT_DEFAULT_FIELD_NAME = 'front_default';

  /// The name of the frontShiny field in the JSON data.
  static const FRONT_SHINY_FIELD_NAME = 'front_shiny';

  /// The name of the backDefault field in the JSON data.
  static const BACK_DEFAULT_FIELD_NAME = 'back_default';

  /// The name of the backShiny field in the JSON data.
  static const BACK_SHINY_FIELD_NAME = 'back_shiny';

  /// The default depiction of this Pokémon form from the front in battle.
  final String frontDefault;

  /// The shiny depiction of this Pokémon form from the front in battle.
  final String frontShiny;

  /// The default depiction of this Pokémon form from the back in battle.
  final String backDefault;

  /// The shiny depiction of this Pokémon form from the back in battle.
  final String backShiny;

  /// Create a new PokemonFormSprites.
  const PokemonFormSprites(
      this.frontDefault, this.frontShiny, this.backDefault, this.backShiny);

  /// Create a new PokemonFormSprites from a Map<String, dynamic>.
  PokemonFormSprites.fromMap(Map<String, dynamic> data)
      : frontDefault = data[FRONT_DEFAULT_FIELD_NAME],
        frontShiny = data[FRONT_SHINY_FIELD_NAME],
        backDefault = data[BACK_DEFAULT_FIELD_NAME],
        backShiny = data[BACK_SHINY_FIELD_NAME];

  /// Convert a PokemonFormSprites to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        FRONT_DEFAULT_FIELD_NAME: frontDefault,
        FRONT_SHINY_FIELD_NAME: frontShiny,
        BACK_DEFAULT_FIELD_NAME: backDefault,
        BACK_SHINY_FIELD_NAME: backShiny,
      };
}
