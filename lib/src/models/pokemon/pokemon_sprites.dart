// SPDX-License-Identifier: MIT

/// This contains information about a PokemonSprites.
///
/// The fields provide information from the PokeAPI documentation.
class PokemonSprites {
  /// The name of the front default field in the JSON data.
  static const FRONT_DEFAULT_FIELD_NAME = 'front_default';

  /// The name of the front shiny field in the JSON data.
  static const FRONT_SHINY_FIELD_NAME = 'front_shiny';

  /// The name of the front frmale field in the JSON data.
  static const FRONT_FEMALE_FIELD_NAME = 'front_female';

  /// The name of the front shiney female field in the JSON data.
  static const FRONT_SHINY_FEMALE_FIELD_NAME = 'front_shiny_female';

  /// The name of the back default field in the JSON data.
  static const BACK_DEFAULT_FIELD_NAME = 'back_default';

  /// The name of the back shiny field in the JSON data.
  static const BACK_SHINY_FIELD_NAME = 'back_shiny';

  /// The name of the back female field in the JSON data.
  static const BACK_FEMALE_FIELD_NAME = 'back_female';

  /// The name of the back shiny female field in the JSON data.
  static const BACK_SHINY_FEMALE_FIELD_NAME = 'back_shiny_female';

  /// The default depiction of this Pokémon from the front in battle.
  final String frontDefault;

  /// The shiny depiction of this Pokémon from the front in battle.
  final String frontShiny;

  /// The female depiction of this Pokémon from the front in battle.
  final String? frontFemale;

  /// The shiny female depiction of this Pokémon from the front in battle.
  final String? frontShinyFemale;

  /// The default depiction of this Pokémon from the back in battle.
  final String backDefault;

  /// The shiny depiction of this Pokémon from the back in battle.
  final String backShiny;

  /// The female depiction of this Pokémon from the back in battle.
  final String? backFemale;

  /// The shiny female depiction of this Pokémon from the back in battle.
  final String? backShinyFemale;

  /// Create a new PokemonSprites.
  const PokemonSprites(
      this.frontDefault,
      this.frontShiny,
      this.frontFemale,
      this.frontShinyFemale,
      this.backDefault,
      this.backShiny,
      this.backFemale,
      this.backShinyFemale);

  /// Create a new PokemonSprites from a Map<String, dynamic>.
  PokemonSprites.fromMap(Map<String, dynamic> data)
      : frontDefault = data[FRONT_DEFAULT_FIELD_NAME],
        frontShiny = data[FRONT_SHINY_FIELD_NAME],
        frontFemale = data[FRONT_FEMALE_FIELD_NAME],
        frontShinyFemale = data[FRONT_SHINY_FEMALE_FIELD_NAME],
        backDefault = data[BACK_DEFAULT_FIELD_NAME],
        backShiny = data[BACK_SHINY_FIELD_NAME],
        backFemale = data[BACK_FEMALE_FIELD_NAME],
        backShinyFemale = data[BACK_SHINY_FEMALE_FIELD_NAME];

  /// Convert a PokemonSprites to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        FRONT_DEFAULT_FIELD_NAME: frontDefault,
        FRONT_SHINY_FIELD_NAME: frontShiny,
        FRONT_FEMALE_FIELD_NAME: frontFemale,
        FRONT_SHINY_FEMALE_FIELD_NAME: frontShinyFemale,
        BACK_DEFAULT_FIELD_NAME: backDefault,
        BACK_SHINY_FIELD_NAME: backShiny,
        BACK_FEMALE_FIELD_NAME: backFemale,
        BACK_SHINY_FEMALE_FIELD_NAME: backShinyFemale,
      };
}
