// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/pokemon_held_item_version.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This contains information about a PokemonHeldItem.
///
/// The fields provide information from the PokeAPI documentation.
class PokemonHeldItem {
  /// The name of the item field in the JSON data.
  static const ITEM_FIELD_NAME = 'item';

  /// The name of the version details field in the JSON data.
  static const VERSION_DETAILS_FIELD_NAME = 'version_details';

  /// The item the referenced Pokémon holds.
  final NamedApiResource item;

  /// The details of the different versions in which the item is held.
  final List<PokemonHeldItemVersion> versionDetails;

  /// Create a new PokemonHeldItem.
  const PokemonHeldItem(this.item, this.versionDetails);

  /// Create a new PokemonHeldItem from a Map<String, dynamic>.
  PokemonHeldItem.fromMap(Map<String, dynamic> data)
      : item = NamedApiResource.fromMap(data[ITEM_FIELD_NAME]),
        versionDetails =
            PokemonHeldItemVersion.listFrom(data[VERSION_DETAILS_FIELD_NAME]);

  /// Convert a PokemonHeldItem to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ITEM_FIELD_NAME: item.toMap(),
        VERSION_DETAILS_FIELD_NAME:
            versionDetails.map((e) => e.toMap()).toList(),
      };

  /// Create a List of PokemonHeldItem objects from the provided data. If the
  /// data is null an empty List is returned.
  static List<PokemonHeldItem> listFrom(List<dynamic>? data) =>
      data?.map((e) => PokemonHeldItem.fromMap(e)).toList() ?? [];
}
