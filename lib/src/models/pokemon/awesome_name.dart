// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This contains information about a AwesomeName.
///
/// The fields provide information from the PokeAPI documentation.
class AwesomeName {
  /// The name of the awesomeName field in the JSON data.
  static const AWESOME_NAME_FIELD_NAME = 'awesome_name';

  /// The name of the langauge field in the JSON data.
  static const LANGAUGE_FIELD_NAME = 'langauge';

  /// The localized "scientific" name for an API resource in a specific
  /// language.
  final String awesomeName;

  /// The language this "scientific" name is in.
  final NamedApiResource? langauge;

  /// Create a new AwesomeName.
  const AwesomeName(this.awesomeName, this.langauge);

  /// Create a new AwesomeName from a Map<String, dynamic>.
  AwesomeName.fromMap(Map<String, dynamic> data)
      : awesomeName = data[AWESOME_NAME_FIELD_NAME],
        langauge = NamedApiResource.tryFromMap(data[LANGAUGE_FIELD_NAME]);

  /// Convert an AwesomeName to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        AWESOME_NAME_FIELD_NAME: awesomeName,
        LANGAUGE_FIELD_NAME: langauge?.toMap(),
      };

  /// Create a List of AwesomeName objects from the provided data. If the data
  /// is null an empty List is returned.
  static List<AwesomeName> listFrom(List<dynamic>? data) =>
      data?.map((e) => AwesomeName.fromMap(e)).toList() ?? [];
}
