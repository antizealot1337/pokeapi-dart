// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/pokemon_move_version.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This contains information about a PokemonMove.
///
/// The fields provide information from the PokeAPI documentation.
class PokemonMove {
  /// The name of the move field in the JSON data.
  static const MOVE_FIELD_NAME = 'move';

  /// The name of the version group details field in the JSON data.
  static const VERSION_GROUP_DETAILS_FIELD_NAME = 'version_group_details';

  /// The move the Pokémon can learn.
  final NamedApiResource move;

  /// The details of the version in which the Pokémon can learn the move.
  final List<PokemonMoveVersion> versionGroupDetails;

  /// Create a new PokemonMove.
  const PokemonMove(this.move, this.versionGroupDetails);

  /// Create a new PokemonMove from a Map<String, dynamic>.
  PokemonMove.fromMap(Map<String, dynamic> data)
      : move = NamedApiResource.fromMap(data[MOVE_FIELD_NAME]),
        versionGroupDetails =
            PokemonMoveVersion.listFrom(data[VERSION_GROUP_DETAILS_FIELD_NAME]);

  /// Convert a PokemonMove to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        MOVE_FIELD_NAME: move.toMap(),
        VERSION_GROUP_DETAILS_FIELD_NAME:
            versionGroupDetails.map((e) => e.toMap()).toList(),
      };

  /// Create a List of PokemonMove objects from the provided data. If the data
  /// is null an empty List is returned.
  static List<PokemonMove> listFrom(List<dynamic>? data) =>
      data?.map((e) => PokemonMove.fromMap(e)).toList() ?? [];
}
