// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/move_stat_affect.dart';

/// This contains information about a MoveStatAffectSets.
///
/// The fields provide information from the PokeAPI documentation.
class MoveStatAffectSets {
  /// The name of the increase field in the JSON data.
  static const INCREASE_FIELD_NAME = 'increase';

  /// The name of the decrease field in the JSON data.
  static const DECREASE_FIELD_NAME = 'decrease';

  /// A list of moves and how they change the referenced stat.
  final List<MoveStatAffect> increase;

  /// A list of moves and how they change the referenced stat.
  final List<MoveStatAffect> decrease;

  /// Create a new MoveStatAffectSets.
  const MoveStatAffectSets(this.increase, this.decrease);

  /// Create a new MoveStatAffectSets from a Map<String, dynamic>.
  MoveStatAffectSets.fromMap(Map<String, dynamic> data)
      : increase = MoveStatAffect.listFrom(data[INCREASE_FIELD_NAME]),
        decrease = MoveStatAffect.listFrom(data[DECREASE_FIELD_NAME]);

  /// Convert a MoveStatAffectSets to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        INCREASE_FIELD_NAME: increase.map((e) => e.toMap()).toList(),
        DECREASE_FIELD_NAME: decrease.map((e) => e.toMap()).toList(),
      };
}
