// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This contains information about a NatureStatChange.
///
/// The fields provide information from the PokeAPI documentation.
class NatureStatChange {
  /// The name of the max change field in the JSON data.
  static const MAX_CHANGE_FIELD_NAME = 'max_change';

  /// The name of the pokeathlon stat field in the JSON data.
  static const POKEATHLON_STAT_FIELD_NAME = 'pokeathlon_stat';

  /// The amount of change.
  final int maxChange;

  /// The stat being affected.
  final NamedApiResource pokeathlonStat;

  /// Create a new NatureStatChange.
  const NatureStatChange(this.maxChange, this.pokeathlonStat);

  /// Create a new NatureStatChange from a Map<String, dynamic>.
  NatureStatChange.fromMap(Map<String, dynamic> data)
      : maxChange = data[MAX_CHANGE_FIELD_NAME],
        pokeathlonStat =
            NamedApiResource.fromMap(data[POKEATHLON_STAT_FIELD_NAME]);

  /// Convert a NatureStatChange to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        MAX_CHANGE_FIELD_NAME: maxChange,
        POKEATHLON_STAT_FIELD_NAME: pokeathlonStat.toMap(),
      };

  /// Create a List of NatureStatChange objects from the provided data. If the
  /// data is null an empty List is returned.
  static List<NatureStatChange> listFrom(List<dynamic>? data) =>
      data?.map((e) => NatureStatChange.fromMap(e)).toList() ?? [];
}
