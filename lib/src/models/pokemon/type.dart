// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/type_pokemon.dart';
import 'package:pokeapi/src/models/pokemon/type_relations.dart';
import 'package:pokeapi/src/models/utility/common/generation_game_index.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// From PokeAPI documentation:
/// Types are properties for Pokémon and their moves. Each type has three
/// properties: which types of Pokémon it is super effective against, which
/// types of Pokémon it is not very effective against, and which types of
/// Pokémon it is completely ineffective against.
///
/// The fields provide information from the PokeAPI documentation.
class Type {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the name field in the JSON data.
  static const NAME_FIELD_NAME = 'name';

  /// The name of the damageRelations field in the JSON data.
  static const DAMAGE_RELATIONS_FIELD_NAME = 'damage_relations';

  /// The name of the gameIndicies field in the JSON data.
  static const GAME_INDICIES_FIELD_NAME = 'game_indicies';

  /// The name of the generation field in the JSON data.
  static const GENERATION_FIELD_NAME = 'generation';

  /// The name of the moveDamageClass field in the JSON data.
  static const MOVE_DAMAGE_CLASS_FIELD_NAME = 'move_damage_class';

  /// The name of the names field in the JSON data.
  static const NAMES_FIELD_NAME = 'names';

  /// The name of the pokemon field in the JSON data.
  static const POKEMON_FIELD_NAME = 'pokemon';

  /// The name of the moves field in the JSON data.
  static const MOVES_FIELD_NAME = 'moves';

  /// The identifier for this resource.
  final int id;

  /// The name for this resource.
  final String name;

  /// A detail of how effective this type is toward others and vice versa.
  final TypeRelations damageRelations;

  /// A list of game indices relevent to this item by generation.
  final List<GenerationGameIndex> gameIndicies;

  /// The generation this type was introduced in.
  final NamedApiResource generation;

  /// The class of damage inflicted by this type.
  final NamedApiResource moveDamageClass;

  /// The name of this resource listed in different languages.
  final List<Name> names;

  /// A list of details of Pokémon that have this type.
  final List<TypePokemon> pokemon;

  /// A list of moves that have this type.
  final List<NamedApiResource> moves;

  /// Create a new Type.
  const Type(
      this.id,
      this.name,
      this.damageRelations,
      this.gameIndicies,
      this.generation,
      this.moveDamageClass,
      this.names,
      this.pokemon,
      this.moves);

  /// Create a new Type from a Map<String, dynamic>.
  Type.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        name = data[NAME_FIELD_NAME],
        damageRelations =
            TypeRelations.fromMap(data[DAMAGE_RELATIONS_FIELD_NAME]),
        gameIndicies =
            GenerationGameIndex.listFrom(data[GAME_INDICIES_FIELD_NAME]),
        generation = NamedApiResource.fromMap(data[GENERATION_FIELD_NAME]),
        moveDamageClass =
            NamedApiResource.fromMap(data[MOVE_DAMAGE_CLASS_FIELD_NAME]),
        names = Name.listFrom(data[NAMES_FIELD_NAME]),
        pokemon = TypePokemon.listFrom(data[POKEMON_FIELD_NAME]),
        moves = NamedApiResource.listFrom(data[MOVES_FIELD_NAME]);

  /// Convert a Type to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        NAME_FIELD_NAME: name,
        DAMAGE_RELATIONS_FIELD_NAME: damageRelations.toMap(),
        GAME_INDICIES_FIELD_NAME: gameIndicies.map((e) => e.toMap()).toList(),
        GENERATION_FIELD_NAME: generation.toMap(),
        MOVE_DAMAGE_CLASS_FIELD_NAME: moveDamageClass.toMap(),
        NAMES_FIELD_NAME: names.map((e) => e.toMap()).toList(),
        POKEMON_FIELD_NAME: pokemon.map((e) => e.toMap()).toList(),
        MOVES_FIELD_NAME: moves.map((e) => e.toMap()).toList(),
      };
}
