// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This contains information about a PokemonHeldItemVersion.
///
/// The fields provide information from the PokeAPI documentation.
class PokemonHeldItemVersion {
  /// The name of the version field in the JSON data.
  static const VERSION_FIELD_NAME = 'version';

  /// How often the item is held.
  static const RARITY_FIELD_NAME = 'rarity';

  /// The version in which the item is held.
  final NamedApiResource version;

  /// How often the item is held.
  final int rarity;

  /// Create a new PokemonHeldItemVersion.
  const PokemonHeldItemVersion(this.version, this.rarity);

  /// Create a new PokemonHeldItemVersion from a Map<String, dynamic>.
  PokemonHeldItemVersion.fromMap(Map<String, dynamic> data)
      : version = NamedApiResource.fromMap(data[VERSION_FIELD_NAME]),
        rarity = data[RARITY_FIELD_NAME];

  /// Convert a PokemonHeldItemVersion to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        VERSION_FIELD_NAME: version.toMap(),
        RARITY_FIELD_NAME: rarity,
      };

  /// Create a List of PokemonHeldItemVersion objects from the provided data.
  /// If the data is null an empty List is returned.
  static List<PokemonHeldItemVersion> listFrom(List<dynamic>? data) =>
      data?.map((e) => PokemonHeldItemVersion.fromMap(e)).toList() ?? [];
}
