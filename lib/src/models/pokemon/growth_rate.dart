// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/growth_rate_experience_level.dart';
import 'package:pokeapi/src/models/utility/common/description.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// From PokeAPI documentation:
/// Growth rates are the speed with which Pokémon gain levels through
/// experience. Check out
/// [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/Experience) for greater
/// detail.
///
/// The fields provide information from the PokeAPI documentation.
class GrowthRate {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the name field in the JSON data.
  static const NAME_FIELD_NAME = 'name';

  /// The name of the formula field in the JSON data.
  static const FORMULA_FIELD_NAME = 'formula';

  /// The name of the descriptions field in the JSON data.
  static const DESCRIPTIONS_FIELD_NAME = 'descriptions';

  /// The name of the levels field in the JSON data.
  static const LEVELS_FIELD_NAME = 'levels';

  /// The name of the pokemonSpecies field in the JSON data.
  static const POKEMON_SPECIES_FIELD_NAME = 'pokemon_species';

  /// The identifier for this resource.
  final int id;

  /// The name for this resource.
  final String name;

  /// The formula used to calculate the rate at which the Pokémon species gains
  /// level.
  final String formula;

  /// The descriptions of this characteristic listed in different languages.
  final List<Description> descriptions;

  /// A list of levels and the amount of experienced needed to atain them based
  /// on this growth rate.
  final List<GrowthRateExperienceLevel> levels;

  /// A list of Pokémon species that gain levels at this growth rate.
  final List<NamedApiResource> pokemonSpecies;

  /// Create a new GrowthRate.
  const GrowthRate(this.id, this.name, this.formula, this.descriptions,
      this.levels, this.pokemonSpecies);

  /// Create a new GrowthRate from a Map<String, dynamic>.
  GrowthRate.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        name = data[NAME_FIELD_NAME],
        formula = data[FORMULA_FIELD_NAME],
        descriptions = Description.listFrom(data[DESCRIPTIONS_FIELD_NAME]),
        levels = GrowthRateExperienceLevel.listFrom(data[LEVELS_FIELD_NAME]),
        pokemonSpecies =
            NamedApiResource.listFrom(data[POKEMON_SPECIES_FIELD_NAME]);

  /// Convert a GrowthRate to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        NAME_FIELD_NAME: name,
        FORMULA_FIELD_NAME: formula,
        DESCRIPTIONS_FIELD_NAME: descriptions.map((e) => e.toMap()).toList(),
        LEVELS_FIELD_NAME: levels.map((e) => e.toMap()).toList(),
        POKEMON_SPECIES_FIELD_NAME:
            pokemonSpecies.map((e) => e.toMap()).toList(),
      };
}
