// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This contains information about a NaturePokeathlonStatAffect.
///
/// The fields provide information from the PokeAPI documentation.
class NaturePokeathlonStatAffect {
  /// The name of the max change field in the JSON data.
  static const MAX_CHANGE_FIELD_NAME = 'max_change';

  /// The name of the nature field in the JSON data.
  static const NATURE_FIELD_NAME = 'nature';

  /// The maximum amount of change to the referenced Pokéathlon stat.
  final int maxChange;

  /// The nature causing the change.
  final NamedApiResource nature;

  /// Create a new NaturePokeathlonStatAffect.
  const NaturePokeathlonStatAffect(this.maxChange, this.nature);

  /// Create a new NaturePokeathlonStatAffect from a Map<String, dynamic>.
  NaturePokeathlonStatAffect.fromMap(Map<String, dynamic> data)
      : maxChange = data[MAX_CHANGE_FIELD_NAME],
        nature = NamedApiResource.fromMap(data[NATURE_FIELD_NAME]);

  /// Convert a NaturePokeathlonStatAffect to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        MAX_CHANGE_FIELD_NAME: maxChange,
        NATURE_FIELD_NAME: nature.toMap(),
      };

  /// Create a List of PokemonEncounter objects from the provided data. If the
  /// data is null an empty List is returned.
  static List<NaturePokeathlonStatAffect> listFrom(List<dynamic>? data) =>
      data?.map((e) => NaturePokeathlonStatAffect.fromMap(e)).toList() ?? [];
}
