// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This contains information about a PokemonType.
///
/// The fields provide information from the PokeAPI documentation.
class PokemonType {
  /// The name of the slot field in the JSON data.
  static const SLOT_FIELD_NAME = 'slot';

  /// The name of the type field in the JSON data.
  static const TYPE_FIELD_NAME = 'type';

  /// The order the Pokémon's types are listed in.
  final int slot;

  /// The type the referenced Pokémon has.
  final NamedApiResource type;

  /// Create a new PokemonType.
  const PokemonType(this.slot, this.type);

  /// Create a new PokemonType from a Map<String, dynamic>.
  PokemonType.fromMap(Map<String, dynamic> data)
      : slot = data[SLOT_FIELD_NAME],
        type = NamedApiResource.fromMap(data[TYPE_FIELD_NAME]);

  /// Convert a PokemonType to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        SLOT_FIELD_NAME: slot,
        TYPE_FIELD_NAME: type.toMap(),
      };

  /// Create a List of PokemonType objects from the provided data. If the data
  /// is null an empty List is returned.
  static List<PokemonType> listFrom(List<dynamic>? data) =>
      data?.map((e) => PokemonType.fromMap(e)).toList() ?? [];
}
