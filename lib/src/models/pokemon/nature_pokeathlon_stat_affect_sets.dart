// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/nature_pokeathlon_stat_affect.dart';

/// This contains information about a NaturePokeathlonStatAffectsSets.
///
/// The fields provide information from the PokeAPI documentation.
class NaturePokeathlonStatAffectSets {
  /// The name of the increase field in the JSON data.
  static const INCREASE_FIELD_NAME = 'increase';

  /// The name of the decrease field in the JSON data.
  static const DECREASE_FIELD_NAME = 'decrease';

  /// A list of natures and how they change the referenced Pokéathlon stat.
  final List<NaturePokeathlonStatAffect> increase;

  /// A list of natures and how they change the referenced Pokéathlon stat.
  final List<NaturePokeathlonStatAffect> decrease;

  /// Create a new NaturePokeathlonStatAffectSets.
  const NaturePokeathlonStatAffectSets(this.increase, this.decrease);

  /// Create a new NaturePokeathlonStatAffectSets from a Map<String, dynamic>.
  NaturePokeathlonStatAffectSets.fromMap(Map<String, dynamic> data)
      : increase =
            NaturePokeathlonStatAffect.listFrom(data[INCREASE_FIELD_NAME]),
        decrease =
            NaturePokeathlonStatAffect.listFrom(data[DECREASE_FIELD_NAME]);

  /// Convert a NaturePokeathlonStatAffectSets to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        INCREASE_FIELD_NAME: increase.map((e) => e.toMap()).toList(),
        DECREASE_FIELD_NAME: decrease.map((e) => e.toMap()).toList(),
      };
}
