// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This contains information about a TypePokemon.
///
/// The fields provide information from the PokeAPI documentation.
class TypePokemon {
  /// The name of the slot field in the JSON data.
  static const SLOT_FIELD_NAME = 'slot';

  /// The name of the pokemon field in the JSON data.
  static const POKEMON_FIELD_NAME = 'pokemon';

  /// The order the Pokémon's types are listed in.
  final int slot;

  /// The Pokémon that has the referenced type.
  final NamedApiResource pokemon;

  /// Create a new TypePokemon.
  const TypePokemon(this.slot, this.pokemon);

  /// Create a new TypePokemon from a Map<String, dynamic>.
  TypePokemon.fromMap(Map<String, dynamic> data)
      : slot = data[SLOT_FIELD_NAME],
        pokemon = NamedApiResource.fromMap(data[POKEMON_FIELD_NAME]);

  /// Convert a TypePokemon to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        SLOT_FIELD_NAME: slot,
        POKEMON_FIELD_NAME: pokemon.toMap(),
      };

  /// Create a List of TypePokemon objects from the provided data. If the data
  /// is null an empty List is returned.
  static List<TypePokemon> listFrom(List<dynamic>? data) =>
      data?.map((e) => TypePokemon.fromMap(e)).toList() ?? [];
}
