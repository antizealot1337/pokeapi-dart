// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This contains information about a TypeRelations.
///
/// The fields provide information from the PokeAPI documentation.
class TypeRelations {
  /// The name of the noDamageTo field in the JSON data.
  static const NO_DAMAGE_TO_FIELD_NAME = 'no_damage_to';

  /// The name of the halfDamageTo field in the JSON data.
  static const HALF_DAMAGE_TO_FIELD_NAME = 'half_damage_to';

  /// The name of the doubleDamageTo field in the JSON data.
  static const DOUBLE_DAMAGE_TO_FIELD_NAME = 'double_damage_to';

  /// The name of the noDamageFrom field in the JSON data.
  static const NO_DAMAGE_FROM_FIELD_NAME = 'no_damage_from';

  /// The name of the halfDamageFrom field in the JSON data.
  static const HALF_DAMAGE_FROM_FIELD_NAME = 'half_damage_from';

  /// The name of the doubleDamageFrom field in the JSON data.
  static const DOUBLE_DAMAGE_FROM_FIELD_NAME = 'double_damage_from';

  /// A list of types this type has no effect on.
  final List<NamedApiResource> noDamageTo;

  /// A list of types this type is not very effect against.
  final List<NamedApiResource> halfDamageTo;

  /// A list of types this type is very effect against.
  final List<NamedApiResource> doubleDamageTo;

  /// A list of types that have no effect on this type.
  final List<NamedApiResource> noDamageFrom;

  /// A list of types that are not very effective against this type.
  final List<NamedApiResource> halfDamageFrom;

  /// A list of types that are very effective against this type.
  final List<NamedApiResource> doubleDamageFrom;

  /// Create a new TypeRelations.
  const TypeRelations(this.noDamageTo, this.halfDamageTo, this.doubleDamageTo,
      this.noDamageFrom, this.halfDamageFrom, this.doubleDamageFrom);

  /// Create a new TypeRelations from a Map<String, dynamic>.
  TypeRelations.fromMap(Map<String, dynamic> data)
      : noDamageTo = NamedApiResource.listFrom(data[NO_DAMAGE_TO_FIELD_NAME]),
        halfDamageTo =
            NamedApiResource.listFrom(data[HALF_DAMAGE_TO_FIELD_NAME]),
        doubleDamageTo =
            NamedApiResource.listFrom(data[DOUBLE_DAMAGE_TO_FIELD_NAME]),
        noDamageFrom =
            NamedApiResource.listFrom(data[NO_DAMAGE_FROM_FIELD_NAME]),
        halfDamageFrom =
            NamedApiResource.listFrom(data[HALF_DAMAGE_FROM_FIELD_NAME]),
        doubleDamageFrom =
            NamedApiResource.listFrom(data[DOUBLE_DAMAGE_FROM_FIELD_NAME]);

  /// Convert a TypeRelations to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        NO_DAMAGE_TO_FIELD_NAME: noDamageTo.map((e) => e.toMap()).toList(),
        HALF_DAMAGE_TO_FIELD_NAME: halfDamageTo.map((e) => e.toMap()).toList(),
        DOUBLE_DAMAGE_TO_FIELD_NAME:
            doubleDamageTo.map((e) => e.toMap()).toList(),
        NO_DAMAGE_FROM_FIELD_NAME: noDamageFrom.map((e) => e.toMap()).toList(),
        HALF_DAMAGE_FROM_FIELD_NAME:
            halfDamageFrom.map((e) => e.toMap()).toList(),
        DOUBLE_DAMAGE_FROM_FIELD_NAME:
            doubleDamageFrom.map((e) => e.toMap()).toList(),
      };
}
