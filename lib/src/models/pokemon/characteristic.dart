// SPDX-License-Identifier: MIT

/// From PokeAPI documentation:
/// Characteristics indicate which stat contains a Pokémon's highest IV. A
/// Pokémon's Characteristic is determined by the remainder of its highest IV
/// divided by 5 (gene_modulo). Check out
/// [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/Characteristic) for
/// greater detail.
///
/// The fields provide information from the PokeAPI documentation.
class Characteristic {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the gene modulo field in the JSON data.
  static const GENE_MODULO_FIELD_NAME = 'gene_modulo';

  /// The name of the possible values field in the JSON data.
  static const POSSIBLE_VALUES_FIELD_NAME = 'possible_values';

  /// The identifier for this resource.
  final int id;

  /// The remainder of the highest stat/IV divided by 5.
  final int geneModulo;

  /// The possible values of the highest stat that would result in a Pokémon
  /// recieving this characteristic when divided by 5.
  final List<int> possibleValues;

  /// Create a new Characteristic.
  const Characteristic(this.id, this.geneModulo, this.possibleValues);

  /// Create a new Characteristic from a Map<String, dynamic>.
  Characteristic.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        geneModulo = data[GENE_MODULO_FIELD_NAME],
        possibleValues = (data[POSSIBLE_VALUES_FIELD_NAME] as List<dynamic>)
            .map((possibleValue) => possibleValue as int)
            .toList();

  /// Convert a Characteristic into a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        GENE_MODULO_FIELD_NAME: geneModulo,
        POSSIBLE_VALUES_FIELD_NAME: possibleValues,
      };
}
