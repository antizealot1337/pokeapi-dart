// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/move_battle_style_preference.dart';
import 'package:pokeapi/src/models/pokemon/nature_stat_change.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// From PokeAPI documentation:
/// Natures influence how a Pokémon's stats grow. See
/// [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/Nature) for greater
/// detail.
///
/// The fields provide information from the PokeAPI documentation.
class Nature {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the name field in the JSON data.
  static const NAME_FIELD_NAME = 'name';

  /// The name of the decreased stata field in the JSON data.
  static const DECREASED_STAT_FIELD_NAME = 'decreased_stat';

  /// The name of the increased stata field in the JSON data.
  static const INCREASED_STAT_FIELD_NAME = 'increased_stat';

  /// The name of the hates flavor field in the JSON data.
  static const HATES_FLAVOR_FIELD_NAME = 'hates_flavor';

  /// The name of the likes flavor field in the JSON data.
  static const LIKES_FLAVOR_FIELD_NAME = 'likes_flavor';

  /// The name of the pokeathlon stat changes field in the JSON data.
  static const POKEATHLON_STAT_CHANGES_FIELD_NAME = 'pokeathlon_stat_changes';

  /// The name of the move battle style preferences field in the JSON data.
  static const MOVE_BATTLE_STYLE_PREFERENCES_FIELD_NAME =
      'move_battle_style_preferences';

  /// The name of the names field in the JSON data.
  static const NAMES_FIELD_NAME = 'names';

  /// The identifier for this resource.
  final int id;

  /// The name for this resource.
  final String name;

  /// The stat decreased by 10% in Pokémon with this nature.
  final NamedApiResource? decreasedStat;

  /// The stat increased by 10% in Pokémon with this nature.
  final NamedApiResource? increasedStat;

  /// The flavor hated by Pokémon with this nature.
  final NamedApiResource? hatesFlavor;

  /// The flavor liked by Pokémon with this nature.
  final NamedApiResource? likesFlavor;

  /// A list of Pokéathlon stats this nature effects and how much it effects
  /// them.
  final List<NatureStatChange> pokeathlonStatChanges;

  /// A list of battle styles and how likely a Pokémon with this nature is to
  /// use them in the Battle Palace or Battle Tent.
  final List<MoveBattleStylePreference> moveBattleStylePreferences;

  /// The name of this resource listed in different languages.
  final List<Name> names;

  /// Create a new Nature.
  const Nature(
      this.id,
      this.name,
      this.decreasedStat,
      this.increasedStat,
      this.hatesFlavor,
      this.likesFlavor,
      this.pokeathlonStatChanges,
      this.moveBattleStylePreferences,
      this.names);

  /// Create a new Nature from a Map<String, dynamic>.
  Nature.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        name = data[NAME_FIELD_NAME],
        decreasedStat =
            NamedApiResource.tryFromMap(data[DECREASED_STAT_FIELD_NAME]),
        increasedStat =
            NamedApiResource.tryFromMap(data[INCREASED_STAT_FIELD_NAME]),
        hatesFlavor =
            NamedApiResource.tryFromMap(data[HATES_FLAVOR_FIELD_NAME]),
        likesFlavor =
            NamedApiResource.tryFromMap(data[LIKES_FLAVOR_FIELD_NAME]),
        pokeathlonStatChanges =
            NatureStatChange.listFrom(data[POKEATHLON_STAT_CHANGES_FIELD_NAME]),
        moveBattleStylePreferences = MoveBattleStylePreference.listFrom(
            data[MOVE_BATTLE_STYLE_PREFERENCES_FIELD_NAME]),
        names = Name.listFrom(data[NAMES_FIELD_NAME]);

  /// Convert a Nature to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        NAME_FIELD_NAME: name,
        DECREASED_STAT_FIELD_NAME: decreasedStat?.toMap(),
        INCREASED_STAT_FIELD_NAME: increasedStat?.toMap(),
        HATES_FLAVOR_FIELD_NAME: hatesFlavor?.toMap(),
        LIKES_FLAVOR_FIELD_NAME: likesFlavor?.toMap(),
        POKEATHLON_STAT_CHANGES_FIELD_NAME:
            pokeathlonStatChanges.map((e) => e.toMap()).toList(),
        MOVE_BATTLE_STYLE_PREFERENCES_FIELD_NAME:
            moveBattleStylePreferences.map((e) => e.toMap()).toList(),
        NAMES_FIELD_NAME: names.map((e) => e.toMap()).toList(),
      };
}
