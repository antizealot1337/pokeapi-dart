// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This contains information about a MoveStatAffect.
///
/// The fields provide information from the PokeAPI documentation.
class MoveStatAffect {
  /// The name of the change field in the JSON data.
  static const CHANGE_FIELD_NAME = 'change';

  /// The name of the move field in the JSON data.
  static const MOVE_FIELD_NAME = 'move';

  /// The maximum amount of change to the referenced stat.
  final int change;

  /// The move causing the change.
  final NamedApiResource move;

  /// Create a new MoveStatAffect.
  const MoveStatAffect(this.change, this.move);

  /// Create a new MoveStatAffect from a Map<String, dynamic>.
  MoveStatAffect.fromMap(Map<String, dynamic> data)
      : change = data[CHANGE_FIELD_NAME],
        move = NamedApiResource.fromMap(data[MOVE_FIELD_NAME]);

  /// Convert to MoveStatAffect to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        CHANGE_FIELD_NAME: change,
        MOVE_FIELD_NAME: move.toMap(),
      };

  /// Create a List of MoveStatAffect objects from the provided data. If the
  /// data is null an empty List is returned.
  static List<MoveStatAffect> listFrom(List<dynamic>? data) =>
      data?.map((e) => MoveStatAffect.fromMap(e)).toList() ?? [];
}
