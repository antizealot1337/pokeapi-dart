// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This contains information about a PalParkEncounterArea.
///
/// The fields provide information from the PokeAPI documentation.
class PalParkEncounterArea {
  /// The name of the baseScore field in the JSON data.
  static const BASE_SCORE_FIELD_NAME = 'base_score';

  /// The name of the rate field in the JSON data.
  static const RATE_FIELD_NAME = 'rate';

  /// The name of the area field in the JSON data.
  static const AREA_FIELD_NAME = 'area';

  /// The base score given to the player when the referenced Pokémon is caught
  /// during a pal park run.
  final int baseScore;

  /// The base rate for encountering the referenced Pokémon in this pal park
  /// area.
  final int rate;

  /// The pal park area where this encounter happens.
  final NamedApiResource area;

  /// Create a new PalParkEncounterArea.
  const PalParkEncounterArea(this.baseScore, this.rate, this.area);

  /// Create a new PalParkEncounterArea from a Map<String, dynamic>.
  PalParkEncounterArea.fromMap(Map<String, dynamic> data)
      : baseScore = data[BASE_SCORE_FIELD_NAME],
        rate = data[RATE_FIELD_NAME],
        area = NamedApiResource.fromMap(data[AREA_FIELD_NAME]);

  /// Convert a PalParkEncounterArea to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        BASE_SCORE_FIELD_NAME: baseScore,
        RATE_FIELD_NAME: rate,
        AREA_FIELD_NAME: area.toMap(),
      };

  /// Create a List of PalParkEncounterArea objects from the provided data. If
  /// the data is null an empty List is returned.
  static List<PalParkEncounterArea> listFrom(List<dynamic>? data) =>
      data?.map((e) => PalParkEncounterArea.fromMap(e)).toList() ?? [];
}
