// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This contains information about an AbilityPokemon.
///
/// The fields provide information from the PokeAPI documentation.
class AbilityPokemon {
  /// The name of the is hidden field in the JSON data.
  static const IS_HIDDEN_FIELD_NAME = 'is_hidden';

  /// The name of the slot field in the JSON data.
  static const SLOT_FIELD_NAME = 'slot';

  /// The name of the pokemon field in the JSON data.
  static const POKEMON_FIELD_NAME = 'pokemon';

  /// The localized name for an API resource in a specific language.
  final bool isHidden;

  /// Pokémon have 3 ability 'slots' which hold references to possible abilities
  /// they could have. This is the slot of this ability for the referenced
  /// pokemon.
  final int slot;

  /// The Pokémon this ability could belong to.
  final NamedApiResource pokemon;

  /// Create a new AbilityPokemon.
  const AbilityPokemon(this.isHidden, this.slot, this.pokemon);

  /// Create a new AbilityPokemon from a Map<String, dynamic>.
  AbilityPokemon.fromMap(Map<String, dynamic> data)
      : isHidden = data[IS_HIDDEN_FIELD_NAME],
        slot = data[SLOT_FIELD_NAME],
        pokemon = NamedApiResource.fromMap(data[POKEMON_FIELD_NAME]);

  /// Convert an AbilityPokemon to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        IS_HIDDEN_FIELD_NAME: isHidden,
        SLOT_FIELD_NAME: slot,
        POKEMON_FIELD_NAME: pokemon.toMap(),
      };

  /// Create a List of AbilityPokemon objects from the provided data. If the
  /// data is null an empty List is returned.
  static List<AbilityPokemon> listFrom(List<dynamic>? data) =>
      data?.map((e) => AbilityPokemon.fromMap(e)).toList() ?? [];
}
