// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This contains information about a MoveBattleStylePreference.
///
/// The fields provide information from the PokeAPI documentation.
class MoveBattleStylePreference {
  /// The name of the low hp preference field in the JSON data.
  static const LOW_HP_PREFERENCE_FIELD_NAME = 'low_hp_preference';

  /// The name of the high hp preference field in the JSON data.
  static const HIGH_HP_PREFERENCE_FIELD_NAME = 'high_hp_preference';

  /// The name of the move battle field in the JSON data.
  static const MOVE_BATTLE_STYLE_FIELD_NAME = 'move_battle_style';

  /// Chance of using the move, in percent, if HP is under one half.
  final int lowHpPreference;

  /// Chance of using the move, in percent, if HP is over one half.
  final int highHpPreference;

  /// The move battle style.
  final NamedApiResource moveBattleStyle;

  /// Create a new MoveBattleStylePreference.
  const MoveBattleStylePreference(
      this.lowHpPreference, this.highHpPreference, this.moveBattleStyle);

  /// Create a new MoveBattleStylePreference from a Map<String, dynamic>.
  MoveBattleStylePreference.fromMap(Map<String, dynamic> data)
      : lowHpPreference = data[LOW_HP_PREFERENCE_FIELD_NAME],
        highHpPreference = data[HIGH_HP_PREFERENCE_FIELD_NAME],
        moveBattleStyle =
            NamedApiResource.fromMap(data[MOVE_BATTLE_STYLE_FIELD_NAME]);

  /// Convert a MoveBattleStylePreference to a Map<String dynamic>.
  Map<String, dynamic> toMap() => {
        LOW_HP_PREFERENCE_FIELD_NAME: lowHpPreference,
        HIGH_HP_PREFERENCE_FIELD_NAME: highHpPreference,
        MOVE_BATTLE_STYLE_FIELD_NAME: moveBattleStyle.toMap(),
      };

  /// Create a List of MoveBattleStylePreference objects from the provided
  /// data. If the data is null an empty List is returned.
  static List<MoveBattleStylePreference> listFrom(List<dynamic>? data) =>
      data?.map((e) => MoveBattleStylePreference.fromMap(e)).toList() ?? [];
}
