// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/pokemon_ability.dart';
import 'package:pokeapi/src/models/pokemon/pokemon_held_item.dart';
import 'package:pokeapi/src/models/pokemon/pokemon_move.dart';
import 'package:pokeapi/src/models/pokemon/pokemon_sprites.dart';
import 'package:pokeapi/src/models/pokemon/pokemon_stat.dart';
import 'package:pokeapi/src/models/pokemon/pokemon_type.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';
import 'package:pokeapi/src/models/utility/common/version_game_index.dart';

/// From PokeAPI documentation:
///
///
/// The fields provide information from the PokeAPI documentation.
class Pokemon {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the name field in the JSON data.
  static const NAME_FIELD_NAME = 'name';

  /// The name of the base experience field in the JSON data.
  static const BASE_EXPERIENCE_FIELD_NAME = 'base_experience';

  /// The name of the height field in the JSON data.
  static const HEIGHT_FIELD_NAME = 'height';

  /// The name of the is default field in the JSON data.
  static const IS_DEFAULT_FIELD_NAME = 'is_default';

  /// The name of the order field in the JSON data.
  static const ORDER_FIELD_NAME = 'order';

  /// The name of the weight field in the JSON data.
  static const WEIGHT_FIELD_NAME = 'weight';

  /// The name of the abilities field in the JSON data.
  static const ABILITIES_FIELD_NAME = 'abilities';

  /// The name of the forms field in the JSON data.
  static const FORMS_FIELD_NAME = 'forms';

  /// The name of the game indicies field in the JSON data.
  static const GAME_INDICIES_FIELD_NAME = 'game_indicies';

  /// The name of the held item field in the JSON data.
  static const HELD_ITEM_FIELD_NAME = 'held_items';

  /// The name of the location area encounters field in the JSON data.
  static const LOCATION_AREA_ENCOUNTERS_FIELD_NAME = 'location_area_encounters';

  /// The name of the sprites field in the JSON data.
  static const SPRITES_FIELD_NAME = 'sprites';

  /// The name of the moves field in the JSON data.
  static const MOVES_FIELD_NAME = 'moves';

  /// The name of the species field in the JSON data.
  static const SPECIES_FIELD_NAME = 'species';

  /// The name of the stats field in the JSON data.
  static const STATS_FIELD_NAME = 'stats';

  /// The name of the types field in the JSON data.
  static const TYPES_FIELD_NAME = 'types';

  /// The identifier for this resource.
  final int id;

  /// The name for this resource.
  final String name;

  ///  The base experience gained for defeating this Pokémon.
  final int baseExperience;

  /// The height of this Pokémon in decimetres.
  final int height;

  /// Set for exactly one Pokémon used as the default for each species.
  final bool isDefault;

  /// Order for sorting. Almost national order, except families are grouped
  /// together.
  final int order;

  /// The weight of this Pokémon in hectograms.
  final int weight;

  /// A list of abilities this Pokémon could potentially have.
  final List<PokemonAbility> abilities;

  /// A list of forms this Pokémon can take on.
  final List<NamedApiResource> forms;

  /// A list of game indices relevent to Pokémon item by generation.
  final List<VersionGameIndex> gameIndicies;

  /// A list of items this Pokémon may be holding when encountered.
  final List<PokemonHeldItem> heldItems;

  /// A link to a list of location areas, as well as encounter details
  /// pertaining to specific versions.
  final String locationAreaEncounters;

  /// A list of moves along with learn methods and level details pertaining to
  /// specific version groups.
  final List<PokemonMove> moves;

  /// A set of sprites used to depict this Pokémon in the game. A visual
  /// representation of the various sprites can be found at
  /// [PokeAPI/sprites](https://github.com/PokeAPI/sprites#sprites).
  final PokemonSprites sprites;

  /// The species this Pokémon belongs to.
  final NamedApiResource species;

  /// A list of base stat values for this Pokémon.
  final List<PokemonStat> stats;

  /// A list of details showing types this Pokémon has.
  final List<PokemonType> types;

  /// Create a new Pokemon.
  const Pokemon(
      this.id,
      this.name,
      this.baseExperience,
      this.height,
      this.isDefault,
      this.order,
      this.weight,
      this.abilities,
      this.forms,
      this.gameIndicies,
      this.heldItems,
      this.locationAreaEncounters,
      this.moves,
      this.sprites,
      this.species,
      this.stats,
      this.types);

  /// Create a new Pokemon from a Map<String, dynamic>.
  Pokemon.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        name = data[NAME_FIELD_NAME],
        baseExperience = data[BASE_EXPERIENCE_FIELD_NAME],
        height = data[HEIGHT_FIELD_NAME],
        isDefault = data[IS_DEFAULT_FIELD_NAME],
        order = data[ORDER_FIELD_NAME],
        weight = data[WEIGHT_FIELD_NAME],
        abilities = PokemonAbility.listFrom(data[ABILITIES_FIELD_NAME]),
        forms = NamedApiResource.listFrom(data[FORMS_FIELD_NAME]),
        gameIndicies =
            VersionGameIndex.listFrom(data[GAME_INDICIES_FIELD_NAME]),
        heldItems = PokemonHeldItem.listFrom(data[HELD_ITEM_FIELD_NAME]),
        locationAreaEncounters = data[LOCATION_AREA_ENCOUNTERS_FIELD_NAME],
        moves = PokemonMove.listFrom(data[MOVES_FIELD_NAME]),
        sprites = PokemonSprites.fromMap(data[SPRITES_FIELD_NAME]),
        species = NamedApiResource.fromMap(data[SPECIES_FIELD_NAME]),
        stats = PokemonStat.listFrom(data[STATS_FIELD_NAME]),
        types = PokemonType.listFrom(data[TYPES_FIELD_NAME]);

  /// Convert a Pokemon to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        NAME_FIELD_NAME: name,
        BASE_EXPERIENCE_FIELD_NAME: baseExperience,
        HEIGHT_FIELD_NAME: height,
        IS_DEFAULT_FIELD_NAME: isDefault,
        ORDER_FIELD_NAME: order,
        WEIGHT_FIELD_NAME: weight,
        ABILITIES_FIELD_NAME: abilities.map((e) => e.toMap()).toList(),
        FORMS_FIELD_NAME: forms.map((e) => e.toMap()).toList(),
        GAME_INDICIES_FIELD_NAME: gameIndicies.map((e) => e.toMap()).toList(),
        HELD_ITEM_FIELD_NAME: heldItems.map((e) => e.toMap()).toList(),
        LOCATION_AREA_ENCOUNTERS_FIELD_NAME: locationAreaEncounters,
        MOVES_FIELD_NAME: moves.map((e) => e.toMap()).toList(),
        SPRITES_FIELD_NAME: sprites.toMap(),
        SPECIES_FIELD_NAME: species.toMap(),
        STATS_FIELD_NAME: stats.map((e) => e.toMap()).toList(),
        TYPES_FIELD_NAME: types.map((e) => e.toMap()).toList(),
      };
}
