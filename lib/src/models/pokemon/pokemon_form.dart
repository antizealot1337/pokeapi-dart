// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/pokemon/pokemon_form_sprites.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// From PokeAPI documentation:
/// Some Pokémon may appear in one of multiple, visually different forms. These
/// differences are purely cosmetic. For variations within a Pokémon species,
/// which do differ in more than just visuals, the 'Pokémon' entity is used to
/// represent such a variety.
///
/// The fields provide information from the PokeAPI documentation.
class PokemonForm {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the name field in the JSON data.
  static const NAME_FIELD_NAME = 'name';

  /// The name of the order field in the JSON data.
  static const ORDER_FIELD_NAME = 'order';

  /// The name of the formOrder field in the JSON data.
  static const FORM_ORDER_FIELD_NAME = 'form_order';

  /// The name of the isDefault field in the JSON data.
  static const IS_DEFAULT_FIELD_NAME = 'is_default';

  /// The name of the isBattleOnly field in the JSON data.
  static const IS_BATTLE_ONLY_FIELD_NAME = 'is_battle_only';

  /// The name of the isMega field in the JSON data.
  static const IS_MEGA_FIELD_NAME = 'is_mega';

  /// The name of the formName field in the JSON data.
  static const FORM_NAME_FIELD_NAME = 'form_name';

  /// The name of the pokemon field in the JSON data.
  static const POKEMON_FIELD_NAME = 'pokemon';

  /// The name of the sprites field in the JSON data.
  static const SPRITES_FIELD_NAME = 'sprites';

  /// The name of the versionGroup field in the JSON data.
  static const VERSION_GROUP_FIELD_NAME = 'version_group';

  /// The name of the names field in the JSON data.
  static const NAMES_FIELD_NAME = 'names';

  /// The name of the formNames field in the JSON data.
  static const FORM_NAMES_FIELD_NAME = 'form_names';

  /// The identifier for this resource.
  final int id;

  /// The name for this resource.
  final String name;

  /// The order in which forms should be sorted within all forms. Multiple forms
  /// may have equal order, in which case they should fall back on sorting by
  /// name.
  final int order;

  /// The order in which forms should be sorted within a species' forms.
  final int formOrder;

  /// True for exactly one form used as the default for each Pokémon.
  final bool isDefault;

  /// Whether or not this form can only happen during battle.
  final bool isBattleOnly;

  /// Whether or not this form requires mega evolution.
  final bool isMega;

  /// The name of this form.
  final String formName;

  /// The Pokémon that can take on this form.
  final NamedApiResource pokemon;

  /// A set of sprites used to depict this Pokémon form in the game.
  final PokemonFormSprites sprites;

  /// The version group this Pokémon form was introduced in.
  final NamedApiResource versionGroup;

  /// The form specific full name of this Pokémon form, or empty if the form
  /// does not have a specific name.
  final List<Name> names;

  /// The form specific form name of this Pokémon form, or empty if the form
  /// does not have a specific name.
  final List<Name> formNames;

  /// Create a new PokemonForm.
  const PokemonForm(
      this.id,
      this.name,
      this.order,
      this.formOrder,
      this.isDefault,
      this.isBattleOnly,
      this.isMega,
      this.formName,
      this.pokemon,
      this.sprites,
      this.versionGroup,
      this.names,
      this.formNames);

  /// Create a new PokemonForm from a Map<String, dynamic>.
  PokemonForm.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        name = data[NAME_FIELD_NAME],
        order = data[ORDER_FIELD_NAME],
        formOrder = data[FORM_ORDER_FIELD_NAME],
        isDefault = data[IS_DEFAULT_FIELD_NAME],
        isBattleOnly = data[IS_BATTLE_ONLY_FIELD_NAME],
        isMega = data[IS_MEGA_FIELD_NAME],
        formName = data[FORM_NAME_FIELD_NAME],
        pokemon = NamedApiResource.fromMap(data[POKEMON_FIELD_NAME]),
        sprites = PokemonFormSprites.fromMap(data[SPRITES_FIELD_NAME]),
        versionGroup = NamedApiResource.fromMap(data[VERSION_GROUP_FIELD_NAME]),
        names = Name.listFrom(data[NAMES_FIELD_NAME]),
        formNames = Name.listFrom(data[FORM_NAMES_FIELD_NAME]);

  /// Convert a PokemonForm to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        NAME_FIELD_NAME: name,
        ORDER_FIELD_NAME: order,
        FORM_ORDER_FIELD_NAME: formOrder,
        IS_DEFAULT_FIELD_NAME: isDefault,
        IS_BATTLE_ONLY_FIELD_NAME: isBattleOnly,
        IS_MEGA_FIELD_NAME: isMega,
        FORM_NAME_FIELD_NAME: formName,
        POKEMON_FIELD_NAME: pokemon.toMap(),
        SPRITES_FIELD_NAME: sprites.toMap(),
        VERSION_GROUP_FIELD_NAME: versionGroup.toMap(),
        NAMES_FIELD_NAME: names.map((e) => e.toMap()).toList(),
        FORM_NAMES_FIELD_NAME: formNames.map((e) => e.toMap()).toList(),
      };
}
