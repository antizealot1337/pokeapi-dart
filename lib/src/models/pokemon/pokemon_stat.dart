// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This contains information about a PokemonStat.
///
/// The fields provide information from the PokeAPI documentation.
class PokemonStat {
  /// The name of the stat field in the JSON data.
  static const STAT_FIELD_NAME = 'stat';

  /// The name of the effort field in the JSON data.
  static const EFFORT_FIELD_NAME = 'effort';

  /// The name of the baseStat field in the JSON data.
  static const BASE_STAT_FIELD_NAME = 'base_stat';

  /// The stat the Pokémon has.
  final NamedApiResource stat;

  /// The effort points (EV) the Pokémon has in the stat.
  final int effort;

  /// The base value of the stat.
  final int baseStat;

  /// Create a new PokemonStat.
  const PokemonStat(this.stat, this.effort, this.baseStat);

  /// Create a new PokemonStat from a Map<String, dynamic>.
  PokemonStat.fromMap(Map<String, dynamic> data)
      : stat = NamedApiResource.fromMap(data[STAT_FIELD_NAME]),
        effort = data[EFFORT_FIELD_NAME],
        baseStat = data[BASE_STAT_FIELD_NAME];

  /// Convert a PokemonStat to Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        STAT_FIELD_NAME: stat.toMap(),
        EFFORT_FIELD_NAME: effort,
        BASE_STAT_FIELD_NAME: baseStat,
      };

  /// Create a List of PokemonStat objects from the provided data. If the data
  /// is null an empty List is returned.
  static List<PokemonStat> listFrom(List<dynamic>? data) =>
      data?.map((e) => PokemonStat.fromMap(e)).toList() ?? [];
}
