// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This contains information about an EvolutionDetial.
///
/// The fields provide information from the PokeAPI documentation.
class EvolutionDetail {
  /// This is the name of the item field in the JSON data.
  static const ITEM_FIELD_NAME = 'item';

  /// This is the name of the trigger field in the JSON data.
  static const TRIGGER_FIELD_NAME = 'trigger';

  /// This is the name of the gender field in the JSON data.
  static const GENDER_FIELD_NAME = 'gender';

  /// The is the name of the held item field in the JSON data.
  static const HELD_ITEM_FIELD_NAME = 'held_item';

  /// This is the name of the known move field in the JSON data.
  static const KNOWN_MOVE_FIELD_NAME = 'known_move';

  /// This is the name of the known move type field in the JSON data.
  static const KNOWN_MOVE_TYPE_FIELD_NAME = 'known_move_type';

  /// This is the name of the location field in the JSON data.
  static const LOCATION_FIELD_NAME = 'location';

  /// This is the name of the min level field in the JSON data.
  static const MIN_LEVEL_FIELD_NAME = 'min_level';

  /// This is the name of the min happiness field in the JSON data.
  static const MIN_HAPPINESS_FIELD_NAME = 'min_happiness';

  /// This is the name of the min beauty field in the JSON data.
  static const MIN_BEAUTY_FIELD_NAME = 'min_beauty';

  /// This is the name of the min affection field in the JSON data.
  static const MIN_AFFECTION_FIELD_NAME = 'min_affection';

  /// This is the name of the needs overworld rain field in the JSON data.
  static const NEEDS_OVERWORLD_RAIN_FIELD_NAME = 'needs_overworld_rain';

  /// This is the name of the party species field in the JSON data.
  static const PARTY_SPECIES_FIELD_NAME = 'party_species';

  /// This is the name of the party type field in the JSON data.
  static const PARTY_TYPE_FIELD_NAME = 'party_type';

  /// This is the name of the relative physical stats field in the JSON data.
  static const RELATIVE_PHYSICAL_STATS_FIELD_NAME = 'relative_physical_stats';

  /// This is the name of the time of day field in the JSON data.
  static const TIME_OF_DAY_FIELD_NAME = 'time_of_day';

  /// This is the name of the trade species field in the JSON data.
  static const TRADE_SPECIES_FIELD_NAME = 'trade_species';

  /// This is the name of the turn upside down field in the JSON data.
  static const TURN_UPSIDE_DOWN_FIELD_NAME = 'turn_upside_down';

  /// The item required to cause evolution this into Pokémon species.
  final NamedApiResource? item;

  /// The item required to cause evolution this into Pokémon species.
  final NamedApiResource trigger;

  /// The id of the gender of the evolving Pokémon species must be in order to evolve into this Pokémon species.
  final int? gender;

  /// The item the evolving Pokémon species must be holding during the evolution
  /// trigger event to evolve into this Pokémon species.
  final NamedApiResource? heldItem;

  /// The move that must be known by the evolving Pokémon species during the
  /// evolution trigger event in order to evolve into this Pokémon species.
  final NamedApiResource? knownMove;

  /// The evolving Pokémon species must know a move with this type during the
  /// evolution trigger event in order to evolve into this Pokémon species.
  final NamedApiResource? knownMoveType;

  /// The location the evolution must be triggered at.
  final NamedApiResource? location;

  /// The minimum required level of the evolving Pokémon species to evolve into
  /// this Pokémon species.
  final int minLevel;

  /// The minimum required level of happiness the evolving Pokémon species to
  /// evolve into this Pokémon species.
  final int? minHappiness;

  /// The minimum required level of beauty the evolving Pokémon species to evolve
  /// into this Pokémon species.
  final int? minBeauty;

  /// The minimum required level of affection the evolving Pokémon species to
  /// evolve into this Pokémon species.
  final int? minAffection;

  /// Whether or not it must be raining in the overworld to cause evolution this
  /// Pokémon species.
  final bool needsOverworldRain;

  /// The Pokémon species that must be in the players party in order for the
  /// evolving Pokémon species to evolve into this Pokémon species.
  final NamedApiResource? partySpecies;

  /// The player must have a Pokémon of this type in their party during the
  /// evolution trigger event in order for the evolving Pokémon species to
  /// evolve into this Pokémon species.
  final NamedApiResource? partyType;

  /// The required relation between the Pokémon's Attack and Defense stats. 1
  /// means Attack > Defense. 0 means Attack = Defense. -1 means Attack <
  /// Defense.
  final int? relativePhysicalStats;

  /// The required time of day. Day or night.
  final String timeOfDay;

  /// Pokémon species for which this one must be traded.
  final NamedApiResource? tradeSpecies;

  /// Whether or not the 3DS needs to be turned upside-down as this Pokémon
  /// levels up.
  final bool turnUpsideDown;

  /// Create a new EvolutionDetail.
  const EvolutionDetail(
      this.item,
      this.trigger,
      this.gender,
      this.heldItem,
      this.knownMove,
      this.knownMoveType,
      this.location,
      this.minLevel,
      this.minHappiness,
      this.minBeauty,
      this.minAffection,
      this.needsOverworldRain,
      this.partySpecies,
      this.partyType,
      this.relativePhysicalStats,
      this.timeOfDay,
      this.tradeSpecies,
      this.turnUpsideDown);

  /// Create a new EvolutionDetail from a Map<String, dynamic>.
  EvolutionDetail.fromMap(Map<String, dynamic> data)
      : item = NamedApiResource.tryFromMap(data[ITEM_FIELD_NAME]),
        trigger = NamedApiResource.fromMap(data[TRIGGER_FIELD_NAME]),
        gender = data[GENDER_FIELD_NAME],
        heldItem = NamedApiResource.tryFromMap(data[HELD_ITEM_FIELD_NAME]),
        knownMove = NamedApiResource.tryFromMap(data[KNOWN_MOVE_FIELD_NAME]),
        knownMoveType =
            NamedApiResource.tryFromMap(data[KNOWN_MOVE_TYPE_FIELD_NAME]),
        location = NamedApiResource.tryFromMap(data[LOCATION_FIELD_NAME]),
        minLevel = data[MIN_LEVEL_FIELD_NAME],
        minHappiness = data[MIN_HAPPINESS_FIELD_NAME],
        minBeauty = data[MIN_BEAUTY_FIELD_NAME],
        minAffection = data[MIN_AFFECTION_FIELD_NAME],
        needsOverworldRain = data[NEEDS_OVERWORLD_RAIN_FIELD_NAME],
        partySpecies =
            NamedApiResource.tryFromMap(data[PARTY_SPECIES_FIELD_NAME]),
        partyType = NamedApiResource.tryFromMap(data[PARTY_TYPE_FIELD_NAME]),
        relativePhysicalStats = data[RELATIVE_PHYSICAL_STATS_FIELD_NAME],
        timeOfDay = data[TIME_OF_DAY_FIELD_NAME],
        tradeSpecies =
            NamedApiResource.tryFromMap(data[TRADE_SPECIES_FIELD_NAME]),
        turnUpsideDown = data[TURN_UPSIDE_DOWN_FIELD_NAME];

  /// Convert an EvolutionDetail to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ITEM_FIELD_NAME: item?.toMap(),
        TRIGGER_FIELD_NAME: trigger.toMap(),
        GENDER_FIELD_NAME: gender,
        HELD_ITEM_FIELD_NAME: heldItem?.toMap(),
        KNOWN_MOVE_FIELD_NAME: knownMove?.toMap(),
        KNOWN_MOVE_TYPE_FIELD_NAME: knownMoveType?.toMap(),
        LOCATION_FIELD_NAME: location?.toMap(),
        MIN_LEVEL_FIELD_NAME: minLevel,
        MIN_HAPPINESS_FIELD_NAME: minHappiness,
        MIN_BEAUTY_FIELD_NAME: minBeauty,
        MIN_AFFECTION_FIELD_NAME: minAffection,
        NEEDS_OVERWORLD_RAIN_FIELD_NAME: needsOverworldRain,
        PARTY_SPECIES_FIELD_NAME: partySpecies?.toMap(),
        PARTY_TYPE_FIELD_NAME: partyType?.toMap(),
        RELATIVE_PHYSICAL_STATS_FIELD_NAME: relativePhysicalStats,
        TIME_OF_DAY_FIELD_NAME: timeOfDay,
        TRADE_SPECIES_FIELD_NAME: tradeSpecies?.toMap(),
        TURN_UPSIDE_DOWN_FIELD_NAME: turnUpsideDown,
      };

  /// Create a List of EvolutionDetail objects from the provided data. If the
  /// data is null an empty List is returned.
  static List<EvolutionDetail> listFrom(List<dynamic>? data) =>
      data?.map((e) => EvolutionDetail.fromMap(e)).toList() ?? [];
}
