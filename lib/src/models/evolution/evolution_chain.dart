// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/evolution/chain_link.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// From PokeAPI documentation:
/// Evolution chains are essentially family trees. They start with the lowest
/// stage within a family and detail evolution conditions for each as well as
/// Pokémon they can evolve into up through the hierarchy.
///
/// The fields provide information from the PokeAPI documentation.
class EvolutionChain {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the baby trigger item field in the JSON data.
  static const BABY_TRIGGER_ITEM_FIELD_NAME = 'baby_trigger_item';

  /// The name of the chain field in the JSON data.
  static const CHAIN_FIELD_NAME = 'chain';

  /// The identifier for this resource.
  final int id;

  /// The item that a Pokémon would be holding when mating that would trigger
  /// the egg hatching a baby Pokémon rather than a basic Pokémon.
  final NamedApiResource? babyTriggerItem;

  /// The base chain link object. Each link contains evolution details for a
  /// Pokémon in the chain. Each link references the next Pokémon in the natural
  /// evolution order.
  final ChainLink chain;

  /// Create a new EvolutionChain.
  const EvolutionChain(this.id, this.babyTriggerItem, this.chain);

  /// Create a new EvolutionChain from a Map<String, dynamic>.
  EvolutionChain.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        babyTriggerItem =
            NamedApiResource.tryFromMap(data[BABY_TRIGGER_ITEM_FIELD_NAME]),
        chain = ChainLink.fromMap(data[CHAIN_FIELD_NAME]);

  /// Convert to an EvolutionChain to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        BABY_TRIGGER_ITEM_FIELD_NAME: babyTriggerItem?.toMap(),
        CHAIN_FIELD_NAME: chain.toMap(),
      };
}
