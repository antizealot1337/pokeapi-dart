// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/evolution/evolution_detail.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This contains information about a ChainLink.
///
/// The fields provide information from the PokeAPI documentation.
class ChainLink {
  /// The name of the is baby field in the JSON data.
  static const IS_BABY_FIELD_NAME = 'is_baby';

  /// The name of the species field in the JSON data.
  static const SPECIES_FIELD_NAME = 'species';

  /// The name of the evolution details field in the JSON data.
  static const EVOLUTION_DETAILS_FIELD_NAME = 'evolution_details';

  /// The name of the evolves to field in the JSON data.
  static const EVOLVES_TO_FIELD_NAME = 'evolves_to';

  /// Whether or not this link is for a baby Pokémon. This would only ever be
  /// true on the base link.
  final bool isBaby;

  /// The Pokémon species at this point in the evolution chain.
  final NamedApiResource species;

  /// All details regarding the specific details of the referenced Pokémon
  /// species evolution.
  final List<EvolutionDetail> evolutionDetails;

  /// A List of chain objects.
  final List<ChainLink> evolvesTo;

  /// Create a new ChainLink.
  const ChainLink(
      this.isBaby, this.species, this.evolutionDetails, this.evolvesTo);

  /// Create a new ChainLink from a Map<String, dynamic>.
  ChainLink.fromMap(Map<String, dynamic> data)
      : isBaby = data[IS_BABY_FIELD_NAME],
        species = NamedApiResource.fromMap(data[SPECIES_FIELD_NAME]),
        evolutionDetails =
            EvolutionDetail.listFrom(data[EVOLUTION_DETAILS_FIELD_NAME]),
        evolvesTo = ChainLink.listFrom(data[EVOLVES_TO_FIELD_NAME]);

  /// Convert a ChainLink to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        IS_BABY_FIELD_NAME: isBaby,
        SPECIES_FIELD_NAME: species.toMap(),
        EVOLUTION_DETAILS_FIELD_NAME:
            evolutionDetails.map((e) => e.toMap()).toList(),
        EVOLVES_TO_FIELD_NAME: evolvesTo.map((e) => e.toMap()).toList(),
      };

  /// Create a List of EvolutionDetail objects from the provided data. If the
  /// data is null an empty List is returned.
  static List<ChainLink> listFrom(List<dynamic>? data) =>
      data?.map((e) => ChainLink.fromMap(e)).toList() ?? [];
}
