// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/games/pokemon_entry.dart';
import 'package:pokeapi/src/models/utility/common/description.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// From PokeAPI documentation:
/// A Pokédex is a handheld electronic encyclopedia device; one which is capable
/// of recording and retaining information of the various Pokémon in a given
/// region with the exception of the national dex and some smaller dexes related
/// to portions of a region. See
/// [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/Pokedex) for greater
/// detail.
///
/// The fields provide information from the PokeAPI documentation.
class Pokedex {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the name field in the JSON data.
  static const NAME_FIELD_NAME = 'name';

  /// The name of the is main series field in the JSON data.
  static const IS_MAIN_SERIES_FIELD_NAME = 'is_main_series';

  /// The name of the descriptions field in the JSON data.
  static const DESCRIPTIONS_FIELD_NAME = 'descriptions';

  /// The name of the names field in the JSON data.
  static const NAMES_FIELD_NAME = 'names';

  /// The name of the pokemon entries field in the JSON data.
  static const POKEMON_ENTRIES_FIELD_NAME = 'pokemon_entries';

  /// The name of the region field in the JSON data.
  static const REGION_FIELD_NAME = 'region';

  /// The name of the version groups field in the JSON data.
  static const VERSION_GROUPS_FIELD_NAME = 'version_groups';

  /// The identifier for this resource.
  final int id;

  /// The name for this resource.
  final String name;

  /// Whether or not this Pokédex originated in the main series of the video
  /// games.
  final bool isMainSeries;

  /// The description of this resource listed in different languages.
  final List<Description> descriptions;

  /// The name of this resource listed in different languages.
  final List<Name> names;

  /// A list of Pokémon catalogued in this Pokédex and their indexes.
  final List<PokemonEntry> pokemonEntries;

  /// The region this Pokédex catalogues Pokémon for.
  final NamedApiResource region;

  /// A list of version groups this Pokédex is relevant to.
  final List<NamedApiResource> versionGroups;

  /// Create a new Pokedex.
  const Pokedex(this.id, this.name, this.isMainSeries, this.descriptions,
      this.names, this.pokemonEntries, this.region, this.versionGroups);

  /// Create a new Pokedex from a Map<String, dynamic>.
  Pokedex.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        name = data[NAME_FIELD_NAME],
        isMainSeries = data[IS_MAIN_SERIES_FIELD_NAME],
        descriptions = Description.listFrom(data[DESCRIPTIONS_FIELD_NAME]),
        names = Name.listFrom(data[NAMES_FIELD_NAME]),
        pokemonEntries =
            PokemonEntry.listFrom(data[POKEMON_ENTRIES_FIELD_NAME]),
        region = NamedApiResource.fromMap(data[REGION_FIELD_NAME]),
        versionGroups =
            NamedApiResource.listFrom(data[VERSION_GROUPS_FIELD_NAME]);

  /// Convert a Pokedex to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        NAME_FIELD_NAME: name,
        IS_MAIN_SERIES_FIELD_NAME: isMainSeries,
        DESCRIPTIONS_FIELD_NAME: descriptions.map((e) => e.toMap()).toList(),
        NAMES_FIELD_NAME: names.map((e) => e.toMap()).toList(),
        POKEMON_ENTRIES_FIELD_NAME:
            pokemonEntries.map((e) => e.toMap()).toList(),
        REGION_FIELD_NAME: region.toMap(),
        VERSION_GROUPS_FIELD_NAME: versionGroups.map((e) => e.toMap()).toList(),
      };
}
