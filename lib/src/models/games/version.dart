// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// From PokeAPI documentation:
/// Versions of the games, e.g., Red, Blue or Yellow.
///
/// The fields provide information from the PokeAPI documentation.
class Version {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the name field in the JSON data.
  static const NAME_FIELD_NAME = 'name';

  /// The name of the names field in the JSON data.
  static const NAMES_FIELD_NAME = 'names';

  /// The name of the version group field in the JSON data.
  static const VERSION_GROUP_FIELD_NAME = 'version_group';

  /// The identifier for this resource.
  final int id;

  /// The name for this resource.
  final String name;

  /// The name of this resource listed in different languages.
  final List<Name> names;

  /// The version group this version belongs to.
  final NamedApiResource versionGroup;

  /// Create a new Version.
  const Version(this.id, this.name, this.names, this.versionGroup);

  /// Create a new Version from a Map<String, dynamic>.
  Version.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        name = data[NAME_FIELD_NAME],
        names = Name.listFrom(data[NAMES_FIELD_NAME]),
        versionGroup = NamedApiResource.fromMap(data[VERSION_GROUP_FIELD_NAME]);

  /// Convert a Version to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        NAME_FIELD_NAME: name,
        NAMES_FIELD_NAME: names.map((e) => e.toMap()).toList(),
        VERSION_GROUP_FIELD_NAME: versionGroup.toMap(),
      };
}
