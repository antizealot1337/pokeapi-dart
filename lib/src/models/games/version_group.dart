// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// From PokeAPI documentation:
/// Version groups categorize highly similar versions of the games.
///
/// The fields provide information from the PokeAPI documentation.
class VersionGroup {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the name field in the JSON data.
  static const NAME_FIELD_NAME = 'name';

  /// The name of the order field in the JSON data.
  static const ORDER_FIELD_NAME = 'order';

  /// The name of the generation field in the JSON data.
  static const GENERATION_FIELD_NAME = 'generation';

  /// The name of the move learn methods field in the JSON data.
  static const MOVE_LEARN_METHODS_FIELD_NAME = 'move_learn_methods';

  /// The name of the pokedexes field in the JSON data.
  static const POKEDEXS_FIELD_NAME = 'pokedexes';

  /// The name of the regions field in the JSON data.
  static const REGIONS_FIELD_NAME = 'regions';

  /// The name of the versions field in the JSON data.
  static const VERSIONS_FIELD_NAME = 'versions';

  /// The identifier for this resource.
  final int id;

  /// The name for this resource.
  final String name;

  /// Order for sorting. Almost by date of release, except similar versions are
  /// grouped together.
  final int order;

  /// The generation this version was introduced in.
  final NamedApiResource generation;

  /// A list of methods in which Pokémon can learn moves in this version group.
  final List<NamedApiResource> moveLearnMethods;

  /// A list of Pokédexes introduces in this version group.
  final List<NamedApiResource> pokedexes;

  /// A list of regions that can be visited in this version group.
  final List<NamedApiResource> regions;

  /// The versions this version group owns.
  final List<NamedApiResource> versions;

  /// Create a new VersionGroup.
  const VersionGroup(this.id, this.name, this.order, this.generation,
      this.moveLearnMethods, this.pokedexes, this.regions, this.versions);

  /// Create a new VersionGroup from a Map<String, dynamic>.
  VersionGroup.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        name = data[NAME_FIELD_NAME],
        order = data[ORDER_FIELD_NAME],
        generation = NamedApiResource.fromMap(data[GENERATION_FIELD_NAME]),
        moveLearnMethods =
            NamedApiResource.listFrom(data[MOVE_LEARN_METHODS_FIELD_NAME]),
        pokedexes = NamedApiResource.listFrom(data[POKEDEXS_FIELD_NAME]),
        regions = NamedApiResource.listFrom(data[REGIONS_FIELD_NAME]),
        versions = NamedApiResource.listFrom(data[VERSIONS_FIELD_NAME]);

  /// Convert a VersionGroup to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        NAME_FIELD_NAME: name,
        ORDER_FIELD_NAME: order,
        GENERATION_FIELD_NAME: generation.toMap(),
        MOVE_LEARN_METHODS_FIELD_NAME:
            moveLearnMethods.map((e) => e.toMap()).toList(),
        POKEDEXS_FIELD_NAME: pokedexes.map((e) => e.toMap()).toList(),
        REGIONS_FIELD_NAME: regions.map((e) => e.toMap()).toList(),
        VERSIONS_FIELD_NAME: versions.map((e) => e.toMap()).toList(),
      };
}
