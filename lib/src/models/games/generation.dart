// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// From PokeAPI documentation:
/// A generation is a grouping of the Pokémon games that separates them based on
/// the Pokémon they include. In each generation, a new set of Pokémon, Moves,
/// Abilities and Types that did not exist in the previous generation are
/// released.
///
/// The fields provide information from the PokeAPI documentation.
class Generation {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the name field in the JSON data.
  static const NAME_FIELD_NAME = 'name';

  /// The name of the abilities field in the JSON data.
  static const ABILITIES_FIELD_NAME = 'abilities';

  /// The name of the names field in the JSON data.
  static const NAMES_FIELD_NAME = 'names';

  /// The name of the main region field in the JSON data.
  static const MAIN_REGION_FIELD_NAME = 'main_region';

  /// The name of the moves field in the JSON data.
  static const MOVES_FIELD_NAME = 'moves';

  /// The name of the pokemon species field in the JSON data.
  static const POKEMON_SPECIES_FIELD_NAME = 'pokemon_species';

  /// The name of the types field in the JSON data.
  static const TYPES_FIELD_NAME = 'types';

  /// The name of the version groups field in the JSON data.
  static const VERSION_GROUPS_FIELD_NAME = 'version_groups';

  /// The identifier for this resource.
  final int id;

  /// The name for this resource.
  final String name;

  /// A list of abilities that were introduced in this generation.
  final List<NamedApiResource> abilities;

  /// The name of this resource listed in different languages.
  final List<Name> names;

  /// The main region travelled in this generation.
  final NamedApiResource mainRegion;

  /// A list of moves that were introduced in this generation.
  final List<NamedApiResource> moves;

  /// A list of Pokémon species that were introduced in this generation.
  final List<NamedApiResource> pokemonSpecies;

  /// A list of types that were introduced in this generation.
  final List<NamedApiResource> types;

  /// A list of version groups that were introduced in this generation.
  final List<NamedApiResource> versionGroups;

  /// Create a new Generation.
  const Generation(
      this.id,
      this.name,
      this.abilities,
      this.names,
      this.mainRegion,
      this.moves,
      this.pokemonSpecies,
      this.types,
      this.versionGroups);

  /// Create a new Generation from a Map<String, dynamic>.
  Generation.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        name = data[NAME_FIELD_NAME],
        abilities = NamedApiResource.listFrom(data[ABILITIES_FIELD_NAME]),
        names = Name.listFrom(data[NAMES_FIELD_NAME]),
        mainRegion = NamedApiResource.fromMap(data[MAIN_REGION_FIELD_NAME]),
        moves = NamedApiResource.listFrom(data[MOVES_FIELD_NAME]),
        pokemonSpecies =
            NamedApiResource.listFrom(data[POKEMON_SPECIES_FIELD_NAME]),
        types = NamedApiResource.listFrom(data[TYPES_FIELD_NAME]),
        versionGroups =
            NamedApiResource.listFrom(data[VERSION_GROUPS_FIELD_NAME]);

  /// Convert a Generation to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        NAME_FIELD_NAME: name,
        ABILITIES_FIELD_NAME: abilities.map((e) => e.toMap()).toList(),
        NAMES_FIELD_NAME: names.map((e) => e.toMap()).toList(),
        MAIN_REGION_FIELD_NAME: mainRegion.toMap(),
        MOVES_FIELD_NAME: moves.map((e) => e.toMap()).toList(),
        POKEMON_SPECIES_FIELD_NAME:
            pokemonSpecies.map((e) => e.toMap()).toList(),
        TYPES_FIELD_NAME: types.map((e) => e.toMap()).toList(),
        VERSION_GROUPS_FIELD_NAME: versionGroups.map((e) => e.toMap()).toList(),
      };
}
