// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This contains information about a Pokemon Entry.
///
/// The fields provide information from the PokeAPI documentation.
class PokemonEntry {
  /// The name of the entry number field in the JSON data.
  static const ENTRY_NUMBER_FIELD = 'entry_number';

  /// The name of the pokemon species field in the JSON data.
  static const POKEMON_SPECIES = 'pokemon_species';

  /// The index of this Pokémon species entry within the Pokédex.
  final int entryNumber;

  /// The Pokémon species being encountered.
  final NamedApiResource pokemonSpecies;

  /// Create a new PokemonEntry.
  const PokemonEntry(this.entryNumber, this.pokemonSpecies);

  /// Create a new PokemonEntry from a Map<String, dynamic>.
  PokemonEntry.fromMap(Map<String, dynamic> data)
      : entryNumber = data[ENTRY_NUMBER_FIELD],
        pokemonSpecies = NamedApiResource.fromMap(data[POKEMON_SPECIES]);

  /// Convert a PokemonEntry to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ENTRY_NUMBER_FIELD: entryNumber,
        POKEMON_SPECIES: pokemonSpecies.toMap(),
      };

  /// Create a List of NamedApiResource objects from the provided data. If the
  /// data is null an empty List is returned.
  static List<PokemonEntry> listFrom(List<dynamic>? data) =>
      data?.map((e) => PokemonEntry.fromMap(e)).toList() ?? [];
}
