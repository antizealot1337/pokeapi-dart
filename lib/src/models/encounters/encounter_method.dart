// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/name.dart';

/// From PokeAPI documentation:
/// Methods by which the player might can encounter Pokémon in the wild, e.g.,
/// walking in tall grass. Check out
/// [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/Wild_Pok%C3%A9mon) for
/// greater detail.
///
/// The fields provide information from the PokeAPI documentation.
class EncounterMethod {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the name field in the JSON data.
  static const NAME_FIELD_NAME = 'name';

  /// The name of the order field in the JSON data.
  static const ORDER_FIELD_NAME = 'order';

  /// The name of the names field in the JSON data.
  static const NAMES_FIELD_NAME = 'names';

  /// The identifier for this resource.
  final int id;

  /// The name for this resource.
  final String name;

  /// A good value for sorting.
  final int order;

  /// The name of this resource listed in different languages.
  final List<Name> names;

  /// Create a new EncounterMethod.
  const EncounterMethod(this.id, this.name, this.order, this.names);

  /// Create a new EncounterMethod from a Map<String, dynamic>.
  EncounterMethod.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        name = data[NAME_FIELD_NAME],
        order = data[ORDER_FIELD_NAME],
        names = Name.listFrom(data[NAMES_FIELD_NAME]);

  /// Convert an EncounterMethod to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        NAME_FIELD_NAME: name,
        ORDER_FIELD_NAME: order,
        NAMES_FIELD_NAME: names.map((e) => e.toMap()).toList(),
      };
}
