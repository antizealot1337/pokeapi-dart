// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// From PokeAPI documentation:
/// Encounter condition values are the various states that an encounter
/// condition can have, i.e., time of day can be either day or night.
///
/// The fields provide information from the PokeAPI documentation.
class EncounterConditionValue {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the name field in the JSON data.
  static const NAME_FIELD_NAME = 'name';

  /// The name of the condition field in the JSON data.
  static const CONDITION_FIELD_NAME = 'condition';

  /// The name of the names field in the JSON data.
  static const NAMES_FIELD_NAME = 'names';

  /// The identifier for this resource.
  final int id;

  /// The name for this resource.
  final String name;

  /// The condition this encounter condition value pertains to.
  final NamedApiResource condition;

  /// The name of this resource listed in different languages.
  final List<Name> names;

  /// Create a new EncounterConditionValue.
  const EncounterConditionValue(this.id, this.name, this.condition, this.names);

  /// Create a new EncounterConditionValue from a Map<String, dynamic>.
  EncounterConditionValue.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        name = data[NAME_FIELD_NAME],
        condition = NamedApiResource.fromMap(data[CONDITION_FIELD_NAME]),
        names = Name.listFrom(data[NAMES_FIELD_NAME]);

  /// Convert an EncounterConditionValue to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        NAME_FIELD_NAME: name,
        CONDITION_FIELD_NAME: condition.toMap(),
        NAMES_FIELD_NAME: names.map((e) => e.toMap()).toList(),
      };
}
