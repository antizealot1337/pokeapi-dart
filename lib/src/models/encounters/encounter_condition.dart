// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// From PokeAPI documentation:
/// Conditions which affect what pokemon might appear in the wild, e.g., day or
/// night.
///
/// The fields provide information from the PokeAPI documentation.
class EncounterCondition {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the name field in the JSON data.
  static const NAME_FIELD_NAME = 'name';

  /// The name of the names field in the JSON data.
  static const NAMES_FIELD_NAME = 'names';

  /// The name of the values field in the JSON data.
  static const VALUES_FIELD_NAME = 'values';

  /// The identifier for this resource.
  final int id;

  /// The name for this resource.
  final String name;

  /// The name of this resource listed in different languages.
  final List<Name> names;

  /// A list of possible values for this encounter condition.
  final List<NamedApiResource> values;

  /// Create a new EncounterCondition.
  const EncounterCondition(this.id, this.name, this.names, this.values);

  /// Create a new EncounterCondition from a Map<String, dynamic>.
  EncounterCondition.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        name = data[NAME_FIELD_NAME],
        names = Name.listFrom(data[NAMES_FIELD_NAME]),
        values = NamedApiResource.listFrom(data[VALUES_FIELD_NAME]);

  /// Convert an EncounterCondition to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        NAME_FIELD_NAME: name,
        NAMES_FIELD_NAME: names.map((e) => e.toMap()).toList(),
        VALUES_FIELD_NAME: values.map((e) => e.toMap()).toList(),
      };
}
