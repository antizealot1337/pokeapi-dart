// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This constains information about a BerryFlavorMap.
///
/// The fields provide information from the PokeAPI documentation.
class BerryFlavorMap {
  /// The name of the potency field in the JSON data.
  static const POTENCY_FIELD_NAME = 'potency';

  /// The name of the flavor field in the JSON data.
  static const FLAVOR_FIELD_NAME = 'flavor';

  /// How powerful the referenced flavor is for this berry.
  final int potency;

  /// The referenced berry flavor.
  final NamedApiResource flavor;

  /// Create a new Flavor.
  const BerryFlavorMap(this.potency, this.flavor);

  /// Create a Flavor from a Map<String, dynamic>.
  BerryFlavorMap.fromMap(Map<String, dynamic> data)
      : potency = data[POTENCY_FIELD_NAME],
        flavor = NamedApiResource.fromMap(data[FLAVOR_FIELD_NAME]);

  /// Convert a BerryFlavorMap to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        POTENCY_FIELD_NAME: potency,
        FLAVOR_FIELD_NAME: flavor.toMap(),
      };

  /// Create a List of BerryFlavorMap objects from the provided data. If the
  /// data is null an empty List is returned.
  static List<BerryFlavorMap> listFrom(List<dynamic>? data) =>
      data?.map((e) => BerryFlavorMap.fromMap(e)).toList() ?? [];
}
