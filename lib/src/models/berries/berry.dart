// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/berries/berry_flavor_map.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// From PokeAPI documentation:
/// Berries are small fruits that can provide HP and status condition
/// restoration, stat enhancement, and even damage negation when eaten by
/// Pokémon. Check out
/// [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/Berry) for greater
/// detail.
///
/// The fields provide information from the PokeAPI documentation.
class Berry {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the name field in the JSON data.
  static const NAME_FIELD_NAME = 'name';

  /// The name of the growth time field in the JSON data.
  static const GROWTH_TIME_FIELD_NAME = 'growth_time';

  /// The name of the max harvet field in the JSON data.
  static const MAX_HARVEST_FIELD_NAME = 'max_harvest';

  /// The name of the natural gift power field in the JSON data.
  static const NATURAL_GIFT_POWER_FIELD_NAME = 'natural_gift_power';

  /// The name of the size field in the JSON data.
  static const SIZE_FIELD_NAME = 'size';

  /// The name of the smoothness field in the JSON data.
  static const SMOOTHNESS_FIELD_NAME = 'smoothness';

  /// The name of the firmness field in the JSON data.
  static const FIRMNESS_FIELD_NAME = 'firmness';

  /// The name of the flavor field in the JSON data.
  static const FLAVORS_FIELD_NAME = 'flavor';

  /// The name of the soid dryness field in the JSON data.
  static const SOIL_DRYNESS_FIELD_NAME = 'soil_dryness';

  /// The name of the item field in the JSON data.
  static const ITEM_FIELD_NAME = 'item';

  /// The name of the natural gift type field in the JSON data.
  static const NATURAL_GIFT_TYPE_FIELD_NAME = 'natural_gift_type';

  /// The identifier for this resource.
  final int id;

  /// The name for this resource.
  final String name;

  /// Time it takes the tree to grow one stage, in hours. Berry trees go through
  /// four of these growth stages before they can be picked.
  final int growthTime;

  /// Time it takes the tree to grow one stage, in hours. Berry trees go through
  /// four of these growth stages before they can be picked.
  final int maxHarvest;

  /// The power of the move "Natural Gift" when used with this Berry.
  final int naturalGiftPower;

  /// The size of this Berry, in millimeters.
  final int size;

  /// The smoothness of this Berry, used in making Pokéblocks or Poffins.
  final int smoothness;

  /// The speed at which this Berry dries out the soil as it grows. A higher
  /// rate means the soil dries more quickly.
  final int soilDryness;

  /// The firmness of this berry, used in making Pokéblocks or Poffins.
  final NamedApiResource firmness;

  /// A list of references to each flavor a berry can have and the potency of
  /// each of those flavors in regard to this berry.
  final List<BerryFlavorMap> flavors;

  /// Berries are actually items. This is a reference to the item specific data
  /// for this berry.
  final NamedApiResource item;

  /// The type inherited by "Natural Gift" when used with this Berry.
  final NamedApiResource naturalGiftType;

  /// Create a new Berry.
  const Berry(
      this.id,
      this.name,
      this.growthTime,
      this.maxHarvest,
      this.naturalGiftPower,
      this.size,
      this.smoothness,
      this.firmness,
      this.flavors,
      this.soilDryness,
      this.item,
      this.naturalGiftType);

  /// Create a Berry from a Map<String, dynamic>.
  Berry.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        name = data[NAME_FIELD_NAME],
        growthTime = data[GROWTH_TIME_FIELD_NAME],
        maxHarvest = data[MAX_HARVEST_FIELD_NAME],
        naturalGiftPower = data[NATURAL_GIFT_POWER_FIELD_NAME],
        size = data[SIZE_FIELD_NAME],
        smoothness = data[SMOOTHNESS_FIELD_NAME],
        firmness = NamedApiResource.fromMap(data[FIRMNESS_FIELD_NAME]),
        flavors = BerryFlavorMap.listFrom(data[FLAVORS_FIELD_NAME]),
        soilDryness = data[SOIL_DRYNESS_FIELD_NAME],
        item = NamedApiResource.fromMap(data[ITEM_FIELD_NAME]),
        naturalGiftType =
            NamedApiResource.fromMap(data[NATURAL_GIFT_TYPE_FIELD_NAME]);

  /// Convert a Berry to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        NAME_FIELD_NAME: name,
        GROWTH_TIME_FIELD_NAME: growthTime,
        MAX_HARVEST_FIELD_NAME: maxHarvest,
        NATURAL_GIFT_POWER_FIELD_NAME: naturalGiftPower,
        SIZE_FIELD_NAME: size,
        SMOOTHNESS_FIELD_NAME: smoothness,
        FIRMNESS_FIELD_NAME: firmness.toMap(),
        FLAVORS_FIELD_NAME: flavors.map((e) => e.toMap()).toList(),
        SOIL_DRYNESS_FIELD_NAME: soilDryness,
        ITEM_FIELD_NAME: item.toMap(),
        NATURAL_GIFT_TYPE_FIELD_NAME: naturalGiftType.toMap(),
      };
}
