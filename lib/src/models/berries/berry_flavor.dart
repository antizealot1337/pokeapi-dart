// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/berries/flavor_berry_map.dart';
import 'package:pokeapi/src/models/utility/common/name.dart';
import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// From PokeAPI documentation:
/// Flavors determine whether a Pokémon will benefit or suffer from eating a
/// berry based on their [nature](https://pokeapi.co/docs/v2#natures). Check out
/// [Bulbapedia](http://bulbapedia.bulbagarden.net/wiki/Flavor) for greater
/// detail.
///
/// The fields provide information from the PokeAPI documentation.
class BerryFlavor {
  /// The name of the id field in the JSON data.
  static const ID_FIELD_NAME = 'id';

  /// The name of the name field in the JSON data.
  static const NAME_FIELD_NAME = 'name';

  /// The name of the berries field in the JSON data.
  static const BERRIES_FIELD_NAME = 'berries';

  /// The name of the berries field in the JSON data.
  static const CONTEST_TYPE_FIELD_NAME = 'contest_type';

  /// The name of the names field in the JSON data.
  static const NAMES_FIELD_NAME = 'names';

  /// The identifier for this resource.
  final int id;

  /// The name for this resource.
  final String name;

  /// A list of the berries with this flavor.
  final List<FlavorBerryMap> berries;

  /// The contest type that correlates with this berry flavor.
  final NamedApiResource contestType;

  /// The name of this resource listed in different languages.
  final List<Name> names;

  /// Create a new BerryFlavor.
  const BerryFlavor(
      this.id, this.name, this.berries, this.contestType, this.names);

  /// Create a BerryFlavor from a Map<String, dynamic>.
  BerryFlavor.fromMap(Map<String, dynamic> data)
      : id = data[ID_FIELD_NAME],
        name = data[NAME_FIELD_NAME],
        berries = FlavorBerryMap.listFrom(data[BERRIES_FIELD_NAME]),
        contestType = NamedApiResource.fromMap(data[CONTEST_TYPE_FIELD_NAME]),
        names = Name.listFrom(data[NAMES_FIELD_NAME]);

  /// Convert a BerryFlavor to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        ID_FIELD_NAME: id,
        NAME_FIELD_NAME: name,
        BERRIES_FIELD_NAME: berries.map((e) => e.toMap()).toList(),
        CONTEST_TYPE_FIELD_NAME: contestType.toMap(),
        NAMES_FIELD_NAME: names.map((e) => e.toMap()).toList(),
      };
}
