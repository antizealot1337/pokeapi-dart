// SPDX-License-Identifier: MIT

import 'package:pokeapi/src/models/utility/common/named_api_resource.dart';

/// This contains information about a FlavorBerryMap.
///
/// The fields provide information from the PokeAPI documentation.
class FlavorBerryMap {
  /// The name of the potency field in the JSON data.
  static const POTENCY_FIELD_NAME = 'potency';

  /// The name of the berry field in the JSON data.
  static const BERRY_FIELD_NAME = 'berry';

  /// How powerful the referenced flavor is for this berry.
  final int potency;

  /// The referenced berry flavor.
  final NamedApiResource berry;

  /// Create a new Flavor.
  const FlavorBerryMap(this.potency, this.berry);

  /// Create a Flavor from a Map<String, dynamic>.
  FlavorBerryMap.fromMap(Map<String, dynamic> data)
      : potency = data[POTENCY_FIELD_NAME],
        berry = NamedApiResource.fromMap(data[BERRY_FIELD_NAME]);

  /// Convert a FlavorBerryMap to a Map<String, dynamic>.
  Map<String, dynamic> toMap() => {
        POTENCY_FIELD_NAME: potency,
        BERRY_FIELD_NAME: berry.toMap(),
      };

  /// Create a List of FlavorBerryMap objects from the provided data. If the
  /// data is null an empty List is returned.
  static List<FlavorBerryMap> listFrom(List<dynamic>? data) =>
      data?.map((e) => FlavorBerryMap.fromMap(e)).toList() ?? [];
}
