// SPDX-License-Identifer: MIT
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:pokeapi/src/models/models.dart';
import 'package:pokeapi/src/models/pokemon/pokeatlon_stat.dart';
import 'package:pokeapi/src/models/resourse.dart';
import 'package:pokeapi/src/models/utility/languages/language.dart';

/// PokeAPIClient makes queries using a Client and will returns data from the
/// PokeAPI v2 end points.
class PokeAPIClient {
  /// This is the base URL of the api v2.
  static const _URL_BASE = 'https://pokeapi.co/api/v2';

  /// This is the berry resource.
  static const _BERRY_RESOURCE = 'berry';

  /// This is the berry firmness resource.
  static const _BERRY_FIRMNESS_RESOURCE = 'berry-firmness';

  /// This is the berry flavor resource.
  static const _BERRY_FLAVOR_RESOURCE = 'berry-flavor';

  /// This is the contest type resource.
  static const _CONTEST_TYPE_RESOURCE = 'contest-type';

  /// This is the contest effect resource.
  static const _CONTEST_EFFECT_RESOURCE = 'contest-effect';

  /// This is the super contest effect resource.
  static const _SUPER_CONTEST_EFFECT_RESOURCE = 'super-contest-effect';

  /// This is the encounter method resource.
  static const _ENCOUNTER_METHOD_RESOURCE = 'encounter-method';

  /// This is the encounter condion resource.
  static const _ENCOUNTER_CONDITION_RESOURCE = 'encounter-condition';

  /// This is the encounter condion value resource.
  static const _ENCOUNTER_CONDITION_VALUE_RESOURCE =
      'encounter-condition-value';

  /// This is the evolution chain resource.
  static const _EVOLUTION_CHAIN_RESOURCE = 'evolution-chain';

  /// This is the evolution trigger resource.
  static const _EVOLUTION_TRIGGER_RESOURCE = 'evolution-trigger';

  /// This is the generations resource.
  static const _GENERATION_RESOURCE = 'generation';

  /// This is the pokedex resource.
  static const _POKEDEX_RESOURCE = 'pokedex';

  /// This is the version resource.
  static const _VERSION_RESOURCE = 'version';

  /// This it the version group resource.
  static const _VERSION_GROUP_RESOURCE = 'version-group';

  /// This is the item resource.
  static const _ITEM_RESOURCE = 'item';

  /// This is the item attribute resource.
  static const _ITEM_ATTRIBUTE_RESOURCE = 'item-attribute';

  /// This is the item category resource.
  static const _ITEM_CATEGORY_RESOURCE = 'item-category';

  /// This is the item fling effect resource.
  static const _ITEM_FLING_EFFECT_RESOURCE = 'item-fling-effect';

  /// This is the item pockets resource.
  static const _ITEM_POCKETS_RESOURCE = 'item-pocket';

  /// This the the location resource.
  static const _LOCATION_RESOURCE = 'location';

  /// This is the location area resource.
  static const _LOCATION_AREA_RESOURCE = 'location-area';

  /// This is the pal park area resource.
  static const _PAL_PARK_AREA_RESOURCE = 'pal-park-area';

  /// This is the region resource.
  static const _REGION_RESOURCE = 'region';

  /// This is the machine resource.
  static const _MACHINE_RESOURCE = 'machine';

  /// This is the move resource.
  static const _MOVE_RESOURCE = 'move';

  /// This is the move ailment resource.
  static const _MOVE_AILMENT_RESOURCE = 'move-ailment';

  /// This is the move battle style resource.
  static const _MOVE_BATTLE_STYLE_RESOURCE = 'move-battle-style';

  /// This is the move category resource.
  static const _MOVE_CATEGORY_RESOURCE = 'move-category';

  /// This is the move damage class resource.
  static const _MOVE_DAMAGE_CLASS_RESOURCE = 'move-damage-class';

  /// This is the move learn method resource.
  static const _MOVE_LEARN_METHOD_RESOURCE = 'move-learn-method';

  /// This is the move target resource.
  static const _MOVE_TARGET_RESOURCE = 'move-target';

  /// This is the ability resource.
  static const _ABILITY_RESOURCE = 'ability';

  /// This is the characteristic resource.
  static const _CHARACTERISTIC_RESOURCE = 'characteristic';

  /// This is the egg group reousrce.
  static const _EGG_GROUP_RESOURCE = 'egg-group';

  /// This is the gender resource.
  static const _GENDER_RESOURCE = 'gender';

  /// This is the growth rate resource.
  static const _GROWTH_RATE_RESOURCE = 'growth-rate';

  /// This is the nature resource.
  static const _NATURE_RESOURCE = 'nature';

  /// This is the pokeathlon resource.
  static const _POKEATHLON_STAT_RESOURCE = 'pokeathlon-stat';

  /// This is the pokemon resource.
  static const _POKEMON_RESOURCE = 'pokemon';

  /// This is the pokemon color resource.
  static const _POKEMON_COLOR_RESOURCE = 'pokemon-color';

  /// This is the pokemon form resource.
  static const _POKEMON_FORM_RESOURCE = 'pokemon-form';

  /// This is the pokemon habitat resource.
  static const _POKEMON_HABITAT_RESOURCE = 'pokemon-habitat';

  /// This is the pokemon shape resource.
  static const _POKEMON_SHAPE_RESOURCE = 'pokemon-shape';

  /// This is the pokemon species resource.
  static const _POKEMON_SPECIES_RESOURCE = 'pokemon-species';

  /// This is the stat resource.
  static const _STAT_RESOURCE = 'stat';

  /// This is the type resource.
  static const _TYPE_RESOURCE = 'type';

  /// This is the language resource.
  static const _LANGUAGE_RESOURCE = 'language';

  /// This is the client for the API to make requests with.
  final http.Client client;

  /// Construct a PokeAPIClient with a Client. This is the primary constructor.
  const PokeAPIClient(this.client);

  /// Construt a PokeAPIClient with a default Client. This is a convience
  /// constructor.
  PokeAPIClient.defaultClient() : this(http.Client());

  /// Build a URL for a resource request to the API. It accepts the resource
  /// name and a map of params. The map uses the key as the param name and the
  /// value as the param value in the URL. Any params that are null are not
  /// included in the URL.
  Uri _buildResourceURL(String resource, Map<String, dynamic> params) {
    final paramStr = params.keys
        .where((key) => params[key] != null)
        .map((key) => '$key=${Uri.encodeQueryComponent(params['key'])}')
        .join('&');
    return Uri.parse('$_URL_BASE/$resource/?$paramStr');
  }

  /// Build a URL for a particular item. It accepts an identifier of types int
  /// and String.
  Uri _buildItemURL(String resource, dynamic identifier) {
    return Uri.parse('$_URL_BASE/$resource/$identifier');
  }

  /// This makes the request to the Uri (API). It checks to make sure the status
  /// is a success and returns the Response.
  Future<http.Response> _makeRequest(Uri uri) async {
    final response = await client.get(uri);
    if (response.statusCode < 200 || response.statusCode > 299) {
      throw Exception('(${response.statusCode}) ${response.reasonPhrase}');
    }
    return response;
  }

  /// Perform a GET request, check for a successful response, and decode the
  /// JSON data into a Map<String, dynamic>. An exception is thrown if the
  /// response isn't in the success range.
  Future<Map<String, dynamic>> _getAndDecode(Uri uri) async =>
      json.decode((await _makeRequest(uri)).body);

  /// This is used to request and parse any of the models given a Uri. This does
  /// not work for a PokemonLocaionArea.
  Future<T> get<T>(Uri uri) async {
    final data = await _getAndDecode(uri);

    switch (T) {
      case Berry:
        return Berry.fromMap(data) as T;
      case BerryFirmness:
        return BerryFirmness.fromMap(data) as T;
      case BerryFlavor:
        return BerryFlavor.fromMap(data) as T;
      case ContestType:
        return ContestType.fromMap(data) as T;
      case ContestEffect:
        return ContestEffect.fromMap(data) as T;
      case SuperContestEffect:
        return SuperContestEffect.fromMap(data) as T;
      case EncounterMethod:
        return EncounterMethod.fromMap(data) as T;
      case EncounterCondition:
        return EncounterCondition.fromMap(data) as T;
      case EncounterConditionValue:
        return EncounterConditionValue.fromMap(data) as T;
      case EvolutionChain:
        return EvolutionChain.fromMap(data) as T;
      case EvolutionTrigger:
        return EvolutionTrigger.fromMap(data) as T;
      case Generation:
        return Generation.fromMap(data) as T;
      case Pokedex:
        return Pokedex.fromMap(data) as T;
      case Version:
        return Version.fromMap(data) as T;
      case VersionGroup:
        return VersionGroup.fromMap(data) as T;
      case Item:
        return Item.fromMap(data) as T;
      case ItemAttribute:
        return ItemAttribute.fromMap(data) as T;
      case ItemCategory:
        return ItemCategory.fromMap(data) as T;
      case ItemFlingEffect:
        return ItemFlingEffect.fromMap(data) as T;
      case ItemPocket:
        return ItemPocket.fromMap(data) as T;
      case Location:
        return Location.fromMap(data) as T;
      case LocationArea:
        return LocationArea.fromMap(data) as T;
      case PalParkArea:
        return PalParkArea.fromMap(data) as T;
      case Region:
        return Region.fromMap(data) as T;
      case Machine:
        return Machine.fromMap(data) as T;
      case Move:
        return Move.fromMap(data) as T;
      case MoveAilment:
        return MoveAilment.fromMap(data) as T;
      case MoveBattleStyle:
        return MoveBattleStyle.fromMap(data) as T;
      case MoveCategory:
        return MoveCategory.fromMap(data) as T;
      case MoveDamageClass:
        return MoveDamageClass.fromMap(data) as T;
      case MoveLearnMethod:
        return MoveLearnMethod.fromMap(data) as T;
      case MoveTarget:
        return MoveTarget.fromMap(data) as T;
      case Ability:
        return Ability.fromMap(data) as T;
      case Characteristic:
        return Characteristic.fromMap(data) as T;
      case EggGroup:
        return EggGroup.fromMap(data) as T;
      case Gender:
        return Gender.fromMap(data) as T;
      case GrowthRate:
        return GrowthRate.fromMap(data) as T;
      case Nature:
        return Nature.fromMap(data) as T;
      case PokeathlonStat:
        return PokeathlonStat.fromMap(data) as T;
      case Pokemon:
        return Pokemon.fromMap(data) as T;
      // NOTE: PokemonLocationArea was obmitted because it returns a List. It
      // doesn't quite fit in this function. It may be added later.
      case PokemonColor:
        return PokemonColor.fromMap(data) as T;
      case PokemonForm:
        return PokemonForm.fromMap(data) as T;
      case PokemonHabitat:
        return PokemonHabitat.fromMap(data) as T;
      case PokemonShape:
        return PokemonShape.fromMap(data) as T;
      case PokemonSpecies:
        return PokemonSpecies.fromMap(data) as T;
      case Stat:
        return Stat.fromMap(data) as T;
      case Type:
        return Type.fromMap(data) as T;
      case Language:
        return Language.fromMap(data) as T;
      default:
        throw Exception('Unable to get type $T');
    }
  }

  /// Get a generic resource from the API. It accepts the name and optional
  /// limit and offset.
  Future<Resource> _getResource(String name, int? limit, int? offset) async {
    final url = _buildResourceURL(name, {'limit': limit, 'offset': offset});
    return Resource.fromMap(await _getAndDecode(url));
  }

  /// Get the berries resource. It optionally accepts a limit and offset. This
  /// is the https://pokeapi.co/api/v2/berry/ endpoint.
  Future<Resource> berries({int? limit, int? offset}) async =>
      _getResource(_BERRY_RESOURCE, limit, offset);

  /// Get a berry by id. This is the https://pokeapi.co/api/v2/berry/{id}/
  /// endpoint.
  Future<Berry> berry(dynamic id) async =>
      get(_buildItemURL(_BERRY_RESOURCE, id));

  /// Get the berriy firmnesses resource. It optionally accepts a limit and
  /// offset. This is the https://pokeapi.co/api/v2/berry-firmness/ endpoint.
  Future<Resource> berryFirmnesses({int? limit, int? offset}) async =>
      _getResource(_BERRY_FIRMNESS_RESOURCE, limit, offset);

  /// Get a berry firmness by id. This is the
  /// https://pokeapi.co/api/v2/berry-firmness/{id}/ endpoint.
  Future<BerryFirmness> berryFirmness(dynamic id) async =>
      get(_buildItemURL(_BERRY_FIRMNESS_RESOURCE, id));

  /// Get the berriy flavors resource. It optionally accepts a limit and
  /// offset. This is the https://pokeapi.co/api/v2/berry-flavor/ endpoint.
  Future<Resource> berryFlavors({int? limit, int? offset}) async =>
      _getResource(_BERRY_FLAVOR_RESOURCE, limit, offset);

  /// Get a berry flabor by id. This is the
  /// https://pokeapi.co/api/v2/berry-flavor/{id}/ endpoint.
  Future<BerryFlavor> berryFlavor(dynamic id) async =>
      get(_buildItemURL(_BERRY_FLAVOR_RESOURCE, id));

  /// Get the contest types resource. It optionally accepts a limit and
  /// offset. This is the https://pokeapi.co/api/v2/contest-type/ endpoint.
  Future<Resource> contestTypes({int? limit, int? offset}) async =>
      _getResource(_CONTEST_TYPE_RESOURCE, limit, offset);

  /// Get a contest type by id. This is the
  /// https://pokeapi.co/api/v2/contest-type/{id}/ endpoint.
  Future<ContestType> contestType(dynamic id) async =>
      get(_buildItemURL(_CONTEST_TYPE_RESOURCE, id));

  /// Get the contest effects resource. It optionally accepts a limit and
  /// offset. This is the https://pokeapi.co/api/v2/contest-effect/ endpoint.
  Future<Resource> contestEffects({int? limit, int? offset}) async =>
      _getResource(_CONTEST_EFFECT_RESOURCE, limit, offset);

  /// Get a contest effect by id. This is the
  /// https://pokeapi.co/api/v2/contest-effect/{id}/ endpoint.
  Future<ContestEffect> contestEffect(dynamic id) async =>
      get(_buildItemURL(_CONTEST_EFFECT_RESOURCE, id));

  /// Get the super contest effects resource. It optionally accepts a limit and
  /// offset. This is the https://pokeapi.co/api/v2/super-contest-effect/
  /// endpoint.
  Future<Resource> superContestEffects({int? limit, int? offset}) async =>
      _getResource(_SUPER_CONTEST_EFFECT_RESOURCE, limit, offset);

  /// Get a super contest effect by id. This is the
  /// https://pokeapi.co/api/v2/super-contest-effect/{id}/ endpoint.
  Future<SuperContestEffect> superContestEffect(dynamic id) async =>
      get(_buildItemURL(_SUPER_CONTEST_EFFECT_RESOURCE, id));

  /// Get the encounter methods resource. It optionally accepts a limit and
  /// offset. This is the https://pokeapi.co/api/v2/encounter-method/ endpoint.
  Future<Resource> encounterMethods({int? limit, int? offset}) async =>
      _getResource(_ENCOUNTER_METHOD_RESOURCE, limit, offset);

  /// Get an encounter method by id. This is the
  /// https://pokeapi.co/api/v2/encounter-method/{id}/ endpoint.
  Future<EncounterMethod> encounterMethod(dynamic id) async =>
      get(_buildItemURL(_ENCOUNTER_METHOD_RESOURCE, id));

  /// Get the encounter contidions resource. It optionally accepts a limit and
  /// offset. This is the https://pokeapi.co/api/v2/encounter-condition/
  /// endpoint.
  Future<Resource> encounterConditions({int? limit, int? offset}) async =>
      _getResource(_ENCOUNTER_CONDITION_RESOURCE, limit, offset);

  /// Get an encounter condition by id. This is the
  /// https://pokeapi.co/api/v2/enconter-condition/{id}/ endpoint.
  Future<EncounterCondition> encounterCondition(dynamic id) async =>
      get(_buildItemURL(_ENCOUNTER_CONDITION_RESOURCE, id));

  /// Get the encounter contidion values resource. It optionally accepts a limit
  /// and offset. This is the https://pokeapi.co/api/v2/encounter-condition/
  /// endpoint.
  Future<Resource> encounterConditionValues({int? limit, int? offset}) async =>
      _getResource(_ENCOUNTER_CONDITION_VALUE_RESOURCE, limit, offset);

  /// Get an encounter condition value by id. This is the
  /// https://pokeapi.co/api/v2/enconter-condition-value/{id}/ endpoint.
  Future<EncounterConditionValue> encounterConditionValue(dynamic id) async =>
      get(_buildItemURL(_ENCOUNTER_CONDITION_VALUE_RESOURCE, id));

  /// Get the evolution chains resource. It optionally accepts a limit and
  /// offset. This is the https://pokeapi.co/api/v2/evolution-chain/ endpoint.
  Future<Resource> evolutionChains({int? limit, int? offset}) async =>
      _getResource(_EVOLUTION_CHAIN_RESOURCE, limit, offset);

  /// Get an evolution chain value by id. This is the
  /// https://pokeapi.co/api/v2/evolution-chain/{id}/ endpoint.
  Future<EvolutionChain> evolutionChain(dynamic id) async =>
      get(_buildItemURL(_EVOLUTION_CHAIN_RESOURCE, id));

  /// Get the evolution triggers resource. It optionally accepts a limit and
  /// offset. This is the https://pokeapi.co/api/v2/evolution-chain/ endpoint.
  Future<Resource> evolutionTriggers({int? limit, int? offset}) async =>
      _getResource(_EVOLUTION_TRIGGER_RESOURCE, limit, offset);

  /// Get an evolution triger value by id. This is the
  /// https://pokeapi.co/api/v2/evolution-trigger/{id}/ endpoint.
  Future<EvolutionTrigger> evolutionTrigger(dynamic id) async =>
      get(_buildItemURL(_EVOLUTION_TRIGGER_RESOURCE, id));

  /// Get the evolution chains resource. It optionally accepts a limit and
  /// offset. This is the https://pokeapi.co/api/v2/evolution-chain/ endpoint.
  Future<Resource> generations({int? limit, int? offset}) async =>
      _getResource(_GENERATION_RESOURCE, limit, offset);

  /// Get a generation value by id. This is the
  /// https://pokeapi.co/api/v2/generation/{id}/ endpoint.
  Future<Generation> generation(dynamic id) async =>
      get(_buildItemURL(_GENERATION_RESOURCE, id));

  /// Get the pokedexes resource. It optionally accepts a limit and offset.
  /// This is the https://pokeapi.co/api/v2/pokedex/ endpoint.
  Future<Resource> pokedexes({int? limit, int? offset}) async =>
      _getResource(_POKEDEX_RESOURCE, limit, offset);

  /// Get a pokedex value by id. This is the
  /// https://pokeapi.co/api/v2/pokedex/{id}/ endpoint.
  Future<Pokedex> pokedex(dynamic id) async =>
      get(_buildItemURL(_POKEDEX_RESOURCE, id));

  /// Get the version resource. It optionally accepts a limit and offset.
  /// This is the https://pokeapi.co/api/v2/version/ endpoint.
  Future<Resource> versions({int? limit, int? offset}) async =>
      _getResource(_VERSION_RESOURCE, limit, offset);

  /// Get a version value by id. This is the
  /// https://pokeapi.co/api/v2/version/{id}/ endpoint.
  Future<Version> version(dynamic id) async =>
      get(_buildItemURL(_VERSION_RESOURCE, id));

  /// Get the version group resource. It optionally accepts a limit and offset.
  /// This is the https://pokeapi.co/api/v2/version-group/ endpoint.
  Future<Resource> versionGroups({int? limit, int? offset}) async =>
      _getResource(_VERSION_GROUP_RESOURCE, limit, offset);

  /// Get a version group value by id. This is the
  /// https://pokeapi.co/api/v2/version-group/{id}/ endpoint.
  Future<VersionGroup> versionGroup(dynamic id) async =>
      get(_buildItemURL(_VERSION_GROUP_RESOURCE, id));

  /// Get the items resource. It optionally accepts a limit and offset.
  /// This is the https://pokeapi.co/api/v2/item/ endpoint.
  Future<Resource> items({int? limit, int? offset}) async =>
      _getResource(_ITEM_RESOURCE, limit, offset);

  /// Get an item value by id. This is the
  /// https://pokeapi.co/api/v2/item/{id}/ endpoint.
  Future<Item> item(dynamic id) async => get(_buildItemURL(_ITEM_RESOURCE, id));

  /// Get the item attributes resource. It optionally accepts a limit and offset.
  /// This is the https://pokeapi.co/api/v2/item-attribute/ endpoint.
  Future<Resource> itemAttributes({int? limit, int? offset}) async =>
      _getResource(_ITEM_ATTRIBUTE_RESOURCE, limit, offset);

  /// Get an item attribute value by id. This is the
  /// https://pokeapi.co/api/v2/item-attribute/{id}/ endpoint.
  Future<ItemAttribute> itemAttribute(dynamic id) async =>
      get(_buildItemURL(_ITEM_ATTRIBUTE_RESOURCE, id));

  /// Get the item categories resource. It optionally accepts a limit and offset.
  /// This is the https://pokeapi.co/api/v2/item-category/ endpoint.
  Future<Resource> itemCategories({int? limit, int? offset}) async =>
      _getResource(_ITEM_CATEGORY_RESOURCE, limit, offset);

  /// Get an item category value by id. This is the
  /// https://pokeapi.co/api/v2/item-category/{id}/ endpoint.
  Future<ItemCategory> itemCategory(dynamic id) async =>
      get(_buildItemURL(_ITEM_CATEGORY_RESOURCE, id));

  /// Get the item fling effect resource. It optionally accepts a limit an
  /// offset. This is the
  /// https://pokeapi.co/api/v2/item-fling-effect/ endpoint.
  Future<Resource> itemFlingEffects({int? limit, int? offset}) async =>
      _getResource(_ITEM_FLING_EFFECT_RESOURCE, limit, offset);

  /// Get an item fling effect value by id. This is the
  /// https://pokeapi.co/api/v2/item-fling-effect/{id}/ endpoint.
  Future<ItemFlingEffect> itemFlingEffect(dynamic id) async =>
      get(_buildItemURL(_ITEM_FLING_EFFECT_RESOURCE, id));

  /// Get the item pocket resource. It optionally accepts a limit and offset.
  /// This is the https://pokeapi.co/api/v2/item-pocket/ endpoint.
  Future<Resource> itemPockets({int? limit, int? offset}) async =>
      _getResource(_ITEM_POCKETS_RESOURCE, limit, offset);

  /// Get an item pocket value by id. This is the
  /// https://pokeapi.co/api/v2/item-pocket/{id}/ endpoint.
  Future<ItemPocket> itemPocket(dynamic id) async =>
      get(_buildItemURL(_ITEM_POCKETS_RESOURCE, id));

  /// Get the location resource. It optionally accepts a limit and offset.
  /// This is the https://pokeapi.co/api/v2/location/ endpoint.
  Future<Resource> locations({int? limit, int? offset}) async =>
      _getResource(_LOCATION_RESOURCE, limit, offset);

  /// Get a location value by id. This is the
  /// https://pokeapi.co/api/v2/location/{id}/ endpoint.
  Future<Location> location(dynamic id) async =>
      get(_buildItemURL(_LOCATION_RESOURCE, id));

  /// Get the location areas resource. It optionally accepts a limit and
  /// offset. This is the https://pokeapi.co/api/v2/location-area/ endpoint.
  Future<Resource> locationAreas({int? limit, int? offset}) async =>
      _getResource(_LOCATION_AREA_RESOURCE, limit, offset);

  /// Get a location area value by id. This is the
  /// https://pokeapi.co/api/v2/location-area/{id}/ endpoint.
  Future<LocationArea> locationArea(dynamic id) async =>
      get(_buildItemURL(_LOCATION_AREA_RESOURCE, id));

  /// Get the pal park areas resource. It optionally accepts a limit and
  /// offset. This is the https://pokeapi.co/api/v2/pal-park-area/ endpoint.
  Future<Resource> palParkAreas({int? limit, int? offset}) =>
      _getResource(_PAL_PARK_AREA_RESOURCE, limit, offset);

  /// Get a pal park area value by id. This is the
  /// https://pokeapi.co/api/v2/pal-park-area/{id}/ endpoint.
  Future<PalParkArea> palParkArea(dynamic id) async =>
      get(_buildItemURL(_PAL_PARK_AREA_RESOURCE, id));

  /// Get the regions resource. It optionally accepts a limit and offset. This
  /// is the https://pokeapi.co/api/v2/region/ endpoint.
  Future<Resource> regions({int? limit, int? offset}) =>
      _getResource(_REGION_RESOURCE, limit, offset);

  /// Get a region value by id. This is the
  /// https://pokeapi.co/api/v2/region/{id}/ endpoint.
  Future<Region> region(dynamic id) async =>
      get(_buildItemURL(_REGION_RESOURCE, id));

  /// Get the machine resource. It optionally accepts a limit and offset.
  /// This is the https://pokeapi.co/api/v2/machine/ endpoint.
  Future<Resource> machines({int? limit, int? offset}) =>
      _getResource(_MACHINE_RESOURCE, limit, offset);

  /// Get a machine value by id. This is the
  /// https://pokeapi.co/api/v2/machine/{id}/ endpoint.
  Future<Machine> machine(dynamic id) async =>
      get(_buildItemURL(_MACHINE_RESOURCE, id));

  /// Get the move resource. It optionally accepts a limit and offset. This is
  /// the https://pokeapi.co/api/v2/move/ endpoint.
  Future<Resource> moves({int? limit, int? offset}) =>
      _getResource(_MOVE_RESOURCE, limit, offset);

  /// Get a move value by id. This is the https://pokeapi.co/api/v2/move/{id}/
  /// endpoint.
  Future<Move> move(dynamic id) async => get(_buildItemURL(_MOVE_RESOURCE, id));

  /// Get the move ailment resource. It optionally accepts a limit and offset.
  /// This is the https://pokeapi.co/api/v2/move-ailment/ endpoint.
  Future<Resource> moveAilments({int? limit, int? offset}) =>
      _getResource(_MOVE_AILMENT_RESOURCE, limit, offset);

  /// Get a move ailment value by id. This is the
  /// https://pokeapi.co/api/v2/move-ailment/{id}/ endpoint.
  Future<MoveAilment> moveAilment(dynamic id) async =>
      get(_buildItemURL(_MOVE_AILMENT_RESOURCE, id));

  /// Get the move battle style resource. It optionally accepts a limit and
  /// offset. This is the https://pokeapi.co/api/v2/move-battle-style/ endpoint.
  Future<Resource> moveBattleStyles({int? limit, int? offset}) =>
      _getResource(_MOVE_BATTLE_STYLE_RESOURCE, limit, offset);

  /// Get a move battle style value by id. This is the
  /// https://pokeapi.co/api/v2/move-battle-style/{id}/ endpoint.
  Future<MoveBattleStyle> moveBattleStyle(dynamic id) async =>
      get(_buildItemURL(_MOVE_BATTLE_STYLE_RESOURCE, id));

  /// Get the move category resource. It optionally accepts a limit and offset.
  /// This is the https://pokeapi.co/api/v2/move-category/ endpoint.
  Future<Resource> moveCategories({int? limit, int? offset}) =>
      _getResource(_MOVE_CATEGORY_RESOURCE, limit, offset);

  /// Get a move category value by id. This is the
  /// https://pokeapi.co/api/v2/move-category/{id}/ endpoint.
  Future<MoveCategory> moveCategory(dynamic id) async =>
      get(_buildItemURL(_MOVE_CATEGORY_RESOURCE, id));

  /// Get the move damage class resource. It optionally accepts a limit and
  /// offset. This is the https://pokeapi.co/api/v2/move-damage-class/ endpoint.
  Future<Resource> moveDamageClasses({int? limit, int? offset}) =>
      _getResource(_MOVE_DAMAGE_CLASS_RESOURCE, limit, offset);

  /// Get a move damage class by id. This is the
  /// https://pokeapi.co/api/v2/move-damage-class/{id}/ endpoint.
  Future<MoveDamageClass> moveDamageClass(dynamic id) async =>
      get(_buildItemURL(_MOVE_DAMAGE_CLASS_RESOURCE, id));

  /// Get the move learn method resource. It optionally accepts a limit and
  /// offset. This is the https://pokeapi.co/api/v2/move-learn-method/ endpoint.
  Future<Resource> moveLearnMethods({int? limit, int? offset}) =>
      _getResource(_MOVE_LEARN_METHOD_RESOURCE, limit, offset);

  /// Get a move learn method by id. This is the
  /// https://pokeapi.co/api/v2/move-learn-method/{id}/ endpoint.
  Future<MoveLearnMethod> moveLearnMethod(dynamic id) async =>
      get(_buildItemURL(_MOVE_LEARN_METHOD_RESOURCE, id));

  /// Get the move target resource. It optionally accepts a limit and offset.
  /// This is the https://pokeapi.co/api/v2/move-target/ endpoint.
  Future<Resource> moveTargets({int? limit, int? offset}) =>
      _getResource(_MOVE_TARGET_RESOURCE, limit, offset);

  /// Get a move target by id. This is the
  /// https://pokeapi.co/api/v2/move-target/{id}/ endpoint.
  Future<MoveTarget> moveTarget(dynamic id) async =>
      get(_buildItemURL(_MOVE_TARGET_RESOURCE, id));

  /// Get the abilty resource. It optionally accepts a limit and offset. This is
  /// the https://pokeapi.co/api/v2/abilty/ endpoint.
  Future<Resource> abilities({int? limit, int? offset}) =>
      _getResource(_ABILITY_RESOURCE, limit, offset);

  /// Get a ability by id. This is the https://pokeapi.co/api/v2/ability/{id}/
  /// endpoint.
  Future<Ability> ability(dynamic id) async =>
      get(_buildItemURL(_ABILITY_RESOURCE, id));

  /// Get the characteristic resource. It optionally accepts a limit and offset.
  /// This is the https://pokeapi.co/api/v2/characteristic/ endpoint.
  Future<Resource> characteristics({int? limit, int? offset}) =>
      _getResource(_CHARACTERISTIC_RESOURCE, limit, offset);

  /// Get a characteristic by id. This is the
  /// https://pokeapi.co/api/v2/characteristic/{id}/ endpoint.
  Future<Characteristic> characteristic(dynamic id) async =>
      get(_buildItemURL(_CHARACTERISTIC_RESOURCE, id));

  /// Get the egg group resource. It optionally accepts a limit and offset. This
  /// is the https://pokeapi.co/api/v2/egg-group/ endpoint.
  Future<Resource> eggGroups({int? limit, int? offset}) =>
      _getResource(_EGG_GROUP_RESOURCE, limit, offset);

  /// Get a egg group by id. This is the
  /// https://pokeapi.co/api/v2/egg-group/{id}/ endpoint.
  Future<EggGroup> eggGroup(dynamic id) async =>
      get(_buildItemURL(_EGG_GROUP_RESOURCE, id));

  /// Get the gender resource. It optionally accepts a limit and offset. This
  /// is the https://pokeapi.co/api/v2/gender/ endpoint.
  Future<Resource> genders({int? limit, int? offset}) =>
      _getResource(_GENDER_RESOURCE, limit, offset);

  /// Get a gender by id. This is the https://pokeapi.co/api/v2/gender{id}/
  /// endpoint.
  Future<Gender> gender(dynamic id) async =>
      get(_buildItemURL(_GENDER_RESOURCE, id));

  /// Get the growth rate resource. It optionally accepts a limit and offset.
  /// This is the https://pokeapi.co/api/v2/growth-rate/ endpoint.
  Future<Resource> growthRates({int? limit, int? offset}) =>
      _getResource(_GROWTH_RATE_RESOURCE, limit, offset);

  /// Get a growth rate by id. This is the
  /// https://pokeapi.co/api/v2/growth-rate/{id}/ endpoint.
  Future<GrowthRate> growthRate(dynamic id) async =>
      get(_buildItemURL(_GROWTH_RATE_RESOURCE, id));

  /// Get the nature resource. It optionally accepts a limit and offset. This is
  /// the https://pokeapi.co/api/v2/nature/ endpoint.
  Future<Resource> natures({int? limit, int? offset}) =>
      _getResource(_NATURE_RESOURCE, limit, offset);

  /// Get a nature by id. This is the https://pokeapi.co/api/v2/nature/{id}/
  /// endpoint.
  Future<Nature> nature(dynamic id) async =>
      get(_buildItemURL(_NATURE_RESOURCE, id));

  /// Get the pokeathlon stat resource. It optionally accepts a limit and
  /// offset. This is the https://pokeapi.co/api/v2/pokeathlon-stat/ endpoint.
  Future<Resource> pokeathlonStats({int? limit, int? offset}) =>
      _getResource(_POKEATHLON_STAT_RESOURCE, limit, offset);

  /// Get a pokeathlon stat by id. This is the
  /// https://pokeapi.co/api/v2/pokeathlon-stat/{id}/ endpoint.
  Future<PokeathlonStat> pokeathlonStat(dynamic id) async =>
      get(_buildItemURL(_POKEATHLON_STAT_RESOURCE, id));

  /// Get the pokemon stat resource. It optionally accepts a limit and offset.
  /// This is the https://pokeapi.co/api/v2/pokemon/ endpoint.
  Future<Resource> allPokemon({int? limit, int? offset}) =>
      _getResource(_POKEMON_RESOURCE, limit, offset);

  /// Get a pokemon by id. This is the https://pokeapi.co/api/v2/pokemon/{id}/
  /// endpoint.
  Future<Pokemon> pokemon(dynamic id) async =>
      get(_buildItemURL(_POKEMON_RESOURCE, id));

  /// Get a pokemon location area by pokemon id. This is the
  /// https://pokeapi.co/api/v2/pokemon/{id}/enconters endpoint.
  Future<List<PokemonLocationArea>> pokemonLocationAreas(dynamic id) async {
    final response = await _makeRequest(Uri.parse(
        '$_URL_BASE/$_POKEMON_RESOURCE/${Uri.encodeQueryComponent(id.toString())}/encounters'));
    return PokemonLocationArea.listFrom(json.decode(response.body));
  }

  /// Get the pokemon color resource. It optionally accepts a limit and offset.
  /// This is the https://pokeapi.co/api/v2/pokemon-color/ endpoint.
  Future<Resource> pokemonColors({int? limit, int? offset}) =>
      _getResource(_POKEMON_COLOR_RESOURCE, limit, offset);

  /// Get a pokemon color by id. This is the
  /// https://pokeapi.co/api/v2/pokemon-color/{id}/ endpoint.
  Future<PokemonColor> pokemonColor(dynamic id) async =>
      get(_buildItemURL(_POKEMON_COLOR_RESOURCE, id));

  /// Get the pokemon color resource. It optionally accepts a limit and offset.
  /// This is the https://pokeapi.co/api/v2/pokemon-form/ endpoint.
  Future<Resource> pokemonForms({int? limit, int? offset}) =>
      _getResource(_POKEMON_FORM_RESOURCE, limit, offset);

  /// Get a pokemon form by id. This is the
  /// https://pokeapi.co/api/v2/pokemon-form/{id}/ endpoint.
  Future<PokemonForm> pokemonForm(dynamic id) async =>
      get(_buildItemURL(_POKEMON_FORM_RESOURCE, id));

  /// Get the pokemon habitats resource. It optionally accepts a limit and
  /// offset. This is the https://pokeapi.co/api/v2/pokemon-habitat/ endpoint.
  Future<Resource> pokemonHabitats({int? limit, int? offset}) =>
      _getResource(_POKEMON_HABITAT_RESOURCE, limit, offset);

  /// Get a pokemon habitat by id. This is the
  /// https://pokeapi.co/api/v2/pokemon-habitat/{id}/ endpoint.
  Future<PokemonHabitat> pokemonHabitat(dynamic id) async =>
      get(_buildItemURL(_POKEMON_HABITAT_RESOURCE, id));

  /// Get the pokemon shape resource. It optionally accepts a limit and offset.
  /// This is the https://pokeapi.co/api/v2/pokemon-shape/ endpoint.
  Future<Resource> pokemonShapes({int? limit, int? offset}) =>
      _getResource(_POKEMON_SHAPE_RESOURCE, limit, offset);

  /// Get a pokemon shape by id. This is the
  /// https://pokeapi.co/api/v2/pokemon-shape/{id}/ endpoint.
  Future<PokemonShape> pokemonShape(dynamic id) async =>
      get(_buildItemURL(_POKEMON_SHAPE_RESOURCE, id));

  /// Get the pokemon species resource. It optionally accepts a limit and
  /// offset. This is the https://pokeapi.co/api/v2/pokemon-species/ endpoint.
  Future<Resource> allPokemonSpecies({int? limit, int? offset}) =>
      _getResource(_POKEMON_SPECIES_RESOURCE, limit, offset);

  /// Get a pokemon species by id. This is the
  /// https://pokeapi.co/api/v2/pokemon-species/{id}/ endpoint.
  Future<PokemonSpecies> pokemonSpecies(dynamic id) async =>
      get(_buildItemURL(_POKEMON_SPECIES_RESOURCE, id));

  /// Get the stat resource. It optionally accepts a limit and offset. This is
  /// the https://pokeapi.co/api/v2/stat/ endpoint.
  Future<Resource> stats({int? limit, int? offset}) =>
      _getResource(_STAT_RESOURCE, limit, offset);

  /// Get a stat by id. This is the https://pokeapi.co/api/v2/stat/{id}/
  /// endpoint.
  Future<Stat> stat(dynamic id) async => get(_buildItemURL(_STAT_RESOURCE, id));

  /// Get the type resource. It optionally accepts a limit and offset. This is
  /// the https://pokeapi.co/api/v2/type/ endpoint.
  Future<Resource> types({int? limit, int? offset}) =>
      _getResource(_TYPE_RESOURCE, limit, offset);

  /// Get a type by id. This is the https://pokeapi.co/api/v2/type/{id}/
  /// endpoint.
  Future<Type> type(dynamic id) async => get(_buildItemURL(_TYPE_RESOURCE, id));

  /// Get the languages resource. It optionally accepts a limit and offset. This
  /// is the https://pokeapi.co/api/v2/language/ endpoint.
  Future<Resource> languages({int? limit, int? offset}) =>
      _getResource(_LANGUAGE_RESOURCE, limit, offset);

  /// Get a language by id. This is the https://pokeapi.co/api/v2/language/{id}/
  /// endpoint.
  Future<Language> language(dynamic id) async =>
      get(_buildItemURL(_LANGUAGE_RESOURCE, id));
}
