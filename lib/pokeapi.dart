// SPDX-License-Identifier: MIT

/// Support for doing something awesome.
///
/// More dartdocs go here.
library pokeapi;

export 'src/pokeapi_client.dart';
