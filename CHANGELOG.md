## 0.4.0
- Add toMap method to all models.
- Fix documentation for listFrom static methods.
- Fix typos in variable names in tests
- Fix typo in field name of PokemonMoveVersion.versionGroup

## 0.3.0
- Make all primary constructors const
- Fix doc comment for NamedApiResource
- Fix type in text variable name
- Fix typo in name of LocationArea.pokemonEncounters
- Fix typo in name of LocationArea.location
- Fix typo in name of ItemPocket.categories
- Fix doc comment for VersionGroup.GENERATION_FIELD_NAME
- Fix name of Berry.soilDryness

## 0.2.0

- Add PokeAPIClient.get<T>(Uri) and use for all model access.
- Add example for using PokeAPIClient.get.
- Make AwesomeName.language nullable.
- Fix some groups/tests.
- Add missing documentation to PokemonForm.

## 0.1.0

- Initial version.
